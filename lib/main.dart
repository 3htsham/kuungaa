import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:social_app/config/app_config.dart' as config;
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/route_generator.dart';
import 'package:social_app/src/repositories/settings_repository.dart' as settingsRepo;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GlobalConfiguration().loadFromAsset("configurations");
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
//  /// Supply 'the Controller' for this application.
//  MyApp({Key key}) : super(con: Controller(), key: key);
  @override
  Widget build(BuildContext context) {
     return ValueListenableBuilder(
       valueListenable: settingsRepo.setting,
      builder: (context, settingsRepo.Setting _setting, _){
         var brightness = _setting.brightness.value;
        return MaterialApp(
          title: "Kuungaa",
          initialRoute: "/Splash",
          onGenerateRoute: RouteGenerator.generateRoute,
          debugShowCheckedModeBanner: false,
          locale: _setting.locale.value,
          localizationsDelegates: [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: S.delegate.supportedLocales,
          localeListResolutionCallback: S.delegate.listResolution(fallback: const Locale('en', '')),
          theme: brightness == Brightness.dark ? ThemeData(
            fontFamily: 'Quicksand',
            primaryColor: config.Colors().scaffoldDarkColor(1),
            brightness: brightness,
            accentColor: config.Colors().mainDarkColor(1),
            focusColor: config.Colors().accentDarkColor(1),
            hintColor: config.Colors().secondDarkColor(1),
            primaryColorDark: config.Colors().scaffoldColor(1),
            textTheme: TextTheme(
              headline: TextStyle(fontSize: 20.0, color: config.Colors().secondDarkColor(1)),
              display1: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600, color: config.Colors().secondDarkColor(1)),
              display2: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600, color: config.Colors().secondDarkColor(1)),
              display3: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w700, color: config.Colors().mainDarkColor(1)),
              display4: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w300, color: config.Colors().secondDarkColor(1)),
              subhead: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500, color: config.Colors().secondDarkColor(1)),
              title: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600, color: config.Colors().mainDarkColor(1)),
              body1: TextStyle(fontSize: 12.0, color: config.Colors().secondDarkColor(1)),
              body2: TextStyle(fontSize: 14.0, color: config.Colors().secondDarkColor(1)),
              caption: TextStyle(fontSize: 12.0, color: config.Colors().accentDarkColor(1)),
            ),
          ) : ThemeData(
            fontFamily: 'Quicksand',
            primaryColor: config.Colors().scaffoldColor(1),
            brightness: brightness,
            accentColor: config.Colors().mainColor(1),
            focusColor: config.Colors().accentColor(1),
            hintColor: config.Colors().secondColor(1),
            primaryColorDark: config.Colors().scaffoldDarkColor(1),
            textTheme: TextTheme(
              headline: TextStyle(fontSize: 20.0, color: config.Colors().secondColor(1)),
              display1: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600, color: config.Colors().secondColor(1)),
              display2: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600, color: config.Colors().secondColor(1)),
              display3: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w700, color: config.Colors().mainColor(1)),
              display4: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w300, color: config.Colors().secondColor(1)),
              subhead: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500, color: config.Colors().secondColor(1)),
              title: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600, color: config.Colors().mainColor(1)),
              body1: TextStyle(fontSize: 12.0, color: config.Colors().secondColor(1)),
              body2: TextStyle(fontSize: 14.0, color: config.Colors().secondColor(1)),
              caption: TextStyle(fontSize: 12.0, color: config.Colors().accentColor(1)),
            ),
          ),
        );
      },
    );
    // return DynamicTheme(
    //   defaultBrightness: Brightness.dark,
    //   data: (brightness) {
    //     if(brightness == Brightness.dark)
    //       {
    //         return ThemeData(
    //           fontFamily: 'Quicksand',
    //           primaryColor: config.Colors().scaffoldDarkColor(1),
    //           brightness: brightness,
    //           accentColor: config.Colors().mainDarkColor(1),
    //           focusColor: config.Colors().accentDarkColor(1),
    //           hintColor: config.Colors().secondDarkColor(1),
    //           primaryColorDark: config.Colors().scaffoldColor(1),
    //           textTheme: TextTheme(
    //             headline: TextStyle(fontSize: 20.0, color: config.Colors().secondDarkColor(1)),
    //             display1: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600, color: config.Colors().secondDarkColor(1)),
    //             display2: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600, color: config.Colors().secondDarkColor(1)),
    //             display3: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w700, color: config.Colors().mainDarkColor(1)),
    //             display4: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w300, color: config.Colors().secondDarkColor(1)),
    //             subhead: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500, color: config.Colors().secondDarkColor(1)),
    //             title: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600, color: config.Colors().mainDarkColor(1)),
    //             body1: TextStyle(fontSize: 12.0, color: config.Colors().secondDarkColor(1)),
    //             body2: TextStyle(fontSize: 14.0, color: config.Colors().secondDarkColor(1)),
    //             caption: TextStyle(fontSize: 12.0, color: config.Colors().accentDarkColor(1)),
    //           ),
    //         );
    //       } else {
    //       return ThemeData(
    //         fontFamily: 'Quicksand',
    //         primaryColor: config.Colors().scaffoldColor(1),
    //         brightness: brightness,
    //         accentColor: config.Colors().mainColor(1),
    //         focusColor: config.Colors().accentColor(1),
    //         hintColor: config.Colors().secondColor(1),
    //         primaryColorDark: config.Colors().scaffoldDarkColor(1),
    //         textTheme: TextTheme(
    //           headline: TextStyle(fontSize: 20.0, color: config.Colors().secondColor(1)),
    //           display1: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w600, color: config.Colors().secondColor(1)),
    //           display2: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600, color: config.Colors().secondColor(1)),
    //           display3: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w700, color: config.Colors().mainColor(1)),
    //           display4: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w300, color: config.Colors().secondColor(1)),
    //           subhead: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500, color: config.Colors().secondColor(1)),
    //           title: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600, color: config.Colors().mainColor(1)),
    //           body1: TextStyle(fontSize: 12.0, color: config.Colors().secondColor(1)),
    //           body2: TextStyle(fontSize: 14.0, color: config.Colors().secondColor(1)),
    //           caption: TextStyle(fontSize: 12.0, color: config.Colors().accentColor(1)),
    //         ),
    //       );
    //     }
    //   },
    //   themedWidgetBuilder: (context, theme) {
    //     return ValueListenableBuilder(
    //       valueListenable: settingsRepo.setting,
    //       builder: (context, Locale value, _){
    //         return MaterialApp(
    //           title: "Kuungaa",
    //           initialRoute: "/Splash",
    //           onGenerateRoute: RouteGenerator.generateRoute,
    //           debugShowCheckedModeBanner: false,
    //           locale: value,
    //           localizationsDelegates: [
    //             S.delegate,
    //             GlobalMaterialLocalizations.delegate,
    //             GlobalWidgetsLocalizations.delegate,
    //           ],
    //           supportedLocales: S.delegate.supportedLocales,
    //           localeListResolutionCallback: S.delegate.listResolution(fallback: const Locale('en', '')),
    //           theme: theme,
    //         );
    //       },
    //     );
    //   },
    // );
  }
}
