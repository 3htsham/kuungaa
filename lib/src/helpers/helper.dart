
class Helper {
  static getUserData(Map<String, dynamic> data) {
    return data['user'] ?? [];
  }

  static getPostsData(Map<String, dynamic> data) {
    return data['posts'] ?? [];
  }

  static getPosts(Map<String, dynamic> data) {
    return data['posts']?? [];
  }

  static getSinglePostData(Map<String, dynamic> data) {
    return data['post'] ?? [];
  }

  static getFriendRequestData(Map<String, dynamic> data) {
    return data['friendRequests'] ?? [];
  }

  static getChatListData(Map<String, dynamic> data) {
    return data['chats'] ?? [];
  }

  static getMessagesListData(Map<String, dynamic> data) {
    return data['messages'] ?? {};
  }

  static getMessageData(Map<String, dynamic> data) {
    return data['message'] ?? {};
  }

  static getSuggestedUsersData(Map<String, dynamic> data) {
    return data['users'] ?? {};
  }

  static getFriendsUsersData(Map<String, dynamic> data) {
    return data['friends'] ?? {};
  }

  static getBlockedUsersData(Map<String, dynamic> data) {
    return data['blocked'] ?? {};
  }

  static getGroupData(Map<String, dynamic> data) {
    return data['group'] ?? {};
  }

  static getGroupsData(Map<String, dynamic> data) {
    return data['groups'] ?? {};
  }

  static getJoinedGroupsData(Map<String, dynamic> data) {
    return data['joinedGroups'] ?? {};
  }

  static getSavedPostsData(Map<String, dynamic> data) {
    return data['savedPosts'] ?? {};
  }

  static getGroupPostsData(Map<String, dynamic> data) {
    return data['group_posts'] ?? {};
  }

  static getUserPostsData(Map<String, dynamic> data) {
    return data['userPosts'] ?? [];
  }

  static getGroupMembersData(Map<String, dynamic> data) {
    return data['members'] ?? {};
  }

  static getCommentsData(Map<String, dynamic> data) {
    return data['comments'] ?? {};
  }

  static getVideosData(Map<String, dynamic> data) {
    return data['videos'] ?? {};
  }

  static getVideoData(Map<String, dynamic> data) {
    return data['video'] ?? {};
  }

  static getImageData(Map<String, dynamic> data) {
    return data['image'] ?? {};
  }

  static getPackagesData(Map<String, dynamic> data) {
    return data['pakages'] ?? {};
  }
}