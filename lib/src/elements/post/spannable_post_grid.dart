import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:social_app/src/models/media.dart';
import 'package:spannable_grid/spannable_grid.dart';

class SpannablePostGrid extends StatefulWidget {

  final int postId;
  final int maxImages;
  final List<Media> media;
  final Function(int) onImageClicked;
  final Function onExpandClicked;

  SpannablePostGrid({
    this.postId,
    this.maxImages, this.media, this.onExpandClicked, this.onImageClicked
  });

  @override
  _SpannablePostGridState createState() => _SpannablePostGridState();
}

class _SpannablePostGridState extends State<SpannablePostGrid> {
  @override
  Widget build(BuildContext context) {
    List<SpannableGridCellData> cells = _buildCells();
    var maxImg = widget.maxImages;
    return SpannableGrid(
      spacing: 1.0,
      editingOnLongPress: false,
      columns: maxImg == 6 ? 3 : 2,
      rows: maxImg == 6 ? 3 : 2,
      cells: cells,
    );
  }

  List<SpannableGridCellData> _buildCells(){
    int numImages = widget.media.length;
    var maxImg = widget.maxImages;
    return List.generate(min(numImages, widget.maxImages), (index) {
      String imageUrl;
      var type = widget.media[index].type;

      if (type == 0) {
        imageUrl = widget.media[index].file;
      } else {
        // imageUrl = widget.media[index].thumbnailUrl;
        // imageUrl = "https://www.pngkit.com/png/full/267-2678423_bacteria-video-thumbnail-default.png";
        imageUrl = widget.media[index].thumbnailUrl;
      }
      var col = (maxImg == 2)
          ? (index == 0) ? 1 : 2
          : (maxImg == 3)
            ? (index==0) ? 1 : 2
          : (index == 0 || index == 3) ? 1 : (index == 1 || index == 2 || index == 5) ? 3 : 2;



      var row = (maxImg == 2)
                ? 1
                : (maxImg == 3)
                  ? (index == 2) ? 2 : 1
                : (index == 0 || index == 1) ? 1 : (index == 2) ? 2 : 3;


      var rowSpan = (maxImg==2)
                    ? 2
                    : (maxImg == 3)
                      ? (index ==0) ? 2 : 1
                    : (index==0) ? 2 : 1;


      var colSpan = (maxImg==2)
                    ? 1
                    : (maxImg == 3)
                      ? 1
                    : (index==0) ? 2 : 1;



      // If its the last image
      if(index == widget.maxImages-1)
        {
          // Check how many more images are left
          int remaining = numImages - widget.maxImages;
          if(remaining == 0) {
            return SpannableGridCellData(
              column: col,
              row: row,
              columnSpan: colSpan,
              rowSpan: rowSpan,
              id: "Test Cell $index ${widget.postId}",
              child:  GestureDetector(
                child: widget.media[index].type == 0
                    ? CachedNetworkImage(
                  imageUrl: imageUrl,
                  placeholder: (context, url) => Image.asset("assets/img/placeholders/image.jpg", fit: BoxFit.cover,),
                  fit: BoxFit.cover,
                )
                // FadeInImage.assetNetwork(
                //   placeholder: "assets/img/placeholders/image.jpg",
                //   image: imageUrl,
                //   fadeInDuration: Duration(milliseconds: 100),
                //   fit: BoxFit.cover,
                // )
                    : Stack(
                  fit: StackFit.expand,
                  children: [
                    CachedNetworkImage(
                      imageUrl: imageUrl,
                      placeholder: (context, url) => Image.asset("assets/img/placeholders/videopholder.jpg", fit: BoxFit.cover,),
                      fit: BoxFit.cover,
                    ),
                    // FadeInImage.assetNetwork(
                    //   placeholder:
                    //   "assets/img/placeholders/videopholder.jpg",
                    //   image: imageUrl,
                    //   fadeInDuration: Duration(milliseconds: 100),
                    //   fit: BoxFit.cover,
                    // ),
                    Container(
                      color: Colors.black26,
                      child: Center(
                        child: Icon(Icons.play_arrow, size: 28,),
                      ),
                    ),
                  ],
                ),
                onTap: () => widget.onImageClicked(index),
              ),
            );
          }
          else {
            // Create the facebook like effect for the last image with number of remaining  images
            return SpannableGridCellData(
              column: col,
              row: row,
              columnSpan: colSpan,
              rowSpan: rowSpan,
              id: "Test Cell $index ${widget.postId}",
              child: Container(
                color: Colors.blueAccent,
                child: GestureDetector(
                  onTap: () => widget.onExpandClicked(),
                  child: Stack(
                    fit: StackFit.expand,
                    children: [
                      widget.media[index].type == 0
                          ? CachedNetworkImage(
                        imageUrl: imageUrl,
                        placeholder: (context, url) => Image.asset("assets/img/placeholders/image.jpg", fit: BoxFit.cover,),
                        fit: BoxFit.cover,
                      )
                      // FadeInImage.assetNetwork(
                      //   placeholder: "assets/img/placeholders/image.jpg",
                      //   image: imageUrl,
                      //   fadeInDuration: Duration(milliseconds: 100),
                      //   fit: BoxFit.cover,
                      // )
                          : Stack(
                        fit: StackFit.expand,
                        children: [
                          CachedNetworkImage(
                            imageUrl: imageUrl,
                            placeholder: (context, url) => Image.asset("assets/img/placeholders/videopholder.jpg", fit: BoxFit.cover,),
                            fit: BoxFit.cover,
                          ),
                          // FadeInImage.assetNetwork(
                          //   placeholder:
                          //   "assets/img/placeholders/videopholder.jpg",
                          //   image: imageUrl,
                          //   fadeInDuration: Duration(milliseconds: 100),
                          //   fit: BoxFit.cover,
                          // ),
                          Container(
                            color: Colors.black26,
                            child: Center(
                              child: Icon(Icons.play_arrow, size: 28,),
                            ),
                          ),
                        ],
                      ),
                      Positioned.fill(
                        child: Container(
                          alignment: Alignment.center,
                          color: Colors.black54,
                          child: Text(
                            '+' + remaining.toString(),
                            style: TextStyle(fontSize: 32),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }
        }
      else
        {
          return SpannableGridCellData(
            column: col,
            row: row,
            columnSpan: colSpan,
            rowSpan: rowSpan,
            id: "Test Cell $index ${widget.postId}",
            child: GestureDetector(
              child: widget.media[index].type == 0
                  ? CachedNetworkImage(
                imageUrl: imageUrl,
                placeholder: (context, url) => Image.asset("assets/img/placeholders/image.jpg", fit: BoxFit.cover,),
                fit: BoxFit.cover,
              )
              // FadeInImage.assetNetwork(
              //   placeholder: "assets/img/placeholders/image.jpg",
              //   image: imageUrl,
              //   fadeInDuration: Duration(milliseconds: 100),
              //   fit: BoxFit.cover,
              // )
                  : Stack(
                fit: StackFit.expand,
                children: [
                  CachedNetworkImage(
                    imageUrl: imageUrl,
                    placeholder: (context, url) => Image.asset("assets/img/placeholders/videopholder.jpg", fit: BoxFit.cover,),
                    fit: BoxFit.cover,
                  ),
                  // FadeInImage.assetNetwork(
                  //   placeholder:
                  //   "assets/img/placeholders/videopholder.jpg",
                  //   image: imageUrl,
                  //   fadeInDuration: Duration(milliseconds: 100),
                  //   fit: BoxFit.cover,
                  // ),
                  Container(
                    color: Colors.black26,
                    child: Center(
                      child: Icon(Icons.play_arrow, size: 28,),
                    ),
                  ),
                ],
              ),
              onTap: () => widget.onImageClicked(index),
            ),
          );
        }
    });

  }

}
