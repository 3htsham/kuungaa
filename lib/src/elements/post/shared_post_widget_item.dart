import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:social_app/config/golbal_strings.dart';
import 'package:social_app/src/elements/others/divider_line_widget.dart';
import 'package:social_app/src/elements/post/in_post_video_view.dart';
import 'package:social_app/src/elements/post/spannable_post_grid.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/post_options.dart';
import 'package:social_app/src/models/route_argument.dart';

class SharedPostWidgetItem extends StatelessWidget {

  Post post;
  List<PostOptions> options;
  VoidCallback likeTap;
  VoidCallback commentTap;
  VoidCallback shareTap;
  Function onOptionsTap;
  VoidCallback onOwnerProfileTap;
  VoidCallback onUserProfileTap;

  bool showShare; //To hide share button for group Posts

  bool isInView = false;

  SharedPostWidgetItem({this.post, this.options, this.showShare =true,
    this.isInView, this.likeTap, this.commentTap, this.shareTap,
    this.onOwnerProfileTap, this.onUserProfileTap, this.onOptionsTap});


  @override
  Widget build(BuildContext context) {
    //2020-10-19T19:26:10.000000Z
    var format = DateFormat('yyyy-MM-ddTHH:mm:ss.SSSSSSZ');
    var dateTime = format.parse(post.createdAt);
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);
    var size = MediaQuery.of(context).size;

    String privacyImg = "assets/icons/public.png";
    if(post.postType == POST_FRIENDS) {
      privacyImg = "assets/icons/profile.png";
    } else if (post.postType == POST_PRIVATE) {
      privacyImg = "assets/icons/lock.png";
    }

    String SubPostPrivacyImg = "assets/icons/public.png";
    if(post.postType == POST_FRIENDS) {
      privacyImg = "assets/icons/profile.png";
    } else if (post.postType == POST_PRIVATE) {
      privacyImg = "assets/icons/lock.png";
    }

    return Container(
      padding: EdgeInsets.only(top: 10),
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(7),
          boxShadow: [
            BoxShadow(
                offset: Offset(1, 1),
                spreadRadius: 0.5,
                blurRadius: 5,
                color: Theme.of(context).focusColor.withOpacity(0.5)
            )
          ]
      ),
      child: Column(
        children: [

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: [
                InkWell(
                  onTap: this.onUserProfileTap,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: post.user.image != null
                        ? CachedNetworkImage(
                      imageUrl: post.user.image,
                      placeholder: (context, url) => Image.asset(
                          "assets/img/placeholders/profile.png"),
                      height: 50,
                      width: 50,
                      fit: BoxFit.cover,
                    )
                    // FadeInImage.assetNetwork(
                    //   placeholder: "assets/img/placeholders/profile.png",
                    //   image: post.user.image,
                    //   height: 50,
                    //   width: 50,
                    //   fit: BoxFit.cover,
                    // )
                        : Image.asset("assets/img/placeholders/profile.png", height: 50, width: 50, fit: BoxFit.cover, ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: this.onUserProfileTap,
                          child: Text(
                            post.user.name,
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                        ),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'shared this post',
                              style: Theme.of(context).textTheme.bodyText2
                                  .merge(TextStyle(color: Theme.of(context).focusColor)),
                            ),
                            SizedBox(width: 3,),
                            ImageIcon(AssetImage(privacyImg), color: Theme.of(context).focusColor, size: 16,),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                PopupMenuButton(
                    onSelected: (value) {
                      this.onOptionsTap(value, post);
                    },
                    icon: RotatedBox(
                      quarterTurns: 1,
                      child: ImageIcon(
                        AssetImage("assets/icons/more.png"),
                        color: Colors.white,
                      ),
                    ),
                    itemBuilder: (context) =>
                        List.generate(this.options.length, (index) {
                          return PopupMenuItem(
                              value: this.options[index].option,
                              child: Row(
                                children: [
                                  ImageIcon(AssetImage(this.options[index].icon)),
                                  SizedBox(width: 10,),
                                  Text(
                                    this.options[index].option,
                                    style: Theme.of(context).textTheme.bodyText2,
                                  ),
                                ],
                              ));
                        })),
              ],
            ),
          ),

          Container(
            padding: EdgeInsets.only(top: 10),
            margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(7),
              border: Border.all(color: Theme.of(context).focusColor.withOpacity(0.5), width: 1)
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    children: [
                      InkWell(
                        onTap: this.onOwnerProfileTap,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: post.owner.image != null
                              ? CachedNetworkImage(
                            imageUrl: post.user.image,
                            placeholder: (context, url) => Image.asset(
                                "assets/img/placeholders/profile.png"),
                            height: 50,
                            width: 50,
                            fit: BoxFit.cover,
                          )
                          // FadeInImage.assetNetwork(
                          //   placeholder: "assets/img/placeholders/profile.png",
                          //   image: post.owner.image,
                          //   height: 50,
                          //   width: 50,
                          //   fit: BoxFit.cover,
                          // )
                              : Image.asset("assets/img/placeholders/profile.png", height: 50, width: 50, fit: BoxFit.cover, ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              InkWell(
                                onTap: this.onOwnerProfileTap,
                                child: Text(
                                  post.owner.name,
                                  style: Theme.of(context).textTheme.subtitle2,
                                ),
                              ),
                              Text(
                                '$day at $time',
                                style: Theme.of(context).textTheme.bodyText2
                                    .merge(TextStyle(color: Theme.of(context).focusColor)),
                              )
                              // Row(
                              //   mainAxisAlignment: MainAxisAlignment.start,
                              //   crossAxisAlignment: CrossAxisAlignment.center,
                              //   children: [
                              //     ImageIcon(AssetImage("assets/icons/star.png"), color: Colors.amberAccent, size: 16,),
                              //     SizedBox(width: 3,),
                              //     Text(
                              //       'Sponsored',
                              //       style: Theme.of(context).textTheme.bodyText2
                              //           .merge(TextStyle(color: Theme.of(context).focusColor)),
                              //     )
                              //   ],
                              // )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                (post.text != null && post.text.length>0)
                    ? Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                  child: Text(
                    // post.text ?? "",
                    post.text ?? "",
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                )
                    : SizedBox(height: 10,),

                (post.mediaList != null && post.mediaList.length > 0)
                    ? (post.mediaList.length==1)

                    ? Container(
                  child: post.mediaList[0].type == 0
                      ? Center(
                    child: CachedNetworkImage(
    imageUrl: post.mediaList[0].file,
    placeholder: (context, url) => Image.asset(
    "assets/img/placeholders/image.jpg", fit: BoxFit.cover,),
    )
    // FadeInImage.assetNetwork(
    //                   placeholder:
    //                   "assets/img/placeholders/image.jpg",
    //                   image: post.mediaList[0].file,
    //                 ),
                  )
                      : Container(
                    height: size.width * 00.5625,
                    padding: EdgeInsets.symmetric(horizontal: 3),
                    child: InPostVideoView(
                      post.mediaList[0].file.toString().contains(
                          RegExp(
                              r"(.mkv|.mp4|.webm|.ogv|.m3u|.m3u8)",
                              caseSensitive: false))
                          ? post.mediaList[0].file
                          : post.mediaList[0].file + ".mp4",
                      isInView: this.isInView ?? false,
                      key: GlobalKey<InPostVideoViewState>(),
                    ),
                  ),
                )
                    : Container(
                  child: Center(
                    child: SpannablePostGrid(
                      postId: post.id,
                      media: post.mediaList,
                      onImageClicked: (i) {
                        print("Image clicked is: $i");
                        Navigator.of(context).pushNamed("/ViewPostMedia",
                            arguments: RouteArgument(post: post, index: i));
                      },
                      onExpandClicked: () {
                        print("Expand Clicked ");
                        Navigator.of(context).pushNamed("/ViewPostMedia",
                            arguments: RouteArgument(post: post, index: 0));
                      },
                      maxImages: (post.mediaList.length == 2)
                          ? 2
                          : (post.mediaList.length < 6)
                          ? 3
                          : 6,
                    ),
                  ),
                )

                    : SizedBox(height: 0,),
              ],
            ),
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              post.totalLikes != null && post.totalLikes > 0
                  ? Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                child: Text("${post.totalLikes} Likes"),
              )
                  : SizedBox(height: 0,),
              post.totalComment != null && post.totalComment > 0
                  ? Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                child: Text("${post.totalComment} Comments"),
              )
                  : SizedBox(height: 0,),
            ],
          ),
          DividerLineWidget(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              FlatButton(
                onPressed: (){
                  this.likeTap();
                },
                child: Row(
                  children: [
                    ImageIcon(AssetImage("assets/emoticons/default.png"), size: 40, color: !post.isLiked ? Theme.of(context).primaryColorDark : Theme.of(context).accentColor,),
                    Text("Like", style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(color: !post.isLiked ? Theme.of(context).primaryColorDark : Theme.of(context).accentColor)),)
                  ],
                ),
              ),
              FlatButton(
                onPressed: (){
                  this.commentTap();
                },
                child: Row(
                  children: [
                    ImageIcon(AssetImage("assets/icons/comment.png"), ),
                    Text("Comment",  style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(color: Theme.of(context).primaryColorDark)),)
                  ],
                ),
              ),
              showShare ? FlatButton(
                onPressed: (){
                  this.shareTap();
                },
                child: Row(
                  children: [
                    ImageIcon(AssetImage("assets/icons/share.png"), ),
                    Text("Share", style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(color: Theme.of(context).primaryColorDark)),)
                  ],
                ),
              ) : SizedBox(width: 0, height: 0,),
            ],
          ),

        ],
      ),
    );


  }
}
