import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/config/golbal_strings.dart';
import 'package:social_app/src/controllers/post_controller.dart';
import 'package:social_app/src/elements/bottomsheet/post_comment_newsfeed.dart';
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/elements/post/user_post_widget_item.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/models/user.dart';

class UserProfilePostsWidget extends StatefulWidget {

  User user;
  User currentUser;

  UserProfilePostsWidget({Key key, this.user, this.currentUser}) : super(key: key);

  @override
  UserProfilePostsWidgetState createState() => UserProfilePostsWidgetState();
}

class UserProfilePostsWidgetState extends StateMVC<UserProfilePostsWidget>
    with AutomaticKeepAliveClientMixin  {

  PostController con;

  UserProfilePostsWidgetState() : super(PostController()) {
    con = controller;
  }

  @override
  void initState() {
    con.user = widget.user;
    con.currentUser = widget.currentUser;
    super.initState();
    con.getUserPostsByUserId();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Stack(
      children: [

        SingleChildScrollView(
          child: Container(
            child: con.posts.isEmpty
                ? CircularLoadingWidget(height: 100,)
                : ListView.builder(
              primary: false,
              shrinkWrap: true,
              itemCount: con.posts.length,
                itemBuilder: (context, index) {
                  return UserPostWidgetItem(post: con.posts[index],
                    options: con.posts[index].userId.toString() == con.currentUser.id.toString() ? con.myPostOptions : con.options,
                    likeTap: (){
                      con.postLike(con.posts[index]);
                    },
                    commentTap: (){
                      this.postCommentsBottomsheet(context, con.posts[index]);
                    },
                    shareTap: (){
                      con.showShareDialog(con.posts[index]);
                      },
                    onProfileTap: (){
                      Navigator.of(context).pushNamed("/UserProfileDetails", arguments: RouteArgument(user: con.posts[index].user));
                    },
                    onOptionsTap: con.onPostOptions,
                    showShare: con.posts[index].postType == POST_FRIENDS || con.posts[index].postType == POST_PUBLIC
                  );
                }
            ),
          ),
        ),

        con.isSharePostDialog
            ? sharePostDialog()
            : Positioned(bottom: 0, child: SizedBox(height: 0)),
      ],
    );
  }

  postCommentsBottomsheet(BuildContext context, Post post) {
    var theme = Theme.of(context);
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: theme.scaffoldBackgroundColor,
        context: context,
        builder: (ctx) {
          return GestureDetector(
            onVerticalDragStart: (_){},
            child: StatefulBuilder(
              builder: (ctx, setState) {
                return PostCommentsBottomSheet(con.currentUser, post: post,);
              },
            ),
          );
        }
    ).then((value){
      if(value != null) {
        var post = value as Post;
        con.posts.forEach((element) {
          if(element.id == post.id) {
            if(post.comments != null && post.comments.length > 0) {
              setState((){
                element.totalComment = post.comments.length;
                element.comments = post.comments;
              });
            }
          }
        });
      }
    });
  }


  Widget sharePostDialog() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height - 70-24-50,
        color: Colors.black54,
        child: Center(
            child: Container(
                width: MediaQuery.of(context).size.width * 0.7,
                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(7),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(1, 1),
                          spreadRadius: 0.5,
                          blurRadius: 5,
                          color: Theme.of(context).focusColor.withOpacity(0.5)
                      )
                    ]
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text("Share Post with...", style: Theme.of(context).textTheme.subtitle1),
                    SizedBox(height: 10),
                    ListView.builder(
                        itemCount: con.newPostPrivacyOptions.length,
                        shrinkWrap: true,
                        primary: false,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: (){
                              con.hideShareDialog(con.newPostPrivacyOptions[index].option);
                            },
                            child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                                child: Row(
                                    children:[
                                      ImageIcon(AssetImage(con.newPostPrivacyOptions[index].icon)),
                                      SizedBox(width: 10,),
                                      Text(
                                        con.newPostPrivacyOptions[index].option,
                                        style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(fontSize: 18)),
                                      )
                                    ]
                                )
                            ),
                          );
                        }
                    )
                  ],
                )
            )
        )
    );
  }

}
