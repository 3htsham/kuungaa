import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:social_app/src/models/saved_post.dart';
import 'package:social_app/src/models/user.dart';


class SavedPostItem extends StatelessWidget {

  SavedPost post;
  User owner;
  List<String> options;

  VoidCallback onTap;
  VoidCallback onUserProfile;
  Function onOptionsTap;

  SavedPostItem({this.post, this.owner, this.options, this.onTap, this.onUserProfile, this.onOptionsTap});

  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    //2020-10-19T19:26:10.000000Z
    var format = DateFormat('yyyy-MM-ddTHH:mm:ss.SSSSSSZ');
    var dateTime = format.parse(post.createdAt);
    var month = DateFormat('MMMM').format(dateTime);
    var day = DateFormat('dd').format(dateTime);
    var year = DateFormat('yyyy').format(dateTime);

    var imageUrl = "";
    var text = "";

    if(post.post.mediaList != null && post.post.mediaList.length > 0) {
      post.post.mediaList.forEach((element) {
        if(element.type == 0) {
          imageUrl = element.file;
        }
      });
    }

    if(imageUrl.length < 1) {
      imageUrl = owner.image;
    }

    if(post.post.text != null && post.post.text.toString().length > 0) {
      text = post.post.text;
    } else {
      text = owner.name;
    }

    return InkWell(
      onTap: this.onTap,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Row(
          children: [
            CachedNetworkImage(
                imageUrl: imageUrl,
                fit: BoxFit.cover,
                width: 60,
                height: 60,
                imageBuilder: (context, provider) {
                  return Container(
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        image: DecorationImage(image: provider,
                            fit: BoxFit.cover)),
                  );
                }),

            SizedBox(width: 10,),
            Expanded(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 200,
                    child: Text(
                      text,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: textTheme.headline6,
                    ),
                  ),
                  SizedBox(height: 10,),
                  Row(
                    children: [
                      InkWell(
                        onTap: this.onUserProfile,
                        child: CachedNetworkImage(
                            imageUrl: owner.image,
                            fit: BoxFit.cover,
                            width: 20,
                            height: 20,
                            imageBuilder: (context, provider) {
                              return Container(
                                width: 20,
                                height: 20,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100),
                                    image: DecorationImage(
                                        image: provider,
                                        fit: BoxFit.cover
                                    )),
                              );
                            }),
                      ),
                      SizedBox(width: 10,),
                      InkWell(
                        onTap: this.onUserProfile,
                        child: Container(
                          constraints: BoxConstraints(
                              maxWidth: 80
                          ),
                          child: Text(
                            owner.name,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: textTheme.bodyText2,
                          ),
                        ),
                      ),
                      SizedBox(width: 5,),
                      Text(
                        '$month $day, $year',
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: textTheme.caption,
                      )
                    ],
                  )
                ],
              ),
            ),

            PopupMenuButton(
                onSelected: (value) {
                  this.onOptionsTap(value, post, owner);
                },
                icon: RotatedBox(
                  quarterTurns: 1,
                  child: ImageIcon(
                    AssetImage("assets/icons/more.png"),
                    color: Colors.white,
                  ),
                ),
                itemBuilder: (context) =>
                    List.generate(options.length, (index) {
                      return PopupMenuItem(
                          value: options[index],
                          child: Text(
                            options[index],
                            style: Theme.of(context).textTheme.bodyText2,
                          ));
                    })),


          ],
        ),
      ),
    );
  }
}
