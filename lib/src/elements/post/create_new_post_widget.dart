import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/models/user.dart';

class CreateNewPostWidget extends StatelessWidget {
  User user;
  VoidCallback onProfileTap;
  VoidCallback onPostTap;

  CreateNewPostWidget({this.user, this.onPostTap, this.onProfileTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            S.of(context).post_something,
            style: Theme.of(context).textTheme.subtitle2,
          ),
          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              InkWell(
                onTap: this.onProfileTap,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: user != null && user.image != null && user.image.length > 0
                      ? CachedNetworkImage(
                          imageUrl: user.image,
                          placeholder: (context, url) => Image.asset(
                              "assets/img/placeholders/profile.png", fit: BoxFit.cover,),
                          width: 60,
                          height: 60,
                          fit: BoxFit.cover,
                        )
                      // FadeInImage.assetNetwork(
    //                 placeholder: "assets/img/placeholders/profile.png",
    //                 image: user.image,
    //                 height: 60,
    //                 width: 60,
    //                 fit: BoxFit.cover,
    //               )
                        : Image.asset("assets/img/placeholders/profile.png", width: 60, height: 60, fit: BoxFit.cover,),
                ),
              ),
              SizedBox(width: 15,),
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: this.onPostTap,
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Theme.of(context).accentColor),
                        borderRadius: BorderRadius.circular(15)
                    ),
                    child: Text(
                      S.of(context).tell_people_about_your_day,
                      style: Theme.of(context).textTheme.bodyText2.merge(
                          TextStyle(color: Theme.of(context).focusColor)
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(width: 15,),
              InkWell(
                onTap: this.onPostTap,
                child: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      color: Theme.of(context).accentColor,
                      borderRadius: BorderRadius.circular(100)
                  ),
                  child: ImageIcon(AssetImage("assets/icons/write.png"), color: Colors.white,),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}