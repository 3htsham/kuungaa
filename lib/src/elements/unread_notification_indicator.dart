import 'package:flutter/material.dart';

class UnreadNotificationIndicator extends StatelessWidget {

  int status;
  UnreadNotificationIndicator({this.status});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      width: 30,
      child: Center(
        child: Container(
          width: 5,
          height: 5,
          decoration: BoxDecoration(
              color: status == 0 ? Colors.red : Colors.transparent,
              borderRadius: BorderRadius.circular(10)
          ),
        ),
      ),
    );
  }
}
