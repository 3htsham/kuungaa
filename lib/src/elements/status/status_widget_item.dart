import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:social_app/src/models/status.dart';

class StatusWidgetItem extends StatelessWidget {

  Status status;
  bool isSeen;
  VoidCallback onTap;

  StatusWidgetItem({this.status, this.isSeen = false, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: this.onTap,
      child: Container(
        height: 130,
        width: 80,
        padding: EdgeInsets.all(2),
        margin: EdgeInsets.all(5),
        decoration: BoxDecoration(
            color: isSeen ? Theme.of(context).accentColor : Colors.yellowAccent,
            borderRadius: BorderRadius.circular(4)
        ),
        child: Stack(
          fit: StackFit.expand,
          children: [
            CachedNetworkImage(
              imageUrl: status.imageLink,
              placeholder: (context, url) => Image.asset("assets/img/placeholders/image.jpg", fit: BoxFit.cover,),
              height: 100,
              width: 70,
              fit: BoxFit.cover,
            )
            // FadeInImage.assetNetwork(
            //   placeholder: "assets/img/placeholders/image.jpg",
            //   image: status.imageLink,
            //   height: 100,
            //   width: 70,
            //   fit: BoxFit.cover,
            // ),
          ],
        ),
      ),
    );
  }
}
