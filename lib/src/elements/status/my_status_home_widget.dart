import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:social_app/src/models/status.dart';
import 'package:social_app/src/models/user.dart';

class MyStatusHomeWidget extends StatelessWidget {

  User currentUser;
  VoidCallback onTap;
  bool isStatus;
  Status status;

  MyStatusHomeWidget({this.currentUser, this.status, this.isStatus = false, this.onTap});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return InkWell(
      onTap: this.onTap,
      child: Container(
        height: 130,
        width: 80,
        padding: EdgeInsets.all(2),
        margin: EdgeInsets.all(5),
        decoration: BoxDecoration(
            // color: Theme.of(context).accentColor,
            borderRadius: BorderRadius.circular(4),
          gradient: LinearGradient(
              colors: [
                Colors.yellowAccent,
                Theme.of(context).accentColor,
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [0.3, 0.7]
          ),
        ),
        child: Stack(
          fit: StackFit.expand,
          children: [

            this.isStatus
                ? CachedNetworkImage(
              imageUrl: status.imageLink,
              placeholder: (context, url) => Image.asset("assets/img/placeholders/image.jpg", fit: BoxFit.cover,),
              height: 130,
              width: 80,
              fit: BoxFit.cover,
            )
            // FadeInImage.assetNetwork(
            //   placeholder: "assets/img/placeholders/image.jpg",
            //   image: status.imageLink,
            //   height: 130,
            //   width: 80,
            //   fit: BoxFit.cover,
            // )
                : Image.asset("assets/img/placeholders/image.jpg",
              height: 130,
              width: 80,
              fit: BoxFit.cover,),

            Container(
              color: Colors.black38,
            ),

            Center(
              child: Container(
                width: 50,
                height: 50,
                padding: EdgeInsets.all(2),
                decoration: BoxDecoration(
                    color: Theme.of(context).accentColor,
                    gradient: LinearGradient(
                      colors: [
                        Theme.of(context).accentColor,
                        Colors.yellowAccent,
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      stops: [0.3, 0.7]
                    ),
                    borderRadius: BorderRadius.circular(100)
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(100),
                  child: CachedNetworkImage(
                    imageUrl: currentUser.image,
                    placeholder: (context, url) => Image.asset("assets/img/placeholders/image.jpg", fit: BoxFit.cover,),
                    height: 130,
                    width: 80,
                    fit: BoxFit.cover,
                  )
                  // FadeInImage.assetNetwork(
                  //   placeholder: "assets/img/placeholders/image.jpg",
                  //   image: currentUser.image,
                  //   height: 130,
                  //   width: 80,
                  //   fit: BoxFit.cover,
                  // ),
                ),
              ),
            ),


            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                    width: 75,
                    padding: EdgeInsets.only(bottom: 5),
                    child: Center(
                      child: Text(
                        this.isStatus ? "Your Status" : "Add Status",
                        style: textTheme.caption.merge(TextStyle(color: Colors.white)),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    )
                )
            )
          ],
        ),
      ),
    );
  }
}
