import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_app/src/models/boost_package.dart';

class PackageListItem extends StatelessWidget {
  BoostPackage package;
  VoidCallback onTap;

  PackageListItem({this.package, this.onTap});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return Container(
      child: ListTile(
        title: Text(package.name.toString().toUpperCase(), style: textTheme.subtitle1.merge(TextStyle(fontWeight: FontWeight.w800)),),
        leading: Icon(Icons.stars, color: Colors.amber,),
        subtitle:
        Column(
          children: [
            Row(
              children: [
                Icon(Icons.attach_money, color: theme.accentColor, size: 20,),
                SizedBox(width: 10),
                Text('Price  \$${package.price}',
                  style: textTheme.bodyText1,),
              ],
            ),
            Row(
              children: [
                Icon(Icons.remove_red_eye_outlined, color: theme.accentColor, size: 20,),
                SizedBox(width: 10),
                Text('Views Count:   ${package.views}',
                  style: textTheme.bodyText1,),
              ],
            )
          ],
        ),
        isThreeLine: true,
        trailing: IconButton(
          onPressed: this.onTap,
          icon: Icon(CupertinoIcons.forward),
        ),
      ),
    );
  }
}
