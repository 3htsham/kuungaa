import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:social_app/src/elements/post/in_post_video_view.dart';
import 'package:social_app/src/elements/post/spannable_post_grid.dart';
import 'package:social_app/src/models/post.dart';

class BoostedPostExample extends StatelessWidget {

  Post post;
  BoostedPostExample({this.post});

  @override
  Widget build(BuildContext context) {
    //2020-10-19T19:26:10.000000Z
    var format = DateFormat('yyyy-MM-ddTHH:mm:ss.SSSSSSZ');
    var dateTime = format.parse(post.createdAt);
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);
    var size = MediaQuery.of(context).size;

    return Container(
      padding: EdgeInsets.only(top: 15, bottom: 15),
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(7),
        border: Border.all(color: Theme.of(context).focusColor, width: 0.5)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: [
                InkWell(
                  onTap: (){},
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: post.user.image != null ?
                    CachedNetworkImage(
                      imageUrl: post.user.image,
                      placeholder: (context, url) => Image.asset("assets/img/placeholders/profile.png", fit: BoxFit.cover,),
                      width: 50,
                      height: 50,
                    )
                    // FadeInImage.assetNetwork(
                    //   placeholder: "assets/img/placeholders/profile.png",
                    //   image: post.user.image,
                    //   height: 50,
                    //   width: 50,
                    //   fit: BoxFit.cover,
                    // )
                        : Image.asset("assets/img/placeholders/profile.png", height: 50, width: 50, fit: BoxFit.cover, ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: (){

                          },
                          child: Text(
                            post.user.name,
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                        ),
                        // Text(
                        //   '$day at $time',
                        //   style: Theme.of(context).textTheme.bodyText2
                        //       .merge(TextStyle(color: Theme.of(context).focusColor)),
                        // )
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            ImageIcon(AssetImage("assets/icons/star.png"), color: Colors.amberAccent, size: 16,),
                            SizedBox(width: 3,),
                            Text(
                              'Sponsored',
                              style: Theme.of(context).textTheme.bodyText2
                                  .merge(TextStyle(color: Theme.of(context).focusColor)),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),


                PopupMenuButton(
                    onSelected: (value) {

                    },
                    icon: RotatedBox(
                      quarterTurns: 1,
                      child: ImageIcon(
                        AssetImage("assets/icons/more.png"),
                        color: Colors.white,
                      ),
                    ),
                    itemBuilder: (context) =>
                        List.generate(["Close    "].length, (index) {
                          return PopupMenuItem(
                              value: ["Close    "],
                              child: Row(
                                children: [
                                  Text(
                                    ["Close    "][index],
                                    style: Theme.of(context).textTheme.bodyText2,
                                  ),
                                ],
                              ));
                        })
                )

              ],
            ),
          ),
          (post.text != null && post.text.length>0)
              ? Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
            child: Text(
              // post.text ?? "",
              post.text ?? "",
              style: Theme.of(context).textTheme.subtitle2,
            ),
          )
              : SizedBox(height: 10,),

          (post.mediaList != null && post.mediaList.length > 0)
              ? (post.mediaList.length==1)

              ? Container(
            child: post.mediaList[0].type == 0
                ? Center(
              child: CachedNetworkImage(
                imageUrl: post.mediaList[0].file,
                placeholder: (context, url) => Image.asset("assets/img/placeholders/image.jpg", fit: BoxFit.cover,),
              ),
              // FadeInImage.assetNetwork(
              //   placeholder:
              //   "assets/img/placeholders/image.jpg",
              //   image: post.mediaList[0].file,
              // ),
            )
                : Container(
              height: size.width * 00.5625,
              padding: EdgeInsets.symmetric(horizontal: 3),
              child: InPostVideoView(
                post.mediaList[0].file.toString().contains(
                    RegExp(
                        r"(.mkv|.mp4|.webm|.ogv|.m3u|.m3u8)",
                        caseSensitive: false))
                    ? post.mediaList[0].file
                    : post.mediaList[0].file + ".mp4",
                isInView: false,
                key: GlobalKey<InPostVideoViewState>(),
              ),
            ),
          )
              : Container(
            child: Center(
              child: SpannablePostGrid(
                postId: post.id,
                media: post.mediaList,
                onImageClicked: (i) {
                  print("Image clicked is: $i");
                  // Navigator.of(context).pushNamed("/ViewPostMedia",
                  //     arguments: RouteArgument(post: post, index: i));
                },
                onExpandClicked: () {
                  print("Expand Clicked ");
                  // Navigator.of(context).pushNamed("/ViewPostMedia",
                  //     arguments: RouteArgument(post: post, index: 0));
                },
                maxImages: (post.mediaList.length == 2)
                    ? 2
                    : (post.mediaList.length < 6)
                    ? 3
                    : 6,
              ),
            ),
          )

              : SizedBox(height: 0,),

          SizedBox(height: 10,)
        ],
      ),
    );
  }
}
