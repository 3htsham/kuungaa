import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/comment_controller.dart';
import 'package:social_app/src/models/comment.dart';
import 'package:social_app/src/models/comment_reply.dart';
import 'package:social_app/src/models/post_options.dart';
import 'package:social_app/src/models/user.dart';

class CommentWidget extends StatefulWidget {
  Comment comment;
  Function onOptionsTap;
  List<PostOptions> options;
  bool enabled;
  User currentUser;

  CommentWidget(
      {@required this.comment,
      @required this.currentUser,
      @required this.enabled,
      @required this.options,
      @required this.onOptionsTap});

  @override
  _CommentWidgetState createState() => _CommentWidgetState();
}

class _CommentWidgetState extends StateMVC<CommentWidget> {
  CommentController _con;
  KeyboardVisibilityController keyboardVisibilityController;
  bool isEmoji = false;

  _CommentWidgetState() : super(CommentController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.comment = widget.comment;
    _con.currentUser = widget.currentUser;
    super.initState();

    keyboardVisibilityController = KeyboardVisibilityController();
    // Query
    print(
        'Keyboard visibility direct query: ${keyboardVisibilityController.isVisible}');

    // Subscribe
    keyboardVisibilityController.onChange.listen((visible) {
      if (visible) {
        setState(() {
          isEmoji = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: widget.comment.user != null &&
                        widget.comment.user.image != null
                    ? CachedNetworkImage(
                        imageUrl: widget.comment.user.image,
                        placeholder: (context, url) => Image.asset(
                          "assets/img/placeholders/profile.png",
                          fit: BoxFit.cover,
                        ),
                        width: 50,
                        height: 50,
                        fit: BoxFit.cover,
                      )
                    : Image.asset(
                        "assets/img/placeholders/profile.png",
                        height: 50,
                        width: 50,
                        fit: BoxFit.cover,
                      ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      child: Text(
                        widget.comment.user.name,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: textTheme.bodyText1
                            .merge(TextStyle(fontWeight: FontWeight.w600)),
                      ),
                    ),
                    Container(
                      child: Text(
                        widget.comment.text,
                        style: textTheme.bodyText1,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10, left: 10),
                      child: Row(
                        children: [
                          _con.comment.likesCount != null &&
                                  _con.comment.likesCount > 0
                              ? Text(
                                  "${_con.comment.likesCount} Likes",
                                  style: textTheme.caption,
                                )
                              : SizedBox(
                                  height: 0,
                                  width: 0,
                                ),
                          SizedBox(
                            width: _con.comment.likesCount != null &&
                                    _con.comment.likesCount > 0
                                ? 10
                                : 0,
                          ),
                          InkWell(
                            onTap: () {
                              _con.postCommentLike();
                            },
                            child: Text(
                              _con.comment.isLiked ? "Liked" : "Like",
                              style: textTheme.bodyText1
                                  .merge(TextStyle(color: theme.accentColor)),
                            ),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {
                                _con.textController.text =
                                    widget.comment.user.name + " ";
                                _con.showReplyBox = true;
                              });
                            },
                            child: Text(
                              "Reply",
                              style: textTheme.bodyText1
                                  .merge(TextStyle(color: theme.accentColor)),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              PopupMenuButton(
                onSelected: (value) {
                  widget.onOptionsTap(value);
                },
                icon: RotatedBox(
                  quarterTurns: 1,
                  child: ImageIcon(
                    AssetImage("assets/icons/more.png"),
                    color: Colors.white,
                  ),
                ),
                itemBuilder: (context) =>
                    List.generate(widget.options.length, (index) {
                  return PopupMenuItem(
                      value: widget.options[index].option,
                      child: Row(
                        children: [
                          ImageIcon(AssetImage(widget.options[index].icon)),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            widget.options[index].option,
                            style: Theme.of(context).textTheme.bodyText2,
                          ),
                        ],
                      ));
                }),
                enabled: widget.enabled,
              ),
            ],
          ),

          (_con.comment.replies != null && _con.comment.replies.length > 0)
              ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: InkWell(
                      onTap: () {
                        setState(() {
                          _con.showReplies = !_con.showReplies;
                          _con.showReplyBox = true;
                        });
                      },
                      child: Text(
                        _con.showReplies ? "Hide Replies" : "Show Replies",
                        style: textTheme.subtitle2,
                      )),
                )
              : SizedBox(height: 0),

          // (_con.showReplies)

          Container(
            margin: EdgeInsets.only(
              left: 30,
            ),
            child: Column(
              children: [
                _con.showReplies
                    ? ListView.builder(
                        primary: false,
                        shrinkWrap: true,
                        padding: EdgeInsets.symmetric(vertical: 7),
                        itemCount: _con.comment.replies.length,
                        itemBuilder: (context, index) {

                          CommentReply reply = _con.comment.replies[index];

                          var _options = reply.userId.toString() ==
                                  _con.currentUser.id.toString()
                              ? _con.myCommentOptions
                              : _con.commentOptions;

                          return Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(100),
                                child: reply.user != null &&
                                        reply.user.image != null
                                    ? CachedNetworkImage(
                                        imageUrl: reply.user.image,
                                        placeholder: (context, url) =>
                                            Image.asset(
                                          "assets/img/placeholders/profile.png",
                                          fit: BoxFit.cover,
                                        ),
                                        width: 40,
                                        height: 40,
                                        fit: BoxFit.cover,
                                      )
                                    : Image.asset(
                                        "assets/img/placeholders/profile.png",
                                        height: 50,
                                        width: 50,
                                        fit: BoxFit.cover,
                                      ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                flex: 1,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Container(
                                      child: Text(
                                        reply.user.name,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: textTheme.bodyText1.merge(
                                            TextStyle(
                                                fontWeight: FontWeight.w600)),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        reply.text,
                                        style: textTheme.bodyText1,
                                      ),
                                    ),
                                    Padding(
                                      padding:
                                          EdgeInsets.only(top: 10, left: 10),
                                      child: Row(
                                        children: [
                                          reply.likesCount != null &&
                                                  reply.likesCount > 0
                                              ? Text(
                                                  "${reply.likesCount} Likes",
                                                  style: textTheme.caption,
                                                )
                                              : SizedBox(
                                                  height: 0,
                                                  width: 0,
                                                ),
                                          SizedBox(
                                            width: reply.likesCount != null &&
                                                    reply.likesCount > 0
                                                ? 10
                                                : 0,
                                          ),
                                          InkWell(
                                            onTap: () {
                                              _con.postCommentReplyLike(reply);
                                            },
                                            child: Text(
                                              reply.isLiked ? "Liked" : "Like",
                                              style: textTheme.bodyText1.merge(
                                                  TextStyle(
                                                      color:
                                                          theme.accentColor)),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 15,
                                          ),
                                          InkWell(
                                            onTap: () {
                                              setState(() {
                                                _con.textController.text =
                                                    reply.user.name + " ";
                                                _con.showReplyBox = true;
                                              });
                                            },
                                            child: Text(
                                              "Reply",
                                              style: textTheme.bodyText1.merge(
                                                  TextStyle(
                                                      color:
                                                          theme.accentColor)),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              PopupMenuButton(
                                onSelected: (value) {
                                  _con.onCommentOptions(value, reply);
                                },
                                icon: RotatedBox(
                                  quarterTurns: 1,
                                  child: ImageIcon(
                                    AssetImage("assets/icons/more.png"),
                                    color: Colors.white,
                                  ),
                                ),
                                itemBuilder: (context) =>
                                    List.generate(_options.length, (index) {
                                  return PopupMenuItem(
                                      value: _options[index].option,
                                      child: Row(
                                        children: [
                                          ImageIcon(
                                              AssetImage(_options[index].icon)),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            _options[index].option,
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyText2,
                                          ),
                                        ],
                                      ));
                                }),
                                enabled: widget.enabled,
                              ),
                            ],
                          );
                        },
                      )
                    : SizedBox(height: 0),
                _con.showReplyBox
                    ? Column(
                        children: [
                          Container(
                            // height: 50,
                            constraints: BoxConstraints(minHeight: 50),
                            decoration: BoxDecoration(
                                color: theme.scaffoldBackgroundColor,
                                borderRadius: BorderRadius.circular(20)),
                            padding: EdgeInsets.symmetric(
                                horizontal: 7, vertical: 3),
                            margin: EdgeInsets.only(top: 10),
                            width: size.width,
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: TextFormField(
                                          keyboardType: TextInputType.multiline,
                                          controller: _con.textController,
                                          maxLines: null,
                                          maxLength: null,
                                          textInputAction:
                                              TextInputAction.newline,
                                          decoration: InputDecoration(
                                            enabledBorder: InputBorder.none,
                                            hintText:
                                                S.of(context).type_your_reply,
                                            border: InputBorder.none,
                                            hintStyle: TextStyle(
                                                color: theme.primaryColorDark
                                                    .withOpacity(0.6),
                                                fontSize: 13),
                                          ),
                                          onSaved: (val) {
                                            setState(() {
                                              _con.reply.text = val;
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                InkWell(
                                  onTap: () {
                                    setState(() {
                                      (_con.isEditingCommentReply)
                                          ? _con.editCommentNow()
                                          : _con.postCommentReply();
                                    });
                                  },
                                  child: Container(
                                    width: 30,
                                    height: 30,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(100),
                                        color: theme.accentColor),
                                    child: Center(
                                      child: Icon(
                                        Icons.send,
                                        size: 15,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          _con.isEditingCommentReply
                              ? Row(
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        setState(() {
                                          _con.isEditingCommentReply = false;
                                          _con.commentReplyEditing = CommentReply();
                                          _con.textController.text = "";
                                        });
                                      },
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10, vertical: 5),
                                        child: Text(
                                          "Cancel",
                                          style: textTheme.subtitle2,
                                        ),
                                      ),
                                    ),
                                    Text("Editing Comment",
                                        style: textTheme.caption)
                                  ],
                                )
                              : SizedBox(height: 0),
                        ],
                      )
                    : SizedBox(height: 0, width: 0),
              ],
            ),
          )
        ],
      ),
    );
  }
}
