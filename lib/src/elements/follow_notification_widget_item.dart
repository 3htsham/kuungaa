import 'package:flutter/material.dart';
import 'package:social_app/src/elements/notification_image.dart';
import 'package:social_app/src/elements/unread_notification_indicator.dart';
import 'package:social_app/src/models/my_notification.dart';

class FollowNotificationWidgetItem extends StatelessWidget {
  String txt;
  MyNotification noti;
  VoidCallback onTap;

  FollowNotificationWidgetItem({this.noti, this.txt, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
      child: InkWell(
        onTap: (){this.onTap();},
        splashColor: Theme.of(context).accentColor.withOpacity(0.5),
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 7),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              UnreadNotificationIndicator(status: noti.status,),
              NotificationImage(image: noti.user.image,),
              SizedBox(width: 10,),
              Expanded(
                flex: 1,
                child: RichText(
                  text: TextSpan(
                      style: Theme.of(context).textTheme.bodyText1,
                      children: [
                        TextSpan(text: '${noti.user.name.split(" ")[0]}', style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(fontWeight: FontWeight.w800))),
                        TextSpan(text: ' $txt')
                      ]
                  ),
                ),
              ),
              SizedBox(width: 10,),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Container(
                    width: 35,
                    height: 35,
                    decoration: BoxDecoration(
                        color: Theme.of(context).focusColor.withOpacity(0.5),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    child: Image.asset("assets/icons/add_friend.png",
                      width: 30, height: 30,
                      color: Theme.of(context).focusColor,
                    )
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
