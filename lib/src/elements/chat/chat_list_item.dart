import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:social_app/src/models/chat.dart';
import 'package:social_app/src/models/route_argument.dart';

class ChatsListItem extends StatelessWidget {

  Chat chat;
  VoidCallback onTap;
  Function onOptions;
  GlobalKey chatItemKey;
  List<String> options;

  ChatsListItem({this.chat, this.onTap, this.onOptions, this.options, this.chatItemKey});

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    var dateTime = chat.time;
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);



    return InkWell(
      onTap: this.onTap,
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
            padding: EdgeInsets.all(3),
            width: 60,
            height: 60,
            decoration: BoxDecoration(
              color: chat.isRead ? theme.accentColor : Colors.yellowAccent,
              borderRadius: BorderRadius.circular(100),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: chat.opponent.image !=
                  null &&
                  chat.opponent.image
                      .length >
                      0
                  ? CachedNetworkImage(
                imageUrl:
                chat.opponent.image,
              )
                  : Image.asset(
                  "assets/img/placeholders/profile.png"),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                chat.opponent.name,
                style: textTheme.headline6.merge(TextStyle(color: theme.primaryColorDark)),
              ),
              Container(
                width: 80,
                child: Text(
                  chat.lastMessage,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: textTheme.bodyText1.merge(TextStyle(color: theme.primaryColorDark)),
                ),
              ),
              Text(
                '$day at $time',
                style: textTheme.caption,
              ),
            ],
          ),
          Spacer(),
          PopupMenuButton(
            onSelected: (value) {
              this.onOptions(value, false, chat);
            },
            icon: RotatedBox(quarterTurns: 1, child: ImageIcon(AssetImage("assets/icons/more.png"), color: Colors.white,),),
            itemBuilder: (context) => List.generate(this.options.length, (index) {
                return PopupMenuItem(
                  value: this.options[index],
                child: Text(this.options[index], style: textTheme.bodyText2,));
              })
          )
        ],
      ),
    );
  }


}
