import 'package:flutter/material.dart';
import 'package:social_app/src/controllers/search_controller.dart';
import 'package:social_app/src/elements/group/group_grid_item.dart';
import 'package:social_app/src/models/group.dart';
import 'package:social_app/src/models/route_argument.dart';

class SearchGroupGrid extends StatelessWidget {

  SearchController con;
  List<Group> groups = <Group>[];

  SearchGroupGrid({this.groups, this.con});

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      primary: false,
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      crossAxisCount: MediaQuery.of(context).orientation == Orientation.portrait ? 2 : 4,
      children: List.generate(groups.length, (index) {
        return GroupGridItem(
          group: groups[index],
          onTap: (){
            Navigator.of(context).pushNamed("/ViewGroup", arguments: RouteArgument(group: groups[index], user: con.currentUser));
          },
          onJoin: (){
            if(groups[index].join != null && (groups[index].join == "false" || groups[index].join==false)) {
              con.joinGroupNow(groups[index]);
            }
          },
        );
      }),
    );
  }
}
