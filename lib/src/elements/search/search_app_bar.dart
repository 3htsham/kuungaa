import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/search_controller.dart';

class SearhcAppBar extends StatelessWidget {

  SearchController con;
  SearhcAppBar({this.con});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    final MediaQueryData media = MediaQuery.of(context);
    final double statusBarHeight = media.padding.top;

    return Container(
      padding: EdgeInsets.only(top: statusBarHeight),
      color: theme.scaffoldBackgroundColor,
      height: 58.0 + statusBarHeight,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          IconButton(
              icon: Icon(CupertinoIcons.back, color: theme.primaryColorDark,),
              onPressed: (){
                Navigator.of(context).pop();
              }
          ),
          SizedBox(width: 10,),
          Expanded(
            flex: 1,
            child: TextField(
              controller: con.searchController,
              textInputAction: TextInputAction.search,
              decoration: InputDecoration(
                // border: InputBorder.none,
                  hintText: S.of(context).search
              ),
              onSubmitted: (value) {
                if(value.length > 1) {
                  con.searchNow(value);
                }
              },
            ),
          ),
          IconButton(
              icon: ImageIcon(AssetImage("assets/icons/search.png")),
              onPressed: (){
                if(con.searchController.text != null && con.searchController.text.length > 0) {
                  con.searchNow(con.searchController.text);
                }
              }
          ),
        ],
      ),
    );
  }
}
