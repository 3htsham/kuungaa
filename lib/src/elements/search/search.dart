import 'package:flutter/material.dart';
import 'package:social_app/generated/i18n.dart';

class SearchWidget extends StatelessWidget {

  VoidCallback onTap;

  SearchWidget({this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      padding: EdgeInsets.only(right: 5, left: 10, top: 5, bottom: 5),
      decoration: BoxDecoration(
          border: Border.all(color: Theme.of(context).accentColor, width: 1),
          borderRadius: BorderRadius.circular(3)
      ),
      child: InkWell(
        onTap: this.onTap,
        child: Row(
          children: [
            Expanded(child: Container(
              child: Text(
                S.of(context).search,
                style: Theme.of(context).textTheme.bodyText2,
              ),
            )),
            ImageIcon(AssetImage("assets/icons/search.png")),
          ],
        ),
      ),
    );
  }
}
