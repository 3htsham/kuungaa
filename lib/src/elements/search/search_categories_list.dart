import 'package:flutter/material.dart';
import 'package:social_app/src/controllers/search_controller.dart';

class SearchCategoriesList extends StatelessWidget {

  SearchController con;

  SearchCategoriesList({this.con});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return Container(
      height: 32,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          primary: false,
          shrinkWrap: true,
          itemCount: con.showCategories.length,
          itemBuilder: (context, i) {
            return Padding(
              padding:
              const EdgeInsets.symmetric(horizontal: 5),
              child: InkWell(
                onTap: () {
                  con.changeShowCategory(con.showCategories[i]);
                },
                splashColor: theme.accentColor,
                child: Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: 10, vertical: 4),
                  decoration: BoxDecoration(
                    color: con.current == con.showCategories[i] ? theme.accentColor : theme.scaffoldBackgroundColor,
                      border: Border.all(
                          color: theme.accentColor,
                          width: 1),
                      borderRadius: BorderRadius.circular(3)),
                  child: Center(
                    child: Text(
                      con.showCategories[i],
                      style: textTheme.bodyText1.merge(
                          TextStyle(
                              color: con.current == con.showCategories[i] ? theme.primaryColorDark : theme.accentColor,
                              fontSize: 14)),
                    ),
                  ),
                ),
              ),
            );
          }),
    );
  }
}
