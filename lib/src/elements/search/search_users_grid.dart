import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:social_app/src/controllers/search_controller.dart';
import 'package:social_app/src/models/route_argument.dart';

class SearchUsersGrid extends StatelessWidget {

  SearchController con;
  SearchUsersGrid({this.con});

  @override
  Widget build(BuildContext context) {
    return GridView.count(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      primary: false,
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
      crossAxisCount: MediaQuery.of(context).orientation == Orientation.portrait ? 3 : 5,
      children: List.generate(con.searchResults.users.length, (i) {
        return InkWell(
          onTap: (){
            Navigator.of(context).pushNamed("/UserProfileDetails", arguments: RouteArgument(user: con.searchResults.users[i]));
          },
          child: Container(
            margin: EdgeInsets.only(left: 5, right: 5, top: 10, bottom: 10),
            width: 100,
            height: 100,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      boxShadow: [
                        BoxShadow(
                            offset: Offset(1, 1),
                            spreadRadius: 0.5,
                            blurRadius: 5,
                            color: Theme.of(context).focusColor.withOpacity(0.5))
                      ]
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: CachedNetworkImage(
                      imageUrl: con.searchResults.users[i].image,
                      placeholder: (context, url) => Image.asset("assets/img/placeholders/profile.png", fit: BoxFit.cover,),
                      height: 78,
                      width: 78,
                      fit: BoxFit.cover,
                    )
                    // FadeInImage.assetNetwork(
                    //   placeholder: "assets/img/placeholders/profile.png",
                    //   image: con.searchResults.users[i].image,
                    //   height: 78,
                    //   width: 78,
                    //   fit: BoxFit.cover,
                    // ),
                  ),
                ),
                SizedBox(height: 6,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  child: Text(
                    con.searchResults.users[0].name,
                    maxLines: 1,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.fade,
                    style: Theme.of(context).textTheme.subtitle2
                        .merge(TextStyle(fontSize: 13)),
                  ),
                ),
                // Text(
                //   '$month $day, $year',
                //   maxLines: 1,
                //   overflow: TextOverflow.ellipsis,
                //   style: Theme.of(context).textTheme.bodyText2
                //       .merge(TextStyle(fontSize: 10)),
                // ),
              ],
            ),
          ),
        );
      }),
    );
  }
}
