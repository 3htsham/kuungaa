import 'package:flutter/material.dart';
import 'package:social_app/src/controllers/search_controller.dart';
import 'package:social_app/src/elements/user/people_widget.dart';
import 'package:social_app/src/models/route_argument.dart';

class PeopleHorizontalList extends StatelessWidget {

  SearchController con;

  PeopleHorizontalList({this.con});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 185,
      child: ListView.builder(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        itemCount: con.searchResults.users.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (ctx, index) {
          return PeopleWidget(
            user: con.searchResults.users[index],
            onTap: (){
              //Open User Profile
              Navigator.of(context).pushNamed("/UserProfileDetails", arguments: RouteArgument(user: con.searchResults.users[index]));
            },
            onAddFriend: (){
              //Send or Cancel Friend Request
              con.sendOrCancelFriendRequest(con.searchResults.users[index]);
            },
            leftMargin: 10,
            rightMargin: (index == con.searchResults.users.length-1) ? 10 : 0,
          );
        },
      ),
    );
  }
}
