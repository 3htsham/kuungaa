import 'package:flutter/material.dart';
import 'package:social_app/src/controllers/search_controller.dart';
import 'package:social_app/src/elements/group/group_icon.dart';
import 'package:social_app/src/models/route_argument.dart';

class GroupsHorizontalWidget extends StatelessWidget {

  SearchController con;

  GroupsHorizontalWidget({this.con});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 140,
      child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          itemCount: con.searchResults.groups.length,
          itemBuilder: (ctx, index) {
            return GroupIcon(group: con.searchResults.groups[index],
              onTap: (){
                Navigator.of(context).pushNamed("/ViewGroup",
                    arguments: RouteArgument(group: con.searchResults.groups[index], user: con.currentUser));
              },
              leftMargin: index==0 ? 20 : 5,
              rightMargin: (index== con.searchResults.groups.length-1) ? 10 : 0,
            );
          }
      ),
    );
  }
}
