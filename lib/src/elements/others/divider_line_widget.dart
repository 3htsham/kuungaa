import 'package:flutter/material.dart';

class DividerLineWidget extends StatelessWidget {

  String text;
  DividerLineWidget({this.text});

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          width: MediaQuery.of(context).size.width * 0.9,
          height: 1,
          color: Theme.of(context).focusColor.withOpacity(0.3),
        ),
        Align(
          alignment: Alignment.center,
          child: Container(
            color: Theme.of(context).scaffoldBackgroundColor,
            padding: EdgeInsets.symmetric(horizontal: text==null ? 0 : 5),
            child: Text(
                text ?? "",
                style: Theme.of(context).textTheme.bodyText2
                    .merge(TextStyle(fontSize: 10),
                )
            ),
          ),
        )
      ],
    );
  }
}