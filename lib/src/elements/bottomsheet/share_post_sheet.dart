import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/controllers/home_controller.dart';
import 'package:social_app/src/models/post.dart';

class SharePostSheet extends StatefulWidget {

  HomeController con;
  Post post;
  SharePostSheet({this.con, this.post});

  @override
  _SharePostSheetState createState() => _SharePostSheetState();
}

class _SharePostSheetState extends StateMVC<SharePostSheet> {

  HomeController con;

  _SharePostSheetState() : super(HomeController()) {
    con = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

