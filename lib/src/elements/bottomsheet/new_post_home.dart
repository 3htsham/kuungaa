
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/home_controller.dart';

class PostNewThingBottomSheetStfulWidget extends StatefulWidget {

  HomeController con;
  PostNewThingBottomSheetStfulWidget({this.con});

  @override
  _PostNewThingBottomSheetStfulWidgetState createState() => _PostNewThingBottomSheetStfulWidgetState();
}

class _PostNewThingBottomSheetStfulWidgetState extends StateMVC<PostNewThingBottomSheetStfulWidget> {

  HomeController con;

  _PostNewThingBottomSheetStfulWidgetState() : super(HomeController()) {
    con = controller;
  }

  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var media = MediaQuery.of(context);
    var size = media.size;
    return Container(
      color: Theme.of(context).primaryColor,
      width: size.width,
      height: size.height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  margin: EdgeInsets.only(top: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(S.of(context).post_something_new, style: textTheme.headline6,),
                    ],
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(S.of(context).privacy, style: textTheme.bodyText2,),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      height: 20,
                      child: PopupMenuButton(
                          onSelected: (value) {
                            con.onPostPrivacyOptions(value);
                          },
                          child: Row(
                            children: [
                              ImageIcon(AssetImage(con.currentPrivacyOption.icon)),
                              SizedBox(width: 10,),
                              Text(
                                con.currentPrivacyOption.option,
                                style: Theme.of(context).textTheme.bodyText2,
                              ),
                            ],
                          ),
                          itemBuilder: (context) =>
                              List.generate(con.newPostPrivacyOptions.length, (index) {
                                return PopupMenuItem(
                                    value: con.newPostPrivacyOptions[index].option,
                                    child: Row(
                                      children: [
                                        ImageIcon(AssetImage(con.newPostPrivacyOptions[index].icon)),
                                        SizedBox(width: 10,),
                                        Text(
                                          con.newPostPrivacyOptions[index].option,
                                          style: Theme.of(context).textTheme.bodyText2,
                                        ),
                                      ],
                                    ));
                              })),
                    )
                  ],
                )
              ],
            ),
          ),

          Container(height: 0.4, width: size.width, color: theme.focusColor.withOpacity(0.5),),

          Expanded(
            child: Container(
              height: size.height,
              width: size.width,
              child: Column(
                children: [

                  Container(
                    height: 200,
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: SingleChildScrollView(
                      child: TextFormField(
                        keyboardType: TextInputType.multiline,
                        controller: con.textController,
                        maxLines: null,
                        maxLength: null,
                        textInputAction: TextInputAction.newline,
                        decoration: InputDecoration(
                          enabledBorder: InputBorder.none,
                          hintText: S.of(context).tell_people_about_your_day,
                          border: InputBorder.none,
                          hintStyle: TextStyle(
                              color: theme.primaryColorDark.withOpacity(0.6),
                              fontSize: 15
                          ),
                        ),
                        onSaved: (val){
                          setState((){
                            con.post.text = val;
                          });
                        },
                      ),
                    ),
                  ),

                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      height: 110,
                      child: con.post !=null && con.post.mediaList !=null ?
                      ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: con.post.mediaList.length,
                          itemBuilder: (ctx, indx) {
                            return Container(
                                padding: EdgeInsets.only(top: 10, left: 10),
                                width: 100,
                                height: 100,
                                child: Align(
                                    alignment: Alignment.bottomLeft,
                                    child: Stack(
                                      fit: StackFit.expand,
                                      children: [
                                        con.post.mediaList[indx].type == 0
                                            ? Image.file(File(con
                                            .post
                                            .mediaList[indx]
                                            .identifier,
                                        ),
                                          width: 100,
                                          height: 100,
                                          fit: BoxFit.cover,
                                        )
                                            : Image.asset(
                                          "assets/img/placeholders/videopholder.jpg",
                                          width: 100,
                                          height: 100,
                                          fit: BoxFit.cover,),

                                        Positioned(
                                          right: 0,
                                          top: 0,
                                          child: IconButton(
                                              onPressed: (){
                                                setState((){

                                                });
                                              },
                                              icon: Icon(Icons.close, size: 20, color: theme.accentColor,)
                                          ),
                                        )
                                      ],
                                    )
                                )
                            );
                          })
                          : SizedBox(width: 0, height: 0,),
                    ),
                  )
                ],
              ),
            ),
          ),

          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      IconButton(
                        onPressed: (){
                          setState((){
                            con.multipleImagePick();
                          });
                        },
                        splashColor: theme.accentColor.withOpacity(0.5),
                        tooltip: "Gallery",
                        icon: ImageIcon(AssetImage("assets/icons/gallery.png"), color: theme.accentColor,),
                      ),
                      IconButton(
                        onPressed: (){
                          setState((){
                            con.getCameraImage();
                          });
                        },
                        splashColor: theme.accentColor.withOpacity(0.5),
                        tooltip: "Camera",
                        icon: ImageIcon(AssetImage("assets/icons/camera.png"), color: theme.accentColor,),
                      ),
                      IconButton(
                        onPressed: (){
                          setState((){
                            con.getVideoFile();
                          });
                        },
                        splashColor: theme.accentColor.withOpacity(0.5),
                        tooltip: "Video",
                        icon: ImageIcon(AssetImage("assets/icons/movie.png"), color: theme.accentColor,),
                      ),
                    ],
                  ),
                  InkWell(
                    onTap: (){
                      setState((){
                        con.post.text = con.textController.text;
                      });
                      Navigator.of(context).pop(con);
                    },
                    child: Container(
                      width: 70,
                      height: 35,
                      decoration: BoxDecoration(
                          color: theme.accentColor,
                          borderRadius: BorderRadius.circular(5)
                      ),
                      child: Center(
                        child: Text(
                          S.of(context).post,
                          style: textTheme.subtitle2,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}