import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_emoji_keyboard/flutter_emoji_keyboard.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/config/emoji_keyboard_icons_icons.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/home_controller.dart';
import 'package:social_app/src/elements/comment/comment_widget.dart';
import 'package:social_app/src/models/comment.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/user.dart';


class PostCommentsBottomSheet extends StatefulWidget {

  Post post;
  User currentUser;

  PostCommentsBottomSheet(this.currentUser, {this.post,});

  @override
  _PostCommentsBottomSheetState createState() => _PostCommentsBottomSheetState();
}

class _PostCommentsBottomSheetState extends StateMVC<PostCommentsBottomSheet> {

  bool isEmoji = false;
  KeyboardVisibilityController keyboardVisibilityController;
  HomeController con;

  _PostCommentsBottomSheetState() : super(HomeController()) {
    con = controller;
  }

  @override
  void initState() {
    con.post = widget.post;
    con.currentUser = widget.currentUser;
    con.getPostCommentsNow();
    super.initState();

    keyboardVisibilityController = KeyboardVisibilityController();
    // Query
    print('Keyboard visibility direct query: ${keyboardVisibilityController.isVisible}');

    // Subscribe
    keyboardVisibilityController.onChange.listen((visible) {
      if(visible) {
        setState((){
          isEmoji = false;
        });
      }
    });
  }


  showEmojiKeyboard(){
    if(keyboardVisibilityController != null
        && keyboardVisibilityController.isVisible) {
      FocusScope.of(context).unfocus();
    }
    Future.delayed(Duration(microseconds: 200), (){
      setState((){
        this.isEmoji = !isEmoji;
      });
    });
  }


  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var media = MediaQuery.of(context);
    var size = media.size;



    return WillPopScope(
      onWillPop: () async {
        if(this.isEmoji) {
          setState((){
            isEmoji = false;
          });
          return false;
        }
        else {
          Navigator.of(context).pop(con.post);
          return true;
        }
      },
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              color: Theme.of(context).primaryColor,
              child: Column(
                children: [
                  SizedBox(height: 30,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        onPressed: (){
                          Navigator.of(context).pop();
                        },
                        icon: Icon(Icons.arrow_back_ios),
                        ),
                      Align(
                        alignment: Alignment.topCenter,
                        child: Text(S.of(context).comments, style: textTheme.headline6),
                      ),

                      IconButton(
                        onPressed: (){
                        },
                        icon: Icon(Icons.view_headline_rounded),
                      ),
                    ],
                  ),
                  SizedBox(height: 5,),
                  Container(height: 0.4, width: size.width, color: theme.focusColor.withOpacity(0.5),),

                  Expanded(
                      child: Container(
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              ListView.builder(
                                  primary: false,
                                  shrinkWrap: true,
                                  itemCount: con.post.comments.length ?? 0,
                                  itemBuilder: (ctx, indx) {

                                    Comment cmnt = con.post.comments[indx];

                                    var _options = cmnt.userId.toString() ==
                                        con.currentUser.id.toString()
                                        ? con.myCommentOptions
                                        : con.commentOptions;

                                    return CommentWidget(
                                      comment: cmnt,
                                      currentUser: con.currentUser,
                                      options: _options,
                                      onOptionsTap: (value){
                                        con.onCommentOptions(value, con.post.comments[indx]);
                                      },
                                      enabled: (con.commentEditing == null ||
                                          con.commentEditing.id.toString() != con.post.comments[indx].id.toString()),
                                    );


                                    /*

                                    return Container(
                                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          ClipRRect(
                                            borderRadius: BorderRadius.circular(100),
                                            child: con.post.comments[indx].user != null && con.post.comments[indx].user.image != null
                                                ? CachedNetworkImage(
                                              imageUrl: con.post.comments[indx].user.image,
                                              placeholder: (context, url) => Image.asset("assets/img/placeholders/profile.png", fit: BoxFit.cover,),
                                              width: 50,
                                              height: 50,
                                              fit: BoxFit.cover,
                                            )
                                            // FadeInImage.assetNetwork(
                                            //   placeholder: "assets/img/placeholders/profile.png",
                                            //   image: con.post.comments[indx].user.image,
                                            //   height: 50,
                                            //   width: 50,
                                            //   fit: BoxFit.cover,
                                            // )
                                          : Image.asset("assets/img/placeholders/profile.png", height: 50, width: 50, fit: BoxFit.cover, ),
                                          ),
                                          SizedBox(width: 10,),
                                          Expanded(
                                            flex: 1,
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: [
                                                Container(
                                                  child: Text(
                                                    con.post.comments[indx].user.name,
                                                    maxLines: 1,
                                                    overflow: TextOverflow.ellipsis,
                                                    style: textTheme.bodyText1.merge(TextStyle(fontWeight: FontWeight.w600)),
                                                  ),
                                                ),
                                                Container(
                                                  child: Text(
                                                    con.post.comments[indx].text,
                                                    style: textTheme.bodyText1,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),

                                          PopupMenuButton(
                                              onSelected: (value) {
                                                con.onCommentOptions(value, con.post.comments[indx]);
                                              },
                                              icon: RotatedBox(
                                                quarterTurns: 1,
                                                child: ImageIcon(
                                                  AssetImage("assets/icons/more.png"),
                                                  color: Colors.white,
                                                ),
                                              ),
                                              itemBuilder: (context) =>
                                                  List.generate(
                                                      _options.length,
                                                          (index) {
                                                    return PopupMenuItem(
                                                        value: _options[index].option,
                                                        child: Row(
                                                          children: [
                                                            ImageIcon(AssetImage(_options[index].icon)),
                                                            SizedBox(width: 10,),
                                                            Text(
                                                              _options[index].option,
                                                              style: Theme.of(context).textTheme.bodyText2,
                                                            ),
                                                          ],
                                                        ));
                                                  }),

                                      enabled: (con.commentEditing == null ||
                                          con.commentEditing.id.toString() != con.post.comments[indx].id.toString()),

                                    ),


                                        ],
                                      ),
                                    );

                                    */

                                  }
                              )
                            ],
                          ),
                        ),
                      )
                  ),

                  Container(height: 0.4, width: size.width, color: theme.focusColor.withOpacity(0.5),),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Column(
                      children: [

                        Container(
                          height: 60,
                          color: theme.scaffoldBackgroundColor,
                          padding: EdgeInsets.symmetric(horizontal: 7, vertical: 5),
                          width: size.width,
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Row(
                                  children: [
                                    IconButton(
                                      onPressed: this.showEmojiKeyboard,
                                      icon: Icon(
                                        EmojiKeyboardIcons.smile,
                                        color: theme.primaryColorDark,
                                        size: 18,
                                      ),
                                    ),
                                    Expanded(
                                      child: TextFormField(
                                        keyboardType: TextInputType.multiline,
                                        controller: con.textController,
                                        maxLines: null,
                                        maxLength: null,
                                        textInputAction: TextInputAction.newline,
                                        decoration: InputDecoration(
                                          enabledBorder: InputBorder.none,
                                          hintText: S.of(context).type_your_comment_here,
                                          border: InputBorder.none,
                                          hintStyle: TextStyle(
                                              color: theme.primaryColorDark.withOpacity(0.6),
                                              fontSize: 15
                                          ),
                                        ),
                                        onSaved: (val){
                                          setState((){
                                            con.post.text = val;
                                          });
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              InkWell(
                                onTap: (){
                                  setState((){
                                    (con.isEditingComment)
                                        ? con.editCommentNow()
                                        : con.commentOnPost(widget.post);
                                  });
                                },
                                child: Container(
                                  width: 40,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(100),
                                      color: theme.accentColor
                                  ),
                                  child: Center(
                                    child: Icon(
                                      Icons.send,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),

                        this.isEmoji
                            ? EmojiKeyboard(
                            column: 5,
                            floatingHeader: false,
                            color: theme.scaffoldBackgroundColor,
                            categoryIcons: CategoryIcons(
                                color: Colors.white,
                                people: EmojiKeyboardIcons.smile,
                                nature: EmojiKeyboardIcons.tiger,
                                food: EmojiKeyboardIcons.coffee_cup,
                                activity: EmojiKeyboardIcons.football,
                                travel: EmojiKeyboardIcons.car,
                                flags: EmojiKeyboardIcons.flags,
                                objects: EmojiKeyboardIcons.lightbulb,
                                symbols: EmojiKeyboardIcons.heart),
                            onEmojiSelected: (_emoji) {
                              con.textController.text+=_emoji.toString();
                            })
                            : SizedBox(height: 0, width: 0),

                      ],
                    ),
                  ),



                ],
              ),
            ),


            ///Loader
            con.loadingComment
                ? Container(
              color: Colors.black.withOpacity(0.5),
              child: Center(
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(15)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      SizedBox(height: 10),
                      Text("Please wait...",
                          style: Theme.of(context).textTheme.bodyText2)
                    ],
                  ),
                ),
              ),
            )
                : Positioned(bottom: 10, child: SizedBox(height: 0)),
          ],
        ),
      ),
    );
  }
}