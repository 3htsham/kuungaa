import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:social_app/src/elements/notification_image.dart';
import 'package:social_app/src/elements/unread_notification_indicator.dart';
import 'package:social_app/src/models/my_notification.dart';

class LikedPostNotificationWidgetItem extends StatelessWidget {
  MyNotification noti;
  VoidCallback onTap;
  String txt;

  LikedPostNotificationWidgetItem({this.noti, this.txt, this.onTap});


  @override
  Widget build(BuildContext context) {
    var day = DateFormat('EEEE').format(noti.dateTime);
    var time = DateFormat('hh:mm aa').format(noti.dateTime);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
      child: InkWell(
        onTap: (){this.onTap();},
        splashColor: Theme.of(context).accentColor.withOpacity(0.5),
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 7),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              UnreadNotificationIndicator(status: noti.status,),
              NotificationImage(image: noti.user.image,),
              SizedBox(width: 10,),
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RichText(
                      text: TextSpan(
                          style: Theme.of(context).textTheme.bodyText1,
                          children: [
                            TextSpan(text: '${noti.user.name.split(" ")[0]}', style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(fontWeight: FontWeight.w800))),
                            TextSpan(text: ' $txt')
                          ]
                      ),
                    ),
                    SizedBox(height: 5,),
                    Text(
                      '$day at $time',
                      style: Theme.of(context).textTheme.bodyText2
                          .merge(TextStyle(color: Theme.of(context).focusColor)),
                    )
                  ],
                ),
              ),
              SizedBox(width: 10,),
              noti.post.image != null && noti.post.image.length > 0
                  ? ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: FadeInImage.assetNetwork(
                  placeholder: "assets/img/placeholders/profile.png",
                  image: noti.post.image,
                  height: 55,
                  width: 55,
                  fit: BoxFit.cover,
                ),
              )
                  : SizedBox(width: 0,),
            ],
          ),
        ),
      ),
    );
  }
}
