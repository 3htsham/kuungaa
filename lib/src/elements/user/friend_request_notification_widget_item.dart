import 'package:flutter/material.dart';
import 'package:social_app/src/elements/unread_notification_indicator.dart';
import 'package:social_app/src/models/my_notification.dart';

import '../notification_image.dart';

class FriendRequestNotificationWidgetItem extends StatelessWidget {
  MyNotification noti;
  String txt;
  VoidCallback onAccept;
  VoidCallback onIgnore;
  VoidCallback onTap;

  FriendRequestNotificationWidgetItem({this.noti, this.txt, this.onAccept, this.onIgnore, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
      child: InkWell(
        onTap: (){this.onTap();},
        splashColor: Theme.of(context).accentColor.withOpacity(0.5),
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 5, horizontal: 7),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              UnreadNotificationIndicator(status: noti.status,),
              NotificationImage(image: noti.user.image,),
              SizedBox(width: 10,),
              Expanded(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                      text: TextSpan(
                          style: Theme.of(context).textTheme.bodyText1,
                          children: [
                            TextSpan(text: '${noti.user.name.split(" ")[0]}', style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(fontWeight: FontWeight.w800))),
                            TextSpan(text: ' $txt')
                          ]
                      ),
                    ),
                    SizedBox(height: 10,),
                    Row(
                      children: [
                        noti.friendRequestNotification.type == 0
                            ? noti.friendRequestNotification.friendRequestStatus == 1 //No response yet
                            ? Row(
                          children: [
                            InkWell(
                              onTap: (){this.onAccept();},
                              splashColor: Theme.of(context).focusColor,
                              child: Container(
                                padding: EdgeInsets.only(left: 7, right: 12, top: 5, bottom: 5),
                                // width: 70,
                                // height: 30,
                                decoration: BoxDecoration(
                                    color: Theme.of(context).accentColor,
                                    borderRadius: BorderRadius.circular(5)
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    ImageIcon(AssetImage("assets/icons/check.png"), size: 20,),
                                    Text("Add", style: Theme.of(context).textTheme.subtitle2.merge(TextStyle(color: Colors.white, fontSize: 14)),)
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(width: 5,),
                            InkWell(
                              onTap: (){this.onIgnore();},
                              splashColor: Theme.of(context).accentColor.withOpacity(0.5),
                              child: Container(
                                padding: EdgeInsets.only(left: 7, right: 12, top: 5, bottom: 5),
                                // width: 70,
                                // height: 30,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5)
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Icon(Icons.clear, size: 20,),
                                    Text("Ignore", style: Theme.of(context).textTheme.subtitle2.merge(TextStyle(color: Colors.white, fontSize: 14)),)
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                            : noti.friendRequestNotification.friendRequestStatus == 0 //Added
                            ? Container(
                          padding: EdgeInsets.only(left: 7, right: 12, top: 5, bottom: 5),
                          // width: 70,
                          // height: 30,
                          decoration: BoxDecoration(
                              color: Theme.of(context).focusColor,
                              borderRadius: BorderRadius.circular(5)
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              ImageIcon(AssetImage("assets/icons/check.png"), size: 20,),
                              Text("Added", style: Theme.of(context).textTheme.subtitle2.merge(TextStyle(color: Colors.white, fontSize: 14)),)
                            ],
                          ),
                        )
                            : Container(
                          padding: EdgeInsets.only(left: 7, right: 12, top: 5, bottom: 5),
                          // width: 70,
                          // height: 30,
                          decoration: BoxDecoration(
                              color: Theme.of(context).focusColor,
                              borderRadius: BorderRadius.circular(5)
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(Icons.clear, size: 20,),
                              Text("Rejected", style: Theme.of(context).textTheme.subtitle2.merge(TextStyle(color: Colors.white, fontSize: 14)),)
                            ],
                          ),
                        )
                            : SizedBox(width: 0, height: 0,)
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(width: 10,),
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Container(
                    width: 35,
                    height: 35,
                    decoration: BoxDecoration(
                        color: Theme.of(context).focusColor.withOpacity(0.5),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    child: Image.asset("assets/icons/add_friend.png",
                      width: 30, height: 30,
                      color: Theme.of(context).focusColor,
                    )
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
