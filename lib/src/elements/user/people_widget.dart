import 'package:flutter/material.dart';
import 'package:social_app/src/models/user.dart';

class PeopleWidget extends StatelessWidget {

  User user;
  double leftMargin = 0;
  double rightMargin = 0;
  VoidCallback onTap;
  VoidCallback onAddFriend;

  PeopleWidget({this.user, this.leftMargin, this.rightMargin, this.onTap, this.onAddFriend});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: this.onTap,
      child: Container(
        margin: EdgeInsets.only(left: leftMargin, right: rightMargin, top: 10, bottom: 10),
        width: 120,
        height: 165,
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(7),
            boxShadow: [
              BoxShadow(
                  offset: Offset(1, 1),
                  spreadRadius: 0.5,
                  blurRadius: 5,
                  color: Theme.of(context).focusColor.withOpacity(0.5))
            ]),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 10,),
            ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: FadeInImage.assetNetwork(
                placeholder: "assets/img/placeholders/profile.png",
                image: user.image,
                height: 80,
                width: 80,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(height: 6,),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 3),
              child: Text(user.name ?? "",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.subtitle2
                    .merge(TextStyle()),
              ),
            ),
            Text(
              user.location ?? "",
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.bodyText2,
            ),
            SizedBox(height: 6,),
            Container(
              height: 30,
              width: 120,
              child: RaisedButton(
                  onPressed: this.onAddFriend,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(bottomRight: Radius.circular(7), bottomLeft: Radius.circular(7)),
                  ),
                  color: Theme.of(context).accentColor,
                  child: Text(
                    user.friendRequestStatus==0 ? "Add Friend" : "Cancel Request",
                    style: Theme.of(context).textTheme.subtitle2.merge(TextStyle(fontSize: user.friendRequestStatus==0 ? 13 : 11)),)
              ),
            )
          ],
        ),
      ),
    );
  }
}