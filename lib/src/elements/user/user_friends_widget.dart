import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/controllers/user_controller.dart';
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/elements/user/people_icon.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/models/user.dart';

class UserFriendsWidget extends StatefulWidget {
  User user;

  UserFriendsWidget({this.user});

  @override
  _UserFriendsWidgetState createState() => _UserFriendsWidgetState();
}

class _UserFriendsWidgetState extends StateMVC<UserFriendsWidget>
    with AutomaticKeepAliveClientMixin {

  UserController _con;

  _UserFriendsWidgetState() : super(UserController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.user = widget.user;
    super.initState();
    _con.getCurrentUserAndFriends();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      child: Column(
        children: [
          _con.friends.isEmpty
              ? CircularLoadingWidget(height: 70,)
              : GridView.count(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            primary: false,
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
            crossAxisCount: MediaQuery.of(context).orientation == Orientation.portrait ? 3 : 5,
            children: List.generate(_con.friends.length, (index) {
              return PeopleIcon(
                user: _con.friends[index],
                onTap: (){
                    Navigator.of(context).pushNamed("/UserProfileDetails", arguments: RouteArgument(user: _con.friends[index]));
                  },
                rightMargin: 5,
                leftMargin: 5,
              );
            }),
          ),
        ],
      ),
    );
  }



}
