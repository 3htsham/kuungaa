import 'package:flutter/material.dart';
import 'package:social_app/src/models/user.dart';


class PeopleIcon extends StatelessWidget {
  double leftMargin;
  User user;
  double rightMargin;
  VoidCallback onTap;

  PeopleIcon({this.user, this.onTap, this.leftMargin, this.rightMargin});

  @override
  Widget build(BuildContext context) {

    return InkWell(
      onTap: this.onTap,
      child: Container(
        margin: EdgeInsets.only(left: leftMargin, right: rightMargin, top: 10, bottom: 10),
        width: 100,
        height: 100,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 10,),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(1, 1),
                        spreadRadius: 0.5,
                        blurRadius: 5,
                        color: Theme.of(context).focusColor.withOpacity(0.5))
                  ]
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: FadeInImage.assetNetwork(
                  placeholder: "assets/img/placeholders/profile.png",
                  image: user.image,
                  height: 60,
                  width: 60,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(height: 6,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Text(
                user.name,
                maxLines: 1,
                textAlign: TextAlign.center,
                overflow: TextOverflow.fade,
                style: Theme.of(context).textTheme.subtitle2
                    .merge(TextStyle(fontSize: 13)),
              ),
            ),
            // Text(
            //   '$month $day, $year',
            //   maxLines: 1,
            //   overflow: TextOverflow.ellipsis,
            //   style: Theme.of(context).textTheme.bodyText2
            //       .merge(TextStyle(fontSize: 10)),
            // ),
          ],
        ),
      ),
    );
  }
}
