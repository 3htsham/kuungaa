import 'package:flutter/material.dart';
import 'package:social_app/src/models/user.dart';


class GroupMemberIcon extends StatelessWidget {
  double leftMargin;
  User user;
  double rightMargin;
  VoidCallback onTap;

  List<String> options;
  Function onOptionsTap;

  GroupMemberIcon({this.user, this.options, this.onOptionsTap, this.onTap, this.leftMargin, this.rightMargin});

  @override
  Widget build(BuildContext context) {

    return InkWell(
      onTap: this.onTap,
      child: Container(
        margin: EdgeInsets.only(left: leftMargin, right: rightMargin, top: 10, bottom: 10),
        width: 100,
        height: 100,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 10,),
            Stack(
              children: [
                Center(
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        boxShadow: [
                          BoxShadow(
                              offset: Offset(1, 1),
                              spreadRadius: 0.5,
                              blurRadius: 5,
                              color: Theme.of(context).focusColor.withOpacity(0.5))
                        ]
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: FadeInImage.assetNetwork(
                        placeholder: "assets/img/placeholders/profile.png",
                        image: user.image,
                        height: 60,
                        width: 60,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    width: 18,
                    height: 19,
                    child: PopupMenuButton(
                      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
                        onSelected: (value) {
                          this.onOptionsTap(value);
                        },
                        icon: RotatedBox(quarterTurns: 1, child: ImageIcon(AssetImage("assets/icons/more.png"), size: 19, color: Theme.of(context).accentColor,),),
                        itemBuilder: (context) => List.generate(options.length, (index) {
                          return PopupMenuItem(
                            value: options[index],
                            height: 30,
                            child: Text(options[index], style: Theme.of(context).textTheme.caption,),
                          );
                        })
                    ),
                  ),
                )
              ],
            ),
            SizedBox(height: 6,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Text(
                user.name,
                maxLines: 1,
                textAlign: TextAlign.center,
                overflow: TextOverflow.fade,
                style: Theme.of(context).textTheme.subtitle2
                    .merge(TextStyle(fontSize: 13)),
              ),
            ),
            // Text(
            //   '$month $day, $year',
            //   maxLines: 1,
            //   overflow: TextOverflow.ellipsis,
            //   style: Theme.of(context).textTheme.bodyText2
            //       .merge(TextStyle(fontSize: 10)),
            // ),
          ],
        ),
      ),
    );
  }
}
