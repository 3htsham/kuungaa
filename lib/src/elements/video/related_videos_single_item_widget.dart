
import 'package:flutter/material.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/video.dart';

class RelatedVideosSingleItemWidget extends StatelessWidget {
  Video videoPost;
  VoidCallback onTap;
  double containerWidth;

  RelatedVideosSingleItemWidget({this.videoPost, this.onTap, this.containerWidth});

  @override
  Widget build(BuildContext context) {
    double containerHeight = containerWidth * 00.5625;
    return InkWell(
      onTap: this.onTap,
      splashColor: Theme.of(context).accentColor.withOpacity(0.5),
      highlightColor: Theme.of(context).accentColor.withOpacity(0.5),
      focusColor: Theme.of(context).accentColor.withOpacity(0.5),
      hoverColor: Theme.of(context).accentColor.withOpacity(0.5),
      child: Container(
        width: containerWidth,
        height: containerHeight,
        margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(5),
            boxShadow: [
              BoxShadow(
                  offset: Offset(1, 1),
                  spreadRadius: 0.5,
                  blurRadius: 5,
                  color: Theme.of(context).focusColor.withOpacity(0.5)
              )
            ]
        ),
        child: Column(
          children: [
            Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: FadeInImage.assetNetwork(
                    placeholder: "assets/img/placeholders/videopholder.jpg",
                    // image: post.media.thumbnailUrl,
                    image: videoPost.thumbnail,
                    height: containerWidth * 00.5625,
                    width: containerWidth,
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  width: containerWidth,
                  height: containerWidth * 00.5625,
                  decoration: BoxDecoration(color: Colors.black26, borderRadius: BorderRadius.circular(5),),
                  child: Align(alignment: Alignment.center, child: Icon(Icons.play_arrow, size: 40,)),
                ),
                Container(
                  width: containerWidth,
                  height: containerWidth * 00.5625,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5),),
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 7, vertical: 5),
                      child: Text(videoPost.post.text ?? videoPost.post.user.name ?? "", maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.subtitle2.merge(TextStyle(fontSize: 11)),
                      ),
                    ),
                  ),
                )

              ],
            ),

            // Padding(
            //   padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            //   child: Row(
            //     children: [
            //       ClipRRect(
            //         borderRadius: BorderRadius.circular(100),
            //         child: FadeInImage.assetNetwork(
            //             placeholder: "assets/img/placeholders/profile.png",
            //             image: post.user.image,
            //           height: 30,
            //           width: 30,
            //           fit: BoxFit.cover,
            //         ),
            //       ),
            //       SizedBox(width: 10,),
            //       Flexible(
            //         child: Text(
            //           post.user.name,
            //           style: Theme.of(context).textTheme.bodyText2,
            //         ),
            //       )
            //     ],
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}