import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:social_app/src/controllers/video_page_controller.dart';
import 'package:social_app/src/elements/others/divider_line_widget.dart';
import 'package:social_app/src/models/post_options.dart';
import 'package:social_app/src/models/video.dart';

class VideoPageCurrentVideoDescription extends StatelessWidget {

  VideoPageController con;
  Video videoPost;
  List<PostOptions> options;
  VoidCallback likeTap;
  VoidCallback commentTap;
  VoidCallback shareTap;
  Function onOptionsTap;
  VoidCallback onProfileTap;

  VideoPageCurrentVideoDescription({this.con, this.options,
    this.videoPost, this.likeTap, this.commentTap,
    this.shareTap, this.onOptionsTap, this.onProfileTap});

  @override
  Widget build(BuildContext context) {
    //2020-10-19T19:26:10.000000Z
    var format = DateFormat('yyyy-MM-ddTHH:mm:ss.SSSSSSZ');
    var dateTime = format.parse(videoPost.post.createdAt);
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);

    return Container(
      margin: EdgeInsets.only(bottom: 10),
      padding: EdgeInsets.only(top: 15),
      decoration: BoxDecoration(
          // borderRadius: BorderRadius.only(bottomRight: Radius.circular(5), bottomLeft: Radius.circular(5) ),
          color: Theme.of(context).primaryColor,
          boxShadow: [
            BoxShadow(
                offset: Offset(1, 1),
                spreadRadius: 0.5,
                blurRadius: 5,
                color: Theme.of(context).focusColor.withOpacity(0.5))
          ]),
      child: Column(
        children: [

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: [
                InkWell(
                  onTap: this.onProfileTap,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: videoPost.post.user.image != null ? FadeInImage.assetNetwork(
                      placeholder: "assets/img/placeholders/profile.png",
                      image: videoPost.post.user.image,
                      height: 50,
                      width: 50,
                      fit: BoxFit.cover,
                    ) : Image.asset("assets/img/placeholders/profile.png", height: 50, width: 50, fit: BoxFit.cover, ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        InkWell(
                          onTap: this.onProfileTap,
                          child: Text(
                            videoPost.post.user.name,
                            style: Theme.of(context).textTheme.subtitle2,
                          ),
                        ),
                        Text(
                          '$day at $time',
                          style: Theme.of(context).textTheme.bodyText2
                              .merge(TextStyle(color: Theme.of(context).focusColor)),
                        )
                      ],
                    ),
                  ),
                ),
                PopupMenuButton(
                    onSelected: (value) {
                      this.onOptionsTap(value, videoPost.post);
                    },
                    icon: RotatedBox(
                      quarterTurns: 1,
                      child: ImageIcon(
                        AssetImage("assets/icons/more.png"),
                        color: Colors.white,
                      ),
                    ),
                    itemBuilder: (context) =>
                        List.generate(this.options.length, (index) {
                          return PopupMenuItem(
                              value: this.options[index].option,
                              child: Row(
                                children: [
                                  ImageIcon(AssetImage(this.options[index].icon)),
                                  SizedBox(width: 10,),
                                  Text(
                                    this.options[index].option,
                                    style: Theme.of(context).textTheme.bodyText2,
                                  ),
                                ],
                              ));
                        })),
              ],
            ),
          ),

          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  child: Text(
                    videoPost.post.text ?? "",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: Theme.of(context).textTheme.subtitle2
                        .merge(TextStyle()),
                  ),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              videoPost.post.totalLikes != null && videoPost.post.totalLikes > 0
                  ? Padding(
                padding: const EdgeInsets.only(left: 10, top: 10),
                child: Text("${videoPost.post.totalLikes} Likes", style: Theme.of(context).textTheme.bodyText2,),
              ) : SizedBox(width: 0, height: 0,),
              videoPost.post.totalComment != null && videoPost.post.totalComment > 0
                  ? Padding(
                padding: const EdgeInsets.only(right: 10, top: 5),
                child: Text("${videoPost.post.totalComment} Comments", style: Theme.of(context).textTheme.bodyText2),
              ) : SizedBox(width: 0, height: 0,)
            ],
          ),
          DividerLineWidget(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              FlatButton(
                onPressed: this.likeTap,
                child: Row(
                  children: [
                    ImageIcon(AssetImage("assets/emoticons/default.png"), size: 40, color: !videoPost.post.isLiked ? Theme.of(context).primaryColorDark : Theme.of(context).accentColor),
                    Text("Like", style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(color: !videoPost.post.isLiked ? Theme.of(context).primaryColorDark : Theme.of(context).accentColor)),)
                  ],
                ),
              ),
              FlatButton(
                onPressed: this.commentTap,
                child: Row(
                  children: [
                    ImageIcon(AssetImage("assets/icons/comment.png"), ),
                    Text("Comment",  style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(color: Theme.of(context).primaryColorDark)),)
                  ],
                ),
              ),
              FlatButton(
                onPressed: this.shareTap,
                child: Row(
                  children: [
                    ImageIcon(AssetImage("assets/icons/share.png"), ),
                    Text("Share", style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(color: Theme.of(context).primaryColorDark)),)
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

