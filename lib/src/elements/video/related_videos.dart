
import 'package:flutter/material.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/video_page_controller.dart';
import 'package:social_app/src/elements/others/divider_line_widget.dart';
import 'package:social_app/src/elements/video/related_videos_single_item_widget.dart';

class RelatedVideos extends StatelessWidget {
  VideoPageController con;

  RelatedVideos({this.con});

  @override
  Widget build(BuildContext context) {
    double containerWidth = 165;
    return con.relatedVideos.isEmpty
        ? SizedBox(height: 0,)
        : Column(
      children: [
        DividerLineWidget(text: S.of(context).related_videos,),
        Container(
          height: (containerWidth * 00.5625) + 20,
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              physics: BouncingScrollPhysics(),
              itemCount: con.relatedVideos.length,
              itemBuilder: (context, index)
              {
                return RelatedVideosSingleItemWidget(
                  containerWidth: containerWidth,
                  videoPost: con.relatedVideos[index],
                  onTap: (){
                    con.playThisVideo(con.relatedVideos[index]);
                  },
                );
              }
          ),
        ),
      ],
    );
  }
}
