import 'package:flutter/material.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/video_page_controller.dart';
import 'package:social_app/src/elements/others/divider_line_widget.dart';
import 'package:social_app/src/elements/video/suggested_video_widget_item.dart';

class SuggestedVideos extends StatelessWidget {
  VideoPageController con;

  SuggestedVideos({this.con});

  @override
  Widget build(BuildContext context) {
    return con.suggestedVideos.isEmpty
        ? SizedBox(
            height: 0,
          )
        : Column(
            children: [
              DividerLineWidget(
                text: S.of(context).suggested,
              ),
              ListView.builder(
                  shrinkWrap: true,
                  primary: false,
                  itemCount: con.suggestedVideos.length,
                  itemBuilder: (ctx, index) {
                    return SuggestedVideoWidgetItem(
                      videoPost: con.suggestedVideos[index],
                      onPlay: (){
                          con.playThisVideo(con.suggestedVideos[index]);
                      },
                      onCaption: (){},
                      onMore: (){},
                      onUserProfile: (){},
                      onOptionsTap: con.onPostOptions,
                      options: con.options,
                    );
                  })
            ],
          );
  }
}
