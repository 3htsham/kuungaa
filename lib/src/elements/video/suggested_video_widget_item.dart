import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/post_options.dart';
import 'package:social_app/src/models/video.dart';

class SuggestedVideoWidgetItem extends StatelessWidget {
  Video videoPost;
  VoidCallback onPlay;
  VoidCallback onMore;
  VoidCallback onUserProfile;
  VoidCallback onCaption;
  List<PostOptions> options;
  Function onOptionsTap;

  SuggestedVideoWidgetItem({ this.videoPost, this.onOptionsTap, this.options, this.onPlay, this.onMore, this.onUserProfile, this.onCaption});

  @override
  Widget build(BuildContext context) {
    double containerWidth = MediaQuery.of(context).size.width;

    var format = DateFormat('yyyy-MM-ddTHH:mm:ss.SSSSSSZ');
    var dateTime = format.parse(videoPost.post.createdAt);
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);

    return Container(
      width: containerWidth,
      height: containerWidth * 00.5625,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(5.0), boxShadow: [
          BoxShadow(
              offset: Offset(1, 1),
              spreadRadius: 0.5,
              blurRadius: 5,
              color: Theme.of(context).focusColor.withOpacity(0.5))
        ]),
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: FadeInImage.assetNetwork(
                placeholder: "assets/img/placeholders/videopholder.jpg",
                image: videoPost.thumbnail,
                fadeInDuration: Duration(milliseconds: 500),
                fadeOutDuration: Duration(milliseconds: 200),
                fit: BoxFit.cover,
                width: containerWidth,
              ),
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(5),
                    bottomLeft: Radius.circular(5)),
                color: Theme.of(context).primaryColor.withOpacity(0.5),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Flexible(
                          child: Text(
                            videoPost.post.text ?? "",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: Theme.of(context)
                                .textTheme
                                .subtitle2
                                .merge(TextStyle()),
                          ),
                        ),
                        PopupMenuButton(
                            onSelected: (value) {
                              this.onOptionsTap(value, videoPost.post);
                            },
                            icon: ImageIcon(
                              AssetImage("assets/icons/more.png"),
                              size: 22,
                              color: Colors.white,
                            ),
                            itemBuilder: (context) =>
                                List.generate(this.options.length, (index) {
                                  return PopupMenuItem(
                                      value: this.options[index].option,
                                      child: Row(
                                        children: [
                                          ImageIcon(AssetImage(this.options[index].icon)),
                                          SizedBox(width: 10,),
                                          Text(
                                            this.options[index].option,
                                            style: Theme.of(context).textTheme.bodyText2,
                                          ),
                                        ],
                                      ));
                                })),
                        // Container(
                        //   height: 30,
                        //   width: 40,
                        //   child: IconButton(
                        //     onPressed: this.onMore,
                        //     icon: ImageIcon(
                        //       AssetImage("assets/icons/more.png"),
                        //       color: Colors.white,
                        //     ),
                        //   ),
                        // )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Align(
                      alignment: Alignment.bottomLeft,
                      child: InkWell(
                        onTap: this.onUserProfile,
                        child: Row(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: FadeInImage.assetNetwork(
                                placeholder:
                                    "assets/img/placeholders/profile.png",
                                image: videoPost.post.user.image,
                                height: 40,
                                width: 40,
                                fit: BoxFit.cover,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  videoPost.post.user.name,
                                  style: Theme.of(context).textTheme.subtitle2,
                                ),
                                Text(
                                  '$day at $time',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText2
                                      .merge(TextStyle(color: Colors.white)),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: IconButton(
                onPressed: (){onPlay();},
                icon: Icon(
                  Icons.play_arrow,
                  size: 50,
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
