import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:social_app/src/controllers/video_page_controller.dart';
import 'package:social_app/src/elements/video/single_video_view.dart';
import 'package:social_app/src/models/video.dart';

class VideoPageCurrentVideoWidget extends StatelessWidget {

  VideoPageController con;
  Video videoPost;

  VideoPageCurrentVideoWidget({this.con, this.videoPost});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: MediaQuery.of(context).size.width * 00.5625,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5.0),
              boxShadow: [
                BoxShadow(
                    offset: Offset(1, 1),
                    spreadRadius: 0.5,
                    blurRadius: 5,
                    color: Theme.of(context).focusColor.withOpacity(0.5)
                )
              ]
          ),
          child: ClipRRect(
            child: SingleVideoView(
              videoPost.file.toString().contains(
                  RegExp(
                      r"(.mkv|.mp4|.webm|.ogv|.m3u|.m3u8)",
                      caseSensitive: false))
                  ? videoPost.file
                  : videoPost.file + ".mp4",
              key: con.currentVideoViewKey,
            ),
          ),
        ),
      ],
    );
  }
}