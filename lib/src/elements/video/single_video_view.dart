import 'package:flutter/material.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:video_player/video_player.dart';
import 'package:screen/screen.dart' as wake;
import 'package:social_app/src/helpers/screen.dart';
import 'package:social_app/config/app_config.dart' as config;

class SingleVideoView extends StatefulWidget {
  String link;
  bool progress;

  SingleVideoView(this.link, {Key key, this.progress = true}) : super(key: key);

  @override
  SingleVideoViewState createState() => SingleVideoViewState();
}

class SingleVideoViewState extends State<SingleVideoView>
    with SingleTickerProviderStateMixin {

  VideoPlayerController _videoPlayerController;
  Future<void> _videoPlayerFuture;
  AnimationController _animationController;
  bool _controls = true;

  String currentDuration = "00:00";

  @override
  void initState() {
    super.initState();
    // Screen.hideSystemBars();
    // Screen.setLandscape();
    wake.Screen.keepOn(true);
    _animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    initializeVideoController(widget.link, false);
  }

  void initializeVideoController(var link, bool isForward) {
    if (link.toString().contains(RegExp(r"(.mkv|.mp4|.webm|.ogv|.m3u|.m3u8)", caseSensitive: false)) &&
        !link.toString().contains(RegExp(r"(<iframe|<div|href=|src=|width=|height=|<frame)", caseSensitive: false)))
    {

      _videoPlayerController = VideoPlayerController.network(link);
      _videoPlayerFuture = _videoPlayerController.initialize().then((_) => {
        _videoPlayerController
        // ..play()
          ..setVolume(1.0)
          ..seekTo(Duration(milliseconds: 500))
          ..addListener(() {
            setState(() {
              currentDuration = _videoPlayerController.value.position.inMinutes > 59 ?
              '${_videoPlayerController.value.position.inHours.toString().padLeft(2, '0')}:'
                  '${(_videoPlayerController.value.position.inMinutes/60).toString().padLeft(2, '0')}'
                  ':${(_videoPlayerController.value.position.inSeconds % 60).toString().padLeft(2, '0')}'
                  : '${_videoPlayerController.value.position.inMinutes.toString().padLeft(2, '0')}:'
                  '${(_videoPlayerController.value.position.inSeconds % 60).toString().padLeft(2, '0')}';
            });
          }),
        if(isForward) {
          _animationController.forward()
        } else
          {
            _animationController.reverse()
          }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // if (widget.link.toString().contains(RegExp(r"(.mkv|.mp4|.webm|.ogv|.m3u|.m3u8)", caseSensitive: false)) &&
    //     !widget.link.toString().contains(RegExp(
    //         r"(<iframe|<div|href=|src=|width=|height=|<frame)",
    //         caseSensitive: false)))
    // {
      return FutureBuilder(
        future: _videoPlayerFuture,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (_videoPlayerController != null && _videoPlayerController.value.initialized && snapshot.connectionState == ConnectionState.done) {
            return Scaffold(
              body: Stack(fit: StackFit.passthrough, children: <Widget>[
                InkWell(
                  child: VideoPlayer(_videoPlayerController),
                  onTap: () {
                    setState(() {
                      _controls = !_controls;
                    });
                  },
                ),
                if (_controls)
                  Align(
                    alignment: Alignment.topRight,
                    child: Container(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          children: [
                            IconButton(
                              icon: (_videoPlayerController.value.volume > 0.0)
                                  ? Icon(Icons.volume_up)
                                  : Icon(Icons.volume_off),
                              onPressed: () => {
                                if (_videoPlayerController.value.volume > 0.0)
                                  {
                                    setState(() {
                                      _videoPlayerController.setVolume(0.0);
                                    })
                                  }
                                else
                                  {
                                    setState(() {
                                      _videoPlayerController.setVolume(1.0);
                                    })
                                  }
                              },
                            ),
                            IconButton(
                              icon: Icon(Icons.fullscreen),
                              onPressed: () {
                                _videoPlayerController.pause();
                                ///Switch to Full Screen Video View
                                var durationInMilliSeconds = _videoPlayerController.value.position.inMilliseconds;
                                if(durationInMilliSeconds > 1000)
                                  {
                                    durationInMilliSeconds-=1000;
                                  }
                                ///Start The FullScreenVideoView Now ith this progress (durationInMilliSeconds) and link (widget.link)
                                Navigator.of(context).pushNamed("/FullScreenVideoView",
                                    arguments: RouteArgument(linkToVideo: widget.link,
                                        durationInMilliSeconds: durationInMilliSeconds,
                                        videoController: _videoPlayerController,
                                        videoPlayerFuture: _videoPlayerFuture,
                                        progress: true));

                              },
                            ),
                          ],
                        )
                    ),
                  ),

                if (widget.progress && _controls)
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 5.0, bottom: 5.0),
                          child: Text(
                            currentDuration,
                            style: Theme.of(context).textTheme.bodyText1
                                .merge(TextStyle(color: Colors.black,
                              shadows: config.Colors().textShadow
                            )),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 5.0, bottom: 5.0),
                          child: Text(
                              _videoPlayerController.value.duration.inMinutes > 59 ?
                                  '${_videoPlayerController.value.duration.inHours.toString().padLeft(2, '0')}:'
                                      '${(_videoPlayerController.value.duration.inMinutes/60).toString().padLeft(2, '0')}'
                                      ':${(_videoPlayerController.value.duration.inSeconds % 60).toString().padLeft(2, '0')}'
                                  : '${_videoPlayerController.value.duration.inMinutes.toString().padLeft(2, '0')}'
                                  ':${(_videoPlayerController.value.duration.inSeconds % 60).toString().padLeft(2, '0')}',
                            style: Theme.of(context).textTheme.bodyText1
                                .merge(TextStyle(color: Colors.black,
                              shadows: config.Colors().textShadow
                            )),
                          ),
                        )
                      ],
                    ),
                  ),

                if (widget.progress && _controls)
                  Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        padding: EdgeInsets.only( right: 60, left: 60, bottom: 10),
                        child: VideoProgressIndicator(
                          _videoPlayerController,
                          padding: EdgeInsets.only(top: 20.0),
                          allowScrubbing: true,
                          colors: VideoProgressColors(
                              bufferedColor: Theme.of(context).accentColor.withOpacity(0.5),
                              backgroundColor: Colors.black.withOpacity(0.5),
                              playedColor: Theme.of(context).accentColor
                          ),
                        ),
                      )),

              ]),
              floatingActionButton: (_controls)
                  ? Padding(
                    padding: const EdgeInsets.only(bottom: 20.0),
                    child: FloatingActionButton(
                onPressed: () {
                    setState(() {
                      if (_videoPlayerController.value.isPlaying) {
                        _videoPlayerController.pause();
                        _animationController.reverse();
                      }
                      else {
                        _videoPlayerController.play();
                        _animationController.forward();
                      }
                    });
                },
                child: AnimatedIcon(
                    icon: AnimatedIcons.play_pause,
                    progress: _animationController,
                ),
              ),
                  )
                  : null,
            );
          } else {
            return Container(
              color: Colors.black,
              height: Screen.heigth(context),
              width: double.infinity,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        },
      );
    // }
    // else
    // {
    //   return WebviewScaffold(
    //     url: widget.link,
    //     withJavascript: true,
    //     withLocalStorage: true,
    //     appCacheEnabled: true,
    //     supportMultipleWindows: true,
    //     scrollBar: false,
    //     hidden: true,
    //   );
    // }
  }

  @override
  void dispose() {
    Screen.resetOrientation();
    Screen.showSystemBars();
    wake.Screen.keepOn(false);
    _videoPlayerController?.dispose();
    _animationController?.dispose();
    super.dispose();
  }

  void updateLink(var _link) {
    try {
      _videoPlayerController.pause();
    } catch (e) {
      print(e);
    }
    initializeVideoController(_link, true);
    _videoPlayerController.play();
  }

  void pauseVideo(){
    _videoPlayerController.pause();
    _animationController.reverse();
  }

}

