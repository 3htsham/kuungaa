import 'package:flutter/material.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/models/user.dart';

class CreateGroupPostWidget extends StatelessWidget {
  VoidCallback onPostTap;

  CreateGroupPostWidget({this.onPostTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            S.of(context).post_something,
            style: Theme.of(context).textTheme.subtitle2,
          ),
          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: this.onPostTap,
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Theme.of(context).accentColor),
                        borderRadius: BorderRadius.circular(15)
                    ),
                    child: Text(
                      S.of(context).tell_group_members_about_your_day,
                      style: Theme.of(context).textTheme.bodyText2.merge(
                          TextStyle(color: Theme.of(context).focusColor)
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(width: 15,),
              InkWell(
                onTap: this.onPostTap,
                child: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                      color: Theme.of(context).accentColor,
                      borderRadius: BorderRadius.circular(100)
                  ),
                  child: ImageIcon(AssetImage("assets/icons/write.png"), color: Colors.white,),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
