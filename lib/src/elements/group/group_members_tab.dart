import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/controllers/group_controller.dart';
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/elements/others/divider_line_widget.dart';
import 'package:social_app/src/elements/user/group_member_icon.dart';
import 'package:social_app/src/models/group.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/models/user.dart';

class GroupMembersTabWidget extends StatefulWidget {
  Group group;
  User user;

  GroupMembersTabWidget({this.group, this.user});

  @override
  _GroupMembersTabWidgetState createState() => _GroupMembersTabWidgetState();
}

class _GroupMembersTabWidgetState extends StateMVC<GroupMembersTabWidget>
    with AutomaticKeepAliveClientMixin    {

  GroupController _con;

  _GroupMembersTabWidgetState():  super(GroupController()) {
    _con = controller;
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    _con.group = widget.group;
    _con.currentUser = widget.user;
    if(_con.group.join == "true" || _con.group.join == true) {
      _con.getGroupMembersNow();
    }
    super.initState();
    _con.getGroupAdminNow();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [

          _con.admin == null ? SizedBox(height: 0,) : DividerLineWidget(text: "Admin",),

          _con.admin ==null ? SizedBox(
                  height: 0,
                )
              : GroupMemberIcon(
                  user: _con.admin,
                  options: _con.notAdminOptions,
                  onOptionsTap: (value) {
                    if(value == _con.adminOptions[0]) {
                      Navigator.of(context).pushNamed("/UserProfileDetails", arguments: RouteArgument(user: _con.admin));
                    }
                  },
                  onTap: () {
                    Navigator.of(context).pushNamed("/UserProfileDetails",
                        arguments: RouteArgument(user: _con.admin));
                  },
                  rightMargin: 5,
                  leftMargin: 5,
                ),


          _con.groupMembers.isEmpty ? SizedBox(height: 0,) : DividerLineWidget(text: "All Members",),

          _con.groupMembers.isEmpty
              ? CircularLoadingWidget(height: 70,)
              : GridView.count(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            primary: false,
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
            crossAxisCount: MediaQuery.of(context).orientation == Orientation.portrait ? 3 : 5,
            children: List.generate(_con.groupMembers.length, (index) {
              return GroupMemberIcon(
                user: _con.groupMembers[index],
                options: _con.group.adminId.toString() == _con.currentUser.id.toString()
                          ? _con.currentUser.id != _con.groupMembers[index].id
                              ? _con.adminOptions
                              : _con.notAdminOptions
                          : _con.notAdminOptions,
                      onOptionsTap: (value){
                        if(value == _con.adminOptions[0]) {
                          Navigator.of(context).pushNamed("/UserProfileDetails", arguments: RouteArgument(user: _con.groupMembers[index]));
                        } else {
                          _con.onGroupMembersOptions(value, _con.groupMembers[index]);
                        }
                      },
                      onTap: (){
                  Navigator.of(context).pushNamed("/UserProfileDetails", arguments: RouteArgument(user: _con.groupMembers[index]));
                },
                rightMargin: 5,
                leftMargin: 5,
              );
            }),
          ),
        ],
      ),
    );
  }
}
