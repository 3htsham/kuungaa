import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/group_controller.dart';
import 'package:social_app/src/elements/bottomsheet/new_post_group.dart';
import 'package:social_app/src/elements/bottomsheet/post_comment_newsfeed.dart';
import 'package:social_app/src/elements/group/create_group_post_widget.dart';
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/elements/others/divider_line_widget.dart';
import 'package:social_app/src/elements/post/user_post_widget_item.dart';
import 'package:social_app/src/models/group.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:inview_notifier_list/inview_notifier_list.dart';

class GroupPostsTabWidget extends StatefulWidget {

  Group group;
  User currentUser;

  GroupPostsTabWidget(this.currentUser, {this.group});

  @override
  _GroupPostsTabWidgetState createState() => _GroupPostsTabWidgetState();
}

class _GroupPostsTabWidgetState extends StateMVC<GroupPostsTabWidget>
    with AutomaticKeepAliveClientMixin   {

  GroupController _con;

  _GroupPostsTabWidgetState() : super(GroupController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.group = widget.group;
    _con.currentUser = widget.currentUser;
    super.initState();
    if(_con.group.join == "true" || _con.group.join == true) {
      _con.getGroupPosts();
    }
  }


  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return RefreshIndicator(
      onRefresh: () async {},
      child: Container(
        child: InViewNotifierList(
            isInViewPortCondition:
                (double deltaTop, double deltaBottom, double vpHeight) {
              return (deltaTop < (0.5 * vpHeight) + 100.0 &&
                  deltaBottom > (0.5 * vpHeight) - 100.0);
            },
            itemCount: 1,
            shrinkWrap: true,
            primary: false,
            builder: (BuildContext context, int index) {
              return SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 7, horizontal: 15),
                      child: CreateGroupPostWidget(
                        onPostTap: () {
                          postNewThingAtGroupBottomSheet(context);
                        },
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    _con.posts.isEmpty
                        ? SizedBox(
                            width: 0,
                            height: 0,
                          )
                        : DividerLineWidget(
                            text: S.of(context).posts,
                          ),
                    _postsWidget(),
                  ],
                ),
              );
            }),
      ),
    );
  }

  Widget _postsWidget(){
    return _con.posts.isEmpty
        ? CircularLoadingWidget(height: 100,)
        : ListView.builder(
      primary: false,
      shrinkWrap: true,
      itemCount: _con.posts.length,
      itemBuilder: (context, index) {
         return InViewNotifierWidget(
          id: _con.posts[index].id.toString(),
            builder: (BuildContext context, bool isInView, Widget child){
              return UserPostWidgetItem(post: _con.posts[index],
                options: _con.options,
                isInView: isInView,
                showShare: false,
                likeTap: (){
                  _con.postLike(_con.posts[index]);
                },
                commentTap: (){
                  this.postCommentsBottomsheet(context, _con.posts[index]);
                },
                shareTap: (){},
                onProfileTap: (){
                  Navigator.of(context).pushNamed("/UserProfileDetails", arguments: RouteArgument(user: _con.posts[index].user));
                },
                onOptionsTap: _con.onPostOptions,
              );
            }
        );
      },
    );
  }

  postNewThingAtGroupBottomSheet(BuildContext context) {
    var theme = Theme.of(context);
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: theme.scaffoldBackgroundColor,
        context: context,
        builder: (ctx) {
          return GestureDetector(
            onVerticalDragStart: (_){},
            child: StatefulBuilder(
              builder: (ctx, setState) {
                return GroupPostNewThingBottomSheetStfulWidget(con: _con);
              },
            ),
          );
        }
    ).then((controller) {
      if(controller != null) {
        //We can post here
        var myController  = controller as GroupController;
        if(myController.post != null &&
            (myController.post.text != null || (myController.post.mediaList != null && myController.post.mediaList.length > 0)) ) {
          _con.uploadPost(myController.post);
        }
      }
    });
  }


  postCommentsBottomsheet(BuildContext context, Post post) {
    var theme = Theme.of(context);
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: theme.scaffoldBackgroundColor,
        context: context,
        builder: (ctx) {
          return GestureDetector(
            onVerticalDragStart: (_){},
            child: StatefulBuilder(
              builder: (ctx, setState) {
                return PostCommentsBottomSheet(_con.currentUser, post: post,);
              },
            ),
          );
        }
    ).then((value){
      if(value != null) {
        var post = value as Post;
        _con.posts.forEach((element) {
          if(element.id == post.id) {
            if(post.comments != null && post.comments.length > 0) {
              setState((){
                element.totalComment = post.comments.length;
                element.comments = post.comments;
              });
            }
          }
        });
      }
    });
  }

}
