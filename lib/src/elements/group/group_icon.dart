import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:social_app/src/models/group.dart';

class GroupIcon extends StatelessWidget {

  double leftMargin;
  Group group;
  double rightMargin;
  VoidCallback onTap;

  GroupIcon({this.group, this.onTap, this.leftMargin, this.rightMargin});

  @override
  Widget build(BuildContext context) {
    var format = DateFormat('yyyy-MM-ddTHH:mm:ss.SSSSSSZ');
    var dateTime = format.parse(group.updatedAt);
    var month = DateFormat("MMMM").format(dateTime) ?? "";
    var day = DateFormat("dd").format(dateTime);
    var year = DateFormat("yyyy").format(dateTime);

    return InkWell(
      onTap: this.onTap,
      child: Container(
        margin: EdgeInsets.only(left: leftMargin, right: rightMargin, top: 10, bottom: 10),
        width: 100,
        height: 100,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 10,),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(1, 1),
                        spreadRadius: 0.5,
                        blurRadius: 5,
                        color: Theme.of(context).focusColor.withOpacity(0.5))
                  ]
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: CachedNetworkImage(
                  imageUrl: group.image,
                  placeholder: (context, url) => Image.asset("assets/img/placeholders/iamge.jpg", fit: BoxFit.cover,),
                  width: 70,
                  height: 70,
                  fit: BoxFit.cover,
                )
                // FadeInImage.assetNetwork(
                //   placeholder: "assets/img/placeholders/profile.png",
                //   image: group.image,
                //   height: 70,
                //   width: 70,
                //   fit: BoxFit.cover,
                // ),
              ),
            ),
            SizedBox(height: 6,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Text(
                group.name,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context).textTheme.subtitle2
                    .merge(TextStyle(fontSize: 13)),
              ),
            ),
            Text(
              '$month $day, $year',
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.bodyText2
                  .merge(TextStyle(fontSize: 10)),
            ),
          ],
        ),
      ),
    );
  }
}