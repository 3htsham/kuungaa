import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:social_app/src/models/group.dart';

class GroupGridItem extends StatelessWidget {

  Group group;

  VoidCallback onTap;
  VoidCallback onJoin;

  GroupGridItem({this.group, this.onTap, this.onJoin});

  @override
  Widget build(BuildContext context) {
    bool isLandscape = MediaQuery.of(context).orientation == Orientation.landscape;


    return InkWell(
      onTap: this.onTap,
      child: Container(
        // width: MediaQuery.of(context).size.width/2.5,
        margin: EdgeInsets.only(left: 5, right: 5, top: 10, bottom: 20),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.all(Radius.circular(5)),
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).focusColor.withOpacity(0.4),
              spreadRadius: 0.2,
              blurRadius: 5,
              offset: Offset(1, 1),
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            // Image of the card
            Container(
              height: isLandscape ? 80 : 95,
              decoration: BoxDecoration(
                image: (group.cover == null)
                    ? DecorationImage(
                  image: AssetImage("assets/img/placeholders/image.jpg"),
                  fit: BoxFit.cover,
                )
                    : DecorationImage(
                  image: NetworkImage(group.cover),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5), topRight: Radius.circular(5)),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 7, vertical: 7),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          group.name ?? "",
                          maxLines: 1,
                          overflow: TextOverflow.fade,
                          softWrap: false,
                          style: Theme
                              .of(context)
                              .textTheme
                              .bodyText1,
                        ),
                        Text(
                          group.description ?? "",
                          maxLines: 1,
                          overflow: TextOverflow.fade,
                          softWrap: false,
                          style: Theme
                              .of(context)
                              .textTheme
                              .caption.merge(TextStyle(fontSize: 10)),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(width: 15),
                  Expanded(
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 25,
                          width: 25,
                          child: FlatButton(
                            padding: EdgeInsets.all(0),
                            onPressed: () {
                              if (group.join == null ||
                                  (group.join == "false" ||
                                      group.join == false)) {
                                this.onJoin();
                              }
                            },
                            child: ImageIcon(
                              group.join != null && (group.join == "true" || group.join==true)
                                  ? AssetImage("assets/icons/check.png")
                                  : AssetImage("assets/icons/plus.png"),
                                color: Theme.of(context).primaryColor
                            ),
                            color: Theme
                                .of(context)
                                .accentColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}