import 'package:flutter/material.dart';

class NotificationImage extends StatelessWidget {
  String image;

  NotificationImage({this.image});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(100),
      child: FadeInImage.assetNetwork(
        placeholder: "assets/img/placeholders/profile.png",
        image: image,
        height: 40,
        width: 40,
        fit: BoxFit.cover,
      ),
    );
  }
}
