import 'dart:math';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app/config/firestore_config.dart';
import 'package:social_app/config/golbal_strings.dart';
import 'package:social_app/src/models/comment.dart';
import 'package:social_app/src/models/media.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/post_options.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/models/status.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/post_repository.dart';
import 'package:social_app/src/repositories/settings_repository.dart';
import 'package:social_app/src/repositories/user_repository.dart';
import 'package:social_app/src/repositories/boost_repository.dart' as boostRepo;


class HomeController extends ControllerMVC {

  User currentUser;
  List<Post> posts = <Post>[];
  List<Post> sponsoredPosts = <Post>[];
  List<User> peopleYouMayKnow = <User>[];
  TextEditingController textController;

  Post post = Post();
  List<Asset> images = List<Asset>();
  String _error;
  bool isLoading = false;
  bool loadingComment = false;

  bool isSharePostDialog = false;
  Post postToShare;

  PostOptions currentPrivacyOption;
  List<PostOptions> newPostPrivacyOptions = <PostOptions>[
    PostOptions(option: "Public", icon: "assets/icons/public.png"),
    PostOptions(option: "Friends", icon: "assets/icons/profile.png"),
    PostOptions(option: "Private", icon: "assets/icons/lock.png"),
  ];
  List<PostOptions> sharePostPrivacyOptions = <PostOptions>[
    PostOptions(option: "Public", icon: "assets/icons/public.png"),
    PostOptions(option: "Friends", icon: "assets/icons/profile.png"),
    PostOptions(option: "Private", icon: "assets/icons/lock.png"),
  ];

  List<PostOptions> options = <PostOptions>[
    PostOptions(option: "Save Post", icon: "assets/icons/save.png")
  ];
  List<PostOptions> myPostOptions = <PostOptions>[
    PostOptions(option: "Save Post", icon: "assets/icons/save.png"),
    PostOptions(option: "Edit", icon: "assets/icons/write.png"),
    PostOptions(option: "Feature Post", icon: "assets/icons/star.png"),
    PostOptions(option: "Delete", icon: "assets/icons/bin.png")
  ];
  List<PostOptions> mySharedPostOptions = <PostOptions>[
    PostOptions(option: "Save Post", icon: "assets/icons/save.png"),
    PostOptions(option: "Feature Post", icon: "assets/icons/star.png")
  ];
  List<PostOptions> sponsoredPostOptions = <PostOptions>[
    PostOptions(option: "Save Post", icon: "assets/icons/save.png"),
    PostOptions(option: "Edit", icon: "assets/icons/write.png"),
    PostOptions(option: "Delete", icon: "assets/icons/bin.png")
  ];
  List<PostOptions> myCommentOptions = <PostOptions>[
    PostOptions(option: "View Profile", icon: "assets/icons/profile.png"),
    PostOptions(option: "Edit", icon: "assets/icons/write.png"),
    PostOptions(option: "Delete Comment", icon: "assets/icons/bin.png"),
  ];
  List<PostOptions> commentOptions = <PostOptions>[
    PostOptions(option: "View Profile", icon: "assets/icons/profile.png"),
  ];

  List<Status> statuses = <Status>[];
  List<Status> myStatuses = <Status>[];

  bool isEditingComment = false;
  Comment commentEditing;

  GlobalKey<ScaffoldState> scaffoldKey;

  HomeController() {
    getUser();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    textController = TextEditingController();
    currentPrivacyOption = newPostPrivacyOptions[0];
  }

  getUser() async {
    User user = await getCurrentUser();
    setState((){
      this.currentUser = user;
    });
  }

  getSuggestedUsers() async {
    peopleYouMayKnow.clear();
    Stream<User> stream = await getSuggestedUsersList();
    stream.listen((_user) {
      setState((){
        peopleYouMayKnow.add(_user);
      });
    },
      onError: (e){
        print("Error in getSuggestedUsers home: " + e);
      },
      onDone: (){}
    );
  }

  void getSponsoredPosts() async {
    final Stream<Post> stream = await boostRepo.getBoostedFeed();
    setState((){
      sponsoredPosts.clear();
    });
    stream.listen((Post _post) {
      setState(() {
        sponsoredPosts.add(_post);
      });
    }, onError: (a) {
      print(a);
    },
        onDone: () {});
  }

  void getMySponsoredPosts() async {
    final Stream<Post> stream = await boostRepo.getMyBoostedFeed();
    setState((){
      sponsoredPosts.clear();
    });
    stream.listen((Post _post) {
      setState(() {
        sponsoredPosts.add(_post);
      });
    }, onError: (a) {
      print(a);
    },
        onDone: () {});
  }

  void countView(Post _post) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(!prefs.containsKey(_post.id.toString())) {
      boostRepo.postBoostView(_post);
    } else {
      //Already Counted
    }
  }

  void getPosts() async {
    final Stream<Post> stream = await getNewsFeed();
    setState((){
      posts.clear();
    });
    stream.listen((Post _post) {
      setState(() {
        posts.add(_post);
      });
    }, onError: (a) {
      print(a);
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Error retrieving posts'),));
    },
        onDone: () {});
  }

  void getStatuses() async {
    if(await isInternetConnection()) {
      MyFirestore.getStatuses().then((_querySnap) {
        _querySnap.docs.forEach((_statusItem) {
          Map<String, dynamic> data = _statusItem.data();
          Status status = new Status();
          status.id = data["id"];
          status.hasMedia = data["hasMedia"];
          status.imageLink = data["imageLink"];
          status.isImage = data["isImage"];
          status.ownerId = data["ownerId"];
          status.imageName = data['imageName'];
          status.seenBy = List.from(data["seenby"]);
          status.text = data["text"];
          status.uploadedAt = data["uploadedAt"];
          status.user = User.fromJSON(data["user"]);
          status.videoLink = data["videoLink"];
          if(status.ownerId != currentUser.id.toString()) {
            //Add to Statuses
            setState((){
              statuses.add(status);
            });
          } else {
            //Add to MyStatuses
            setState((){
              // statuses.add(status);
              myStatuses.add(status);
            });
          }
        });
      },
          onError: (e){});
      MyFirestore.deleteStatuses();
    }
  }

  void getPostCommentsNow() async {
    if(post != null) {
      Stream<Comment> stream = await getPostComments(this.post);
      setState((){
        post.comments.clear();
      });
      stream.listen((Comment _comment) {
        setState(() {
          post.comments.add(_comment);
        });
      }, onError: (a) {
        print(a);
        scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Error retrieving comments'),));
      },
          onDone: () {});
    }
  }

  void postLike(Post post) async {
    Comment data = Comment(postId: post.id);
    likePost(data).then((value) {
      if (value != null) {
        for(int i=0; i<posts.length; i++) {
          if(posts[i].id == value.id) {
            setState(() {
              posts[i] = value;
            });
          }
        }
      }
    });
  }

  void closeShareDialog(){
    setState((){
      isSharePostDialog = false;
    });
  }

  void showShareDialog(Post _post) {
    setState((){
      postToShare = _post;
      isSharePostDialog = true;
    });
  }

  void hideShareDialog(String privacy) {
    setState((){
      postToShare.postType = privacy.toLowerCase();
      isSharePostDialog = false;
    });
    sharePostNow(postToShare);
  }

  void sharePostNow(Post post) async {
    if(await isInternetConnection()) {
      setState((){isLoading = true;});
      sharePost(post).then((value) {
        if (value != null) {
          showToast("Post Shared");
        } else {
          showToast("Unable to share Post");
        }
        setState((){isLoading = false;});
      }, onError: (e) {
        print(e);
        setState((){isLoading = false;});
      });
    }
  }

  void enableCommentEditing(Comment _cmnt) async {
    setState((){
      isEditingComment = true;
      commentEditing = _cmnt;
      textController.text = _cmnt.text.toString();
    });
  }
  void editCommentNow() async {
    if(textController.text != null && textController.text.length > 0) {
      if(await isInternetConnection()) {
        setState((){loadingComment = true; });
        Comment data = new Comment(id: commentEditing.id, text: textController.text);
        var commentData = textController.text;
        setState(() {
          textController.clear();
        });
        editComment(data).then((value) {
          setState((){loadingComment = false; });
          if (value != null) {
              for(int i = 0; i<post.comments.length; i++) {
                if(post.comments[i].id.toString() == commentEditing.id.toString()) {
                  setState(() {
                    post.comments[i].text = value.text;
                  });
                }
              }
            setState((){
              isEditingComment = false;
              commentEditing = Comment();
            });
            posts.forEach((element) {
              if (element.id == post.id) {
                setState(() {
                  element.comments.forEach((_cmnt) {
                    if(_cmnt.id.toString() == commentEditing.id.toString()) {
                      _cmnt = commentEditing;
                    }
                  });
                });
              }
            });
          } else {
            showToast("Unable to post comment");
            setState(() {
              textController.text = commentData;
            });
          }
        });
      }
    }
  }
  void commentOnPost(Post post) async {
    if(textController.text != null && textController.text.length > 0) {
      if(await isInternetConnection()) {
        setState((){loadingComment = true; });
        Comment data = new Comment(postId: post.id, text: textController.text);
        var commentData = textController.text;
        setState(() {
          textController.clear();
        });
        postComment(data).then((value) {
          setState((){loadingComment = false; });
          if (value != null) {
            setState(() {
              post.comments.add(value);
            });
            posts.forEach((element) {
              if (element.id == post.id) {
                setState(() {
                  element.comments.add(value);
                });
              }
            });
          } else {
            showToast("Unable to post comment");
            setState(() {
              textController.text = commentData;
            });
          }
        });
      }
    }
  }

  void getCameraImage() async {
    var _rand = Random();
    final pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    if(pickedFile!=null) {
      debugPrint("pickedFile: " + pickedFile.path);
      setState((){
        if(post.mediaList == null) {
          post.mediaList = <Media>[];
        }
        post.mediaList.add(
            Media(identifier: pickedFile.path,  type: 0,
                name: "Image_${_rand.nextInt(100000)}")
        );
      });
      // Auth().uploadImage(File(pickedFile.path));
    }
  }

  void multipleImagePick() async {
    List<Asset> resultList;
    String error;

    try {
      resultList = await MultiImagePicker.pickImages(
          maxImages: 10,
          enableCamera: true,
          selectedAssets: images ?? List<Asset>(),
          cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
          materialOptions: MaterialOptions(
            statusBarColor: "#25d366",
            actionBarColor: "#25d366",
            actionBarTitle: "Pick image",
            allViewTitle: "All Photos",
            useDetailsView: false,
            selectCircleStrokeColor: "#000000",
          ));
    } on Exception catch (e) {
      error = e.toString();
    }
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.

    if(resultList != null) {
      setState(() {
        images = resultList;
        post.mediaList = <Media>[];
        post.mediaList.clear();
        if (error == null) _error = 'No Error Dectected';
      });
    }
    if(images!=null) {
      images.forEach((e) async {
        var path = await FlutterAbsolutePath.getAbsolutePath(e.identifier);
        setState(() {
          post.mediaList.add(Media(name: e.name,  type: 0, identifier: path));
        });
      });
    }
  }

  void getVideoFile() async {
    var _rand = Random();
    final result = await FilePicker.platform.pickFiles(type: FileType.video);
    if(result != null) {
      debugPrint("pickedVideo: " + result.files.single.path);
      setState((){
            if(post.mediaList == null) {
              post.mediaList = <Media>[];
            }
            post.mediaList.add(
                Media(identifier: result.files.single.path, type: 1,
                    name: "Video_${_rand.nextInt(100000)}")
            );
          });
    }
  }

  void uploadPost(Post _myPost) async {
    if((_myPost.text != null && _myPost.text.length > 0) || _myPost.mediaList.length > 0 ) {
      //_myPost.postType = currentPrivacyOption.option.toLowerCase();
      final Stream<Post> stream = await addNewPost(_myPost);
      stream.listen((Post _uploadedPost) {
        setState(() {
          posts.add(_uploadedPost);
        });
      }, onError: (a) {
        print(a);
        scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text('Error uploading post'),
        ));
      }, onDone: () {
        getPosts();
      });
    }

  }

  void sendOrCancelFriendRequest(User user) async {
    bool processed = true;
    String msg = "";
    if(user.friendRequestStatus==0)
      {
        processed = await sendFriendRequest(user);
      }
    else {
      //Cancel friend request
    }
    if(!processed) //error = true
    {
      peopleYouMayKnow.forEach((element) {
        if(element.id == user.id) {
          setState((){
            element.friendRequestStatus = 1;
          });
        }
      });
    }
  }

  onPostOptions(var value, Post _post) {
    if(value == options[0].option) {
      //Save Post
      setState((){isLoading = true;});
      savePost(_post).then((savedPost){
        setState((){isLoading = false;});
        if(savedPost != null) {
          showToast("Post saved");
        }
      });
    } else if(value == myPostOptions[1].option) {
      //Edit Post
      Navigator.of(context).pushNamed("/EditPost", arguments: RouteArgument(post: _post, currentUser: this.currentUser)).then((value) {
        if(value != null) {
          if(value == true || value == "true") {
            this.getPosts();
          }
        }
      });
    } else if(value == myPostOptions[2].option) {
      //Feature Post
      Navigator.of(context).pushNamed("/ViewPackages", arguments: RouteArgument(post: _post, currentUser: this.currentUser));
    } else if(value == myPostOptions[3].option) {
      //Delete Post
      deletePostNow(_post);
    }
  }

  onPostPrivacyOptions(var value) {
    sharePostPrivacyOptions.forEach((element) {
      if(element.option.toLowerCase() == value.toString().toLowerCase()) {
        setState((){
          currentPrivacyOption = element;
        });
      }
    });
    if(value == newPostPrivacyOptions[0].option) { //Public
      this.post.postType = POST_PUBLIC;
    } else if(value == newPostPrivacyOptions[1].option) { //Public
      this.post.postType = POST_FRIENDS;
    } else if(value == newPostPrivacyOptions[2].option) { //Public
      this.post.postType = POST_PRIVATE;
    }
  }

  deletePostNow(Post _post) async {
    deletePost(_post)
        .then((_isDeleted) {
      if(_isDeleted) {
        for(int i=0; i<posts.length; i++) {
          if(posts[i].id.toString() == _post.id.toString()) {
            posts.removeAt(i);
          }
        }
      }
    }, onError: (e){
      print(e);
    });
  }

  onCommentOptions(var value, Comment _cmnt) {
   if(value == myCommentOptions[0].option) {
     //View user profile
     Navigator.of(context).pushNamed(
         "/UserProfileDetails",
         arguments: RouteArgument(user: _cmnt.user));
   } else if (value == myCommentOptions[1].option) {
     //Edit Comment
     enableCommentEditing(_cmnt);
   }
   else if (value == myCommentOptions[2].option) {
     //Delete comment now
     deleteComment(_cmnt).then((value) {
       if(value) {
         showToast("Comment Deleted");
         for(int i=0; i<this.post.comments.length; i++) {
           if(_cmnt.id.toString() == this.post.comments[i].id.toString()) {
             setState((){
               this.post.comments.removeAt(i);
             });
           }
         }
       } else {
         showToast("Unable to delete comment now");
       }
     },
         onError: (e){
       print(e);
       showToast("Verify your internet connection");
     });
   }
  }


}