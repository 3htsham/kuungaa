import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/config/call_methods.dart';
import 'package:social_app/config/firestore_config.dart';
import 'package:social_app/config/my_permissions.dart';
import 'package:social_app/src/models/call.dart';
import 'package:social_app/src/models/chat.dart';
import 'package:social_app/src/models/message.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/chat_repository.dart';
import 'package:social_app/src/repositories/settings_repository.dart';
import 'package:social_app/src/repositories/user_repository.dart';


class ChatController extends ControllerMVC {

  FirebaseFirestore firestore = FirebaseFirestore.instance;
  User currentUser;
  String opponentId = "";

  List<Chat> chats = <Chat>[];
  bool loadingChats = false;

  Chat chat;
  List<Message> messages = <Message>[];
  bool loadingMessages = true;
  TextEditingController textController;
  ScrollController scrollController;

  bool loadingWidget = false;

  User user; //to whome message to send

  GlobalKey<ScaffoldState> scaffoldKey;

  ChatController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    textController = TextEditingController();
    scrollController = ScrollController();
  }

  getCurrentUserNow() async {
    User user = await getCurrentUser();
    setState((){
      this.currentUser = user;
    });
  }

  getAllChats() async {
    chats.clear();
    setState((){
      loadingChats = true;
    });

    Stream<QuerySnapshot> stream = MyFirestore.getChatsList(currentUser.id);
    stream.listen((_querySnaps) {
      chats.clear();
      setState((){
        loadingChats = false;
      });
      _querySnaps.docs.forEach((_chatItem) {
        Map<String, dynamic> data = _chatItem.data();

        Chat _chat = Chat(
          fireStoreChatId: data['chatId'],
          lastMessage: data['lastMessage'],
          isRead: data['lastBy'].toString() == currentUser.id.toString(),
          id: data['serverChatId'],
          ownerId: currentUser.id,
          time: DateTime.fromMillisecondsSinceEpoch(data['updatedAt']),
        );

        for(String key in data.keys) {
          if (key.toString() != currentUser.id.toString() &&
              key.toString() != "users" &&
              key.toString() != "chatId" &&
              key.toString() != "serverChatId" &&
              key.toString() != "updatedAt" &&
              key.toString() != "lastBy" &&
              key.toString() != "lastMessage") {
            User opponentUser = User(
                id: key, name: data[key]['name'], image: data[key]['image']);

            _chat.opponent = opponentUser;
            _chat.opponentId = key;
            setState((){
              chats.add(_chat);
            });
          }
        }
      });
    }, onError: (e) {
      print(e);
      setState(() {
        loadingChats = false;
      });
    }, onDone: () {
      setState(() {
        loadingChats = false;
      });
    });
  }

  getChatMessages() async {
    if(chat != null) {

      Stream<QuerySnapshot> stream = MyFirestore.getChatMessages(chat.fireStoreChatId);
      stream.listen((_querySnaps) {
        messages.clear();
        setState(() {
          loadingMessages = false;
        });
        _querySnaps.docs.forEach((_messageItem) {
          Map<String, dynamic> data = _messageItem.data();
          var isMe = data['sender'] == currentUser.id.toString() ? 1 : 0;
          Message myMessage = Message(type: isMe, time: DateTime.fromMillisecondsSinceEpoch(data['time']), txt: data['txt']);
          setState(() {
                messages.add(myMessage);
                loadingMessages = false;
                scrollController.jumpTo(scrollController.position.maxScrollExtent);
              });
        });
      }, onError: (e) {
        setState(() {
              loadingMessages = false;
            });
      }, onDone: (){
          setState(() {
                loadingMessages = false;
              });
      });

    } else {
      setState(() {
        loadingMessages = false;
      });
    }
  }

  getChatMessagesByUserId() async {
    if(user != null) {

      var chat1 = '${currentUser.id.toString()}_${user.id.toString()}';
      var chat2 = '${user.id.toString()}_${currentUser.id.toString()}';

      bool isExist1 = await MyFirestore.checkExist(chat1);
      bool isExist2 = isExist1 ? false : await MyFirestore.checkExist(chat2);
      var fireStoreChatId = isExist1 ? chat1 : chat2;

      MyFirestore.getChatMessages(fireStoreChatId).listen((_querySnaps) {
        messages.clear();
        setState(() {
          loadingMessages = false;
        });
        _querySnaps.docs.forEach((_messageItem) {
          Map<String, dynamic> data = _messageItem.data();
          var isMe = data['sender'] == currentUser.id.toString() ? 1 : 0;
          Message myMessage = Message(type: isMe,
              time: DateTime.fromMillisecondsSinceEpoch(data['time']),
              txt: data['txt']);
          setState(() {
            chat = Chat(opponent:  this.user, opponentId: this.user.id, fireStoreChatId: fireStoreChatId);
            messages.add(myMessage);
            loadingMessages = false;
            scrollController.jumpTo(scrollController.position.maxScrollExtent);
          });
        });
      }, onError: (e){
        setState(() {
          loadingMessages = false;
        });
      }, onDone: (){
        setState(() {
          loadingMessages = false;
        });
      });

    } else {
      setState(() {
        loadingMessages = false;
      });
    }
  }

  sendANewMessage() async {
    if(textController.text != null && textController.text.length > 0) {
      Message message = Message(txt: textController.text);
      if(chat != null)
      {
        message.recieverId = chat.opponentId;
      }
      else {
        setState((){
          chat = Chat(opponent: user, opponentId: user.id);
        });
        message.recieverId = user.id;

        var chat1 = '${currentUser.id.toString()}_${user.id.toString()}';
        var chat2 = '${user.id.toString()}_${currentUser.id.toString()}';

        bool isExist1 = await MyFirestore.checkExist(chat1);
        bool isExist2 = isExist1 ? false : await MyFirestore.checkExist(chat2);
        chat.fireStoreChatId = isExist1 ? chat1 : chat2;

        if(!isExist1 && !isExist2) {
          Map<String, dynamic> data = new Map<String, dynamic>();
          data['users'] = [currentUser.id.toString(), user.id.toString()];
          data['chatId'] = chat.fireStoreChatId;

          Map<String, dynamic> userMap = new Map<String, dynamic>();
          userMap['name'] = currentUser.name;
          userMap['image'] = currentUser.image;
          data[currentUser.id.toString()] = userMap;

          userMap = new Map<String, dynamic>();
          userMap['name'] = user.name;
          userMap['image'] = user.image;
          data[user.id.toString()] = userMap;

          MyFirestore.createUserChatRoom(chat.fireStoreChatId, data);

        }

      }

      textController.text = "";
      sendMessage(message).then((value) {
        if(value!=null) {

          Map<String, dynamic> msgData = new Map<String, dynamic>();
          msgData['txt'] = message.txt;
          msgData['sender'] = currentUser.id;
          MyFirestore.createChatRoomMessage(chat.fireStoreChatId, message.txt, value.chatId, msgData, currentUser);

          setState((){
            chat.id = value.chatId;
            scrollController.jumpTo(scrollController.position.maxScrollExtent);
          });

        }
      }, onError: (e) {
        scaffoldKey.currentState.showSnackBar(
          SnackBar(content: Text("Unable to send Message"),)
        );
      });
    }
  }

  List<String> options = <String>["Delete Chat", "Vie User Profile"];
  void onOptions(var value, bool moveBack, Chat _chat) {
    if(value==options[0]) {
      deleteThisChat(_chat, moveBack);
    } else if(value==options[1]){
      Navigator.of(context).pushNamed("/UserProfileDetails", arguments: RouteArgument(user: _chat.opponent));
    }
  }

  deleteThisChat(Chat _chat, bool moveBack) async {
    setState(() {
      loadingChats = true;
    });
    deleteAChat(_chat).then((value) {
      if (!value && !moveBack) {
        setState(() {
          for (int i = 0; i < chats.length; i++) {
            if (chats[i].id == _chat.id) {
              chats.removeAt(i);
            }
          }
        });
      }
      else {
        scaffoldKey.currentState.showSnackBar(SnackBar(
          content: Text("Error deleting chat"),
        ));
      }
      setState(() {
        loadingChats = false;
      });
      if(moveBack) {
        Navigator.of(context).pop(true);
      }
    }, onError: (e) {
      setState(() {
        loadingChats = false;
      });
      print(e);
      scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Unable to delete Chat now"),
      ));
    });
  }


  ///Dial Call
  dialCall(BuildContext context, bool isVideoCall) async {
    if(await MyPermissions.cameraAndMicrophonePermissionsGranted()) {
      setState(() {
        loadingMessages = true;
      });
      if (await isInternetConnection()) {
        User _receiver;
        CallMethods callMethods = CallMethods();
        if (chat != null) {
          _receiver = chat.opponent;
        } else {
          _receiver = this.user;
        }
        Call _call = Call(
            callerId: currentUser.id,
            callerName: currentUser.name,
            callerPic: currentUser.image,
            receiverId: _receiver.id,
            receiverName: _receiver.name,
            receiverPic: _receiver.image,
            channelId: '${currentUser.id}_${_receiver.id}',
            isVideoCall: isVideoCall
        );

        bool callMade = await callMethods.makeCall(call: _call);

        _call.hasDialed = true;

        if (callMade) {
          setState(() {
            loadingMessages = false;
          });
          Navigator.of(context).pushNamed("/MakeCall",
              arguments: RouteArgument(
                  call: _call,
                  currentUser: this.currentUser,
                  user: _receiver
              ));
        }
      } else {
        setState(() {
          loadingMessages = false;
        });
        showToast("Verify your internet connection");
      }
    } else {
      showToast("Kuungaa needs Camera and Microphone permissions to make a call");
    }

  }


}