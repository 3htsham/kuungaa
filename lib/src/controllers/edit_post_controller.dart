import 'dart:math';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/models/media.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/repositories/settings_repository.dart';
import 'package:social_app/src/repositories/edit_post_repository.dart' as repo;

class EditPostController extends ControllerMVC {

  Post post;
  List<Media> newMedia = <Media>[];

  List<String> deletedPhotos = <String>[];
  List<String> deletedVideos = <String>[];

  GlobalKey<ScaffoldState> scaffoldKey;
  TextEditingController textController;
  List<Asset> images = List<Asset>();
  String _error = "";
  bool isLoading = false;

  EditPostController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    textController = TextEditingController();
  }

  updatePost() async {
    if(await isInternetConnection()) {
      setState((){isLoading = true;});
      repo.updatePost(post, deletedPhotos, deletedVideos, newMedia)
          .then((_isUpdated) {
            if(_isUpdated) {
              showToast("Post updated Successfully");
              Navigator.of(context).pop(true);
            } else {
              showToast("Post not updated, try again later");
            }
      },
          onError: (e){
            print(e);
            setState((){isLoading = false;});
            scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Something went wrong")));
          });
    } else {
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection")));
    }
  }

  ///MediaTypes >>> 0 = image, 1 = video
  removeMedia(Media _media) {
    if(_media.type == 0) { //Image
      deletedPhotos.add(_media.id.toString());
    } else {
      deletedVideos.add(_media.id.toString());
    }
    for (int i = 0; i < post.mediaList.length; i++) {
      if (post.mediaList[i].id.toString() == _media.id.toString() && _media.type == post.mediaList[i].type) {
        setState((){
          post.mediaList.removeAt(i);
        });
      }
    }
  }
  removeNewMedia(Media _media) {
    for (int i = 0; i < newMedia.length; i++) {
      if (newMedia[i].name.toString() == _media.name.toString() && _media.identifier == newMedia[i].identifier) {
        setState((){
          newMedia.removeAt(i);
        });
      }
    }
  }

  void multipleImagePick() async {
    List<Asset> resultList;
    String error;

    try {
      resultList = await MultiImagePicker.pickImages(
          maxImages: 10,
          enableCamera: true,
          selectedAssets: images ?? List<Asset>(),
          cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
          materialOptions: MaterialOptions(
            statusBarColor: "#25d366",
            actionBarColor: "#25d366",
            actionBarTitle: "Pick image",
            allViewTitle: "All Photos",
            useDetailsView: false,
            selectCircleStrokeColor: "#000000",
          ));
    } on Exception catch (e) {
      error = e.toString();
    }
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.

    if(resultList != null) {
      setState(() {
        images = resultList;
        newMedia = <Media>[];
        newMedia.clear();
        if (error == null) _error = 'No Error Dectected';
      });
    }
    if(images!=null) {
      images.forEach((e) async {
        var path = await FlutterAbsolutePath.getAbsolutePath(e.identifier);
        setState(() {
          newMedia.add(Media(name: e.name,  type: 0, identifier: path));
        });
      });
    }
  }

  void getCameraImage() async {
    var _rand = Random();
    final pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    if(pickedFile!=null) {
      debugPrint("pickedFile: " + pickedFile.path);
      setState((){
        if(newMedia == null) {
          newMedia = <Media>[];
        }
        newMedia.add(
            Media(identifier: pickedFile.path,  type: 0,
                name: "Image_${_rand.nextInt(100000)}")
        );
      });
      // Auth().uploadImage(File(pickedFile.path));
    }
  }

  void getVideoFile() async {
    var _rand = Random();
    final result = await FilePicker.platform.pickFiles(type: FileType.video);
    if(result != null) {
      debugPrint("pickedVideo: " + result.files.single.path);
      setState((){
        if(newMedia == null) {
          newMedia = <Media>[];
        }
        newMedia.add(
            Media(identifier: result.files.single.path, type: 1,
                name: "Video_${_rand.nextInt(100000)}")
        );
      });
    }
  }

}