import 'dart:math';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/config/firestore_config.dart';
import 'package:social_app/src/models/post_options.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/settings_repository.dart';
import 'package:social_app/src/repositories/user_repository.dart';

class ProfilePageController extends ControllerMVC {

  GlobalKey<ScaffoldState> scaffoldKey;

  User currentUser;
  User user;
  var loading = true;

  GlobalKey<FormState> formKey;
  TextEditingController nameController;
  TextEditingController emailController;
  TextEditingController bioController;
  bool isloading = false;
  bool unFriendDialog = false;
  bool blockDialog = false;

  List<PostOptions> userOptions = <PostOptions>[
    PostOptions(option: "Block", icon: "assets/icons/block.png"),
  ];


  ProfilePageController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    this.formKey = GlobalKey<FormState>();
    this.nameController = TextEditingController();
    this.emailController = TextEditingController();
    this.bioController = TextEditingController();
  }

  getCurrentUserNow() async {
    User _user = await getCurrentUser();
    setState((){
      this.currentUser = _user;
    });
  }

  getUserProfile(User user){
      getUserDetails(user).then((_user) {
        setState((){
          loading = false;
        });
        if (_user != null) {
          setState(() {
            this.user = _user;
          });
        } else {
          scaffoldKey.currentState.showSnackBar(
            SnackBar(content: Text("Unable to get user details"),)
          );
        }
      });
  }

  onOptionsTap(String _value) {
    if(_value == userOptions[0].option ) {
      //Block User
      setState((){blockDialog = true;});
    }
  }

  blockUserNow(){
    setState((){blockDialog = false;});
    blockUser(this.user).then((value) {
        if(value != null) {
          showToast("${this.user.name} blocked");
          this.deleteChat();
          Navigator.of(context).pop();
        } else {
          showToast("Something went wrong");
        }
      }, onError: (e){
        print(e);
          showToast("Unable to block user now, try again later please.");
      });
  }

  void sendOrCancelFriendRequest(User user) async {
    bool processed = true;
    String msg = "";
    if(user.friendRequestStatus==0)
    {
      processed = await sendFriendRequest(user);
    }
    else {
      //Cancel friend request
    }
    if(!processed) //error = true
        {
          setState((){
            user.friendRequestStatus = 1;
          });
        }
  }

  void updateUserProfile() async {
    if(formKey.currentState.validate()) {
      formKey.currentState.save();
      setState((){
        isloading = true;
      });
      Stream<User> stream = await updateUser(user);
      stream.listen((event) {
            if(event != null) {
              Navigator.of(context).pop(true);
            }
          },
          onError: (e){
            setState((){
              isloading = false;
            });
            print(e);
          },
          onDone: (){
            setState((){
              isloading = false;
            });
      });
    }
  }

  void getGalleryImage() async {
    var _rand = Random();
    final pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
    if(pickedFile!=null) {
      debugPrint("pickedFile: " + pickedFile.path);
      setState((){
        user.image = pickedFile.path;
      });
      // Auth().uploadImage(File(pickedFile.path));
    }
  }

  bool isValidEmail(var email){
    bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);
    return emailValid;
  }

  void unFriendUserNow(User _user) async {
    if(await isInternetConnection()) {
      setState((){isloading = true; unFriendDialog = false;});
      unFriendUser(_user).then((value){
        if(value!=null) {
          setState((){
            this.user = value;
          });
        }
        setState((){isloading = false;});
      });
    }
  }


  void deleteChat() async {
    var chat1 = '${currentUser.id.toString()}_${user.id.toString()}';
    var chat2 = '${user.id.toString()}_${currentUser.id.toString()}';

    bool isExist1 = await MyFirestore.checkExist(chat1);
    bool isExist2 = isExist1 ? false : await MyFirestore.checkExist(chat2);
    var fireStoreChatId = isExist1 ? chat1 : chat2;

    if(isExist1 || isExist2) {
      MyFirestore.deleteChat(fireStoreChatId);
    }
  }

}