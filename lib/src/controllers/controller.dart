import 'dart:io';
import 'dart:math';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/user_repository.dart' as userRepo;

class Controller extends ControllerMVC {

  GlobalKey<ScaffoldState> scaffoldKey;
  User currentUser;
  FirebaseMessaging _firebaseMessaging;

  Controller() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  @override
  void initState() {
    userRepo.getCurrentUser().then((user) {
      setState(() {
        userRepo.currentUser = user;
      });
    });
    super.initState();
    Firebase.initializeApp();
  }

  getCurrentUserNow() async {
     currentUser = await userRepo.getCurrentUser();
  }


  initFCM() async {
    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.getToken().then((String _deviceToken) {
      currentUser.firebaseToken = _deviceToken;
      userRepo.updateUserFirebaseToken(this.currentUser).then((value) {});
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        onMessage(message);
      },
      // onBackgroundMessage: Platform.isIOS ? null : myBackgroundMessageHandler,
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );



    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });

  }

  static void onMessage(Map<String, dynamic> message) async {

    var initializationSettingsAndroid = AndroidInitializationSettings('noti_icon');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initializationSettings = InitializationSettings(android: initializationSettingsAndroid, iOS: initializationSettingsIOS);


    const AndroidNotificationChannel channel = AndroidNotificationChannel(
      'high_importance_channel', // id
      'High Importance Notifications', // title
      'This channel is used for important notifications.', // description
      importance: Importance.max,
    );

    final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: null);

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    Random rand = Random();
    var _id = rand.nextInt(100000);

    flutterLocalNotificationsPlugin.show(
        0,
        message['notification']['title'],
        message['notification']['body'],
        NotificationDetails(
            android: AndroidNotificationDetails(
              channel.id,
              channel.name,
              channel.description,
              // icon: android?.smallIcon,
              // other properties...
            ),
            iOS: IOSNotificationDetails(
                badgeNumber: 0,
                presentAlert: true,
                presentSound: true,
                presentBadge: true
            )
        ));

  }

  static Future<dynamic> onDidReceiveLocalNotification(int id, String a, String b, String c) {

  }


  static Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }

    // Or do other work.
  }

}