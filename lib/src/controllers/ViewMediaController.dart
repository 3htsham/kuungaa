import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/models/post.dart';

class ViewMediaController extends ControllerMVC {

  Post post;
  PageController controller;
  double index = 0;

  GlobalKey<ScaffoldState> scaffoldKey;

  ViewMediaController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    controller = PageController();
    controller.addListener(() {
      setState((){
        index = controller.page;
      });
    });
  }

}