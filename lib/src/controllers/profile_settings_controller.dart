import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/settings_repository.dart';
import 'package:social_app/src/repositories/user_repository.dart';

class ProfileSettingsController extends ControllerMVC {

  GlobalKey<ScaffoldState> scaffoldKey;
  TextEditingController nameController;
  TextEditingController passwordController;
  TextEditingController confirmPasswordController;
  TextEditingController nicknameController;
  bool isloading = false;

  User currentUser;
  User user;

  List<User> blockedUsers = <User>[];

  bool isEditingName = false;
  bool isEditingPassword = false;
  bool isEditingStatus = false;
  bool isEditingNickname = false;

  List<DropdownMenuItem<String>> statusDropdownMenuItems;
  List<String> statuses = ["Single", "In a Relationship", "In love", "Married",];
  String selectedItem = "";



  ProfileSettingsController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    this.nameController = TextEditingController();
    this.nicknameController = TextEditingController();
    this.passwordController = TextEditingController();
    this.confirmPasswordController = TextEditingController();
    statusDropdownMenuItems = buildStatusDropDownMenuItems(statuses);
    selectedItem = statuses[0];
  }

  getCurrentUserNow() async {
    User user = await getCurrentUser();
    setState((){
      this.currentUser = user;
      nameController.text = this.currentUser.name;
      nicknameController.text = this.currentUser.nickName ?? "";
      selectedItem = currentUser.userStatus != null && currentUser.userStatus.length > 1 ? currentUser.userStatus : statuses[0];
    });
  }

  void changeStatus(String status)
  {
    setState((){
      selectedItem = status;
    });
  }

  List<DropdownMenuItem<String>> buildStatusDropDownMenuItems(List listItems) {
    List<DropdownMenuItem<String>> items = List();
    for (String listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Text(listItem),
          value: listItem,
        ),
      );
    }
    return items;
  }

  void updatePassword() {
    if(passwordController.text !=null && passwordController.text.length > 5) {
      if(passwordController.text == confirmPasswordController.text) {
        user = User();
        this.user.password = passwordController.text;
        updateUserProfile();
      } else {
        showToast("Passwords didn't match");
      }
    } else {
      showToast("Password length must be greater than 5");
    }
  }

  void updateName() {
    if(nameController.text !=null && nameController.text.length > 2) {
      user = User();
        this.user.name = nameController.text;
        updateUserProfile();
    } else {
      showToast("Name must be at least 3 characters");
    }
  }

  void updateNick() {
    if(nicknameController.text !=null && nameController.text.length > 2) {
      user = User();
      this.user.nickName = nicknameController.text;
      updateUserProfile();
    } else {
      showToast("Nick name must be at least 3 characters");
    }
  }

  void updateStatus() {
      user = User();
      this.user.userStatus = selectedItem;
      updateUserProfile();
  }

  void updateUserProfile() async {
    if(currentUser != null) {
      setState((){
        isloading = true;
      });
      Stream<User> stream = await updateUser(user);
      stream.listen((event) {
        if(event != null) {
          getCurrentUserNow();
          setState((){
            this.isEditingName = false;
            this.isEditingPassword = false;
            this.isEditingStatus = false;
            this.isEditingNickname = false;
          });
        }
      },
          onError: (e){
            setState((){
              isloading = false;
            });
            print(e);
          },
          onDone: (){
            setState((){
              isloading = false;
            });
          });
    }
  }


  void getBlockedUsers() async {
    Stream<User> stream = await getBlockedPeople();
    stream.listen((_user) {
        setState((){
          blockedUsers.add(_user);
        });
    }, 
    onError: (e){
      print(e);
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Error retrieving blocklist")));
    }, 
    onDone: (){} );
  }

  void unblockUserNow(User _user) async {
    setState((){isloading = true; });
    unblockUser(_user).then((_isUnblocked){
      setState((){isloading = false; });
      if(_isUnblocked) {
        for(int i = 0; i<blockedUsers.length; i++) {
          if(blockedUsers[i].id.toString() == _user.id.toString()) {
            blockedUsers.removeAt(i);
          }
        }
      } else {
        showToast("Error unblocking user, try later please.");
      }
    },
    onError: (e){
      print(e);
      setState((){isloading = false; });
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Something went wrong, Try Later.")));
    });
  }


}