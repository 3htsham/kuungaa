import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:mpesa_flutter_plugin/mpesa_flutter_plugin.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/config/mpesa.dart';
import 'package:social_app/src/models/boost_package.dart';
import 'package:social_app/src/models/mpesa.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/repositories/boost_repository.dart' as repo;
import 'package:social_app/src/repositories/settings_repository.dart';

class BoostController extends ControllerMVC {

  MPESAModel mpesa;
  Post post;
  BoostPackage package;
  List<BoostPackage> packages = <BoostPackage>[];

  GlobalKey<ScaffoldState> scaffoldKey;
  TextEditingController textController;

  bool isLoading = false;

  BoostController() {
    textController = TextEditingController();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }


  getPackages() async {
    Stream<BoostPackage> stream = await repo.getPackages();
    stream.listen((_package) {
      setState((){
        packages.add(_package);
      });
    },
        onError: (e){
      print(e);
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Error getting available packages")));
        },
        onDone: (){});
  }

  getCredentials() async {
    repo.getMPESACredentials().then((_mpesa) {
      setState(() {
        this.mpesa = _mpesa;
      });
    }, onError: (e) {
      print(e);
    });
  }

  makePayment() async {
    if(textController.text!=null && textController.text.length > 5) {
      try {
        setState((){isLoading = true;});

        MpesaFlutterPlugin.initializeMpesaSTKPush(

            businessShortCode: mpesa.SHORT_CODE.toString(),
            transactionType: TransactionType.CustomerPayBillOnline,
            amount: double.parse(this.package.price.toString()),
            partyA: textController.text,
            partyB: mpesa.SHORT_CODE.toString(),
            callBackURL: Uri.parse('${GlobalConfiguration().getValue('api_base_url')}callback'),
            accountReference: "${package.name.toString().toUpperCase()}_${this.post.id.toString()}",
            phoneNumber: textController.text,
            baseUri: Uri(scheme: "https", host: "sandbox.safaricom.co.ke"),
            transactionDesc: "Purchased ${this.package.name.toString().toUpperCase()} package to boost post",
            passKey: mpesa.PASS_KEY

        ).then((value) {

          print(value);

          if((value as Map).containsKey("errorMessage")) {
            setState((){isLoading = false;});
            showToast((value as Map)['errorMessage']);
          } else if((value as Map).containsKey("CustomerMessage")) {
            var map = (value as Map);
            var merchantReqId = map['MerchantRequestID'].toString();
            var checkoutReqId = map['CheckoutRequestID'].toString();
            var packageId = this.package.id.toString();
            var postId = this.post.id.toString();

            repo.postBoost(this.post, packageId, merchantReqId, checkoutReqId)
            .then((value){
              setState((){isLoading = false;});
              if(value) {
                showToast("Your post will be boosted after successful payment transfer");
                Navigator.of(scaffoldKey.currentContext).pushNamedAndRemoveUntil('/Pages',  (Route<dynamic> route) => false, arguments: 0);
              } else {
                showToast("Try again later");
              }
            }, 
            onError: (e) {
              print(e);
              setState((){isLoading = false;});
                showToast("Some error occured, try later please.");
            });

          }

        },
            onError: (e) {
              print(e);
              setState((){isLoading = false;});
            });

      } catch (e) {
        setState((){isLoading = false;});
        //you can implement your exception handling here.
        //Network unreachability is a sure exception.
        print(e);
      }
    } else {
      showToast("Enter a phone number to proceed");
    }
  }

}