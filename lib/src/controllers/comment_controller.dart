import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/models/comment.dart';
import 'package:social_app/src/models/comment_reply.dart';
import 'package:social_app/src/models/post_options.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/post_repository.dart' as repo;
import 'package:social_app/src/repositories/settings_repository.dart';

class CommentController extends ControllerMVC {

  User currentUser;
  Comment comment;
  CommentReply reply = CommentReply();
  CommentReply commentReplyEditing;
  bool isEditingCommentReply = false;

  bool showReplies = false;
  bool showReplyBox = false;

  List<PostOptions> myCommentOptions = <PostOptions>[
    PostOptions(option: "View Profile", icon: "assets/icons/profile.png"),
    // PostOptions(option: "Edit", icon: "assets/icons/write.png"),
    // PostOptions(option: "Delete Comment", icon: "assets/icons/bin.png"),
  ];
  List<PostOptions> commentOptions = <PostOptions>[
    PostOptions(option: "View Profile", icon: "assets/icons/profile.png"),
  ];

  TextEditingController textController;

  bool loadingComment = false;

  CommentController(){
    textController = TextEditingController();
  }

  postCommentReply() async {
    if(textController.text != null && textController.text.length > 0) {
      if(await isInternetConnection()) {
        var commentData = textController.text;
        setState((){loadingComment = true; });
        CommentReply _cmnt = CommentReply();
        _cmnt.commentId = this.comment.id;
        _cmnt.text = textController.text;
        setState(() {
          textController.clear();
        });
        repo.postCommentReply(_cmnt).then((value) {
          setState((){loadingComment = false; });
          if(value != null) {
            setState((){
              if(this.comment.replies == null) {
                this.comment.replies = <CommentReply>[];
              }
              this.comment.replies.add(value);
            });
          } else {
            showToast("Unable to post comment reply");
            setState(() {
              textController.text = commentData;
            });
          }
        });
      }
    }
  }

  postCommentReplyLike(CommentReply _rply) async {
    repo.commentReplyLikeUnlike(_rply).then((_comment){
      if(_comment!=null) {
          for(int i = 0; i<this.comment.replies.length; i++) {
            if(this.comment.replies[i].id.toString() == _comment.id.toString()) {
              setState((){
                this.comment.replies[i].isLiked = _comment.isLiked;
                this.comment.replies[i].likesCount = _comment.likesCount;
              });
            }
          }
      }
    });
  }

  postCommentLike() async {
    repo.commentLikeUnlike(this.comment).then((_comment){
      if(_comment!=null) {
        setState((){
          this.comment.isLiked = _comment.isLiked;
          this.comment.likesCount = _comment.likesCount;
          this.comment.repliesCount = _comment.repliesCount;
        });
      }
    });
  }

  onCommentOptions(var value, CommentReply _cmnt) {
    if(value == myCommentOptions[0].option) {
      //View user profile
      Navigator.of(context).pushNamed(
          "/UserProfileDetails",
          arguments: RouteArgument(user: _cmnt.user));
    } else if (value == myCommentOptions[1].option) {
      //Edit Comment
      enableCommentReplyEditing(_cmnt);
    }
    else if (value == myCommentOptions[2].option) {
      //Delete comment now
      Comment _cmntRply = Comment();
      _cmntRply.id = _cmnt.id;
      repo.deleteComment(_cmntRply).then((value) {
        if(value) {
          showToast("Comment Deleted");
          for(int i=0; i<this.comment.replies.length; i++) {
            if(_cmnt.id.toString() == this.comment.replies[i].id.toString()) {
              setState((){
                this.comment.replies.removeAt(i);
              });
            }
          }
        } else {
          showToast("Unable to delete comment now");
        }
      },
          onError: (e){
            print(e);
            showToast("Verify your internet connection");
          });
    }
  }

  void enableCommentReplyEditing(CommentReply _cmnt) async {
    setState((){
      isEditingCommentReply = true;
      commentReplyEditing = _cmnt;
      textController.text = _cmnt.text.toString();
    });
  }
  void editCommentNow() async {
    if(textController.text != null && textController.text.length > 0) {
      if(await isInternetConnection()) {
        setState((){loadingComment = true; });
        Comment data = new Comment(id: commentReplyEditing.id, text: textController.text);
        var commentData = textController.text;
        setState(() {
          textController.clear();
        });
        repo.editComment(data).then((value) {
          setState((){loadingComment = false; });
          if (value != null) {
            setState((){
              isEditingCommentReply = false;
              commentReplyEditing = CommentReply();
            });
            this.comment.replies.forEach((element){
              if(element.id.toString() == value.id.toString()) {
                CommentReply rply = CommentReply();
                rply.text = value.text;
                rply.id = value.id;
                rply.likesCount = value.likesCount;
                rply.userId = value.userId;
                rply.user = value.user;
                rply.isLiked = value.isLiked;
                setState((){
                  element = reply;
                });
              }
            });
          } else {
            showToast("Unable to post comment");
            setState(() {
              textController.text = commentData;
            });
          }
        });
      }
    }
  }


}