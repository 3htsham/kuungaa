import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/models/comment.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/post_options.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/models/saved_post.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/post_repository.dart';
import 'package:social_app/src/repositories/settings_repository.dart';


class PostController extends ControllerMVC {

  User currentUser; //For view Post only and Saved Posts List only

  Post post;
  bool showComments = false;
  TextEditingController textController;

  User user;
  List<Post> posts = <Post>[];

  bool loadingSavedPosts = false;
  List<SavedPost> savedPosts = <SavedPost>[];
  List<String> savedPostOptions = <String>["View Post", "View User Profile"];

  bool isSharePostDialog = false;
  Post postToShare;
  PostOptions currentPrivacyOption;
  List<PostOptions> newPostPrivacyOptions = <PostOptions>[
    PostOptions(option: "Public", icon: "assets/icons/public.png"),
    PostOptions(option: "Friends", icon: "assets/icons/profile.png"),
    PostOptions(option: "Private", icon: "assets/icons/lock.png"),
  ];

  List<PostOptions> options = <PostOptions>[
    PostOptions(option: "Save Post", icon: "assets/icons/save.png")
  ];
  List<PostOptions> myPostOptions = <PostOptions>[
    PostOptions(option: "Save Post", icon: "assets/icons/save.png"),
    PostOptions(option: "Edit", icon: "assets/icons/write.png"),
    PostOptions(option: "Feature Post", icon: "assets/icons/star.png"),
    PostOptions(option: "Delete", icon: "assets/icons/bin.png")
  ];
  bool isLoading = false;

  GlobalKey<ScaffoldState> scaffoldKey;

  PostController() {
    textController = TextEditingController();
    currentPrivacyOption = newPostPrivacyOptions[0];
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  getSavedPostsNow() async {
    if(await isInternetConnection()) {
      setState((){
        loadingSavedPosts = true;
      });
      //Load saved posts
      Stream<SavedPost> stream = await getSavedPosts();
      stream.listen((_post) {
        savedPosts.add(_post);
      },
          onError: (e){
            setState((){
              loadingSavedPosts = false;
            });
            print(e);
            },
          onDone: (){
            setState((){
              loadingSavedPosts = false;
            });
          });
    }
  }

  onSavedPostOptions(String value, SavedPost _post, User owner) {
    if(value == savedPostOptions[0]) {
      //Vew Post
      Navigator.of(context).pushNamed("/ViewPost", arguments: RouteArgument(post: _post.post));
    } else if (value == savedPostOptions[1]) {
      //View User Profile
      Navigator.of(context).pushNamed("/UserProfileDetails", arguments: RouteArgument(user: owner));
    }
  }

  onPostOptions(var value, Post _post) {
    if(value == options[0].option) {
      //Save Post
      setState((){isLoading = true;});
      savePost(_post).then((savedPost){
        setState((){isLoading = false;});
        if(savedPost != null) {
          showToast("Post saved");
        }
      });
    } else if(value == myPostOptions[1].option) {
      //Edit Post
      Navigator.of(context).pushNamed("/EditPost", arguments: RouteArgument(post: _post, currentUser: this.currentUser)).then((value) {
        if(value != null) {
          if(value == true || value == "true") {
            this.getUserPostsByUserId();
          }
        }
      });
    } else if(value == myPostOptions[2].option) {
      //Feature Post
      Navigator.of(context).pushNamed("/ViewPackages", arguments: RouteArgument(post: _post, currentUser: this.currentUser));
    } else if(value == myPostOptions[3].option) {
      //Delete Post
      deletePostNow(_post);
    }
  }

  void postLike(Post _post) async {
    Comment data = Comment(postId: _post.id);
    likePost(data).then((value) {
      if(this.post!=null && value != null) {
        setState(() {
          this.post = value;
        });
      }
      if (value != null) {
        for(int i=0; i<posts.length; i++) {
          if(posts[i].id == value.id) {
            setState(() {
              posts[i] = value;
            });
          }
        }
      }
    });
  }

  void closeShareDialog(){
    setState((){
      isSharePostDialog = false;
    });
  }
  void showShareDialog(Post _post) {
    setState((){
      postToShare = _post;
      isSharePostDialog = true;
    });
  }
  void hideShareDialog(String privacy) {
    setState((){
      postToShare.postType = privacy.toLowerCase();
      isSharePostDialog = false;
    });
    sharePostNow(postToShare);
  }
  void sharePostNow(Post post) async {
    if(await isInternetConnection()) {
      setState((){isLoading = true;});
      sharePost(post).then((value) {
        if (value != null) {
          showToast("Post Shared");
        } else {
          showToast("Unable to share Post");
        }
        setState((){isLoading = false;});
      }, onError: (e) {
        print(e);
        setState((){isLoading = false;});
      });
    }
  }

  void commentOnPost(Post post) async {
    if(textController.text != null && textController.text.length > 0) {
      Comment data = new Comment(postId: post.id, text: textController.text);
      var commentData = textController.text;
      setState((){
        textController.clear();
      });
      postComment(data).then((value) {
        if(value != null) {
          setState((){
            this.post.comments.add(value);
          });
        } else {
          showToast("Unable to post comment");
          setState((){
            textController.text = commentData;
          });
        }
      });
    }
  }

  void getUserPostsByUserId() async {
      Stream<Post> stream = await getUserPosts(this.user);
      stream.listen((_post) {
        setState((){
          this.posts.add(_post);
        });
      },
          onError: (e){
        print(e);
          },
          onDone: (){});
    }

  deletePostNow(Post _post) async {
    deletePost(_post)
        .then((_isDeleted) {
      if(_isDeleted) {
        for(int i=0; i<posts.length; i++) {
          if(posts[i].id.toString() == _post.id.toString()) {
            posts.removeAt(i);
          }
        }
      }
    }, onError: (e){
      print(e);
    });
  }
}