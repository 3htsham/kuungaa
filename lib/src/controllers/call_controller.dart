import 'package:flutter/material.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/config/call_methods.dart';
import 'package:social_app/config/my_permissions.dart';
import 'package:social_app/src/models/call.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/settings_repository.dart';
import 'package:social_app/src/repositories/user_repository.dart';

class CallController extends ControllerMVC {

  CallMethods callMethods;
  User currentUser;
  User receiver;
  Call call;

  GlobalKey<ScaffoldState> scaffoldKey;

  CallController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    callMethods = CallMethods();
  }

  getCurrentUserNow() async {
    User user = await getCurrentUser();
    setState((){
      this.currentUser = user;
    });
  }

  ///End Call
  endCall(Call _call, _context) async {
    await callMethods.endCall(call: _call);
    if(_call.hasDialed) {
      Navigator.of(_context).pop();
    }
  }

  ///Pick Call
  pickCall(Call _call, BuildContext context, User _currentUser, User _receiver) async {
    stopRingtone();
    if(await MyPermissions.cameraAndMicrophonePermissionsGranted()) {
      Navigator.of(context).pushNamed("/MakeCall",
          arguments: RouteArgument(
            call: _call,
            currentUser: _currentUser,
            user: _receiver,
          ));
    } else {
      showToast("Kuungaa needs Camera and Microphone permissions to make a call");
    }
  }

  playRingtone() async {
    FlutterRingtonePlayer.playRingtone();
  }

  stopRingtone() async {
    FlutterRingtonePlayer.stop();
  }

  ///Dial
  dial(BuildContext context) async {
    Call _call = Call(
      callerId: currentUser.id,
      callerName: currentUser.name,
      callerPic: currentUser.image,
      receiverId: receiver.id,
      receiverName: receiver.name,
      receiverPic: receiver.image,
      channelId: '${currentUser.id}_${receiver.id}'
    );

    bool callMade = await callMethods.makeCall(call: _call);

    _call.hasDialed = true;

    if(callMade) {
      Navigator.of(context).pushNamed("/MakeCall",
          arguments: RouteArgument(
              call: _call,
              currentUser: this.currentUser,
              user: this.receiver
          ));
    }

  }


}