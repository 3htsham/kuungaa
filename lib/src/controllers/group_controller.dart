import 'dart:math';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/models/comment.dart';
import 'package:social_app/src/models/group.dart';
import 'package:social_app/src/models/media.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/post_options.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/group_repository.dart';
import 'package:social_app/src/repositories/post_repository.dart';
import 'package:social_app/src/repositories/settings_repository.dart';
import 'package:social_app/src/repositories/user_repository.dart';
import 'package:social_app/generated/i18n.dart';

class GroupController extends ControllerMVC {

  Group group = Group();
  List<Group> groups = <Group>[];
  List<Group> myGroups = <Group>[];
  List<Group> groupsIJoined = <Group>[];

  List<String> mainGroupsPageOptions = <String>[];

  GlobalKey<ScaffoldState> scaffoldKey;
  TextEditingController nameController;
  TextEditingController descriptionController;
  bool isLoading = false;

  Post post = Post();
  List<Asset> images = List<Asset>();
  String _error;
  List<Post> posts = <Post>[];
  TextEditingController textController;

  List<PostOptions> options = <PostOptions>[
    PostOptions(option: "Save Post", icon: "assets/icons/save.png")
  ];
  List<PostOptions> myPostOptions = <PostOptions>[
    PostOptions(option: "Save Post", icon: "assets/icons/save.png"),
    PostOptions(option: "Edit", icon: "assets/icons/write.png"),
    PostOptions(option: "Delete", icon: "assets/icons/bin.png")
  ];

  User admin;
  User currentUser;
  List<User> groupMembers = <User>[];
  List<String> adminOptions = ["View Profile", "Remove Member"];
  List<String> notAdminOptions = ["View Profile"];

  GroupController() {
    mainGroupsPageOptions.add("Refresh");
    mainGroupsPageOptions.add(S().create_new_group);
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    this.nameController = TextEditingController();
    this.descriptionController = TextEditingController();
    textController = TextEditingController();
  }

  Future<void> onRefreshGroups() async {
    getGroups();
    getMyGroups();
    getGroupsIJoined()();
  }

  getCurrentUserNow() async {
    User user = await getCurrentUser();
    setState((){
      this.currentUser = user;
    });
  }

  getGroups() async {
    Stream<Group> stream = await getListOfGroups();
    setState((){groups.clear();});
    stream.listen((_group) {
      setState((){
        groups.add(_group);
      });
    },
        onError: (e){
      print(e);
          scaffoldKey.currentState.showSnackBar(
              SnackBar(content: Text("Unable to get groups list"),)
          );
        },
        onDone: (){});
  }

  getMyGroups() async {
    Stream<Group> stream = await getUserGroups();
    setState((){myGroups.clear();});
    stream.listen((_group) {
      setState((){
        myGroups.add(_group);
      });
    },
        onError: (e){
          print(e);
          scaffoldKey.currentState.showSnackBar(
              SnackBar(content: Text("Unable to get groups list"),)
          );
        },
        onDone: (){});
  }

  getGroupsIJoined() async {
    Stream<Group> stream = await getGroupsUserJoined();
    setState((){groupsIJoined.clear();});
    stream.listen((_group) {
      setState((){
        groupsIJoined.add(_group);
      });
    },
        onError: (e){
          print(e);
        },
        onDone: (){});
  }

  void onGroupMembersOptions(var value, User _user) {
    if(value == adminOptions[0]) {
      //View User Profile, will be handled in UI Widget
    } else if(value == adminOptions[1]) {
      //Remove Member
      removeGroupMember(_user);
    }
  }



  getGroupMembersNow() async {
    Stream<User> stream = await getGroupMembersList(this.group);
    stream.listen((_user) {
      setState((){
        groupMembers.add(_user);
      });
    },
        onError: (e){
      print(e);
        },
        onDone: (){});
  }

  getGroupAdminNow() async {
    User _user = User(id: this.group.adminId);
    getUserDetails(_user).then((value) {
      if(value != null) {
        setState((){
          admin = value;
        });
      }
    });
  }


  void onMainGroupsOptions(var value) {
    if(value == mainGroupsPageOptions[0]) {
      onRefreshGroups();
    } else if(value == mainGroupsPageOptions[1]) {
      //Navigate to Create Group
      Navigator.of(context).pushNamed("/CreateGroup");
    }
  }

  void getGroupCoverFromGallery() async {
    var _rand = Random();
    final pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
    if(pickedFile!=null) {
      debugPrint("pickedFile: " + pickedFile.path);
      setState((){
        group.cover = pickedFile.path;
      });
    }
  }

  void getGroupImageFromGallery() async {
    var _rand = Random();
    final pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
    if(pickedFile!=null) {
      debugPrint("pickedFile: " + pickedFile.path);
      setState((){
        group.image = pickedFile.path;
      });
    }
  }

  createGroupNow() async {
    bool error = false;
    var errorText = "";
    if(nameController.text != null && nameController.text.length > 0) {
      if(descriptionController.text != null && descriptionController.text.length > 0) {
        if(group.image != null && group.image.length>0) {
          //Go for create
          group.name = nameController.text;
          group.description = descriptionController.text;
          setState((){isLoading = true; });
          Stream<Group> stream = await createGroup(group);
          stream.listen(
                  (_group){
                    if(_group!=null) {
                      showToast("Group Created Successfully");
                      Navigator.of(context).pop(true);
                    }
                  },
              onError: (e){
                setState((){isLoading = false; });
                    print(e);
              },
              onDone: (){
                setState((){isLoading = false; });});

        } else {
          error = true;
          errorText = "You must select a group image";
        }
      } else {
        error = true;
        errorText = "Group description must be specified";
      }
    } else {
      error = true;
      errorText = "Group name can't be empty";
    }

    if(error) {
      showToast(errorText);
    }
  }

  bool joinDialog = false;
  bool leaveDialog = false;

  joinGroupNow(Group _group) async {
    if(await isInternetConnection()) {
      setState(() {
        isLoading = true;
      });
      joinGroup(_group).then((value) {
        if (value != null) {
          setState(() {
            this.group = value;
            if(this.groups != null && this.groups.length > 0) {
              for(int i = 0; i<groups.length; i++) {
                if(groups[i].id.toString() == value.id.toString()) {
                  groups[i] = value;
                }
              }
            }
          });
        }
        setState(() {
          isLoading = false;
        });
      }).catchError((e) {
        setState(() {
          isLoading = false;
        });
      });
    }
  }

  leaveGroupNow(Group _group) async {
    if(await isInternetConnection()) {
      setState(() {
        isLoading = true;
      });
      leaveGroup(_group).then((value) {
        if (value != null) {
          setState(() {
            this.group = value;
          });
        }
        setState(() {
          isLoading = false;
        });
      }).catchError((e) {
        setState(() {
          isLoading = false;
        });
      });
    }
  }

  removeGroupMember(User _user) async {
    if(await isInternetConnection()) {
      setState(() {
        isLoading = true;
      });
      removeMember(_user, this.group).then((_removed) {
        if(_removed) {
          for(int i = 0; i< groupMembers.length; i++) {
            if(groupMembers[i].id.toString() == _user.id.toString()) {
              setState((){
                groupMembers.removeAt(i);
              });
            }
          }
        }
        setState(() {
          isLoading = false;
        });
      }).catchError((e){
        print(e);
      });
    }
  }

  getGroupPosts() async {
    final Stream<Post> stream = await getPostsOfGroup(this.group);
    setState((){
      posts.clear();
    });
    stream.listen((Post _post) {
      setState(() {
        posts.add(_post);
      });
    }, onError: (a) {
      print(a);
      scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Error retrieving posts'),));
    },
        onDone: () {});
  }


  void getCameraImage() async {
    var _rand = Random();
    final pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    if(pickedFile!=null) {
      debugPrint("pickedFile: " + pickedFile.path);
      setState((){
        if(post.mediaList == null) {
          post.mediaList = <Media>[];
        }
        post.mediaList.add(
            Media(identifier: pickedFile.path,  type: 0,
                name: "Image_${_rand.nextInt(100000)}")
        );
      });
      // Auth().uploadImage(File(pickedFile.path));
    }
  }

  void multipleImagePick() async {
    List<Asset> resultList;
    String error;

    try {
      resultList = await MultiImagePicker.pickImages(
          maxImages: 10,
          enableCamera: true,
          selectedAssets: images ?? List<Asset>(),
          cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
          materialOptions: MaterialOptions(
            statusBarColor: "#25d366",
            actionBarColor: "#25d366",
            actionBarTitle: "Pick image",
            allViewTitle: "All Photos",
            useDetailsView: false,
            selectCircleStrokeColor: "#000000",
          ));
    } on Exception catch (e) {
      error = e.toString();
    }
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.

    if(resultList != null) {
      setState(() {
        images = resultList;
        post.mediaList = <Media>[];
        post.mediaList.clear();
        if (error == null) _error = 'No Error Dectected';
      });
    }
    if(images!=null) {
      images.forEach((e) async {
        var path = await FlutterAbsolutePath.getAbsolutePath(e.identifier);
        setState(() {
          post.mediaList.add(Media(name: e.name,  type: 0, identifier: path));
        });
      });
    }
  }

  void getVideoFile() async {
    var _rand = Random();
    final result = await FilePicker.platform.pickFiles(type: FileType.video);
    if(result != null) {
      debugPrint("pickedVideo: " + result.files.single.path);
      setState((){
        if(post.mediaList == null) {
          post.mediaList = <Media>[];
        }
        post.mediaList.add(
            Media(identifier: result.files.single.path, type: 1,
                name: "Video_${_rand.nextInt(100000)}")
        );
      });
    }
  }

  void uploadPost(Post _myPost) async {
    final Stream<Post> stream = await addNewGroupPost(_myPost, this.group);
    stream.listen((Post _post) {
      setState(() => posts.add(_post));
    }, onError: (a) {
      print(a);
      scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text('Error uploading post'),
      ));
    }, onDone: () {
      // getPosts();
    });

  }


  void postLike(Post post) async {
    Comment data = Comment(postId: post.id);
    likePost(data).then((value) {
      if (value != null) {
        for(int i=0; i<posts.length; i++) {
          if(posts[i].id == value.id) {
            setState(() {
              posts[i] = value;
            });
          }
        }
      }
    });
  }

  onPostOptions(var value, Post _post) {
    if(value == options[0].option) {
      //Save Post
      setState((){isLoading = true;});
      savePost(_post).then((savedPost){
        setState((){isLoading = false;});
        if(savedPost != null) {
          showToast("Post saved");
        }
      });
    } else if(value == myPostOptions[1].option) {
      //Edit Post
      Navigator.of(context).pushNamed("/EditPost", arguments: RouteArgument(post: _post, currentUser: this.currentUser));
    } else if(value == myPostOptions[2].option) {
      //Delete Post
      deletePostNow(_post);
    }
  }

  deletePostNow(Post _post) async {
    deletePost(_post)
        .then((_isDeleted) {
      if(_isDeleted) {
        for(int i=0; i<posts.length; i++) {
          if(posts[i].id.toString() == _post.id.toString()) {
            posts.removeAt(i);
          }
        }
      }
    }, onError: (e){
      print(e);
    });
  }

}