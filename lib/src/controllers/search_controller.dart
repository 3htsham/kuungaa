import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/models/comment.dart';
import 'package:social_app/src/models/group.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/post_options.dart';
import 'package:social_app/src/models/search.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/group_repository.dart';
import 'package:social_app/src/repositories/post_repository.dart';
import 'package:social_app/src/repositories/settings_repository.dart';
import 'package:social_app/src/repositories/search_repository.dart'
    as searchRepo;
import 'package:social_app/src/repositories/user_repository.dart';
import 'package:social_app/generated/i18n.dart';

class SearchController extends ControllerMVC {
  GlobalKey<ScaffoldState> scaffoldKey;
  User currentUser;
  TextEditingController searchController;

  List<String> showCategories = <String>[];
  var current = "";
  bool showAll = true;
  bool showPosts = false;
  bool showGroups = false;
  bool showUsers = false;

  Search searchResults = Search();
  bool isSearched = false;
  bool isLoading = false;
  bool isShareLoading = false;


  List<PostOptions> options = <PostOptions>[
    PostOptions(option: "Save Post", icon: "assets/icons/save.png")
  ];

  SearchController() {
    showCategories.add("All");
    showCategories.add(S().posts);
    showCategories.add(S().groups);
    showCategories.add(S().people);

    current = showCategories[0];
    this.searchController = TextEditingController();
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  searchNow(String text) async {
    setState(() {
      isLoading = true;
      isSearched = false;
      this.searchResults = Search();
    });
    if (await isInternetConnection()) {
      searchRepo.search(text).then((value) {
        if (value != null) {
          setState(() {
            this.searchResults = value;
          });
        }
        setState(() {
          isLoading = false;
          isSearched = true;
        });
      }, onError: (e) {
        print(e);
        setState(() {
          isLoading = false;
          isSearched = true;
        });
      });
    } else {
      scaffoldKey.currentState.showSnackBar(
          SnackBar(content: Text("Verify your internet connection")));
    }
  }

  void sendOrCancelFriendRequest(User user) async {
    bool processed = true;
    String msg = "";
    if(user.friendRequestStatus==0)
    {
      processed = await sendFriendRequest(user);
    }
    else {
      //Cancel friend request
    }
    if(!processed) //error = true
        {
      searchResults.users.forEach((element) {
        if(element.id == user.id) {
          setState((){
            element.friendRequestStatus = 1;
          });
        }
      });
    }
  }

  void postLike(Post post) async {
    Comment data = Comment(postId: post.id);
    likePost(data).then((value) {
      if (value != null) {
        for(int i=0; i<searchResults.posts.length; i++) {
          if(searchResults.posts[i].id == value.id) {
            setState(() {
              searchResults.posts[i] = value;
            });
          }
        }
      }
    });
  }

  void sharePostNow(Post post) async {
    if(await isInternetConnection()) {
      setState((){isShareLoading = true;});
      sharePost(post).then((value) {
        if (value != null) {
          showToast("Post Shared");
        } else {
          showToast("Unable to share Post");
        }
        setState((){isShareLoading = false;});
      }, onError: (e) {
        print(e);
        setState((){isShareLoading = false;});
      });
    }
  }

  onPostOptions(var value, Post _post) {
    if(value == options[0].option) {
      //Save Post
      setState((){isLoading = true;});
      savePost(_post).then((savedPost){
        setState((){isLoading = false;});
        if(savedPost != null) {
          showToast("Post saved");
        }
      });
    }
  }

  changeShowCategory(var value) {
    setState((){
      current = value;
    });
  }

  joinGroupNow(Group _group) async {
    if(await isInternetConnection()) {
      setState(() {
        isLoading = true;
      });
      joinGroup(_group).then((value) {
        if (value != null) {
          setState(() {
            if(this.searchResults.groups != null && this.searchResults.groups.length > 0) {
              for(int i = 0; i<this.searchResults.groups.length; i++) {
                if(this.searchResults.groups[i].id.toString() == value.id.toString()) {
                  this.searchResults.groups[i] = value;
                }
              }
            }
          });
        }
        setState(() {
          isLoading = false;
        });
      }).catchError((e) {
        setState(() {
          isLoading = false;
        });
      });
    }
  }


}
