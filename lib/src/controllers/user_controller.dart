import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/models/gender_enum.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/user_repository.dart' as userRepo;
import 'package:social_app/src/repositories/settings_repository.dart' as settingsRepo;
import 'package:social_app/src/repositories/user_repository.dart';

class UserController extends ControllerMVC {

  User user;
  GenderEnum selectedGender = GenderEnum.male;
  User currentUser;
  List<User> friends = <User>[];

  bool hidePassword = true;
  bool isLoading = false;

  GlobalKey<ScaffoldState> scaffoldKey;
  GlobalKey<FormState> formKey;

  UserController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    this.formKey = GlobalKey<FormState>();
    user = User();
    user.gender = "male";
  }

  loginUser() async {
    if(formKey.currentState.validate()){
      formKey.currentState.save();
      if(await  settingsRepo.isInternetConnection()) {
        setState(() {
          isLoading = true;
        });
        userRepo.login(user).then((value) {
          setState((){ isLoading = false; });
          if(value != null && value.apiToken != null) {
            Navigator.of(scaffoldKey.currentContext).pushReplacementNamed('/Pages', arguments: 0);
          }
          else {
              scaffoldKey.currentState.showSnackBar(SnackBar(
                content: Text(S.of(context).wrong_email_or_password),
              ));
            }
        });
      }
      else
        {
          scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(S.of(context).verify_internet_connection),));
        }
    }
  }

  register() async {
    if(formKey.currentState.validate()){
      formKey.currentState.save();
      if(await  settingsRepo.isInternetConnection()) {
        setState(() {
          isLoading = true;
        });
        userRepo.register(user).then((value) {
          setState((){ isLoading = false; });
          if(value != null && value.apiToken != null) {
            Navigator.of(scaffoldKey.currentContext).pushReplacementNamed('/Pages', arguments: 0);
          }
          else {
            scaffoldKey.currentState.showSnackBar(SnackBar(
              content: Text(S.of(context).wrong_email_or_password),
            ));
          }
        });
      }
      else {
        scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(S.of(context).verify_internet_connection),));
      }
    }
  }


  bool isValidEmail(var email){
    bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);
    return emailValid;
  }


  getCurrentUserAndFriends() async {
    User _user = await getCurrentUser();
    setState((){
      this.currentUser = _user;
    });
    getFriendsNow();
  }

  getFriendsNow() async {
    if(user.id == currentUser.id) {
      Stream stream = await userRepo.getMyFriendsList();
      stream.listen((event) {
        if(event != null) {
          setState((){
            friends.add(event);
          });
        }
      },
          onError: (e) {
        print(e);
          },
          onDone: () {});
    }
    else {
      //Get Friends by user ID
      Stream stream = await userRepo.getFriendsListByUserId(this.user);
      stream.listen((event) {
        if(event != null) {
          setState((){
            friends.add(event);
          });
        }
      },
          onError: (e) {
            print(e);
          },
          onDone: () {});
    }
  }


}