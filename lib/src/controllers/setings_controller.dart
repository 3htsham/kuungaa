import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/user_repository.dart';

class SettingsController extends ControllerMVC {

  GlobalKey<ScaffoldState> scaffoldKey;
  User currentUser;

  SettingsController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }


  getCurrentUserNow() async {
    User user = await getCurrentUser();
    setState((){
      this.currentUser = user;
    });
  }

}