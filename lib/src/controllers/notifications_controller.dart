import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/models/add_group_notification.dart';
import 'package:social_app/src/models/friend_request.dart';
import 'package:social_app/src/models/group.dart';
import 'package:social_app/src/models/my_notification.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/settings_repository.dart';
import 'package:social_app/src/repositories/user_repository.dart';

class NotificationsController extends ControllerMVC {

  GlobalKey<ScaffoldState> scaffoldKey;

  List<MyNotification> notifications = <MyNotification>[];
  List<FriendRequest> friendRequests = <FriendRequest>[];
  int id = 0;

  bool isLoading = false;

  NotificationsController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void addNotifications(){
    setState((){

      ///Accepted Your Friend Request
      notifications.add(MyNotification(id: 2, status: 1, type: 2, dateTime: DateTime.parse("20180626T170555"),
          user: User(
              name: "Shawn Mendes",
              location: "Konia",
              image: "https://i.pinimg.com/474x/8b/5c/2f/8b5c2f9f15b0123aca0f74c568f4d34d.jpg"),
          friendRequestNotification: FriendRequest(type: 1, friendRequestStatus: 0)
      ));

    ///Liked Your Post
    notifications.add(MyNotification(id: 0, status: 0, type: 0, dateTime: DateTime.parse("20180626T170555"),
        user: User(
        name: "Elizabeth",
        location: "Kutul Amare",
        image: "https://ankushtiwari.files.wordpress.com/2015/07/elizabeth-holmes.jpg?w=593"),
        post: Post(caption: "Just bought new 🥰",
            image: "https://www.setaswall.com/wp-content/uploads/2018/03/Lamborghini-Wallpaper-04-1680x1050.jpg",
            totalLikes: 14,
            totalComment: 31,
            dateTime: DateTime.parse("20180626T170555"),
        )));

    ///Sent you a Friend Request
    notifications.add(MyNotification(id: 1, status: 0, type: 2, dateTime: DateTime.parse("20180626T170555"),
        user: User(
            name: "Zain",
            location: "Bangalore",
            image: "https://srivideo.net/uploads/posts/2017-12/1514476280_zain-imam.jpg"),
            friendRequestNotification: FriendRequest(type: 0, friendRequestStatus: 1)
        ));

    ///Commented On Your Post
    notifications.add(MyNotification(id: 3, status: 0, type: 1, dateTime: DateTime.parse("20180626T170555"),
        user: User(
            name: "Shawn Mendes",
            location: "Konia",
            image: "https://i.pinimg.com/474x/8b/5c/2f/8b5c2f9f15b0123aca0f74c568f4d34d.jpg"),
        post: Post(caption: "Thank you for existing deano ❤️\n Good bye 💔",
            image: "https://d2r2ijn7njrktv.cloudfront.net/apnlive/uploads/2020/09/24171413/dean-jones-new-min-1.jpeg",
            totalLikes: 43,
            totalComment: 71,
            dateTime: DateTime.parse("20180626T170555"),
        )));

      ///Added you to the group
      notifications.add(MyNotification(id: 4, status: 1, type: 4, dateTime: DateTime.parse("20180626T170555"),
          user: User(
              name: "Zain",
              location: "Bangalore",
              image: "https://srivideo.net/uploads/posts/2017-12/1514476280_zain-imam.jpg"),
          group: Group(id: 2, name: "Freelancers",
              joined: DateTime.parse("20191226T170555"),
              description: "Find & hire top freelancers, web developers & designers inexpensively. World's largest marketplace of 43m. Receive quotes in seconds",
              image: "https://public-media.interaction-design.org/images/ux-daily/557191c260b50957a997f2e3f671dfbb646fb0348ff92.jpg"),
          addGroupNotification: AddGroupNotification(status: 0, type: 1)
      ));

      ///Sent you a Friend Request and Accepted
      notifications.add(MyNotification(id: 1, status: 0, type: 2, dateTime: DateTime.parse("20180626T170555"),
          user: User(
              name: "Paul Molive",
              location: "California",
              image: "https://i.pinimg.com/originals/4d/81/7d/4d817df74af7b438170dd3ec3cb7ab0c.png"),
          friendRequestNotification: FriendRequest(type: 0, friendRequestStatus: 0)
      ));



    ///Follows You
    notifications.add(MyNotification(id: 5, status: 1, type: 3, dateTime: DateTime.parse("20180626T170555"),
        user: User(
            name: "Anna Mull",
            location: "New York",
            image: "https://freepikpsd.com/wp-content/uploads/2019/09/Cute-Girl-Picture-for-Profile-18.png"),
    ));

    /// Your Request to Join Group was accepted
    notifications.add(MyNotification(id: 6, status: 1, type: 4, dateTime: DateTime.parse("20180626T170555"),
        group: Group(id: 6, name: "Wowonder Modifications",
            joined: DateTime.parse("20191226T170555"),
            description: "Wowonder is one of the Famous PHP Social Network. If you need any customization or required any new features.",
            image: "https://appscodes.xyz/wp-content/uploads/2020/07/WoWonder-Social-Network-.png"),
        addGroupNotification: AddGroupNotification(status: 0, type: 0)
    ));

    });
  }

  void getMyFriendRequests() async {
    Stream<FriendRequest> stream = await getFriendRequestsList();
    stream.listen((_friendRequest) {
      setState((){
        friendRequests.add(_friendRequest);
        var format = DateFormat('yyyy-MM-ddTHH:mm:ss.SSSSSSZ');
        var dateTime = format.parse(_friendRequest.createdAt);

        notifications.add(MyNotification(id: id, status: 0, type: 2, dateTime: dateTime,
            user: _friendRequest.sender,
            friendRequestNotification: _friendRequest
        ));

        id++;

      });
    },
        onError: (e){
      print(e);
        },
        onDone: (){});
  }

  void acceptFriendRequestNow(MyNotification notification) async {
    if(await isInternetConnection()) {
      setState((){ isLoading = true; });
      acceptFriendRequest(notification.friendRequestNotification).then((value) {
          setState((){
            if(!value) { //If is not error
              notification.friendRequestNotification.friendRequestStatus = 0;
            }
            isLoading = false;
          });
      });
    }
  }

  void rejectFriendRequestNow(MyNotification notification) async {
    if(await isInternetConnection()) {
      setState((){ isLoading = true; });
      rejectFriendRequest(notification.friendRequestNotification).then((value) {
        setState((){
          if(!value) { //If is not error
            notification.friendRequestNotification.friendRequestStatus = 2;
          }
          isLoading = false;
        });
      });
    }
  }


}