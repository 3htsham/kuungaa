import 'dart:math';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/config/firestore_config.dart';
import 'package:social_app/config/my_storage.dart';
import 'package:social_app/src/models/status.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/settings_repository.dart';

class StatusController extends ControllerMVC {

  GlobalKey<ScaffoldState> scaffoldKey;

  User currentUser;
  List<Status> statuses = <Status>[];
  String pickedImage = "";
  bool isLoading = false;

  Status status;

  StatusController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
  }

  void getGalleryImage() async {
    var _rand = Random();
    final pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
    if(pickedFile!=null) {
      debugPrint("pickedFile: " + pickedFile.path);
      setState((){
        pickedImage = pickedFile.path;
      });
      // Auth().uploadImage(File(pickedFile.path));
    }
  }

  void clearImage() {
    setState((){
      pickedImage = "";
    });
  }

  void uploadStatus() async {
    if(pickedImage != null && pickedImage.length > 2) {
      setState((){isLoading = true;});
      if(await isInternetConnection()) {
        String _name = DateTime.now().millisecondsSinceEpoch.toString() + ".jpg";
        Stream<TaskSnapshot> stream = MyStorage.uploadImage(pickedImage, _name);
        var dowurl = "";
        stream.listen((event) async {
          if (event.state == TaskState.success) {
            dowurl = await event.ref.getDownloadURL();
            var imageName = await event.ref.getData();
            Status _status = Status();
            _status.id = DateTime.now().millisecondsSinceEpoch.toString();
            _status.hasMedia = true;
            _status.imageLink = dowurl.toString();
            _status.imageName = _name;
            _status.isImage = true;
            _status.ownerId = this.currentUser.id.toString();
            _status.seenBy = [];
            _status.text = "";
            _status.uploadedAt = DateTime.now().millisecondsSinceEpoch;
            _status.user = this.currentUser;
            Map<String, String> _user = Map<String, String>();
            _user['id'] = this.currentUser.id.toString();
            _user['image'] = this.currentUser.image.toString() ?? "";
            _user['name'] = this.currentUser.name;

            _status.videoLink = "";

            Map<String, dynamic> _statusMap = _status.toMap();
            _statusMap['user'] = _user;

            print(_statusMap);
            MyFirestore.createStatus(_status.id, _statusMap).then((value) {
                showToast("Status Uploaded");
                setState((){isLoading = false;
                if(this.statuses != null) {
                  statuses.add(_status);
                }
              });
                Navigator.of(context).pop(_status);
            });
          }
        }, onError: (e) {
          print(e);
          setState((){isLoading = false;});
        }, onDone: () {
          setState((){isLoading = false;});
        });
      } else {
        scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Verify your internet connection")));
      }
    }
  }

  void addSeen(){
    if(!this.status.seenBy.contains(currentUser.id.toString()) && this.status.ownerId != currentUser.id.toString()) {
      setState(() {
        this.status.seenBy.add(currentUser.id.toString());
      });
      MyFirestore.markSeen(this.status.id, this.status.seenBy);
    }
  }

  void deleteStatus(Status _status) async {
    if(await isInternetConnection()) {
      MyFirestore.deleteAStatus(_status.id).delete().then((value) {
        MyStorage.deletePhoto(_status.imageName);
        for(int i = 0; i<statuses.length; i++) {
          if(statuses[i].id == _status.id) {
            setState((){
              statuses.removeAt(i);
            });
          }
        }
      },
          onError: (e) {
        print(e);
          });
    }
  }

}