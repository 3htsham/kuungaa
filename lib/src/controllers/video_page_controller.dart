import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/elements/video/single_video_view.dart';
import 'package:social_app/src/models/comment.dart';
import 'package:social_app/src/models/media.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/post_options.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/models/video.dart';
import 'package:social_app/src/repositories/post_repository.dart';
import 'package:social_app/src/repositories/settings_repository.dart';
import 'package:social_app/src/repositories/user_repository.dart';
import 'package:social_app/src/repositories/videos_repository.dart' as videoRepo;

class VideoPageController extends ControllerMVC {

  User currentUser;

  GlobalKey<ScaffoldState> scaffoldKey;
  GlobalKey<SingleVideoViewState> currentVideoViewKey;
  ScrollController scrollController;

  List<Post> posts = <Post>[];
  Post currentPost;

  List<Video> suggestedVideos = <Video>[];
  List<Video> relatedVideos = <Video>[];
  Video currentVideo;

  bool isSharePostDialog = false;
  Post postToShare;
  PostOptions currentPrivacyOption;
  List<PostOptions> newPostPrivacyOptions = <PostOptions>[
    PostOptions(option: "Public", icon: "assets/icons/public.png"),
    PostOptions(option: "Friends", icon: "assets/icons/profile.png"),
    PostOptions(option: "Private", icon: "assets/icons/lock.png"),
  ];


  bool isLoading = false;
  bool loadingComment = false;

  List<PostOptions> options = <PostOptions>[
    PostOptions(option: "Save Post", icon: "assets/icons/save.png")
  ];


  VideoPageController() {
    this.scaffoldKey = new GlobalKey<ScaffoldState>();
    this.currentVideoViewKey = new GlobalKey<SingleVideoViewState>();
    scrollController = ScrollController();
  }

  getUser() async {
    User user = await getCurrentUser();
    setState((){
      this.currentUser = user;
    });
  }

  void setCurrentVideo(Post post) {
    setState(() {
      currentPost = post;
    });
    currentVideoViewKey.currentState.updateLink(post.media.mediaUrl);
  }

  void playSuggestedVideo(Post post) {
    setCurrentVideo(post);
    scrollController.animateTo(0, duration: Duration(milliseconds: 1000), curve: Curves.ease);
  }

  void playThisVideo(Video _videoPost){
    setState((){
      currentVideo = _videoPost;
      currentVideoViewKey.currentState.updateLink(_videoPost.file.toString().contains(
          RegExp(
              r"(.mkv|.mp4|.webm|.ogv|.m3u|.m3u8)",
              caseSensitive: false))
          ? _videoPost.file
          : _videoPost.file + ".mp4" );
    });
  }

  void getRelatedVideos() async {
    Stream<Video> stream = await videoRepo.getVideos();
    stream.listen((_video) {
      setState(() {
        relatedVideos.add(_video);
      });
    }, onError: (e) {
      print(e);
      scaffoldKey.currentState.showSnackBar(
          SnackBar(content: Text("Verify your internet connection")));
    }, onDone: () {
      if(relatedVideos.length > 0) {
        currentVideo = relatedVideos[0];
      }
    });
  }

  void getSuggestedVideos() async {
    Stream<Video> stream = await videoRepo.getVideos();
    stream.listen((_video) {
      setState(() {
        suggestedVideos.add(_video);
      });
    }, onError: (e) {
      print(e);
      scaffoldKey.currentState.showSnackBar(
          SnackBar(content: Text("Verify your internet connection")));
    }, onDone: () {});
  }

  onPostOptions(var value, Post _post) {
    if(value == options[0].option) {
      //Save Post
      setState((){isLoading = true;});
      savePost(_post).then((savedPost){
        setState((){isLoading = false;});
        if(savedPost != null) {
          showToast("Post saved");
        }
      });
    }
  }

  void postLike(Post post) async {
    Comment data = Comment(postId: post.id);
    likePost(data).then((value) {
      if (value != null) {
        setState((){
          currentVideo.post.isLiked = value.isLiked;
          currentVideo.post.totalLikes = value.totalLikes;
        });
      }
    });
  }


  void closeShareDialog(){
    setState((){
      isSharePostDialog = false;
    });
  }

  void showShareDialog(Post _post) {
    setState((){
      postToShare = _post;
      isSharePostDialog = true;
    });
  }

  void hideShareDialog(String privacy) {
    setState((){
      postToShare.postType = privacy.toLowerCase();
      isSharePostDialog = false;
    });
    sharePostNow(postToShare);
  }

  void sharePostNow(Post post) async {
    if(await isInternetConnection()) {
      setState((){isLoading = true;});
      sharePost(post).then((value) {
        if (value != null) {
          showToast("Post Shared");
        } else {
          showToast("Unable to share Post");
        }
        setState((){isLoading = false;});
      }, onError: (e) {
        print(e);
        setState((){isLoading = false;});
      });
    }
  }



}