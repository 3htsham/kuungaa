import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/controllers/controller.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';
import 'package:social_app/src/pages/main_pages/groups.dart';
import 'package:social_app/src/pages/main_pages/home.dart';
import 'package:social_app/src/pages/main_pages/notifications.dart';
import 'package:social_app/src/pages/main_pages/profile.dart';
import 'package:social_app/src/pages/main_pages/videos_page.dart';

class PagesWidget extends StatefulWidget {
  int currentTab;
  // Widget currentPage = OrdersWidget();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  PagesWidget({
    Key key,
    this.currentTab,
  }) {
    currentTab = currentTab != null ? currentTab : 1;
  }

  @override
  _PagesWidgetState createState() => _PagesWidgetState();
}

class _PagesWidgetState extends StateMVC<PagesWidget> with WidgetsBindingObserver {

  Controller _con;
  PageController _pageController;
  GlobalKey<VideosPageState> videoPageKey;
  GlobalKey<HomePageState> homePageKey;

  final List<Widget> _pages = [];

  _PagesWidgetState() : super(Controller()) {
    _con = controller;
  }

  @override
  initState() {
    _con.getCurrentUserNow();
    super.initState();
    videoPageKey = GlobalKey<VideosPageState>();
    homePageKey = GlobalKey<HomePageState>();
    _pageController = PageController();
    setState(() {
      // _pages.add(VideosPage(key: videoPageKey,));
      _pages.add(HomePage(key: homePageKey,));
      _pages.add(GroupsPage());
      _pages.add(VideosPage(key: videoPageKey,));
      _pages.add(NotificationsPage());
      _pages.add(ProfilePage());
    });
    Future.delayed(Duration.zero, () {
      _selectPage(widget.currentTab);
    });
    _con.initFCM();

    WidgetsBinding.instance.addObserver(this);
  }



  void _onPageChanged(int index) {
    setState(() {
      widget.currentTab = index;
    });
    if(videoPageKey.currentState != null) {
      videoPageKey.currentState.pauseVideo();
    }
  }

  void _selectPage(int index) {
    setState(() {
      widget.currentTab = index;
    });
    if(videoPageKey.currentState != null) {
      videoPageKey.currentState.pauseVideo();
    }
    _pageController.jumpToPage(index);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if(homePageKey.currentState.con.isSharePostDialog) {
            homePageKey.currentState.con.closeShareDialog();
        }
        if(videoPageKey.currentState.con.isSharePostDialog) {
          videoPageKey.currentState.con.closeShareDialog();
        }
        return false;
      },
        child: PickupCallLayout(
          currentUser: _con.currentUser,
          scaffold: Scaffold(
            key: widget.scaffoldKey,
            body: PageView(
              children: _pages,
              controller: _pageController,
              onPageChanged: _onPageChanged,
              physics: NeverScrollableScrollPhysics()
            ),
            bottomNavigationBar: CurvedNavigationBar(
              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              color: Theme.of(context).accentColor,
              height: 50,
              animationDuration: Duration(milliseconds: 300),
              index: widget.currentTab,
              buttonBackgroundColor: Theme.of(context).accentColor,
              items: [
                ImageIcon(AssetImage("assets/icons/home.png"), color: Colors.white,),
                ImageIcon(AssetImage("assets/icons/groups.png"), color: Colors.white,),
                ImageIcon(AssetImage("assets/icons/video.png"), color: Colors.white,),
                ImageIcon(AssetImage("assets/icons/bell.png"), color: Colors.white,),
                ImageIcon(AssetImage("assets/icons/profile.png"), color: Colors.white,),
              ],
              onTap: (index){
                _selectPage(index);
              },
            ),
          ),
        )
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    //User's Online and offline status can be handled here
    switch(state){
      case AppLifecycleState.resumed:
        // TODO: Handle this case.
        break;
      case AppLifecycleState.inactive:
        // TODO: Handle this case.
        break;
      case AppLifecycleState.paused:
        // TODO: Handle this case.
        break;
      case AppLifecycleState.detached:
        // TODO: Handle this case.
        break;
    }
  }

}
