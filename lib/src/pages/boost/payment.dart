import 'package:flutter/material.dart';
import 'package:mpesa_flutter_plugin/mpesa_flutter_plugin.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/config/mpesa.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/boost_controller.dart';
import 'package:social_app/src/elements/boost/package_list_item.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class PaymentWidget extends StatefulWidget {
  RouteArgument argument;
  PaymentWidget({this.argument});

  @override
  _PaymentWidgetState createState() => _PaymentWidgetState();
}

class _PaymentWidgetState extends StateMVC<PaymentWidget> {

  BoostController _con;

  _PaymentWidgetState() : super(BoostController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.post = widget.argument.post;
    _con.package = widget.argument.package;
    _con.mpesa = widget.argument.mpesa;
    MpesaFlutterPlugin.setConsumerKey(_con.mpesa.CONSUMER_KEY);
    MpesaFlutterPlugin.setConsumerSecret(_con.mpesa.CONSUMER_SECRET);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () async {
        if(_con.isLoading) {
          return false;
        } else {
          return true;
        }
      },
      child: PickupCallLayout(
        currentUser: widget.argument.currentUser,
        scaffold: Scaffold(
          key: _con.scaffoldKey,
          appBar: AppBar(
            title: Text(S.of(context).make_payment,
              style: Theme.of(context).textTheme.headline4
                  .merge(TextStyle(color: Theme.of(context).accentColor)),
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
            centerTitle: true,
          ),
          body: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: Center(
                        child: Text(
                          S.of(context).mpesa_will_be_used,
                          style: textTheme.bodyText1,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            S.of(context).your_package,
                            style: textTheme.caption,
                            textAlign: TextAlign.center,
                          ),
                          Opacity(
                            opacity: 0.8,
                              child: PackageListItem(package: _con.package, onTap: (){},)
                          ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            S.of(context).your_phone_number,
                            style: textTheme.subtitle2,
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            'e.g. 254710529574',
                            style: textTheme.caption,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(height: 15,),
                          TextFormField(
                            controller: _con.textController,
                            keyboardType: TextInputType.phone,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5)
                              ),
                              hintText: "Phone Number",
                              labelText: "Phone number",
                              prefixIcon: Icon(Icons.phone,)
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 15,),

                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: Container(
                        alignment: Alignment.center,
                        width: double.infinity,
                        child: OutlineButton(
                          borderSide: BorderSide(color: Theme.of(context).accentColor, width: 2,),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
                          textColor: Theme.of(context).accentColor,
                          highlightedBorderColor: Theme.of(context).accentColor,
                          textTheme: ButtonTextTheme.accent,
                          onPressed: _con.makePayment,
                          child: Text(S.of(context).process, style: Theme.of(context).textTheme.subtitle2.merge(
                              TextStyle(color: Theme.of(context).accentColor, letterSpacing: 1)
                          )),
                          color: Theme.of(context).accentColor,
                          padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width/3.6, vertical: 10),
                        ),
                      ),
                    )

                  ],
                ),
              ),

              ///Loader
              _con.isLoading
                  ? Container(
                color: Colors.black.withOpacity(0.5),
                child: Center(
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(15)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(),
                        SizedBox(height: 10),
                        Text("Please wait...",
                            style: Theme.of(context).textTheme.bodyText2)
                      ],
                    ),
                  ),
                ),
              )
                  : Positioned(bottom: 10, child: SizedBox(height: 0)),
            ],
          ),
        ),
      ),
    );
  }
}
