import 'package:flutter/material.dart';
import 'package:inview_notifier_list/inview_notifier_list.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/home_controller.dart';
import 'package:social_app/src/elements/boost/boosted_post_widget_item.dart';
import 'package:social_app/src/elements/bottomsheet/post_comment_newsfeed.dart';
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class ViewMyCampaignsWidget extends StatefulWidget {
  RouteArgument argument;
  ViewMyCampaignsWidget({this.argument});

  @override
  _ViewMyCampaignsWidgetState createState() => _ViewMyCampaignsWidgetState();
}

class _ViewMyCampaignsWidgetState extends StateMVC<ViewMyCampaignsWidget> {

  HomeController _con;

  _ViewMyCampaignsWidgetState() : super(HomeController()) {
     _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    super.initState();
    _con.getMySponsoredPosts();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return PickupCallLayout(
      currentUser: widget.argument.currentUser,
      scaffold: Scaffold(
        key: _con.scaffoldKey,
        appBar: AppBar(
          title: Text(S.of(context).my_campaigns,
            style: Theme.of(context).textTheme.headline4
                .merge(TextStyle(color: Theme.of(context).accentColor)),
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
          centerTitle: true,
        ),
        body: InViewNotifierList(
          isInViewPortCondition: (double deltaTop, double deltaBottom, double vpHeight) {
            return (deltaTop < (0.5 * vpHeight) + 100.0 &&
                deltaBottom > (0.5 * vpHeight) - 100.0);
          },
          itemCount: 1,
          shrinkWrap: true,
          primary: false,
          builder: (BuildContext context, int index){
            return SingleChildScrollView(
              child: Column(
                children: [
                  _con.sponsoredPosts.isEmpty
                      ? CircularLoadingWidget(height: 100,)
                      : ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    itemCount: _con.sponsoredPosts.length,
                    itemBuilder: (context, index) {
                      return InViewNotifierWidget(
                          id: _con.sponsoredPosts[index].id.toString(),
                          builder: (BuildContext context, bool isInView, Widget child){

                            Post _post = _con.sponsoredPosts[index];

                            return BoostedPostWidgetItem(
                              post: _post,
                              options: _con.sponsoredPostOptions,
                              isInView: isInView,
                              likeTap: () {
                                _con.postLike(_post);
                              },
                              commentTap: () {
                                this.postCommentsBottomsheet(context, _post);
                              },
                              shareTap: () {
                                _con.sharePostNow(_post);
                              },
                              onProfileTap: () {
                                Navigator.of(context).pushNamed(
                                    "/UserProfileDetails",
                                    arguments: RouteArgument(user: _post.user));
                              },
                              onOptionsTap: _con.onPostOptions,
                              onViewCount: (){

                              },
                            );
                          }
                      );
                    },
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }


  postCommentsBottomsheet(BuildContext context, Post post) {
    var theme = Theme.of(context);
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: theme.scaffoldBackgroundColor,
        context: context,
        builder: (ctx) {
          return GestureDetector(
            onVerticalDragStart: (_){},
            child: StatefulBuilder(
              builder: (ctx, setState) {
                return PostCommentsBottomSheet(_con.currentUser, post: post,);
              },
            ),
          );
        }
    ).then((value){
      if(value != null) {
        var post = value as Post;
        _con.posts.forEach((element) {
          if(element.id == post.id) {
            if(post.comments != null && post.comments.length > 0) {
              setState((){
                element.totalComment = post.comments.length;
                element.comments = post.comments;
              });
            }
          }
        });
      }
    });
  }

}
