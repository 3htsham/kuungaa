import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/boost_controller.dart';
import 'package:social_app/src/elements/boost/boosted_post_example.dart';
import 'package:social_app/src/elements/boost/package_list_item.dart';
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/elements/others/divider_line_widget.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class ViewBoostPostPackages extends StatefulWidget {

  RouteArgument argument;
  ViewBoostPostPackages({this.argument});

  @override
  _ViewBoostPostPackagesState createState() => _ViewBoostPostPackagesState();
}

class _ViewBoostPostPackagesState extends StateMVC<ViewBoostPostPackages> {

  BoostController _con;

  _ViewBoostPostPackagesState() : super(BoostController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.post = widget.argument.post;
    super.initState();
    _con.getPackages();
    _con.getCredentials();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return PickupCallLayout(
      currentUser: widget.argument.currentUser,
      scaffold: Scaffold(
        key: _con.scaffoldKey,
        appBar: AppBar(
          title: Text(S.of(context).boost_packages,
            style: Theme.of(context).textTheme.headline4
                .merge(TextStyle(color: Theme.of(context).accentColor)),
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: Center(
                  child: Text(
                    S.of(context).wait_for_packages,
                    style: textTheme.caption,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),

              _con.packages.isEmpty
                  ? CircularLoadingWidget(height: 100,)
                  : ListView.builder(
                primary: false,
                shrinkWrap: true,
                itemCount: _con.packages.length,
                  itemBuilder: (context, index) {
                    return PackageListItem(package: _con.packages[index],
                      onTap: (){
                        Navigator.of(context).pushNamed("/Payment",
                            arguments: RouteArgument(post: _con.post,
                            package: _con.packages[index],
                            mpesa: _con.mpesa,
                            currentUser: widget.argument.currentUser,
                            ));
                      },
                    );
                  }
              ),

              SizedBox(height: 10,),

              DividerLineWidget(text: S.of(context).only_you_can_see_this,),

              BoostedPostExample(post: _con.post,)

            ],
          ),
        ),
      ),
    );
  }
}
