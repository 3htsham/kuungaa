import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/controllers/ViewMediaController.dart';
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/elements/post/in_post_video_view.dart';

class ViewPostMediaWidget extends StatefulWidget {

  RouteArgument argument;

  ViewPostMediaWidget({this.argument});

  @override
  _ViewPostMediaWidgetState createState() => _ViewPostMediaWidgetState();
}

class _ViewPostMediaWidgetState extends StateMVC<ViewPostMediaWidget> {

  ViewMediaController _con;

  _ViewPostMediaWidgetState() : super(ViewMediaController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.post = widget.argument.post;
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      key: _con.scaffoldKey,
      backgroundColor: Colors.black87,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(""),
        centerTitle: true,
      ),
      body: Container(
        child: Center(
          child: _con.post == null || _con.post.mediaList.length < 1
              ? CircularLoadingWidget(height: 100,)
              : PageView.builder(
              controller: _con.controller,
              physics: PageScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemCount: _con.post.mediaList.length,
              itemBuilder: (context, index) {
                return Container(
                  width: MediaQuery.of(context).size.width,
                  child: _con.post.mediaList[index].type == 0
                      ? CachedNetworkImage(
                    imageUrl: _con.post.mediaList[index].file,
                  )
                      : Center(
                        child: Container(
                    height: size.width * 00.5625,
                    child: Center(
                        child: InPostVideoView(
                          _con.post.mediaList[index].file.toString().contains(RegExp(r"(.mkv|.mp4|.webm|.ogv|.m3u|.m3u8)", caseSensitive: false)) ?
                          _con.post.mediaList[index].file : _con.post.mediaList[index].file+".mp4",
                          isInView: _con.controller.page == index,
                          key: GlobalKey<InPostVideoViewState>(),
                        ),
                    ),
                  ),
                      ),
                  // CachedNetworkImage(
                  //   imageUrl: _con.post.mediaList[index].thumbnailUrl,
                  // ),
                );
              }
          ),
        ),
      ),
    );
  }
}
