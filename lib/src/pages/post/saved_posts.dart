import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/post_controller.dart';
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/elements/post/saved_post_item.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/models/saved_post.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class SavedPostsWidget extends StatefulWidget {
  RouteArgument argument;

  SavedPostsWidget({this.argument});

  @override
  _SavedPostsWidgetState createState() => _SavedPostsWidgetState();
}

class _SavedPostsWidgetState extends StateMVC<SavedPostsWidget> {

  PostController _con;

  _SavedPostsWidgetState() : super(PostController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    _con.getSavedPostsNow();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return PickupCallLayout(
      currentUser: _con.currentUser,
      scaffold: Scaffold(
        key: _con.scaffoldKey,
        appBar: AppBar(
          title: Text(S.of(context).saved,
            style: Theme.of(context).textTheme.headline4
                .merge(TextStyle(color: Theme.of(context).accentColor)),
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              _con.savedPosts.isEmpty
                  ? CircularLoadingWidget(height: 100,)
                  : ListView.builder(
                shrinkWrap: true,
                primary: false,
                itemCount: _con.savedPosts.length,
                  itemBuilder: (ctx, index) {
                  User owner = _con.savedPosts[index].post.user;
                    return SavedPostItem(
                      post: _con.savedPosts[index],
                      owner: owner,
                      options: _con.savedPostOptions,
                      onTap: (){
                          Navigator.of(context).pushNamed("/ViewPost", arguments: RouteArgument(user: owner, currentUser: _con.currentUser, post: _con.savedPosts[index].post));
                        },
                      onUserProfile: (){
                          Navigator.of(context).pushNamed("/UserProfileDetails", arguments: RouteArgument(user: owner));
                        },
                      onOptionsTap: _con.onSavedPostOptions,
                    );
              })
            ],
          ),
        ),
      ),
    );
  }
}
