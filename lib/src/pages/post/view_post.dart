import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/config/golbal_strings.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/post_controller.dart';
import 'package:social_app/src/elements/bottomsheet/post_comment_newsfeed.dart';
import 'package:social_app/src/elements/post/user_post_widget_item.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';


class ViewPostWidget extends StatefulWidget {

  RouteArgument arguments;

  ViewPostWidget({this.arguments});

  @override
  _ViewPostWidgetState createState() => _ViewPostWidgetState();
}

class _ViewPostWidgetState extends StateMVC<ViewPostWidget> {

  PostController _con;

  _ViewPostWidgetState() : super(PostController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.post = widget.arguments.post;
    _con.currentUser = widget.arguments.currentUser;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () async {
        if(_con.isSharePostDialog) {
          _con.closeShareDialog();
          return false;
        }
        return true;
      },
      child: PickupCallLayout(
        currentUser: _con.currentUser,
        scaffold: Scaffold(
          key: _con.scaffoldKey,
          appBar: AppBar(
            title: Text(S.of(context).post_details,
              style: Theme.of(context).textTheme.headline4
                  .merge(TextStyle(color: Theme.of(context).accentColor)),
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
            centerTitle: true,
          ),
          body: Stack(
            children: [
              SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: [
                    UserPostWidgetItem(
                      post: _con.post,
                      options: _con.post.userId.toString() == _con.currentUser.id.toString() ? _con.myPostOptions : _con.options,
                      likeTap: () {
                        _con.postLike(_con.post);
                      },
                      commentTap: () {
                        this.postCommentsBottomsheet(context, _con.post);
                      },
                      shareTap: () {
                        _con.showShareDialog(_con.post);
                      },
                      onProfileTap: () {
                        Navigator.of(context).pushNamed("/UserProfileDetails",
                            arguments: RouteArgument(user: _con.post.user));
                      },
                      onOptionsTap: _con.onPostOptions,
                      showShare: _con.post.postType == POST_FRIENDS || _con.post.postType == POST_PUBLIC
                    ),

                    SizedBox(height: 70,)
                  ],
                ),
              ),


              ///Loader
              _con.isLoading
                  ? Container(
                color: Colors.black.withOpacity(0.5),
                child: Center(
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(15)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(),
                        SizedBox(height: 10),
                        Text("Please wait...",
                            style: Theme.of(context).textTheme.bodyText2)
                      ],
                    ),
                  ),
                ),
              )
                  : Positioned(bottom: 10, child: SizedBox(height: 0)),


              _con.isSharePostDialog
                  ? sharePostDialog()
                  : Positioned(bottom: 10, child: SizedBox(height: 0)),

            ],
          ),
        ),
      ),
    );
  }


  postCommentsBottomsheet(BuildContext context, Post post) {
    var theme = Theme.of(context);
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: theme.scaffoldBackgroundColor,
        context: context,
        builder: (ctx) {
          return GestureDetector(
            onVerticalDragStart: (_){},
            child: StatefulBuilder(
              builder: (ctx, setState) {
                return PostCommentsBottomSheet(_con.currentUser, post: post,);
              },
            ),
          );
        }
    ).then((value){
      if(value != null) {
        var post = value as Post;
        if(post.comments != null && post.comments.length > 0) {
          setState((){
            _con.post.totalComment = post.comments.length;
            _con.post.comments = post.comments;
          });
        }
      }
    });
  }


  Widget sharePostDialog() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.black54,
        child: Center(
            child: Container(
                width: MediaQuery.of(context).size.width * 0.7,
                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(7),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(1, 1),
                          spreadRadius: 0.5,
                          blurRadius: 5,
                          color: Theme.of(context).focusColor.withOpacity(0.5)
                      )
                    ]
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text("Share Post with...", style: Theme.of(context).textTheme.subtitle1),
                    SizedBox(height: 10),
                    ListView.builder(
                        itemCount: _con.newPostPrivacyOptions.length,
                        shrinkWrap: true,
                        primary: false,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: (){
                              _con.hideShareDialog(_con.newPostPrivacyOptions[index].option);
                            },
                            child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                                child: Row(
                                    children:[
                                      ImageIcon(AssetImage(_con.newPostPrivacyOptions[index].icon)),
                                      SizedBox(width: 10,),
                                      Text(
                                        _con.newPostPrivacyOptions[index].option,
                                        style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(fontSize: 18)),
                                      )
                                    ]
                                )
                            ),
                          );
                        }
                    )
                  ],
                )
            )
        )
    );
  }

}
