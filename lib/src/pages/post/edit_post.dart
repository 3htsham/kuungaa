import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/edit_post_controller.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class EditPostWidget extends StatefulWidget {
  RouteArgument argument;

  EditPostWidget({this.argument});

  @override
  _EditPostWidgetState createState() => _EditPostWidgetState();
}

class _EditPostWidgetState extends StateMVC<EditPostWidget> {
  EditPostController _con;

  _EditPostWidgetState() : super(EditPostController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.post = widget.argument.post;
    super.initState();
    if (_con.post.text != null) {
      _con.textController.text = _con.post.text;
    }
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () async {
        if (_con.isLoading) {
          return false;
        } else {
          return true;
        }
      },
      child: PickupCallLayout(
        currentUser: widget.argument.currentUser,
        scaffold: Scaffold(
          key: _con.scaffoldKey,
          appBar: AppBar(
            title: Text(
              S.of(context).edit_post,
              style: Theme.of(context)
                  .textTheme
                  .headline4
                  .merge(TextStyle(color: Theme.of(context).accentColor)),
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
            centerTitle: true,
          ),
          body: Stack(
            children: [
              Container(
                height: size.height,
                width: size.width,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        height: 200,
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: SingleChildScrollView(
                          child: TextFormField(
                            keyboardType: TextInputType.multiline,
                            controller: _con.textController,
                            maxLines: null,
                            maxLength: null,
                            textInputAction: TextInputAction.newline,
                            decoration: InputDecoration(
                              enabledBorder: InputBorder.none,
                              hintText: S.of(context).tell_people_about_your_day,
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                  color: theme.primaryColorDark.withOpacity(0.6),
                                  fontSize: 15),
                            ),
                            onSaved: (val) {
                              setState(() {
                                _con.post.text = val;
                              });
                            },
                          ),
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Container(
                            height: 220,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                _con.post != null && _con.post.mediaList != null
                                    ? Container(
                                        width: size.width,
                                        height: 110,
                                        child: ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            itemCount: _con.post.mediaList.length,
                                            itemBuilder: (ctx, indx) {
                                              return Container(
                                                  padding: EdgeInsets.only(
                                                      top: 10, left: 10),
                                                  width: 100,
                                                  height: 100,
                                                  child: Align(
                                                      alignment:
                                                          Alignment.bottomLeft,
                                                      child: Stack(
                                                        fit: StackFit.expand,
                                                        children: [
                                                          _con
                                                                      .post
                                                                      .mediaList[
                                                                          indx]
                                                                      .type ==
                                                                  0
                                                              ? Center(
                                                                  child: FadeInImage
                                                                      .assetNetwork(
                                                                    placeholder:
                                                                        "assets/img/placeholders/image.jpg",
                                                                    width: 100,
                                                                    height: 100,
                                                                    fit: BoxFit
                                                                        .cover,
                                                                    image: _con
                                                                        .post
                                                                        .mediaList[
                                                                            indx]
                                                                        .file,
                                                                  ),
                                                                )
                                                              : Stack(
                                                                  fit: StackFit
                                                                      .expand,
                                                                  children: [
                                                                    FadeInImage
                                                                        .assetNetwork(
                                                                      placeholder:
                                                                          "assets/img/placeholders/videopholder.jpg",
                                                                      image: _con
                                                                          .post
                                                                          .mediaList[
                                                                              indx]
                                                                          .file,
                                                                      fadeInDuration:
                                                                          Duration(
                                                                              milliseconds:
                                                                                  100),
                                                                      width: 100,
                                                                      height: 100,
                                                                      fit: BoxFit
                                                                          .cover,
                                                                    ),
                                                                    Container(
                                                                      color: Colors
                                                                          .black26,
                                                                      child:
                                                                          Center(
                                                                        child:
                                                                            Icon(
                                                                          Icons
                                                                              .play_arrow,
                                                                          size:
                                                                              28,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                          Positioned(
                                                            right: 0,
                                                            top: 0,
                                                            child: IconButton(
                                                                onPressed: () {
                                                                  _con.removeMedia(_con
                                                                          .post
                                                                          .mediaList[
                                                                      indx]);
                                                                },
                                                                icon: Icon(
                                                                  Icons.close,
                                                                  size: 20,
                                                                  color: theme
                                                                      .accentColor,
                                                                )),
                                                          )
                                                        ],
                                                      )));
                                            }),
                                      )
                                    : SizedBox(
                                        height: 0,
                                        width: 0,
                                      ),
                                _con.newMedia != null && _con.newMedia.length > 0
                                    ? Container(
                                        width: size.width,
                                        height: 110,
                                        child: ListView.builder(
                                            scrollDirection: Axis.horizontal,
                                            itemCount: _con.newMedia.length,
                                            itemBuilder: (ctx, indx) {
                                              return Container(
                                                  padding: EdgeInsets.only(
                                                      top: 10, left: 10),
                                                  width: 100,
                                                  height: 100,
                                                  child: Align(
                                                      alignment:
                                                          Alignment.bottomLeft,
                                                      child: Stack(
                                                        fit: StackFit.expand,
                                                        children: [
                                                          _con.newMedia[indx].type == 0
                                                              ? Image.file(
                                                                  File(
                                                                    _con.newMedia[indx].identifier,
                                                                  ),
                                                                  width: 100,
                                                                  height: 100,
                                                                  fit: BoxFit.cover,
                                                                )
                                                              : Image.asset(
                                                                  "assets/img/placeholders/videopholder.jpg",
                                                                  width: 100,
                                                                  height: 100,
                                                                  fit: BoxFit.cover,
                                                                ),
                                                          Positioned(
                                                            right: 0,
                                                            top: 0,
                                                            child: IconButton(
                                                                onPressed: () {
                                                                  _con.removeNewMedia(_con.newMedia[indx]);
                                                                },
                                                                icon: Icon(
                                                                  Icons.close,
                                                                  size: 20,
                                                                  color: theme.accentColor,
                                                                )),
                                                          )
                                                        ],
                                                      )));
                                            }),
                                      )
                                    : SizedBox(
                                        width: 0,
                                        height: 0,
                                      ),
                              ],
                            )),
                      )
                    ],
                  ),
                ),
              ),

              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          IconButton(
                            onPressed: () {
                              setState(() {
                                _con.multipleImagePick();
                              });
                            },
                            splashColor: theme.accentColor.withOpacity(0.5),
                            tooltip: "Gallery",
                            icon: ImageIcon(
                              AssetImage("assets/icons/gallery.png"),
                              color: theme.accentColor,
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              setState(() {
                                _con.getCameraImage();
                              });
                            },
                            splashColor: theme.accentColor.withOpacity(0.5),
                            tooltip: "Camera",
                            icon: ImageIcon(
                              AssetImage("assets/icons/camera.png"),
                              color: theme.accentColor,
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              setState(() {
                                _con.getVideoFile();
                              });
                            },
                            splashColor: theme.accentColor.withOpacity(0.5),
                            tooltip: "Video",
                            icon: ImageIcon(
                              AssetImage("assets/icons/movie.png"),
                              color: theme.accentColor,
                            ),
                          ),
                        ],
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            _con.post.text = _con.textController.text;
                          });
                          _con.updatePost();
                        },
                        child: Container(
                          width: 70,
                          height: 35,
                          decoration: BoxDecoration(
                              color: theme.accentColor,
                              borderRadius: BorderRadius.circular(5)),
                          child: Center(
                            child: Text(
                              S.of(context).post,
                              style: textTheme.subtitle2,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),

              ///Loader
              _con.isLoading
                  ? Container(
                      color: Colors.black.withOpacity(0.5),
                      child: Center(
                        child: Container(
                          width: 100,
                          height: 100,
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              borderRadius: BorderRadius.circular(15)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircularProgressIndicator(),
                              SizedBox(height: 10),
                              Text("Please wait...",
                                  style: Theme.of(context).textTheme.bodyText2)
                            ],
                          ),
                        ),
                      ),
                    )
                  : Positioned(bottom: 10, child: SizedBox(height: 0)),
            ],
          ),
        ),
      ),
    );
  }
}
