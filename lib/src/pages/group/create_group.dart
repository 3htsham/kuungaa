import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/group_controller.dart';
import 'package:social_app/src/elements/others/my_scroll_behaviour.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class CreateGroupWidget extends StatefulWidget {
  @override
  _CreateGroupWidgetState createState() => _CreateGroupWidgetState();
}

class _CreateGroupWidgetState extends StateMVC<CreateGroupWidget> {

  GroupController _con;

  _CreateGroupWidgetState(): super(GroupController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.getCurrentUserNow();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;
    var handler;

    return PickupCallLayout(
      currentUser: _con.currentUser,
      scaffold: Scaffold(
        key: _con.scaffoldKey,
        body: Stack(
          children: [
            ScrollConfiguration(
              behavior: MyScrollBehavior(),
              child: NestedScrollView(
                  physics: BouncingScrollPhysics(),
                  headerSliverBuilder: (context, innerBoxIsScrolled) {
                    handler = NestedScrollView.sliverOverlapAbsorberHandleFor(context);
                    return [
                      SliverOverlapAbsorber(
                        handle: handler,
                        sliver: SliverAppBar(
                          actions: [],
                          forceElevated: innerBoxIsScrolled,
                          expandedHeight: (size.width * 00.5625),
                          floating: false,
                          pinned: true,
                          elevation: 0,
                          centerTitle: true,
                          backgroundColor: theme.scaffoldBackgroundColor,
                          flexibleSpace: FlexibleSpaceBar(
                            titlePadding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                            title: Text(""),
                            background: Stack(
                              children: [
                                _con.group != null && _con.group.cover != null
                                    ? Image.file(
                                        File(_con.group.cover),
                                        fit: BoxFit.cover,
                                        height: (size.width * 00.5625)+20,
                                        width: size.width,
                                      )
                                    : Image.asset(
                                        "assets/img/placeholders/image.jpg",
                                        fit: BoxFit.cover,
                                        height: (size.width * 00.5625)+20,
                                        width: size.width,
                                      ),
                                Container(
                                  height: size.width,
                                  decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter,
                                          colors: [
                                            theme.scaffoldBackgroundColor.withOpacity(0.2),
                                            theme.scaffoldBackgroundColor.withOpacity(0.6),
                                            theme.scaffoldBackgroundColor
                                          ],
                                          stops: [0.0, 0.7, 1]
                                      )
                                  ),
                                ),
                                Positioned(
                                  bottom: 0,
                                  right: 0,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: theme.focusColor.withOpacity(0.5),
                                      borderRadius: BorderRadius.only(topLeft: Radius.circular(100), bottomLeft: Radius.circular(100))
                                    ),
                                    child: IconButton(
                                      onPressed: _con.getGroupCoverFromGallery,
                                      icon: ImageIcon(AssetImage("assets/icons/gallery.png"), color: theme.accentColor, size: 32,),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    ];
                  },
                  body: Builder(
                      builder: (context) {
                        return CustomScrollView(
                          slivers: [
                            SliverOverlapInjector(
                              handle: handler,
                            ),
                            SliverToBoxAdapter(
                              child: SingleChildScrollView(
                                child: Container(
                                  // padding: EdgeInsets.only(top: 10),
                                  child: Container(
                                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                    decoration: BoxDecoration(
                                        color: theme.primaryColor,
                                        borderRadius: BorderRadius.circular(10),
                                        boxShadow: [
                                          BoxShadow(
                                              offset: Offset(1, 1),
                                              spreadRadius: 0.5,
                                              blurRadius: 5,
                                              color: Theme.of(context).focusColor.withOpacity(0.5)
                                          )
                                        ]
                                    ),
                                    child: Column(
                                      children: [

                                        Container(
                                          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                                          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [

                                              Text(S.of(context).group_details, style: textTheme.headline6,),

                                              SizedBox(height: 15,),

                                              Row(
                                                children: [
                                                  ClipRRect(
                                                    borderRadius: BorderRadius.circular(100),
                                                    child: InkWell(
                                                      onTap: _con.getGroupImageFromGallery,
                                                      child: Container(
                                                        width: 80, height: 80,
                                                        child: Stack(
                                                          fit: StackFit.expand,
                                                          children: [
                                                            Container(
                                                              width: 70, height: 70,
                                                              child: _con.group!=null && _con.group.image != null
                                                                  ? Image.file(File(_con.group.image), width: 70, height: 70, fit: BoxFit.cover,)
                                                                  : Image.asset(
                                                                "assets/img/placeholders/image.jpg",
                                                                fit: BoxFit.cover,
                                                                width: 70, height: 70,
                                                              ),
                                                            ),

                                                            Positioned(
                                                              bottom: 10,
                                                              right: 10,
                                                              child: ImageIcon(AssetImage("assets/icons/gallery.png"), color: theme.accentColor, size: 26,),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),

                                                  Container(
                                                    width: size.width-100-50,
                                                    padding: EdgeInsets.only(left: 20),
                                                    child: TextFormField(
                                                      controller: _con.nameController,
                                                      validator: (val) => val.isEmpty ? "Group Name can't be empty" : null,
                                                      keyboardType: TextInputType.text,
                                                      decoration: InputDecoration(
                                                        enabledBorder: UnderlineInputBorder(
                                                            borderSide: BorderSide(color: Colors.white)
                                                        ),
                                                        labelText: S.of(context).group_name,
                                                        labelStyle: TextStyle(color: theme.hintColor.withOpacity(0.5)),
                                                        prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                                      ),
                                                      onSaved: (val){
                                                        _con.group.name = val;
                                                      },
                                                    ),
                                                  )

                                                ],
                                              ),

                                              SizedBox(height: 20,),
                                              TextFormField(
                                                controller: _con.descriptionController,
                                                keyboardType: TextInputType.multiline,
                                                minLines: 2,
                                                textAlignVertical: TextAlignVertical.top,
                                                maxLines: null,
                                                decoration: InputDecoration(
                                                  border: OutlineInputBorder(
                                                      borderSide: BorderSide(color: Colors.white),
                                                    borderRadius: BorderRadius.circular(10)
                                                  ),
                                                  labelText: S.of(context).group_description,
                                                  prefixIcon: ImageIcon(AssetImage("assets/icons/comment.png")),
                                                  prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                                ),
                                                onSaved: (val){
                                                  _con.group.description = val;
                                                },
                                              ),
                                              SizedBox(height: 20,),

                                              Container(
                                                alignment: Alignment.center,
                                                width: double.infinity,
                                                child: OutlineButton(
                                                  borderSide: BorderSide(color: Theme.of(context).accentColor, width: 2,),
                                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
                                                  textColor: Theme.of(context).accentColor,
                                                  highlightedBorderColor: Theme.of(context).accentColor,
                                                  textTheme: ButtonTextTheme.accent,
                                                  onPressed: (){
                                                    _con.createGroupNow();
                                                  },
                                                  child: Text(S.of(context).create_group, style: Theme.of(context).textTheme.subtitle2.merge(
                                                      TextStyle(color: Theme.of(context).accentColor, letterSpacing: 1)
                                                  )),
                                                  color: Theme.of(context).accentColor,
                                                  padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width/3.6, vertical: 10),
                                                ),
                                              )

                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        );
                      }
                  )
              ),
            ),


            ///Loader
            _con.isLoading
                ? Container(
              color: Colors.black.withOpacity(0.5),
              child: Center(
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(15)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      SizedBox(height: 10),
                      Text("Please wait...",
                          style: Theme.of(context).textTheme.bodyText2)
                    ],
                  ),
                ),
              ),
            )
                : Positioned(bottom: 10, child: SizedBox(height: 0)),
          ],
        ),
      ),
    );
  }
}
