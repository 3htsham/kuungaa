import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/group_controller.dart';
import 'package:social_app/src/elements/group/group_members_tab.dart';
import 'package:social_app/src/elements/group/group_posts_tab.dart';
import 'package:social_app/src/elements/others/my_scroll_behaviour.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class ViewGroupWidget extends StatefulWidget {

  RouteArgument argument;

  ViewGroupWidget({this.argument});

  @override
  _ViewGroupWidgetState createState() => _ViewGroupWidgetState();
}

class _ViewGroupWidgetState extends StateMVC<ViewGroupWidget>
    with TickerProviderStateMixin  {

  GroupController _con;
  TabController _tabController;

  List<String> joinedOptions = <String>[];
  List<String> notJoinedOptions = <String>[];
  var optionIcons = <AssetImage>[];


  _ViewGroupWidgetState() : super(GroupController()) {
    _con = controller;
  }

  @override
  void initState() {
    notJoinedOptions.add("Details");
    joinedOptions.add("Details");
    joinedOptions.add(S().leave_group);
    optionIcons.add(AssetImage("assets/icons/info.png"));
    optionIcons.add(AssetImage("assets/icons/block.png"));
    _con.group = widget.argument.group;
    _con.currentUser = widget.argument.user;
    _tabController = TabController(length: 3, vsync: this);
    super.initState();
    if(_con.group.join == "true" || _con.group.join == true) {
      _con.getGroupPosts();
    }
  }


  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;
    var handler;

    var options = _con.group.join == "true" || _con.group.join == true ? joinedOptions : notJoinedOptions;

    return PickupCallLayout(
      currentUser: _con.currentUser,
      scaffold: Scaffold(
        key: _con.scaffoldKey,
        body: Stack(
          children: [
            ScrollConfiguration(
              behavior: MyScrollBehavior(),
              child: NestedScrollView(
                  physics: BouncingScrollPhysics(),
                  headerSliverBuilder: (context, innerBoxIsScrolled) {
                    handler = NestedScrollView.sliverOverlapAbsorberHandleFor(context);
                    return [
                      SliverOverlapAbsorber(
                        handle: handler,
                        sliver: SliverAppBar(
                          actions: [],
                          forceElevated: innerBoxIsScrolled,
                          expandedHeight: (size.width * 00.5625),
                          floating: false,
                          pinned: true,
                          elevation: 0,
                          centerTitle: true,
                          backgroundColor: theme.scaffoldBackgroundColor,
                          flexibleSpace: FlexibleSpaceBar(
                            titlePadding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                            title: Text(""),
                            background: Stack(
                              children: [
                                _con.group != null && _con.group.cover != null
                                    ? CachedNetworkImage(
                                  imageUrl: _con.group.cover,
                                  fit: BoxFit.cover,
                                  height: (size.width * 00.5625) + 20,
                                  width: size.width,
                                )
                                    : Image.asset(
                                        "assets/img/placeholders/image.jpg",
                                        fit: BoxFit.cover,
                                        height: (size.width * 00.5625) + 20,
                                        width: size.width,
                                      ),
                                Container(
                                  height: size.width,
                                  decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter,
                                          colors: [
                                            theme.scaffoldBackgroundColor.withOpacity(0.2),
                                            theme.scaffoldBackgroundColor.withOpacity(0.6),
                                            theme.scaffoldBackgroundColor
                                          ],
                                          stops: [0.0, 0.7, 1]
                                      )
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    ];
                  },
                  body: Builder(
                      builder: (context) {
                        return CustomScrollView(
                          slivers: [
                            SliverOverlapInjector(
                              handle: handler,
                            ),
                            SliverToBoxAdapter(
                              child: SingleChildScrollView(
                                child: Container(
                                  // padding: EdgeInsets.only(top: 10),
                                  child: Container(
                                    margin: EdgeInsets.only(top: 10),
                                    decoration: BoxDecoration(
                                        color: theme.primaryColor,
                                        borderRadius: BorderRadius.circular(10),
                                        boxShadow: [
                                          BoxShadow(
                                              offset: Offset(1, 1),
                                              spreadRadius: 0.5,
                                              blurRadius: 5,
                                              color: Theme.of(context).focusColor.withOpacity(0.5)
                                          )
                                        ]
                                    ),
                                    child: Column(
                                      children: [

                                        Container(
                                          // padding: EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                                          // margin: EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [

                                              SizedBox(height: 15,),

                                              Container(
                                                margin: EdgeInsets.symmetric(horizontal: 15, vertical: 7),
                                                child: Row(
                                                  children: [
                                                    ClipRRect(
                                                      borderRadius: BorderRadius.circular(100),
                                                      child: Container(
                                                        width: 80, height: 80,
                                                        child: Stack(
                                                          fit: StackFit.expand,
                                                          children: [
                                                            Container(
                                                              width: 70, height: 70,
                                                              child: _con.group!=null && _con.group.image != null
                                                                  ? CachedNetworkImage(imageUrl: _con.group.image, width: 70, height: 70, fit: BoxFit.cover,)
                                                                  : Image.asset(
                                                                "assets/img/placeholders/image.jpg",
                                                                fit: BoxFit.cover,
                                                                width: 70, height: 70,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),

                                                    Column(
                                                      children: [
                                                        Container(
                                                          width: size.width-100-50,
                                                          padding: EdgeInsets.only(left: 20),
                                                          child: Text(
                                                            _con.group.name,
                                                            style: textTheme.headline6,
                                                          )
                                                        ),
                                                        SizedBox(height: 5,),
                                                        Container(
                                                            width: size.width-100-50,
                                                            padding: EdgeInsets.only(left: 20),
                                                            child: Text(
                                                              _con.group.description ?? "",
                                                              maxLines: 2,
                                                              overflow: TextOverflow.ellipsis,
                                                              style: textTheme.bodyText2,
                                                            )
                                                        ),
                                                      ],
                                                    ),

                                                  ],
                                                ),
                                              ),

                                              SizedBox(height: 20,),

                                              Container(
                                                margin: EdgeInsets.symmetric(horizontal: 15),
                                                padding: EdgeInsets.symmetric(vertical: 7),
                                                decoration: BoxDecoration(
                                                  color: theme.focusColor.withOpacity(0.2),
                                                  borderRadius: BorderRadius.circular(10)
                                                ),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                  children: [
                                                    (_con.group.join == "false" ||
                                                        _con.group.join == false)
                                                    ? InkWell(
                                                        onTap: (){ _con.joinGroupNow(_con.group); },
                                                        child: Column(
                                                          children: [
                                                            ImageIcon(
                                                              AssetImage(
                                                                  "assets/icons/plus.png"),
                                                              size: 28,
                                                            ),
                                                            Text(
                                                              S.of(context).join_group,
                                                              style: textTheme
                                                                  .bodyText1
                                                                  .merge(TextStyle(
                                                                      fontSize:
                                                                          13)),
                                                            )
                                                          ],
                                                        ),
                                                      )
                                                    : InkWell(
                                                      onTap: (){  },
                                                        child: Column(
                                                          children: [
                                                            ImageIcon(
                                                              AssetImage(
                                                                  "assets/icons/check.png"),
                                                              color: theme
                                                                  .accentColor,
                                                              size: 28,
                                                            ),
                                                            Text(
                                                              S.of(context).joined,
                                                              style: textTheme
                                                                  .bodyText1
                                                                  .merge(TextStyle(
                                                                      color: theme
                                                                          .accentColor,
                                                                      fontSize:
                                                                          13)),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                InkWell(
                                                      onTap: (){
                                                        _tabController.animateTo(1);
                                                      },
                                                      child: Container(
                                                        child: Column(
                                                          children: [
                                                            ImageIcon(AssetImage("assets/icons/groups.png"), size: 28,),
                                                            Text(S.of(context).members, style: textTheme.bodyText1.merge(TextStyle(fontSize: 13)),)
                                                          ],
                                                        ),
                                                      ),
                                                    ),

                                                    // InkWell(
                                                    //   onTap: (){},
                                                    //   child: Column(
                                                    //     children: [
                                                    //       ImageIcon(AssetImage("assets/icons/more.png"), size: 32,),
                                                    //       Text("More", style: textTheme.bodyText1.merge(TextStyle(fontSize: 13)),)
                                                    //     ],
                                                    //   ),
                                                    // ),

                                                    Column(
                                                      children: [
                                                        PopupMenuButton(
                                                            onSelected: (value) {
                                                              if(value==options[0]) {
                                                                _tabController.animateTo(2);
                                                              } else if(value==options[1]) {
                                                                _con.leaveGroupNow(_con.group);
                                                              }
                                                            },
                                                            icon: ImageIcon(AssetImage("assets/icons/more.png"), size: 32,),
                                                            itemBuilder: (context) => List.generate(
                                                                options.length, (index) {
                                                              return PopupMenuItem(
                                                                  value: options[index],
                                                                  height: 40,
                                                                  child: Row(
                                                                    children: [
                                                                      ImageIcon(optionIcons[index],
                                                                        size: 32, color: theme.accentColor,),
                                                                      SizedBox(width: 10,),
                                                                      Text(options[index], style: textTheme.bodyText2,),
                                                                      SizedBox(width: 10,),
                                                                    ],
                                                                  ));
                                                            }),
                                                        ),
                                                        Text(S.of(context).more, style: textTheme.bodyText1.merge(TextStyle(fontSize: 13)),)
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ),

                                              SizedBox(height: 10,),

                                              Container(
                                                height: 60,
                                                child: TabBar(
                                                  physics: BouncingScrollPhysics(),
                                                  isScrollable: false,
                                                  controller: _tabController,
                                                  labelColor: Theme.of(context).primaryColor,
                                                  indicatorColor: theme.accentColor,
                                                  tabs: [
                                                    Tab(child: Text(S.of(context).posts, style: textTheme.bodyText1,),),
                                                    Tab(child: Text(S.of(context).members,style: textTheme.bodyText1,),),
                                                    Tab(child: Text(S.of(context).details,style: textTheme.bodyText1,),),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                height: size.height-65-24-50, //85 from TabBar(), 24 from StatusBar, 50 from AppBar
                                                child: TabBarView(
                                                  controller: _tabController,
                                                  children: [

                                                    ///Posts Section of Group
                                                    (_con.group.join == "false" ||
                                                        _con.group.join == false)
                                                    ? Container(
                                                      height: 60,
                                                      child: Center(
                                                        child: Text(S.of(context).join_group_to_see,
                                                          textAlign: TextAlign.center,
                                                          style: textTheme.bodyText2,),
                                                      ),
                                                    ) : Container(
                                                        child: GroupPostsTabWidget(_con.currentUser, group: _con.group,)),

                                                    ///Members Section of Group
                                                    GroupMembersTabWidget(group: _con.group, user: _con.currentUser,),

                                                    ///Details Section of Group
                                                    Container(
                                                      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                                                      child: SingleChildScrollView(
                                                          child: Column(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: [
                                                              Text(_con.group.name, style: textTheme.headline4,),
                                                              SizedBox(height: 10,),
                                                              Text(S.of(context).group_description, style: textTheme.subtitle2.merge(TextStyle(color: theme.accentColor)),),
                                                              SizedBox(height: 5),
                                                              Text(
                                                                _con.group.description ?? "",
                                                                style: textTheme.bodyText2,
                                                              )
                                                            ],
                                                          )
                                                      ),
                                                    ),

                                                  ],
                                                ),
                                              )

                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        );
                      }
                  )
              ),
            ),


            ///Loader
            _con.isLoading
                ? Container(
              color: Colors.black.withOpacity(0.5),
              child: Center(
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(15)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      SizedBox(height: 10),
                      Text("Please wait...",
                          style: Theme.of(context).textTheme.bodyText2)
                    ],
                  ),
                ),
              ),
            )
                : Positioned(bottom: 10, child: SizedBox(height: 0)),
          ],
        ),
      ),
    );

  }
}
