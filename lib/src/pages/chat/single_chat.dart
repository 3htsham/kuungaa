import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_emoji_keyboard/flutter_emoji_keyboard.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/config/emoji_keyboard_icons_icons.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/chat_controlelr.dart';
import 'package:flutter/cupertino.dart' as c;
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/models/message.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class SingleChatWidget extends StatefulWidget {

  RouteArgument argument;

  SingleChatWidget({this.argument});

  @override
  _SingleChatWidgetState createState() => _SingleChatWidgetState();
}

class _SingleChatWidgetState extends StateMVC<SingleChatWidget> {

  bool isEmoji = false;
  KeyboardVisibilityController keyboardVisibilityController;
  ChatController _con;

  _SingleChatWidgetState() : super(ChatController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    if(widget.argument.chat != null) {
      _con.chat = widget.argument.chat;
    } else {
      _con.user = widget.argument.user;
    }
    super.initState();
    if(widget.argument.chat != null) {
      _con.getChatMessages();
    } else {
      _con.getChatMessagesByUserId();
    }


    keyboardVisibilityController = KeyboardVisibilityController();
    // Query
    print('Keyboard visibility direct query: ${keyboardVisibilityController.isVisible}');

    // Subscribe
    keyboardVisibilityController.onChange.listen((visible) {
      if(visible) {
        setState((){
          isEmoji = false;
        });
      }
    });
  }

  showEmojiKeyboard(){
    if(keyboardVisibilityController != null
        && keyboardVisibilityController.isVisible) {
      FocusScope.of(context).unfocus();
    }
    Future.delayed(Duration(microseconds: 200), (){
      setState((){
        this.isEmoji = !isEmoji;
      });
    });
  }


  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () async {
        if(this.isEmoji) {
          setState((){
            isEmoji = false;
          });
          return false;
        } else {
          return true;
        }
      },
      child: PickupCallLayout(
        currentUser: _con.currentUser,
        scaffold: Scaffold(
          key: _con.scaffoldKey,
          appBar: AppBar(
            title: Text(_con.chat == null ? _con.user.name : _con.chat.opponent.name, style: textTheme.subtitle1,),
            centerTitle: true,
            elevation: 0,
            backgroundColor: Colors.transparent,
            actions: [

              IconButton(onPressed: (){
                  _con.dialCall(context, true);
                },
                  icon: ImageIcon(AssetImage("assets/icons/camera.png"))
              ),

              IconButton(onPressed: (){
                _con.dialCall(context, false);
              },
                  icon: Icon(c.CupertinoIcons.phone)),


              PopupMenuButton(
                  onSelected: (value) {
                    _con.onOptions(value, true, _con.chat);
                  },
                  icon: RotatedBox(quarterTurns: 1, child: ImageIcon(AssetImage("assets/icons/more.png"), color: theme.primaryColorDark,),),
                  itemBuilder: (context) => List.generate(_con.options.length, (index) {
                    return PopupMenuItem(
                        value: _con.options[index],
                        child: Text(_con.options[index], style: textTheme.bodyText2,));
                  })
              ),
            ],
          ),
          body: Stack(
            children: [

              Container(
                child: Column(
                  children: [
                    Expanded(
                      child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        controller: _con.scrollController,
                        child: Column(
                          children: [
                            _con.loadingMessages
                                ? CircularLoadingWidget(height: 100,)
                                : _con.messages.isEmpty
                                ? Container(
                              height: size.height-100,
                              child: Center(
                                child: Opacity(
                                  opacity: 0.2,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        height: 30,
                                      ),
                                      Image.asset(
                                        "assets/img/placeholders/no_chat.png",
                                        width: 120,
                                        height: 120,
                                        color: theme.accentColor,
                                      ),
                                      SizedBox(
                                        height: 30,
                                      ),
                                      Text(
                                        S.of(context).no_messages_in_chats,
                                        style: textTheme.bodyText1,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            )
                                : ListView.builder(
                                        primary: false,
                                        shrinkWrap: true,
                                        // padding: EdgeInsets.only(bottom: 70),
                                        itemCount: _con.messages.length,
                                        itemBuilder: (context, index) {
                                          Message message =
                                              _con.messages[index];

                                          bool isMe = message.type == 1;

                                          var format = DateFormat(
                                              'yyyy-MM-ddTHH:mm:ss.SSSSSSZ');
                                          var dateTime =
                                              _con.messages[index].time;
                                          // var day = DateFormat('EEEE').format(dateTime);
                                          var time = DateFormat('hh:mm aa')
                                              .format(dateTime);

                                          var constraints = BoxConstraints(
                                              maxWidth: (size.width / 2) + 15);
                                          var boxDecoration = BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(
                                                      isMe ? 10 : 0),
                                                  topRight: Radius.circular(10),
                                                  bottomRight: Radius.circular(
                                                      isMe ? 0 : 10),
                                                  bottomLeft:
                                                      Radius.circular(10)),
                                              color: isMe
                                                  ? theme.accentColor
                                                  : theme.primaryColor);
                                          var clipRectBorder =
                                              BorderRadius.only(
                                                  topLeft: Radius.circular(10),
                                                  topRight: Radius.circular(10),
                                                  bottomRight: Radius.circular(
                                                      isMe ? 0 : 10),
                                                  bottomLeft: Radius.circular(
                                                      isMe ? 10 : 0));

                                          var textualPadding =
                                              const EdgeInsets.symmetric(
                                                  vertical: 10.0,
                                                  horizontal: 12);

                                          return Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 5, horizontal: 15),
                                            child: InkWell(
                                              onTap: () {},
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                textDirection: isMe
                                                    ? c.TextDirection.rtl
                                                    : c.TextDirection.ltr,
                                                children: [
                                                  isMe
                                                      ?
                                                      // message.type == 0
                                                      //     ?
                                                      Container(
                                                          constraints:
                                                              constraints,
                                                          decoration:
                                                              boxDecoration,
                                                          child: Padding(
                                                            padding:
                                                                textualPadding,
                                                            child: Text(
                                                              message.txt ?? "",
                                                              style: textTheme
                                                                  .bodyText2
                                                                  .merge(TextStyle(
                                                                      color: theme
                                                                          .primaryColorDark)),
                                                            ),
                                                          ),
                                                        )
                                                      //     : Container(
                                                      //   constraints: constraints,
                                                      //   padding: EdgeInsets.all(3),
                                                      //   decoration: boxDecoration,
                                                      //   child: ClipRRect(
                                                      //     borderRadius: clipRectBorder,
                                                      //     child: FadeInImage.assetNetwork(
                                                      //         placeholder: "assets/placeholders/image.jpg",
                                                      //         image: message.image),
                                                      //   ),
                                                      // )
                                                      : Row(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Container(
                                                              height: 40,
                                                              width: 40,
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(1),
                                                              decoration: BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              100),
                                                                  color: theme
                                                                      .accentColor),
                                                              child: ClipRRect(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            100),
                                                                child: _con.chat.opponent.image !=
                                                                            null &&
                                                                        _con.chat.opponent.image.length >
                                                                            0
                                                                    ? CachedNetworkImage(
                                                                        imageUrl: _con
                                                                            .chat
                                                                            .opponent
                                                                            .image,
                                                                        height:
                                                                            40,
                                                                        width:
                                                                            40,
                                                                      )
                                                                    : Image.asset(
                                                                        "assets/img/placeholders/profile.png"),
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              width: 10,
                                                            ),
                                                            Column(
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              children: [
                                                                // message.type == 0
                                                                //     ?
                                                                Container(
                                                                  constraints: constraints,
                                                                  decoration: boxDecoration.copyWith(
                                                                      boxShadow: [
                                                                        BoxShadow(
                                                                            color: theme.focusColor.withOpacity(0.2),
                                                                            blurRadius: 2,
                                                                            offset: Offset(0.2, 0.2))
                                                                      ]),
                                                                  child: Padding(
                                                                    padding: textualPadding,
                                                                    child: Text(
                                                                      message.txt ?? "",
                                                                      style: textTheme.bodyText2,
                                                                    ),
                                                                  ),
                                                                )
                                                              ],
                                                            )
                                                          ],
                                                        ),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Container(
                                                      width: 50,
                                                      child: Text(
                                                        "$time",
                                                        style: textTheme.caption.merge(TextStyle(fontSize: 10)),
                                                      ))
                                                ],
                                              ),
                                            ),
                                          );
                                        }),

                            // SizedBox(height: 60),
                          ],
                        ),
                      ),
                    ),

                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            // height: 50,
                            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(width: 5,),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 7),
                                    decoration: BoxDecoration(
                                        color: theme.primaryColor,
                                        borderRadius: BorderRadius.circular(15)
                                    ),
                                    constraints: BoxConstraints(maxHeight: 300),
                                    child: Row(
                                      children: [
                                        IconButton(
                                          onPressed: this.showEmojiKeyboard,
                                          icon: Icon(
                                            EmojiKeyboardIcons.smile,
                                            color: theme.primaryColorDark,
                                            size: 18,
                                          ),
                                        ),
                                        Expanded(
                                          child: TextFormField(
                                            controller: _con.textController,
                                            textInputAction: TextInputAction.newline,
                                            keyboardType: TextInputType.multiline,
                                            maxLines: null,
                                            decoration: InputDecoration(
                                                hintText: "Type your message",
                                                border: InputBorder.none
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(width: 5,),
                                Container(
                                  decoration: BoxDecoration(
                                      color: theme.primaryColor,
                                      borderRadius: BorderRadius.circular(100)
                                  ),
                                  child: IconButton(
                                    onPressed: (){},
                                    icon: Icon(Icons.attach_file, color: theme.primaryColorDark,),
                                    splashColor: theme.focusColor,
                                  ),
                                ),
                                SizedBox(width: 5,),
                                Container(
                                  decoration: BoxDecoration(
                                      color: theme.accentColor,
                                      borderRadius: BorderRadius.circular(100)
                                  ),
                                  child: IconButton(
                                    onPressed: _con.sendANewMessage,
                                    icon: Icon(Icons.send, color: Colors.white,),
                                    splashColor: theme.focusColor,
                                  ),
                                ),
                              ],
                            ),
                          ),

                          this.isEmoji
                              ? EmojiKeyboard(
                              column: 5,
                              floatingHeader: false,
                              color: theme.scaffoldBackgroundColor,
                              categoryIcons: CategoryIcons(
                                  color: Colors.white,
                                  people: EmojiKeyboardIcons.smile,
                                  nature: EmojiKeyboardIcons.tiger,
                                  food: EmojiKeyboardIcons.coffee_cup,
                                  activity: EmojiKeyboardIcons.football,
                                  travel: EmojiKeyboardIcons.car,
                                  flags: EmojiKeyboardIcons.flags,
                                  objects: EmojiKeyboardIcons.lightbulb,
                                  symbols: EmojiKeyboardIcons.heart),
                              onEmojiSelected: (_emoji) {
                                _con.textController.text+=_emoji.toString();
                              })
                              : SizedBox(height: 0, width: 0),
                        ],
                      ),
                    ),

                  ],
                ),
              ),


              ///Loader
              _con.loadingChats
                  ? Container(
                color: Colors.black.withOpacity(0.5),
                child: Center(
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(15)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(),
                        SizedBox(height: 10),
                        Text("Please wait...",
                            style: Theme.of(context).textTheme.bodyText2)
                      ],
                    ),
                  ),
                ),
              )
                  : Positioned(bottom: 10, child: SizedBox(height: 0)),
            ],
          ),
        ),
      ),
    );
  }
}
