import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/chat_controlelr.dart';
import 'package:social_app/src/elements/chat/chat_list_item.dart';
import 'package:social_app/src/models/chat.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class ChatsWidget extends StatefulWidget {

  RouteArgument args;

  ChatsWidget({this.args});

  @override
  _ChatsWidgetState createState() => _ChatsWidgetState();
}

class _ChatsWidgetState extends StateMVC<ChatsWidget> {
  ChatController _con;

  var _tapPosition;

  _ChatsWidgetState() : super(ChatController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.args.user;
    super.initState();
    _con.getAllChats();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return PickupCallLayout(
      currentUser: _con.currentUser,
      scaffold: Scaffold(
        key: _con.scaffoldKey,
        appBar: AppBar(
          title: Text(
            S.of(context).chats,
            style: textTheme.headline4
                .merge(TextStyle(color: Theme.of(context).accentColor)),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                children: [
                  _con.chats.isEmpty
                          ? Center(
                              child: Opacity(
                                opacity: 0.2,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Image.asset(
                                      "assets/img/placeholders/no_chat.png",
                                      width: 120,
                                      height: 120,
                                      color: theme.accentColor,
                                    ),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Text(
                                      S.of(context).no_chats,
                                      style: textTheme.bodyText1,
                                    )
                                  ],
                                ),
                              ),
                            )
                          : ListView.builder(
                              primary: false,
                              shrinkWrap: true,
                              itemCount: _con.chats.length,
                              itemBuilder: (context, index) {
                                return ChatsListItem(
                                  chat: _con.chats[index],
                                  options: _con.options,
                                  onTap: (){
                                    Navigator.of(context)
                                        .pushNamed("/SingleChat",
                                            arguments: RouteArgument( currentUser: _con.currentUser,
                                                chat: _con.chats[index]))
                                        .then((value) {
                                      if (value != null && value) {
                                        _con.getAllChats();
                                      }
                                    });
                                  },
                                  onOptions: _con.onOptions,
                                );
                              }),
                ],
              ),
            ),

            ///Loader
            _con.loadingChats
                ? Container(
              color: Colors.black.withOpacity(0.5),
              child: Center(
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(15)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      SizedBox(height: 10),
                      Text("Please wait...",
                          style: Theme.of(context).textTheme.bodyText2)
                    ],
                  ),
                ),
              ),
            )
                : Positioned(bottom: 10, child: SizedBox(height: 0)),
          ],
        ),
      ),
    );
  }



  void storePosition(TapDownDetails details) {
    _tapPosition = details.globalPosition;
    final RenderBox overlay = Overlay.of(context).context.findRenderObject();
    showMenu(
        context: context,
        position: RelativeRect.fromRect(
            _tapPosition & Size(40, 40), // smaller rect, the touch area
            Offset.zero & overlay.size // Bigger rect, the entire screen
        ),
        items: List.generate(_con.options.length, (index) {
          return PopupMenuItem(
            child: Text(_con.options[index], style: Theme.of(context).textTheme.bodyText2,),
          );
        })
    );
  }
  
  showMenuItems(BuildContext ctx, Chat chat, GlobalKey chatItemKey) {
    var screenHeight = MediaQuery.of(context).size.height;
    RenderBox renderBox = chatItemKey.currentContext.findRenderObject();
    Offset offset = renderBox.localToGlobal(Offset.zero);
    var xPos = offset.dx;
    var yPos = offset.dy;

    var left = xPos - 100;
    var top = yPos + 30;
    var right = xPos;
    var bottom = yPos + 30 + 50;

    var position = RelativeRect.fromLTRB(left, top, right, bottom);
    showMenu(
        context: ctx,
        position: position,

        items: List.generate(_con.options.length, (index) {
          return PopupMenuItem(
            value: _con.options[index],
            child: Text(_con.options[index], style: Theme.of(context).textTheme.bodyText2,),
          );
        })
    );
  }


}
