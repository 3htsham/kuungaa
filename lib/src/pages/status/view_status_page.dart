import 'dart:async';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/controllers/status_controller.dart';
import 'package:social_app/src/elements/others/pausable_timer.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class ViewStatusPage extends StatefulWidget {

  RouteArgument argument;

  ViewStatusPage({this.argument});

  @override
  _ViewStatusPageState createState() => _ViewStatusPageState();
}

class _ViewStatusPageState extends StateMVC<ViewStatusPage> {


  StatusController _con;
  var totalTime = 10; //In Seconds
  var value = 0.0;
  PausableTimer timer;
  Timer periodicTimer;

  var _opacity = 1.0;

  _ViewStatusPageState() : super(StatusController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.status = widget.argument.status;
    _con.currentUser = widget.argument.currentUser;
    super.initState();
    _con.addSeen();
    startTimer();
  }

  void startTimer() {
    timer = PausableTimer(Duration(milliseconds: totalTime*1000), (){
      if(periodicTimer != null) {
        periodicTimer.cancel();
        Navigator.of(context).pop();
      }
    });
    timer.start();
    periodicTimer = Timer.periodic(Duration(milliseconds: 1) , (_timer) {
      setState((){
        //Counting progress as with milliseconds i.e. totalTimeInSeconds * 1000 = totalTimeInMilliSeconds
        value = (timer.elapsed.inMilliseconds / (totalTime * 1000));
      });
    });
  }

  void _onPointerDown(PointerDownEvent event){
    setState((){
      if(timer!=null) {
        timer.pause();
      }
      _opacity = 0.0;
    });
  }

  void _onPointerUp(PointerUpEvent event){
    setState((){
      if(timer!=null) {
        timer.start();
      }
      _opacity = 1.0;
    });
  }

  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    var dateTime = DateTime.fromMillisecondsSinceEpoch(_con.status.uploadedAt);
    var day = DateFormat('EEEE').format(dateTime);
    var time = DateFormat('hh:mm aa').format(dateTime);

    return WillPopScope(
      onWillPop: () async {
        if(periodicTimer != null) {
          periodicTimer.cancel();
        }
        if(timer!=null) {
          timer.cancel();
        }
        return true;
      },
      child: PickupCallLayout(
        currentUser: _con.currentUser,
        scaffold: Scaffold(
          key: _con.scaffoldKey,
          body: Stack(
            children: [
              Stack(
                fit: StackFit.expand,
                children: [

                  AnimatedOpacity(
                    opacity: _opacity,
                    duration: Duration(milliseconds: 150),
                    child: Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [
                                Theme.of(context).accentColor,
                                Colors.yellowAccent,
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              stops: [0.4, 0.8]
                          )
                      ),
                    ),
                  ),

                  Listener(
                    onPointerDown: _onPointerDown,
                    onPointerUp: _onPointerUp,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      child: CachedNetworkImage(
                        imageUrl: _con.status.imageLink,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),

                  Positioned(
                      top: 0,
                      right: 0,
                      left: 0,
                      child: AnimatedOpacity(
                        opacity: _opacity,
                        duration: Duration(milliseconds: 150),
                        child: Padding(
                          padding: const EdgeInsets.only(top: (16.0+12.0), right: 10, left: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              IconButton(
                                  icon: Icon(
                                    CupertinoIcons.back,
                                    color: Colors.white,
                                  ),
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                  }),

                              Container(
                                padding: EdgeInsets.symmetric(horizontal: 5),
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        colors: [
                                          Colors.black.withOpacity(0),
                                          Colors.black54,
                                          Colors.black38,
                                          Colors.black.withOpacity(0)
                                        ],
                                        stops: [0, 0.2, 0.6, 1],
                                        begin: Alignment.topCenter,
                                        end: Alignment.bottomCenter
                                    )
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text(
                                      "Seen by ${_con.status.seenBy.length} people",
                                      style: textTheme.bodyText2.merge(TextStyle(fontWeight: FontWeight.w600)),
                                    )
                                  ],
                                ),
                              ),

                              SizedBox(width: 3,)
                            ],
                          ),
                        ),
                      )
                  ),

                  Positioned(
                      bottom: 0,
                      right: 0,
                      left: 0,
                      child: AnimatedOpacity(
                        opacity: _opacity,
                        duration: Duration(milliseconds: 150),
                        child: Column(
                          children: [

                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  ClipRRect(
                                    borderRadius: BorderRadius.circular(100),
                                    child: _con.status.user != null && _con.status.user.image != null && _con.status.user.image.length > 0
                                        ? FadeInImage.assetNetwork(
                                      placeholder: "assets/img/placeholders/profile.png",
                                      image: _con.status.user.image,
                                      height: 50,
                                      width: 50,
                                      fit: BoxFit.cover,
                                    ) : Image.asset("assets/img/placeholders/profile.png", width: 50, height: 50, fit: BoxFit.cover,),
                                  ),
                                  Container(
                                    padding: EdgeInsets.symmetric(horizontal: 10),
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                            colors: [
                                              Colors.black.withOpacity(0),
                                              Colors.black54,
                                              Colors.black38,
                                              Colors.black.withOpacity(0)
                                            ],
                                            stops: [0, 0.2, 0.6, 1],
                                            begin: Alignment.topCenter,
                                            end: Alignment.bottomCenter
                                        )
                                    ),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          _con.status.user.name,
                                          style: textTheme.bodyText2.merge(TextStyle(fontWeight: FontWeight.w800)),
                                        ),
                                        Text(
                                          '$day at $time',
                                          style: textTheme.caption,
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),

                            SizedBox(height: 15,),

                            LinearProgressIndicator(
                              value: value,
                              valueColor: AlwaysStoppedAnimation<Color>(theme.accentColor),
                              backgroundColor: Colors.yellowAccent,
                            ),
                          ],
                        ),
                      )
                  )
                ],
              ),

            ],
          ),
        ),
      ),
    );
  }
}
