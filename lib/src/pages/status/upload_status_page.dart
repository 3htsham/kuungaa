import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/controllers/status_controller.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class UploadStatusPageWidget extends StatefulWidget {

  RouteArgument argument;

  UploadStatusPageWidget({this.argument});

  @override
  _MyStatusesWidgetState createState() => _MyStatusesWidgetState();
}

class _MyStatusesWidgetState extends StateMVC<UploadStatusPageWidget> {

  StatusController _con;

  _MyStatusesWidgetState() : super(StatusController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return WillPopScope(
      onWillPop: () async {
        if(_con.isLoading) {
          return false;
        } else  {
          return true;
        }
      },
      child: PickupCallLayout(
        currentUser: _con.currentUser,
        scaffold: Scaffold(
          key: _con.scaffoldKey,
          body: Stack(
            children: [
              Stack(
                fit: StackFit.expand,
                children: [
                  Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [
                                Theme.of(context).accentColor,
                                Colors.yellowAccent,
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              stops: [0.4, 0.8]
                          )
                      ),
                  ),

                  _con.pickedImage != null && _con.pickedImage.length > 1
                      ? Image.file(
                          File(
                            _con.pickedImage,
                          ),
                          width: MediaQuery.of(context).size.width,
                          fit: BoxFit.cover,
                        )
                      : SizedBox(
                          width: 0,
                          height: 0,
                        ),


                  Positioned(
                    top: 0,
                      right: 0,
                      left: 0,
                      child: Padding(
                        padding: const EdgeInsets.only(top: (16.0+12.0), right: 10, left: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(
                                icon: Icon(
                              CupertinoIcons.back,
                              color: Colors.white,
                            ),
                                onPressed: (){
                                  Navigator.of(context).pop();
                                }),

                            SizedBox(width: 3,)
                          ],
                        ),
                      )
                  ),

                  Positioned(
                    bottom: 0,
                      right: 0,
                      left: 0,
                      child: Container(
                        padding: EdgeInsets.only(bottom: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [

                            _con.pickedImage != null && _con.pickedImage.length > 2
                                ? ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: InkWell(
                                onTap: _con.clearImage,
                                child: Container(
                                  width: 40,
                                  height: 40,
                                  margin: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      color: theme.accentColor,
                                      borderRadius: BorderRadius.circular(100),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black26,
                                            offset: Offset(0.6, 0.6),
                                            spreadRadius: 0.6,
                                            blurRadius: 7
                                        )
                                      ]
                                  ),
                                  child: Center(
                                    child: Icon(CupertinoIcons.clear, color: Colors.white, size: 32,),
                                  ),
                                ),
                              ),
                            )
                                : SizedBox(width: 0, height: 0,),

                            ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: InkWell(
                                onTap: _con.getGalleryImage,
                                child: Container(
                                  width: 60,
                                  height: 60,
                                  margin: EdgeInsets.all(15),
                                  decoration: BoxDecoration(
                                    color: theme.accentColor,
                                    borderRadius: BorderRadius.circular(100),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black26,
                                        offset: Offset(1, 1),
                                        spreadRadius: 1,
                                        blurRadius: 10
                                      )
                                    ]
                                  ),
                                  child: Center(
                                    child: ImageIcon(AssetImage("assets/icons/gallery.png"), color: Colors.white, size: 34,),
                                  ),
                                ),
                              ),
                            ),

                            _con.pickedImage != null && _con.pickedImage.length > 2
                                ? ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: InkWell(
                                onTap: _con.uploadStatus,
                                child: AnimatedContainer(
                                  width: 40,
                                  height: 40,
                                  duration: Duration(milliseconds: 100),
                                  margin: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      color: theme.accentColor,
                                      borderRadius: BorderRadius.circular(100),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black26,
                                            offset: Offset(0.6, 0.6),
                                            spreadRadius: 0.6,
                                            blurRadius: 7
                                        )
                                      ]
                                  ),
                                  child: Center(
                                    child: Icon(CupertinoIcons.checkmark, color: Colors.white, size: 32,),
                                  ),
                                ),
                              ),
                            )
                                : SizedBox(width: 0, height: 0,),

                          ],
                        ),
                      )
                  )
                ],
              ),

              ///Loader
              _con.isLoading
                  ? Container(
                color: Colors.black.withOpacity(0.5),
                child: Center(
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(15)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(),
                        SizedBox(height: 10),
                        Text("Please wait...",
                            style: Theme.of(context).textTheme.bodyText2)
                      ],
                    ),
                  ),
                ),
              )
                  : Positioned(bottom: 10, child: SizedBox(height: 0)),
            ],
          ),
        ),
      ),
    );
  }
}
