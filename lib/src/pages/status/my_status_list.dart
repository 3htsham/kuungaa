import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/status_controller.dart';
import 'package:social_app/src/elements/others/divider_line_widget.dart';
import 'package:social_app/src/elements/status/my_status_home_widget.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/models/status.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class MyStatusListWidget extends StatefulWidget {
  RouteArgument argument;

  MyStatusListWidget({this.argument});

  @override
  _MyStatusListWidgetState createState() => _MyStatusListWidgetState();
}

class _MyStatusListWidgetState extends StateMVC<MyStatusListWidget> {
  StatusController _con;

  _MyStatusListWidgetState() : super(StatusController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    _con.statuses = widget.argument.statuses;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return PickupCallLayout(
      currentUser: _con.currentUser,
      scaffold: Scaffold(
        key: _con.scaffoldKey,
        appBar: _appbar(),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              MyStatusHomeWidget(
                  currentUser: _con.currentUser,
                  isStatus: false,
                  status: Status(),
                  onTap: () {
                    Navigator.of(context).pushNamed("/UploadStatus",
                        arguments: RouteArgument(currentUser: _con.currentUser));
                  }),
              DividerLineWidget(
                text: S.of(context).your_status,
              ),
              GridView.count(
                crossAxisCount: 3,
                shrinkWrap: true,
                primary: false,
                crossAxisSpacing: 5,
                mainAxisSpacing: 5,
                padding: EdgeInsets.symmetric(horizontal: 5),
                semanticChildCount: _con.statuses.length,
                children: List.generate(_con.statuses.length, (i) {

                  var dateTime = DateTime.fromMillisecondsSinceEpoch(_con.statuses[i].uploadedAt);
                  var day = DateFormat('EEEE').format(dateTime);
                  var time = DateFormat('hh:mm aa').format(dateTime);

                  return InkWell(
                    onTap: (){
                        Navigator.of(context).pushNamed("/ViewStatus", arguments: RouteArgument(currentUser: _con.currentUser, status: _con.statuses[i]));
                      },
                    child: Container(
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5)),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          gradient: LinearGradient(
                              colors: [
                                Colors.yellowAccent,
                                Theme.of(context).accentColor,
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              stops: [0.3, 0.7]),
                        ),
                        child: Stack(
                          fit: StackFit.expand,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(5),
                              child: CachedNetworkImage(
                                imageUrl: _con.statuses[i].imageLink,
                                fit: BoxFit.cover,
                              ),
                            ),

                            Container(
                              color: Colors.black38,
                            ),

                            Positioned(
                              bottom: 0,
                                right: 0,
                                left: 0,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 3),
                                  child: Text(
                                    '$day at $time',
                                    style: textTheme.caption.merge(TextStyle(color: Colors.white70)),
                                  ),
                                )
                            ),

                            Positioned(
                              top: 0,
                              right: 0,
                              child: InkWell(
                                onTap: () { _con.deleteStatus(_con.statuses[i]); },
                                child: Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Icon(
                                    CupertinoIcons.trash,
                                    color: Colors.white70,
                                  ),
                                ),
                              ),
                            )

                          ],
                        ),
                      ),
                    ),
                  );
                }),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _appbar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      centerTitle: true,
      title: Text(
        S.of(context).your_status,
        style: Theme.of(context)
            .textTheme
            .headline4
            .merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
    );
  }
}
