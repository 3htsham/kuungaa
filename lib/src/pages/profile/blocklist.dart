import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/profile_settings_controller.dart';
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class BlockListWidget extends StatefulWidget {
  RouteArgument argument;
  BlockListWidget({this.argument});
  @override
  _BlockListWidgetState createState() => _BlockListWidgetState();
}

class _BlockListWidgetState extends StateMVC<BlockListWidget> {

  ProfileSettingsController _con;

  _BlockListWidgetState() : super(ProfileSettingsController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.getBlockedUsers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () async {
        if(_con.isloading) {
          return false;
        } else {
          return true;
        }
      },
      child: PickupCallLayout(
        currentUser: widget.argument.currentUser,
        scaffold: Scaffold(
          key: _con.scaffoldKey,
          appBar: AppBar(
            title: Text(
              S.of(context).block_list,
              style: textTheme.headline4
                  .merge(TextStyle(color: Theme.of(context).accentColor)),
            ),
            centerTitle: true,
            backgroundColor: Colors.transparent,
            elevation: 0,
          ),
          body: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    _con.blockedUsers.isEmpty 
                    ? CircularLoadingWidget(height: 100)
                    : ListView.builder(
                      shrinkWrap: true,
                      primary: false,
                      itemCount: _con.blockedUsers.length,
                      itemBuilder: (context, index) {
                        User _user = _con.blockedUsers[index];
                        return Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                            child: Row(
                              children: [
                                InkWell(
                                  onTap: (){},
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(100),
                                    child: _user.image != null ? FadeInImage.assetNetwork(
                                      placeholder: "assets/img/placeholders/profile.png",
                                      image: _user.image,
                                      height: 50,
                                      width: 50,
                                      fit: BoxFit.cover,
                                    ) : Image.asset("assets/img/placeholders/profile.png", height: 50, width: 50, fit: BoxFit.cover, ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(horizontal: 12.0),
                                    child: InkWell(
                                      onTap: (){},
                                      child: Text(
                                        _user.name,
                                        style: Theme.of(context).textTheme.subtitle2,
                                      ),
                                    ),
                                  ),
                                ),

                                RaisedButton(
                                  onPressed: (){
                                    _con.unblockUserNow(_user);
                                  },
                                  child: Center(
                                    child: Text(S.of(context).unblock, style: textTheme.bodyText1)
                                  ),
                                  color: theme.accentColor,
                                ),
                                
                              ],
                            )
                          );
                      }
                    ),
                  ],
                )
              ),

              _con.isloading
                ? Container(
              color: Colors.black.withOpacity(0.5),
              child: Center(
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(15)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      SizedBox(height: 10),
                      Text("Please wait...",
                          style: Theme.of(context).textTheme.bodyText2)
                    ],
                  ),
                ),
              ),
            )
                : Positioned(bottom: 10, child: SizedBox(height: 0)),
            ],
          )
        )
      ),
    );
  }
}