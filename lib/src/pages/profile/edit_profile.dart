import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/profile_page_controller.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class EditProfileWidget extends StatefulWidget {

  RouteArgument argument;
  EditProfileWidget({this.argument});

  @override
  _EditProfileWidgetState createState() => _EditProfileWidgetState();
}

class _EditProfileWidgetState extends StateMVC<EditProfileWidget> {

  ProfilePageController _con;

  _EditProfileWidgetState() : super(ProfilePageController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.user;
    _con.user = User();
    super.initState();
    setState((){
      _con.nameController.text = _con.currentUser.name;
      _con.emailController.text = _con.currentUser.email;
      _con.bioController.text = _con.currentUser.bio ?? "";
    });
  }

  @override
  Widget build(BuildContext context) {


    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return PickupCallLayout(
      currentUser: _con.currentUser,
      scaffold: Scaffold(
        key: _con.scaffoldKey,
        appBar: AppBar(
          title: Text(S.of(context).edit_profile,
            style: textTheme.headline4
                .merge(TextStyle(color: Theme.of(context).accentColor)),
          ),
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.transparent,
          actions: [
            IconButton(
              onPressed: (){
                _con.updateUserProfile();
              },
              icon: ImageIcon(AssetImage("assets/icons/check.png"), color: theme.accentColor, size: 36,),
            )
          ],
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Center(
                    child: Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: _con.user != null && _con.user.image != null
                              ? Image.file(File(_con.user.image), width: 130, height: 130, fit: BoxFit.cover,)
                              : FadeInImage.assetNetwork(
                            placeholder:
                            "assets/img/placeholders/profile.png",
                            image: _con.currentUser.image ?? "",
                            fit: BoxFit.cover,
                            height: 130,
                            width: 130,
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          right: 0,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: InkWell(
                              onTap: (){
                                _con.getGalleryImage();
                              },
                              child: Container(
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(100),
                                  color: theme.focusColor
                                ),
                                child: ImageIcon(AssetImage("assets/icons/gallery.png"), color: theme.accentColor, size: 32,),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),

                  SizedBox(height: 20,),
                  Form(
                    key: _con.formKey,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: Column(
                        children: [
                          TextFormField(
                            controller: _con.nameController,
                            validator: (val) => val.isEmpty ? "Name can't be empty" : null,
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white)
                              ),
                              labelText: S.of(context).name,
                              prefixIcon: ImageIcon(AssetImage("assets/icons/profile.png")),
                              prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                            ),
                            onSaved: (val){
                              _con.user.name = val;
                            },
                          ),
                          SizedBox(height: 10,),
                          TextFormField(
                            controller: _con.emailController,
                            validator: (val) => val.isEmpty || !val.contains("@") || !_con.isValidEmail(val) ? "Enter Valid Email" : null,
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white)
                              ),
                              labelText: S.of(context).email,
                              prefixIcon: ImageIcon(AssetImage("assets/icons/email.png")),
                              prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                            ),
                            onSaved: (val){
                              _con.user.email = val;
                            },
                          ),
                          SizedBox(height: 25,),
                          TextFormField(
                            controller: _con.bioController,
                            keyboardType: TextInputType.multiline,
                            minLines: 2,
                            textAlignVertical: TextAlignVertical.top,
                            maxLines: null,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                  borderRadius: BorderRadius.circular(10)),
                              labelText: S.of(context).about_you,
                              prefixIcon: ImageIcon(AssetImage("assets/icons/comment.png")),
                              prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                            ),
                            onSaved: (val){
                              _con.user.bio = val;
                            },
                          ),
                        ],
                      ),
                    ),
                  )

                ],
              ),
            ),

            ///Loader
            _con.isloading
                ? Container(
              color: Colors.black.withOpacity(0.5),
              child: Center(
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(15)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      SizedBox(height: 10),
                      Text("Please wait...",
                          style: Theme.of(context).textTheme.bodyText2)
                    ],
                  ),
                ),
              ),
            )
                : Positioned(bottom: 10, child: SizedBox(height: 0)),

          ],
        ),
      ),
    );
  }
}
