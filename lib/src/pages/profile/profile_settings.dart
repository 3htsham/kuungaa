import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/profile_settings_controller.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class ProfileSettingsWidget extends StatefulWidget {
  @override
  _ProfileSettingsWidgetState createState() => _ProfileSettingsWidgetState();
}

class _ProfileSettingsWidgetState extends StateMVC<ProfileSettingsWidget> {

  ProfileSettingsController _con;

  _ProfileSettingsWidgetState() : super(ProfileSettingsController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.getCurrentUserNow();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var size = MediaQuery.of(context).size;

    return PickupCallLayout(
      currentUser: _con.currentUser,
      scaffold: Scaffold(
        key: _con.scaffoldKey,
        appBar: AppBar(
          title: Text(
            S.of(context).profile_settings,
            style: textTheme.headline4
                .merge(TextStyle(color: Theme.of(context).accentColor)),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [

                  ListTile(
                    leading: ImageIcon(AssetImage("assets/icons/block.png"), color: theme.accentColor),
                    title: Text(S.of(context).block_list, style: textTheme.subtitle1,),
                    subtitle: Text(S.of(context).see_block_list, style: textTheme.subtitle2,),
                    trailing: Icon(Icons.arrow_forward_ios, size: 15, color: theme.focusColor,),
                    onTap: (){
                      Navigator.of(context).pushNamed("/Blocklist", arguments: RouteArgument(currentUser: _con.currentUser));
                    },
                  ),

                  ListTile(
                    leading: ImageIcon(AssetImage("assets/icons/profile.png"), color: theme.accentColor),
                    title: Text(S.of(context).your_name, style: textTheme.subtitle1,),
                    subtitle: Text(_con.currentUser==null ? "" : _con.currentUser.name, style: textTheme.subtitle2,),
                    trailing: Icon(_con.isEditingName ? Icons.keyboard_arrow_down : Icons.arrow_forward_ios, size: _con.isEditingName ? 22 : 15, color: theme.focusColor,),
                    onTap: (){
                      setState((){
                        _con.isEditingName = true;
                        _con.isEditingPassword = false;
                        _con.isEditingStatus = false;
                        _con.isEditingNickname = false;
                      });
                    },
                    selectedTileColor: theme.focusColor.withOpacity(0.2),
                    selected: _con.isEditingName,
                  ),

                  _con.isEditingName ? Container(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(S.of(context).update_your_name, style: textTheme.subtitle1.merge(TextStyle(color: theme.accentColor)),),
                        SizedBox(height: 20,),
                        TextFormField(
                          controller: _con.nameController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                            labelText: S.of(context).name,
                            labelStyle: TextStyle(color: theme.accentColor),
                              contentPadding: EdgeInsets.all(12),
                            prefixIcon: ImageIcon(AssetImage("assets/icons/profile.png")),
                            prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                          ),
                        ),
                        SizedBox(height: 10,),
                        Row(
                          children: [
                            Spacer(),
                            RaisedButton(
                              onPressed: (){
                                setState((){
                                  _con.isEditingName = false;
                                });
                              },
                              color: theme.focusColor,
                              child: Text(S.of(context).cancel, style: textTheme.subtitle2,),
                            ),
                            SizedBox(width: 10,),
                             RaisedButton(
                               onPressed: _con.updateName,
                               color: theme.accentColor,
                               child: Text(S.of(context).update, style: textTheme.subtitle2.merge(TextStyle(color: Colors.white)),),
                             ),
                          ]
                        ),
                        SizedBox(height: 5,)
                      ],
                    ),
                  ) : SizedBox(height: 0),

                  ListTile(
                    leading: ImageIcon(AssetImage("assets/icons/lock.png"), color: theme.accentColor),
                    title: Text(S.of(context).nickname, style: textTheme.subtitle1,),
                    subtitle: Text(_con.currentUser==null ? "" : (_con.currentUser.nickName!=null && _con.currentUser.nickName.length > 0) ? _con.currentUser.nickName : S.of(context).update_nickname, style: textTheme.subtitle2,),
                    trailing: Icon(_con.isEditingNickname ? Icons.keyboard_arrow_down : Icons.arrow_forward_ios, size: _con.isEditingNickname ? 22 : 15, color: theme.focusColor,),
                    onTap: (){
                      setState((){
                        _con.isEditingName = false;
                        _con.isEditingPassword = false;
                        _con.isEditingStatus = false;
                        _con.isEditingNickname = true;
                      });
                    },
                    selectedTileColor: theme.focusColor.withOpacity(0.2),
                    selected: _con.isEditingNickname,
                  ),

                  _con.isEditingNickname ? Container(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(S.of(context).update_nickname, style: textTheme.subtitle1.merge(TextStyle(color: theme.accentColor)),),
                        SizedBox(height: 20,),
                        TextFormField(
                          controller: _con.nicknameController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                            labelText: S.of(context).nickname,
                            labelStyle: TextStyle(color: theme.accentColor),
                            contentPadding: EdgeInsets.all(12),
                            prefixIcon: ImageIcon(AssetImage("assets/icons/profile.png")),
                            prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                          ),
                        ),
                        SizedBox(height: 10,),
                        Row(
                            children: [
                              Spacer(),
                              RaisedButton(
                                onPressed: (){
                                  setState((){
                                    _con.isEditingNickname = false;
                                  });
                                },
                                color: theme.focusColor,
                                child: Text(S.of(context).cancel, style: textTheme.subtitle2,),
                              ),
                              SizedBox(width: 10,),
                              RaisedButton(
                                onPressed: _con.updateNick,
                                color: theme.accentColor,
                                child: Text(S.of(context).update, style: textTheme.subtitle2.merge(TextStyle(color: Colors.white)),),
                              ),
                            ]
                        ),
                        SizedBox(height: 5,)
                      ],
                    ),
                  ) : SizedBox(height: 0),


                  ListTile(
                    leading: ImageIcon(AssetImage("assets/icons/lock.png"), color: theme.accentColor),
                    title: Text(S.of(context).password, style: textTheme.subtitle1,),
                    subtitle: Text(S.of(context).change_password, style: textTheme.subtitle2,),
                    trailing: Icon(_con.isEditingPassword ? Icons.keyboard_arrow_down : Icons.arrow_forward_ios, size: _con.isEditingPassword ? 22 : 15, color: theme.focusColor,),
                    onTap: (){
                      setState((){
                        _con.isEditingName = false;
                        _con.isEditingPassword = true;
                        _con.isEditingStatus = false;
                        _con.isEditingNickname = false;
                      });
                    },
                    selectedTileColor: theme.focusColor.withOpacity(0.2),
                    selected: _con.isEditingPassword,
                  ),


                  _con.isEditingPassword
                      ? Container(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(S.of(context).change_password, style: textTheme.subtitle1.merge(TextStyle(color: theme.accentColor)),),
                        SizedBox(height: 20,),
                        TextFormField(
                          obscureText: true,
                          controller: _con.passwordController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                            labelText: S.of(context).password,
                            labelStyle: TextStyle(color: theme.accentColor),
                            contentPadding: EdgeInsets.all(12),
                            prefixIcon: ImageIcon(AssetImage("assets/icons/lock.png")),
                            prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                          ),
                        ),
                        SizedBox(height: 20,),
                        TextFormField(
                          obscureText: true,
                          controller: _con.confirmPasswordController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                            labelText: S.of(context).confirm_password,
                            labelStyle: TextStyle(color: theme.accentColor),
                            contentPadding: EdgeInsets.all(12),
                            prefixIcon: ImageIcon(AssetImage("assets/icons/lock.png")),
                            prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                          ),
                        ),
                        SizedBox(height: 10,),
                        Row(
                            children: [
                              Spacer(),
                              RaisedButton(
                                onPressed: (){
                                  setState((){
                                    _con.isEditingPassword = false;
                                  });
                                },
                                color: theme.focusColor,
                                child: Text(S.of(context).cancel, style: textTheme.subtitle2,),
                              ),
                              SizedBox(width: 10,),
                              RaisedButton(
                                onPressed: _con.updatePassword,
                                color: theme.accentColor,
                                child: Text(S.of(context).update, style: textTheme.subtitle2.merge(TextStyle(color: Colors.white)),),
                              ),
                            ]
                        ),
                        SizedBox(height: 5,)
                      ],
                    ),
                  )
                      : SizedBox(height: 0),


                  ListTile(
                    leading: ImageIcon(AssetImage("assets/icons/status.png"), color: theme.accentColor),
                    title: Text(S.of(context).status, style: textTheme.subtitle1,),
                    subtitle: Text(_con.currentUser==null ? "" : (_con.currentUser.userStatus != null && _con.currentUser.userStatus.length > 0) ? _con.currentUser.userStatus : S.of(context).update_status, style: textTheme.subtitle2,),
                    trailing: Icon(_con.isEditingStatus ? Icons.keyboard_arrow_down : Icons.arrow_forward_ios, size: _con.isEditingStatus ? 22 : 15, color: theme.focusColor,),
                    onTap: (){
                      setState((){
                        _con.isEditingName = false;
                        _con.isEditingPassword = false;
                        _con.isEditingStatus = true;
                        _con.isEditingNickname = false;
                      });
                    },
                    selectedTileColor: theme.focusColor.withOpacity(0.2),
                    selected: _con.isEditingStatus,
                  ),


                  _con.isEditingStatus
                      ? Container(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(S.of(context).update_status, style: textTheme.subtitle1.merge(TextStyle(color: theme.accentColor)),),
                        SizedBox(height: 20,),
                        Container(
                          width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                border: Border.all(color: Theme.of(context).focusColor.withOpacity(0.2)),
                                borderRadius: BorderRadius.circular(5)
                            ),
                            padding: EdgeInsets.symmetric(horizontal: 12),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                  value: _con.selectedItem,
                                  style: TextStyle(color: theme.accentColor),
                                  hint: Text(S.of(context).status, style: TextStyle(color: Theme.of(context).focusColor.withOpacity(0.7)),),
                                  items: _con.statusDropdownMenuItems,
                                  onChanged: _con.changeStatus
                              ),
                            )
                        ),
                        SizedBox(height: 10,),
                        Row(
                            children: [
                              Spacer(),
                              RaisedButton(
                                onPressed: (){
                                  setState((){
                                    _con.isEditingStatus = false;
                                  });
                                  },
                                color: theme.focusColor,
                                child: Text(S.of(context).cancel, style: textTheme.subtitle2,),
                              ),
                              SizedBox(width: 10,),
                              RaisedButton(
                                onPressed: _con.updateStatus,
                                color: theme.accentColor,
                                child: Text(S.of(context).update, style: textTheme.subtitle2.merge(TextStyle(color: Colors.white)),),
                              ),
                            ]
                        ),
                        SizedBox(height: 5,)
                      ],
                    ),
                  )
                      : SizedBox(height: 0),

                ],
              ),
            ),

            _con.isloading
                ? Container(
              color: Colors.black.withOpacity(0.5),
              child: Center(
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(15)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      SizedBox(height: 10),
                      Text("Please wait...",
                          style: Theme.of(context).textTheme.bodyText2)
                    ],
                  ),
                ),
              ),
            )
                : Positioned(bottom: 10, child: SizedBox(height: 0)),
          ],
        ),
      ),
    );
  }
}
