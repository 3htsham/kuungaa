import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/profile_page_controller.dart';
import 'package:social_app/src/elements/others/my_scroll_behaviour.dart';
import 'package:social_app/src/elements/post/user_profile_post.dart';
import 'package:social_app/src/elements/user/user_friends_widget.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class UserProfileWidget extends StatefulWidget {

  RouteArgument routeArgument;

  UserProfileWidget({this.routeArgument});

  @override
  _UserProfileWidgetState createState() => _UserProfileWidgetState();
}

class _UserProfileWidgetState extends StateMVC<UserProfileWidget>
    with TickerProviderStateMixin  {

  ProfilePageController _con;
  TabController _tabController;
  GlobalKey<UserProfilePostsWidgetState> postsPageKey;

  _UserProfileWidgetState() : super(ProfilePageController()) {
    _con = controller;
  }

  @override
  void initState() {
    postsPageKey = GlobalKey<UserProfilePostsWidgetState>();
    _con.user = widget.routeArgument.user;
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    _con.getCurrentUserNow();
    _con.getUserProfile(_con.user);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var handler;

    return WillPopScope(
      onWillPop: () async {
        if(postsPageKey.currentState.con.isSharePostDialog) {
          postsPageKey.currentState.con.closeShareDialog();
          return false;
        } else if(_con.unFriendDialog || _con.blockDialog) {
          setState((){
            _con.unFriendDialog = false;
            _con.blockDialog = false;
          });
          return false;
        } else if (_con.isloading) {
          return false;
        } else {
          return true;
        }
      },
          child: RefreshIndicator(
          onRefresh: () async {},
          child: PickupCallLayout(
            currentUser: _con.currentUser,
            scaffold: Scaffold(
              key: _con.scaffoldKey,
              body: Stack(
                children: [
                  ScrollConfiguration(
                    behavior: MyScrollBehavior(),
                    child: NestedScrollView(
                        physics: BouncingScrollPhysics(),
                        headerSliverBuilder: (context, innerBoxIsScrolled) {
                          handler = NestedScrollView.sliverOverlapAbsorberHandleFor(context);
                          return [
                            SliverOverlapAbsorber(
                              handle: handler,
                              sliver: SliverAppBar(
                                actions: [
                                  _con.currentUser != null && _con.currentUser.id == _con.user.id
                                      ?
                                  IconButton(
                                    onPressed: (){
                                      Navigator.of(context).pushNamed("/EditProfile", arguments: RouteArgument(user: _con.user))
                                          .then((value){
                                            if(value!=null && value) {
                                              _con.getCurrentUserNow();
                                              _con.getUserProfile(_con.user);
                                            }
                                      });
                                      },
                                    icon: ImageIcon(AssetImage("assets/icons/write.png"), size: 32, color: theme.accentColor,),
                                  )
                                      : _con.user != null && _con.currentUser != null
                                      ? PopupMenuButton(
                      onSelected: (value) {
                        _con.onOptionsTap(value);
                      },
                      icon: RotatedBox(
                        quarterTurns: 1,
                        child: ImageIcon(
                          AssetImage("assets/icons/more.png"),
                          color: Colors.white,
                        ),
                      ),
                      itemBuilder: (context) =>
                          List.generate(_con.userOptions.length, (index) {
                            return PopupMenuItem(
                                value: _con.userOptions[index].option,
                                child: Row(
                                  children: [
                                    ImageIcon(AssetImage(_con.userOptions[index].icon)),
                                    SizedBox(width: 10,),
                                    Text(
                                      _con.userOptions[index].option,
                                      style: Theme.of(context).textTheme.bodyText2,
                                    ),
                                    SizedBox(width: 10,),
                                  ],
                                ));
                          })) : SizedBox(height: 0, width: 0),

                                ],
                                forceElevated: innerBoxIsScrolled,
                                expandedHeight: size.width,
                                floating: false,
                                pinned: true,
                                elevation: 0,
                                centerTitle: true,
                                backgroundColor: theme.scaffoldBackgroundColor,
                                flexibleSpace: FlexibleSpaceBar(
                                  titlePadding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                                  title: Text(
                                    _con.user.name ?? "",
                                    style: textTheme.headline4
                                        .merge(TextStyle(color: theme.accentColor, fontSize: 15)),
                                  ),
                                  background: Stack(
                                    children: [
                                      _con.user != null
                                          ? FadeInImage.assetNetwork(
                                              placeholder:
                                                  "assets/img/placeholders/profile.png",
                                              image: _con.user.image ?? "",
                                              fit: BoxFit.cover,
                                              height: size.width,
                                              width: size.width,
                                            )
                                          : Image.asset(
                                              "assets/img/placeholders/profile.png",
                                              fit: BoxFit.cover,
                                              height: size.width,
                                              width: size.width,
                                            ),
                                      Container(
                                        height: size.width,
                                        decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                                begin: Alignment.topCenter,
                                                end: Alignment.bottomCenter,
                                                colors: [
                                                  theme.scaffoldBackgroundColor.withOpacity(0.2),
                                                  theme.scaffoldBackgroundColor.withOpacity(0.6),
                                                  theme.scaffoldBackgroundColor
                                                ],
                                                stops: [0.0, 0.5, 1]
                                            )
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            )

                          ];
                        },
                        body: Builder(
                            builder: (context) {
                              return CustomScrollView(
                                slivers: [
                                  SliverOverlapInjector(
                                    handle: handler,
                                  ),
                                  SliverToBoxAdapter(
                                    child: SingleChildScrollView(
                                      child: Container(
                                        padding: EdgeInsets.only(top: 10),
                                        child: Container(
                                          // margin: EdgeInsets.only(bottom: 10),
                                          decoration: BoxDecoration(
                                              color: theme.primaryColor,
                                              borderRadius: BorderRadius.circular(10),
                                              boxShadow: [
                                                BoxShadow(
                                                    offset: Offset(1, 1),
                                                    spreadRadius: 0.5,
                                                    blurRadius: 5,
                                                    color: Theme.of(context).focusColor.withOpacity(0.5)
                                                )
                                              ]
                                          ),
                                          child: Column(
                                            children: [

                                              _con.currentUser != null && _con.user != null && _con.user.id != _con.currentUser.id
                                                  ? Padding(
                                                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 7),
                                                    child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                children: [

                                                  _con.user.isFriend == "false" || _con.user.isFriend == false
                                                          ? _con.user.friendRequestStatus == 0
                                                              ? RaisedButton(
                                                                  onPressed: () {
                                                                    _con.sendOrCancelFriendRequest(
                                                                        _con.user);
                                                                  },
                                                                  color:
                                                                      theme.accentColor,
                                                                  shape:
                                                                      RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                                5.0),
                                                                  ),
                                                                  child: Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceAround,
                                                                    children: [
                                                                      ImageIcon(
                                                                              AssetImage(
                                                                                  "assets/icons/add_friend.png"),
                                                                              color: Colors
                                                                                  .white,
                                                                            ),
                                                                      Text(
                                                                        "Add Friend",
                                                                        style: textTheme
                                                                            .subtitle1,
                                                                      ),
                                                                    ],
                                                                  ),
                                                                )
                                                              : _con.user.friendRequestStatus == 1
                                                                  ? RaisedButton(
                                                                      onPressed: () {
                                                                        // _con.sendOrCancelFriendRequest(_con.user);
                                                                      },
                                                                      color: theme
                                                                          .accentColor,
                                                                      shape:
                                                                          RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius
                                                                                .circular(
                                                                                    5.0),
                                                                      ),
                                                                      child: Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment
                                                                                .spaceAround,
                                                                        children: [
                                                                          ImageIcon(
                                                                                  AssetImage(
                                                                                      "assets/icons/add_friend.png"),
                                                                                  color:
                                                                                      Colors.white,
                                                                                ),
                                                                          Text(
                                                                            "Request sent",
                                                                            style: textTheme
                                                                                .subtitle1,
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    )
                                                                  : RaisedButton(
                                                                      onPressed: () {
                                                                        setState(() {
                                                                          _con.unFriendDialog = true;
                                                                        });
                                                                      },
                                                                      color: theme
                                                                          .accentColor,
                                                                      shape:
                                                                          RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius
                                                                                .circular(
                                                                                    5.0),
                                                                      ),
                                                                      child: Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment
                                                                                .spaceAround,
                                                                        children: [
                                                                          Text(
                                                                            "   Unfriend   ",
                                                                            style: textTheme
                                                                                .subtitle1,
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    )
                                                          : RaisedButton(
                                                              onPressed: () {
                                                                setState(() {
                                                                  _con.unFriendDialog = true;
                                                                });
                                                              },
                                                              color: theme.accentColor,
                                                              shape:
                                                                  RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(5.0),
                                                              ),
                                                              child: Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceAround,
                                                                children: [
                                                                  Text(
                                                                    "    Unfriend    ",
                                                                    style: textTheme
                                                                        .subtitle1,
                                                                  ),
                                                                ],
                                                              ),
                                                            ),


                                                      RaisedButton(
                                                      onPressed: (){
                                                        Navigator.of(context).pushNamed("/SingleChat", arguments: RouteArgument(user: _con.user, currentUser: _con.currentUser));
                                                      },
                                                      color: theme.focusColor,
                                                      shape: RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(5.0),
                                                      ),
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                        children: [
                                                          ImageIcon(AssetImage("assets/icons/message.png"), color: Colors.white,),
                                                          Text("Message", style: textTheme.subtitle1,)
                                                        ],
                                                      ),
                                                    )
                                                ],
                                              ),
                                                  ) : SizedBox(height: 0,),

                                              Container(
                                                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 7),
                                                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                                                decoration: BoxDecoration(
                                                    color: theme.focusColor.withOpacity(0.4),
                                                    borderRadius: BorderRadius.circular(10)
                                                ),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                  crossAxisAlignment: CrossAxisAlignment.center,
                                                  children: [
                                                    Column(
                                                      children: [
                                                        Text('${_con.user.totalPosts ?? "0"}', style: textTheme.headline5.merge(TextStyle(color: theme.accentColor)),),
                                                        Text("Posts", style: textTheme.bodyText1,)
                                                      ],
                                                    ),
                                                    Container(
                                                      height: 18,
                                                      width: 1,
                                                      color: Theme.of(context).focusColor,
                                                    ),
                                                    Column(
                                                      children: [
                                                        Text('${_con.user.totalFriends ?? "0"}', style: textTheme.headline5.merge(TextStyle(color: theme.accentColor)),),
                                                        Text("Friends", style: textTheme.bodyText1,)
                                                      ],
                                                    ),
                                                    // Container(
                                                    //   height: 18,
                                                    //   width: 1,
                                                    //   color: Theme.of(context).focusColor,
                                                    // ),
                                                    // Column(
                                                    //   children: [
                                                    //     Text("37", style: textTheme.headline5.merge(TextStyle(color: theme.accentColor)),),
                                                    //     Text("Followers", style: textTheme.bodyText1,)
                                                    //   ],
                                                    // ),
                                                  ],
                                                ),
                                              ),

                                              Container(
                                                height: 70,
                                                child: TabBar(
                                                  controller: _tabController,
                                                  labelColor: Theme.of(context).primaryColor,
                                                  indicatorColor: theme.accentColor,
                                                  tabs: [
                                                    Tab(child: Text(S.of(context).timeline,style: textTheme.bodyText1,),),
                                                    Tab(child: Text(S.of(context).friends,style: textTheme.bodyText1,),),
                                                    Tab(child: Text(S.of(context).about,style: textTheme.bodyText1,),),
                                                  ],
                                                ),
                                              ),

                                              Container(
                                                height: size.height-70-24-50, //70 from TabBar(), 24 from StatusBar, 50 from AppBar
                                                child: TabBarView(
                                                  controller: _tabController,
                                                  children: [
                                                    ///Profile Posts
                                                    UserProfilePostsWidget(key: postsPageKey, user: _con.user, currentUser: _con.currentUser),

                                                    ///Friends List
                                                    _con.user != null && _con.currentUser != null
                                                    ? _con.user.id == _con.currentUser.id
                                                        ? UserFriendsWidget(user: _con.user,)
                                                        : _con.user.isFriend == "false" || _con.user.isFriend == false
                                                            ? Container(
                                                                height: 60,
                                                                child: Center(
                                                                  child: Text(
                                                                    S.of(context).you_cant_see_friends(_con.user.name),
                                                                    // "You can't see ${_con.user.name}'s friends\nunless you're friends",
                                                                    textAlign: TextAlign
                                                                        .center,
                                                                    style: textTheme
                                                                        .bodyText2,
                                                                  ),
                                                                ),
                                                              )
                                                            : UserFriendsWidget(user: _con.user,)
                                                    : Container(),

                                                ///About User
                                                Container(
                                                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                                                  child: Column(
                                                    children: [
                                                      Row(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          Container(
                                                            width: 120,
                                                            child: Text(S.of(context).nickname, style: textTheme.headline6,),
                                                          ),
                                                          Container(
                                                            width: 200,
                                                            child: Text((_con.user != null ) ? _con.user.nickName ?? "" : "", style: textTheme.bodyText1,),
                                                          )
                                                        ],
                                                      ),
                                                      SizedBox(height: 15),
                                                      Row(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          Container(
                                                            width: 120,
                                                            child: Text(S.of(context).status, style: textTheme.headline6,),
                                                          ),
                                                          Container(
                                                            width: 200,
                                                            child: Text((_con.user != null ) ? _con.user.userStatus ?? "" : "", style: textTheme.bodyText1,),
                                                          )
                                                        ],
                                                      ),
                                                      SizedBox(height: 15),
                                                      Row(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          Container(
                                                            width: 120,
                                                            child: Text(S.of(context).gender, style: textTheme.headline6,),
                                                          ),
                                                          Container(
                                                            width: 200,
                                                            child: Text((_con.user != null ) ? _con.user.gender ?? "" : "", style: textTheme.bodyText1,),
                                                          )
                                                        ],
                                                      ),
                                                      SizedBox(height: 15),
                                                      Row(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          Container(
                                                            width: 120,
                                                            child: Text(S.of(context).about, style: textTheme.headline6,),
                                                          ),
                                                          Container(
                                                            width: 200,
                                                            child: Text((_con.user != null ) ? _con.user.bio ?? "" : "", style: textTheme.bodyText1,),
                                                          )
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                )
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              );
                            }
                        )
                    ),
                  ),



                  ///Loader
                  _con.unFriendDialog
                      ? Container(
                    color: Colors.black.withOpacity(0.5),
                    child: Center(
                      child: Container(
                        width: 220,
                        height: 150,
                        padding: EdgeInsets.symmetric(vertical: 7),
                        decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(15)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(height: 20,),
                            Text("You are about to unfriend ${_con.user.name}",
                                textAlign: TextAlign.center,
                                style: Theme.of(context).textTheme.subtitle2),
                            Spacer(),
                            RaisedButton(
                              onPressed: (){
                                _con.unFriendUserNow(
                                    _con.user);
                              },
                              color: theme.focusColor,
                              child: Text("Unfriend",
                                  style: Theme.of(context).textTheme.bodyText2),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                      : Positioned(bottom: 10, child: SizedBox(height: 0)),


                   ///Loader
                  _con.blockDialog
                      ? Container(
                    color: Colors.black.withOpacity(0.5),
                    child: Center(
                      child: Container(
                        width: 220,
                        height: 150,
                        padding: EdgeInsets.symmetric(vertical: 7, horizontal: 10),
                        decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(15)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(height: 20,),
                            Text("You are about to block ${_con.user.name}\nYou won't be able to see ${_con.user.name}'s profile after block.",
                                textAlign: TextAlign.center,
                                style: Theme.of(context).textTheme.subtitle2),
                            Spacer(),
                            RaisedButton(
                              onPressed: (){
                                _con.blockUserNow();
                              },
                              color: Colors.redAccent,
                              child: Text("Block",
                                  style: Theme.of(context).textTheme.bodyText2),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                      : Positioned(bottom: 10, child: SizedBox(height: 0)),

                  ///Loader
                  _con.isloading
                      ? Container(
                    color: Colors.black.withOpacity(0.5),
                    child: Center(
                      child: Container(
                        width: 100,
                        height: 100,
                        decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(15)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CircularProgressIndicator(),
                            SizedBox(height: 10),
                            Text("Please wait...",
                                style: Theme.of(context).textTheme.bodyText2)
                          ],
                        ),
                      ),
                    ),
                  )
                      : Positioned(bottom: 10, child: SizedBox(height: 0)),
                ],
              ),
            ),
          )
      ),
    );
  }
}
