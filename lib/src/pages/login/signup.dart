import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/user_controller.dart';
import 'package:social_app/src/models/gender_enum.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends StateMVC<SignUp> {

  UserController _con;

  _SignUpState(): super(UserController()){
    _con = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _con.scaffoldKey,
        body: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: double.infinity,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/img/background.jpg",),
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Container(
                    width: double.infinity,
                    height: double.infinity,
                    color: Theme.of(context).primaryColor.withOpacity(0.85)
                ),
                LayoutBuilder(
                  builder: (context, constr) {
                    return SingleChildScrollView(
                      child: SafeArea(
                        child: Container(
                          constraints: BoxConstraints(
                            minHeight: constr.maxHeight
                          ),
                          child: IntrinsicHeight(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                SizedBox(height: 20),
                                Hero(
                                  tag: "loginSignupLogo",
                                  child: Center(child: Image.asset("assets/icon/kungaa.png",height: 40,
                                    alignment: Alignment.center,)),
                                ),
                                SizedBox(height: 20,),
                                Text(S.of(context).create_an_account, textAlign: TextAlign.center,style: TextStyle(
                                  color: Theme.of(context).primaryColorDark,
                                  letterSpacing: 1,
                                  fontSize: 23,
                                ),),
                                SizedBox(height: 8,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Hero(
                                      tag: "loginSignupSubtitle",
                                      child: Text(S.of(context).lets_connect_with_people,
                                        textAlign: TextAlign.center,
                                        style: Theme.of(context).textTheme.subtitle2,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 15,),
                                Form(
                                  key: _con.formKey,
                                  child: Container(
                                    margin: EdgeInsets.symmetric(vertical: 10,horizontal: 45),
                                    child: Column(
                                      children: <Widget>[
                                        TextFormField(
                                          validator: (val) => val.isEmpty ? "Enter Valid Name":null,
                                          keyboardType: TextInputType.text,
                                          decoration: InputDecoration(
                                            enabledBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(color: Theme.of(context).primaryColorDark)
                                            ),
                                            hintText: S.of(context).name,
                                            hintStyle: TextStyle(
                                                color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                                                fontSize: 15
                                            ),
                                            prefixIcon: ImageIcon(AssetImage("assets/icons/profile.png")),
                                            prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                          ),
                                          onSaved: (val){
                                            _con.user.name = val;
                                          },
                                        ),
                                        SizedBox(height: 16,),
                                        TextFormField(
                                          validator: (val) => val.isEmpty || !val.contains("@") || !_con.isValidEmail(val) ? "Enter Valid Email":null,
                                          keyboardType: TextInputType.emailAddress,
                                          decoration: InputDecoration(
                                            enabledBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(color: Theme.of(context).primaryColorDark)
                                            ),
                                            hintText: S.of(context).email,
                                            hintStyle: TextStyle(
                                                color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                                                fontSize: 15
                                            ),
                                            prefixIcon: ImageIcon(AssetImage("assets/icons/email.png")),
                                            prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                          ),
                                          onSaved: (val){
                                            _con.user.email = val;
                                          },
                                        ),
                                        SizedBox(height: 16,),
                                        TextFormField(
                                          obscureText: _con.hidePassword,
                                          validator: (val) => val.isEmpty ? "Enter Password":null,
                                          decoration: InputDecoration(
                                              enabledBorder: UnderlineInputBorder(
                                                  borderSide: BorderSide(color: Theme.of(context).primaryColorDark)
                                              ),
                                              hintText: S.of(context).password,
                                              hintStyle: TextStyle(
                                                  color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                                                  fontSize: 15
                                              ),
                                              prefixIcon: ImageIcon(AssetImage("assets/icons/lock.png")),
                                              prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                              suffixIcon: IconButton(
                                                onPressed: (){
                                                  setState(() { _con.hidePassword = !_con.hidePassword; });
                                                },
                                                color: Theme.of(context).focusColor,
                                                icon: Icon(_con.hidePassword ? Icons.visibility : Icons.visibility_off),
                                              )
                                          ),
                                          onSaved: (val){
                                            _con.user.password = val;
                                          },
                                        ),
                                        SizedBox(height: 16,),
                                        TextFormField(
                                          obscureText: _con.hidePassword,
                                          validator: (val) => val.isEmpty ? "Enter Password":null,
                                          decoration: InputDecoration(
                                              enabledBorder: UnderlineInputBorder(
                                                  borderSide: BorderSide(color: Theme.of(context).primaryColorDark)
                                              ),
                                              hintText: S.of(context).confirm_password,
                                              hintStyle: TextStyle(
                                                  color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                                                  fontSize: 15
                                              ),
                                              prefixIcon: ImageIcon(AssetImage("assets/icons/lock.png")),
                                              prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                              suffixIcon: IconButton(
                                                onPressed: (){
                                                  setState(() { _con.hidePassword = !_con.hidePassword; });
                                                },
                                                color: Theme.of(context).focusColor,
                                                icon: Icon(_con.hidePassword ? Icons.visibility : Icons.visibility_off),
                                              )
                                          ),
                                          onSaved: (val){
                                          },
                                        ),
                                        SizedBox(height: 16,),
                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(S.of(context).gender, style: Theme.of(context).textTheme.subtitle2),
                                            SizedBox(height: 10,),
                                            Row(
                                              children: [
                                                Container(
                                                  width: 120,
                                                  height: 20,
                                                  child: Row(
                                                    children: [
                                                      Radio(
                                                        value: GenderEnum.male,
                                                        activeColor: Theme.of(context).accentColor,
                                                        groupValue: _con.selectedGender,
                                                        onChanged: (GenderEnum value) {
                                                          setState(() {
                                                            _con.selectedGender = value;
                                                            _con.user.gender = "male";
                                                          });
                                                        },
                                                      ),
                                                      SizedBox(width: 10),
                                                      Text("Male", style: Theme.of(context).textTheme.bodyText1,)
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                    width: 120,
                                                    height: 20,
                                                    child:Row(
                                                      children: [
                                                        Radio(
                                                          value: GenderEnum.female,
                                                          activeColor: Theme.of(context).accentColor,
                                                          groupValue: _con.selectedGender,
                                                          onChanged: (GenderEnum value) {
                                                            setState(() {
                                                              _con.selectedGender = value;
                                                              _con.user.gender = "female";
                                                            });
                                                          },
                                                        ),
                                                        SizedBox(width: 10),
                                                        Text("Female", style: Theme.of(context).textTheme.bodyText1,),
                                                      ],
                                                    )
                                                ),
                                              ],
                                            ),
                                            SizedBox(height: 10,),
                                            Container(
                                              width: 120,
                                              height: 20,
                                              child: Row(
                                                children: [
                                                  Radio(
                                                    value: GenderEnum.custom,
                                                    activeColor: Theme.of(context).accentColor,
                                                    groupValue: _con.selectedGender,
                                                    onChanged: (GenderEnum value) {
                                                      setState(() {
                                                        _con.selectedGender = value;
                                                        _con.user.gender = "custom";
                                                      });
                                                    },
                                                  ),
                                                  SizedBox(width: 10),
                                                  Text("Other", style: Theme.of(context).textTheme.bodyText1,),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),

                                        SizedBox(height: 30,),
                                        Hero(
                                          tag: 'authUser',
                                          child: Container(
                                            alignment: Alignment.center,
                                            width: double.infinity,
                                            child: OutlineButton(
                                              borderSide: BorderSide(color: Theme.of(context).accentColor, width: 2,),
                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
                                              textColor: Theme.of(context).accentColor,
                                              highlightedBorderColor: Theme.of(context).accentColor,
                                              textTheme: ButtonTextTheme.accent,
                                              onPressed: (){
                                                _con.register();
                                              },
                                              child: Text(S.of(context).create_account, style: Theme.of(context).textTheme.subtitle2.merge(
                                                  TextStyle(color: Theme.of(context).accentColor, letterSpacing: 1)
                                              )),
                                              color: Theme.of(context).accentColor,
                                              padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width/5.6, vertical: 10),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(height: 15,),
                                GestureDetector(
                                  onTap: (){
                                    Navigator.of(context).pushReplacementNamed("/Login");
                                  },
                                  child: Text(S.of(context).already_have_an_account,
                                      style: Theme.of(context).textTheme.subtitle2.merge(
                                          TextStyle(decoration: TextDecoration.underline,
                                              color: Theme.of(context).focusColor
                                          ))
                                  ),
                                ),
                                SizedBox(height: 15,),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),


                ///Loader
                _con.isLoading
                    ? Container(
                  color: Colors.black.withOpacity(0.5),
                  child: Center(
                    child: Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(15)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CircularProgressIndicator(),
                          SizedBox(height: 10),
                          Text("Please wait...",
                              style: Theme.of(context).textTheme.bodyText2)
                        ],
                      ),
                    ),
                  ),
                )
                    : Positioned(bottom: 10, child: SizedBox(height: 0)),

              ],
            ),
          ),
        )
    );
  }
}
