import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/user_controller.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends StateMVC<Login> {
  UserController _con;

  _LoginState() : super(UserController()) {
    _con = controller;
  }



  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _con.scaffoldKey,
        body: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: double.infinity,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/img/background.jpg",),
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: double.infinity,
                  color: Theme.of(context).primaryColor.withOpacity(0.85)
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Hero(
                        tag: "loginSignupLogo",
                        child: Center(child: Image.asset("assets/icon/kungaa.png",height: 40,
                         alignment: Alignment.center,)),
                      ),
                      SizedBox(height: 30,),
                      Text(S.of(context).signIn, textAlign: TextAlign.center,style: TextStyle(
                        color: Theme.of(context).primaryColorDark,
                            letterSpacing: 1,
                            fontSize: 23,
                          ),),
                      SizedBox(height: 8,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Hero(
                            tag: "loginSignupSubtitle",
                            child: Text(S.of(context).lets_connect_with_people,
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.subtitle2,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 30,),
                      Form(
                        key: _con.formKey,
                        child: Container(
                          margin: EdgeInsets.symmetric(vertical: 10,horizontal: 45),
                          child: Column(
                            children: <Widget>[
                              TextFormField(
                                validator: (val) => val.isEmpty || !val.contains("@") || !_con.isValidEmail(val) ? "Enter Valid Email" : null,
                                keyboardType: TextInputType.emailAddress,
                                decoration: InputDecoration(
                                  enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: Theme.of(context).primaryColorDark)
                                  ),
                                  hintText: S.of(context).email,
                                    hintStyle: TextStyle(
                                    color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                                    fontSize: 15
                                ),
                                  prefixIcon: ImageIcon(AssetImage("assets/icons/email.png")),
                                  prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                ),
                                onSaved: (val){
                                  _con.user.email = val;
                                },
                              ),
                              SizedBox(height: 16,),
                              TextFormField(
                                obscureText: _con.hidePassword,
                                validator: (val) => val.isEmpty ? "Enter Password":null,
                                decoration: InputDecoration(
                                  enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: Theme.of(context).primaryColorDark)
                                  ),
                                  hintText: S.of(context).password,
                                    hintStyle: TextStyle(
                                        color: Theme.of(context).primaryColorDark.withOpacity(0.6),
                                    fontSize: 15
                                ),
                                    prefixIcon: ImageIcon(AssetImage("assets/icons/lock.png")),
                                    prefixIconConstraints: BoxConstraints(maxWidth: 100, minWidth: 50),
                                  suffixIcon: IconButton(
                                    onPressed: (){
                                      setState(() { _con.hidePassword = !_con.hidePassword; });
                                    },
                                    color: Theme.of(context).focusColor,
                                    icon: Icon(_con.hidePassword ? Icons.visibility : Icons.visibility_off),
                                  )
                                ),
                                onSaved: (val){
                                  _con.user.password = val;
                                },
                              ),
                              SizedBox(height: 30,),
                              Hero(
                                tag: "authUser",
                                child: Container(
                                  alignment: Alignment.center,
                                  width: double.infinity,
                                  child: OutlineButton(
                                    borderSide: BorderSide(color: Theme.of(context).accentColor, width: 2,),
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
                                    textColor: Theme.of(context).accentColor,
                                    highlightedBorderColor: Theme.of(context).accentColor,
                                    textTheme: ButtonTextTheme.accent,
                                    onPressed: (){
                                      _con.loginUser();
                                    },
                                    child: Text(S.of(context).submit, style: Theme.of(context).textTheme.subtitle2.merge(
                                      TextStyle(color: Theme.of(context).accentColor, letterSpacing: 1)
                                    )),
                                    color: Theme.of(context).accentColor,
                                    padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width/3.6, vertical: 10),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 20,),
                      Text("OR",style: Theme.of(context).textTheme.subtitle2.merge(
                          TextStyle(color: Theme.of(context).focusColor))),
                      SizedBox(height: 20,),
                      SizedBox(height: 30,),
                      GestureDetector(
                        onTap: (){
                          Navigator.of(context).pushNamed("/Signup");
                        },
                        child: Text(S.of(context).dont_have_account,
                            style: Theme.of(context).textTheme.subtitle2.merge(
                                TextStyle(decoration: TextDecoration.underline,
                                  color: Theme.of(context).focusColor
                                ))
                        ),
                      )
                    ],
                  ),
                ),


                ///Loader
                _con.isLoading
                    ? Container(
                  color: Colors.black.withOpacity(0.5),
                  child: Center(
                    child: Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(15)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CircularProgressIndicator(),
                          SizedBox(height: 10),
                          Text("Please wait...",
                              style: Theme.of(context).textTheme.bodyText2)
                        ],
                      ),
                    ),
                  ),
                )
                    : Positioned(bottom: 10, child: SizedBox(height: 0)),


              ],
            ),
          ),
        )
    );
  }
}
