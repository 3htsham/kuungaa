import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/controller.dart';
import 'package:social_app/src/repositories/user_repository.dart' as userRepo;

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends StateMVC<SplashScreen> {
  Controller _con;

  _SplashScreenState() :super(Controller()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    loadData();
  }

  Future<Timer> loadData() async {
    return new Timer(Duration(milliseconds: 3000), onDoneLoading);
  }

  onDoneLoading() async {
    // if(await _con.checkConnection()) {
    //   SchedulerBinding.instance.addPostFrameCallback((_) {
        if (userRepo.currentUser.apiToken == null) {
          Navigator.of(context).pushReplacementNamed('/Login');
        } else {
          Navigator.of(context).pushReplacementNamed('/Pages', arguments: 0);
        }
      // });
    // } else {
    //   SchedulerBinding.instance.addPostFrameCallback((_) {
    //     Navigator.of(context).pushReplacementNamed('/NoInternet');
    //   });
    // }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _con.scaffoldKey,
      backgroundColor: Colors.black54,
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset("assets/icon/kungaa.png", width: 160,),
              // Text(S.of(context).appName, style: Theme.of(context).textTheme.headline2.merge(TextStyle(fontSize: 42,)),),
              SizedBox(height: 10,),
              Text(S.of(context).appSlogan.toUpperCase(), style: Theme.of(context).textTheme.subtitle2.merge(
                TextStyle(fontSize: 12, color: Colors.white)
              ),),
            ],
          )
      ),
    );
  }
}
