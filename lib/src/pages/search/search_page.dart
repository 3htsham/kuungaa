import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inview_notifier_list/inview_notifier_list.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/search_controller.dart';
import 'package:social_app/src/elements/bottomsheet/post_comment_newsfeed.dart';
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/elements/others/divider_line_widget.dart';
import 'package:social_app/src/elements/post/shared_post_widget_item.dart';
import 'package:social_app/src/elements/post/user_post_widget_item.dart';
import 'package:social_app/src/elements/search/groups_horizontal_widget.dart';
import 'package:social_app/src/elements/search/people_horizontal_list.dart';
import 'package:social_app/src/elements/search/search_app_bar.dart';
import 'package:social_app/src/elements/search/search_categories_list.dart';
import 'package:social_app/src/elements/search/search_group_grid.dart';
import 'package:social_app/src/elements/search/search_users_grid.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class SearchPageWidget extends StatefulWidget {
  RouteArgument argument;

  SearchPageWidget({this.argument});

  @override
  _SearchPageWidgetState createState() => _SearchPageWidgetState();
}

class _SearchPageWidgetState extends StateMVC<SearchPageWidget> {
  SearchController _con;

  _SearchPageWidgetState() : super(SearchController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    final MediaQueryData media = MediaQuery.of(context);
    final double statusBarHeight = media.padding.top;

    return PickupCallLayout(
      currentUser: _con.currentUser,
      scaffold: Scaffold(
        key: _con.scaffoldKey,
        body: Stack(
          children: [
            Stack(
              children: [
                InViewNotifierList(
                    isInViewPortCondition: (double deltaTop, double deltaBottom, double vpHeight) {
                      return (deltaTop < (0.5 * vpHeight) + 100.0 &&
                          deltaBottom > (0.5 * vpHeight) - 100.0);
                    },
                    itemCount: 1, //Post new thing(1), SuggestedUsers(2), Posts(2)
                    shrinkWrap: true,
                    primary: false,
                    builder: (BuildContext context, int index){
                      return SingleChildScrollView(
                        child: Column(
                          children: [

                            ///Top Padding
                            Padding(
                                padding: EdgeInsets.only(top: statusBarHeight + 58.5 + 10)),

                            ///List of View Search Results By
                            Row(
                              children: [
                                SizedBox(
                                  width: 10,
                                ),

                                SearchCategoriesList(con: _con,),

                                SizedBox(
                                  width: 10,
                                ),
                              ],
                            ),

                            SizedBox(height: 10,),

                            ///Search Results Pages
                            _con.current == _con.showCategories[0] ///ALL
                                ? Container(
                              child: Column(
                                children: [

                                  ///Peoples
                                  _con.searchResults != null && _con.searchResults.users != null && _con.searchResults.users.length > 0
                                      ? DividerLineWidget(text: S.of(context).people,)
                                      : SizedBox(height: 0,),

                                  _con.searchResults != null && _con.searchResults.users != null && _con.searchResults.users.length > 0
                                      ? PeopleHorizontalList(con: _con,)
                                      : SizedBox(height: 0,),

                                  ///Groups
                                  _con.searchResults != null && _con.searchResults.groups != null && _con.searchResults.groups.length > 0
                                      ? DividerLineWidget(text: S.of(context).groups,)
                                      : SizedBox(height: 0,),

                                  _con.searchResults != null && _con.searchResults.groups != null && _con.searchResults.groups.length > 0
                                      ? GroupsHorizontalWidget(con: _con,)
                                      : SizedBox(height: 0,),

                                  ///Posts
                                  _con.searchResults != null && _con.searchResults.posts != null && _con.searchResults.posts.length > 0
                                      ? DividerLineWidget(text: S.of(context).posts,)
                                      : SizedBox(height: 0,),

                                  _con.searchResults != null && _con.searchResults.posts != null && _con.searchResults.posts.length > 0
                                      ? _postsWidget()
                                      : SizedBox(height: 0,),

                                ],
                              ),
                            )


                                : _con.current == _con.showCategories[1] ///Posts
                                    ? Container(
                              child: Column(
                                children: [
                                  ///Posts
                                  DividerLineWidget(text: S.of(context).posts,),

                                  _con.searchResults != null && _con.searchResults.posts != null && _con.searchResults.posts.length > 0
                                      ? _postsWidget()
                                      : Container(
                                    height: 100,
                                    child: Center(
                                      child: Text(S.of(context).no_results, style: textTheme.bodyText2,),
                                    ),
                                  ),
                                ],
                              ),
                            )


                                : _con.current == _con.showCategories[2] ///Groups
                                ? Container(
                              child: Column(
                                children: [
                                  DividerLineWidget(text: S.of(context).groups,),
                                  _con.searchResults != null && _con.searchResults.posts != null && _con.searchResults.posts.length > 0
                                      ? SearchGroupGrid(con: _con, groups: _con.searchResults.groups,)
                                      : Container(
                                    height: 100,
                                    child: Center(
                                      child: Text(S.of(context).no_results, style: textTheme.bodyText2,),
                                    ),
                                  )
                                ],
                              ),
                            )


                                ///People
                                : Container(
                              child: Column(
                                children: [

                                  DividerLineWidget(text: S.of(context).people,),
                                  _con.searchResults != null && _con.searchResults.users != null && _con.searchResults.users.length > 0
                                      ? SearchUsersGrid(con: _con)
                                      : Container(
                                    height: 100,
                                    child: Center(
                                      child: Text(S.of(context).no_results, style: textTheme.bodyText2,),
                                    ),
                                  )

                                ],
                              ),
                            ),


                            ///If is loading
                            _con.isLoading
                                ? CircularLoadingWidget(height: 100,)
                                : SizedBox(height: 0, width: 0,),


                            ///If no results found
                            (!_con.isLoading && _con.isSearched)
                                && (_con.searchResults != null
                                && (_con.searchResults.posts == null || _con.searchResults.posts.length == 0)
                                && (_con.searchResults.users == null || _con.searchResults.users.length == 0)
                                && (_con.searchResults.groups == null || _con.searchResults.groups.length == 0))
                                ? Container(
                              height: 100,
                              child: Center(
                                child: Text(S.of(context).no_results, style: textTheme.bodyText2,),
                              ),
                            )
                                : SizedBox(height: 0, width: 0,),

                          ],
                        ),
                      );
                    }
                ),

                ///App bar for Search Page
                SearhcAppBar(
                  con: _con,
                )
              ],
            ),


            ///Loader
            _con.isShareLoading
                ? Container(
              color: Colors.black.withOpacity(0.5),
              child: Center(
                child: Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(15)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      SizedBox(height: 10),
                      Text("Please wait...",
                          style: Theme.of(context).textTheme.bodyText2)
                    ],
                  ),
                ),
              ),
            )
                : Positioned(bottom: 10, child: SizedBox(height: 0)),

          ],
        ),
      ),
    );
  }

  Widget _postsWidget(){
    return _con.searchResults == null || _con.searchResults.posts == null || _con.searchResults.posts.length == 0
        ? CircularLoadingWidget(height: 100,)
        :
    ListView.builder(
      physics: ScrollPhysics(),
      primary: false,
      shrinkWrap: true,
      itemCount: _con.searchResults.posts.length,
      itemBuilder: (context, index) {
        return InViewNotifierWidget(
            id: _con.searchResults.posts[index].id.toString(),
            builder: (BuildContext context, bool isInView, Widget child){
              Post _post = _con.searchResults.posts[index];

              return _post.shared == true || _post.shared == "true"
                        ? SharedPostWidgetItem(
                            post: _post,
                            options: _con.options,
                            isInView: isInView,
                            likeTap: () {
                              _con.postLike(_post);
                            },
                            commentTap: () {
                              this.postCommentsBottomsheet(context, _post);
                            },
                            shareTap: () {
                              _con.sharePostNow(_post);
                            },
                            onOwnerProfileTap: () {
                              Navigator.of(context).pushNamed(
                                  "/UserProfileDetails",
                                  arguments: RouteArgument(user: _post.owner));
                            },
                            onUserProfileTap: () {
                              Navigator.of(context).pushNamed(
                                  "/UserProfileDetails",
                                  arguments: RouteArgument(user: _post.user));
                            },
                            onOptionsTap: _con.onPostOptions,
                          )
                        : UserPostWidgetItem(
                            post: _post,
                            options: _con.options,
                            isInView: isInView,
                            likeTap: () {
                              _con.postLike(_post);
                            },
                            commentTap: () {
                              this.postCommentsBottomsheet(context, _post);
                            },
                            shareTap: () {
                              _con.sharePostNow(_post);
                            },
                            onProfileTap: () {
                              Navigator.of(context).pushNamed(
                                  "/UserProfileDetails",
                                  arguments: RouteArgument(user: _post.user));
                            },
                            onOptionsTap: _con.onPostOptions,
                          );
                  }
        );
      },
    );
  }

  postCommentsBottomsheet(BuildContext context, Post post) {
    var theme = Theme.of(context);
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: theme.scaffoldBackgroundColor,
        context: context,
        builder: (ctx) {
          return GestureDetector(
            onVerticalDragStart: (_){},
            child: StatefulBuilder(
              builder: (ctx, setState) {
                return PostCommentsBottomSheet(_con.currentUser, post: post,);
              },
            ),
          );
        }
    ).then((value){
      if(value != null) {
        var post = value as Post;
        _con.searchResults.posts.forEach((element) {
          if(element.id == post.id) {
            if(post.comments != null && post.comments.length > 0) {
              setState((){
                element.totalComment = post.comments.length;
                element.comments = post.comments;
              });
            }
          }
        });
      }
    });
  }

}
