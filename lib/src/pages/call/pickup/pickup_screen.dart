import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:social_app/src/controllers/call_controller.dart';
import 'package:social_app/src/models/call.dart';

class PickupScreen extends StatelessWidget {

  final Call call;
  final CallController con;

  PickupScreen({@required this.call, @required this.con});


  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    var callType = call.isVideoCall ? "Video Call" : " Call";

    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(vertical: 100),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [

            Text("Incoming $callType...", style: textTheme.bodyText2.merge(TextStyle(fontSize: 23)),),

            SizedBox(height: 50,),

            ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: call != null && call.receiverPic != null && call.receiverPic.length > 0
                  ? FadeInImage.assetNetwork(
                placeholder: "assets/img/placeholders/profile.png",
                image: call.callerPic,
                height: 100,
                width: 100,
                fit: BoxFit.cover,
              ) : Image.asset("assets/img/placeholders/profile.png", width: 100, height: 100, fit: BoxFit.cover,),
            ),

            SizedBox(height: 15,),

            Text(
              call.callerName,
              style: textTheme.headline6,
            ),
            SizedBox(height: 50,),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                  onPressed: (){con.endCall(this.call, context);},
                  splashColor: Colors.red,
                  icon: Icon(Icons.call_end, color: Colors.redAccent,), ),
                SizedBox(width: 40,),
                IconButton(
                  onPressed: () async {
                    con.pickCall(
                        this.call, context,
                        con.currentUser, con.receiver
                    );
                  },
                  splashColor: theme.accentColor,
                  icon: Icon(CupertinoIcons.phone_solid, color: Colors.white,), ),
              ],
            )


          ],
        ),
      ),
    );
  }
}

