import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:social_app/config/call_methods.dart';
import 'package:social_app/src/controllers/call_controller.dart';
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/models/call.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/pages/call/pickup/pickup_screen.dart';

class PickupCallLayout extends StatelessWidget {
  final Widget scaffold;
  final CallMethods callMethods = CallMethods();
  final User currentUser;
  CallController con =  CallController();

  PickupCallLayout({@required this.scaffold, @required this.currentUser});

  @override
  Widget build(BuildContext context) {
    return this.currentUser != null
        ? StreamBuilder<DocumentSnapshot>(
            stream: callMethods.callStream(uid: this.currentUser.id),
            builder: (context, snapshot) {
              if (snapshot.hasData &&
                  snapshot.data != null &&
                  snapshot.data.data() != null) {
                Call call = Call.fromMap(snapshot.data.data());

                if (!call.hasDialed) {
                  CallController con = CallController();
                  con.currentUser = this.currentUser;
                  con.playRingtone();
                  return PickupScreen(call: call, con: con);
                } else {
                  con.stopRingtone();
                  return scaffold;
                }
              } else {
                con.stopRingtone();
                return scaffold;
              }
            })
        : Scaffold(
            body: Center(
              child: CircularLoadingWidget(
                height: 100,
              ),
            ),
          );
  }
}
