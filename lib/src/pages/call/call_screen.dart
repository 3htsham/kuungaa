import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/src/controllers/call_controller.dart';
import 'package:screen/screen.dart' as wake;
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/config/app_config.dart' as config;

import 'package:social_app/config/agora_config.dart';
import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:agora_rtc_engine/rtc_local_view.dart' as RtcLocalView;
import 'package:agora_rtc_engine/rtc_remote_view.dart' as RtcRemoteView;

class CallScreenWidget extends StatefulWidget {
  final RouteArgument argument;

  CallScreenWidget({@required this.argument});

  @override
  _CallScreenWidgetState createState() => _CallScreenWidgetState();
}

class _CallScreenWidgetState extends StateMVC<CallScreenWidget> {

  CallController _con;
  StreamSubscription callStreamSubscription;

  final _users = <int>[];
  final _infoStrings = <String>[];
  bool muted = false;
  bool isVideoOff = false;
  RtcEngine _engine;
  ClientRole role;
  //ClientRole.Broadcaster
  //ClientRole.Audience
  String callStatus = "Calling...";

  _CallScreenWidgetState() : super(CallController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.currentUser = widget.argument.currentUser;
    _con.call = widget.argument.call;
    // role = _con.call.hasDialed ? ClientRole.Broadcaster : ClientRole.Audience;
    role = ClientRole.Broadcaster;
    super.initState();
    _con.stopRingtone();
    addPostFrameCallback();
    // initialize agora sdk
    initializeAgora();
    wake.Screen.keepOn(true);
  }

  
  Future<void> initializeAgora() async {
    if (APP_ID.isEmpty) {
      setState(() {
        _infoStrings.add(
          'APP_ID missing, please provide your APP_ID in settings.dart',
        );
        _infoStrings.add('Agora Engine is not starting');
      });
      return;
    }

    await _initAgoraRtcEngine();
    _addAgoraEventHandlers();
    await _engine.enableWebSdkInteroperability(true);
    VideoEncoderConfiguration configuration = VideoEncoderConfiguration();
    configuration.dimensions = VideoDimensions(1920, 1080);
    await _engine.setVideoEncoderConfiguration(configuration);
    // await _engine.joinChannel(Token, _con.call.channelId, null, 0);
    await _engine.joinChannel(null, _con.call.channelId, null, 0);
  }

  /// Create agora sdk instance and initialize
  Future<void> _initAgoraRtcEngine() async {
    _engine = await RtcEngine.create(APP_ID);
    if(_con.call.isVideoCall) {
      await _engine.enableVideo();
    }
    // await _engine.setParameters(parameters);
    await _engine.setChannelProfile(ChannelProfile.LiveBroadcasting);
    await _engine.setClientRole(this.role);
  }

  /// Add agora event handlers
  void _addAgoraEventHandlers() {
    _engine.setEventHandler(RtcEngineEventHandler(error: (code) {
      setState(() {
        final info = 'onError: $code';
        _infoStrings.add(info);
      });
    }, joinChannelSuccess: (channel, uid, elapsed) {
      setState(() {
        final info = 'onJoinChannel: $channel, uid: $uid';
        _infoStrings.add(info);
      });
    }, leaveChannel: (stats) {
      // _con.endCall(_con.call, context);
      setState(() {
        _infoStrings.add('onLeaveChannel');
        _users.clear();
      });
    }, userJoined: (uid, elapsed) {
      setState(() {
        final info = 'userJoined: $uid';
        callStatus = "On Call";
        _infoStrings.add(info);
        _users.add(uid);
      });
    }, userOffline: (uid, elapsed) {
      _con.endCall(_con.call, context);
      setState(() {
        final info = 'userOffline: $uid';
        callStatus = "Reconnecting...";
        _infoStrings.add(info);
        _users.remove(uid);
      });
    }, firstRemoteVideoFrame: (uid, width, height, elapsed) {
      setState(() {
        final info = 'firstRemoteVideo: $uid ${width}x $height';
        _infoStrings.add(info);
      });
    }));
  }

  void _onToggleMute() {
    setState(() {
      muted = !muted;
    });
    _engine.muteLocalAudioStream(muted);
  }

  void _onSwitchCamera() {
    _engine.switchCamera();
  }

  void _onToggleCamera(){
    setState(() {
      isVideoOff = !isVideoOff;
    });
    _engine.enableLocalVideo(!isVideoOff);
  }

  @override
  void dispose(){
    // clear users
    _users.clear();
    // destroy sdk
    _engine.leaveChannel();
    _engine.destroy();
    callStreamSubscription.cancel();
    super.dispose();
  }

  addPostFrameCallback() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      callStreamSubscription = _con.callMethods.callStream(uid: _con.currentUser.id)
          .listen((snapshot) {
            switch(snapshot.data()) {
              case null:
                Navigator.of(context).pop();
                break;
              default:
                break;
            }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        body: Center(
          child: Stack(
            children: <Widget>[

              _con.call.isVideoCall ? _videoViews() : _profileViews(),

              // _panel(),
              _toolbar(),


              Container(
                alignment: Alignment.bottomCenter,
                padding: EdgeInsets.only(bottom: 20),
                child: Text(
                  callStatus,
                  style: textTheme.bodyText2
                      .merge(TextStyle(shadows: config.Colors().textShadowBlack)),

                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _profileViews(){
    return Stack(
      children: [
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child:  _con.call != null && _con.call.receiverPic != null && _con.call.receiverPic.length > 0
              ? FadeInImage.assetNetwork(
            placeholder: "assets/img/placeholders/profile.png",
            image: _con.call.hasDialed ? _con.call.receiverPic : _con.call.callerPic,
            fit: BoxFit.cover,
          ) : Image.asset("assets/img/placeholders/profile.png", fit: BoxFit.cover,),
        ),
        Container(
          alignment: Alignment.topLeft,
          child: Padding(
            padding: const EdgeInsets.only(top: 34.0, left: 10),
            child: Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black54,
                    blurRadius: 10,
                    spreadRadius: 3,
                    offset: Offset(1, 1)
                  )
                ]
              ),
              child: _con.call != null && _con.call.callerPic != null && _con.call.callerPic.length > 0
                  ? FadeInImage.assetNetwork(
                placeholder: "assets/img/placeholders/profile.png",
                image: _con.call.hasDialed ? _con.call.callerPic : _con.call.receiverPic,
                fit: BoxFit.cover,
              ) : Image.asset("assets/img/placeholders/profile.png",  fit: BoxFit.cover,),
            ),
          ),
        ),
        // Positioned(
        //   top: 0,
        //   left: 0,
        //   child: Padding(
        //     padding: const EdgeInsets.only(top: 34.0, left: 10),
        //     child: Container(
        //       height: 128,
        //       width: 72,
        //       child: _videoView(views[0]),
        //     ),
        //   ),
        // ),
      ],
    );
  }

  Widget _videoViews() {
    final views = _getRenderViews();
    switch (views.length) {
      case 1:
        return Container(
            child: Column(
              children: <Widget>[_videoView(views[0])],
            ));
      case 2:
        var size = MediaQuery.of(context).size;
        return Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: views[1],
              ),
              Container(
                // alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.only(top: 34.0, left: 10),
                  child: Container(
                    height: 128,
                    // height: size.height * 0.25,
                    width: 72,
                    // width: size.width * 0.25,
                    child: views[0],
                  ),
                ),
              ),
            ],
        );
      case 3:
        return Container(
            child: Column(
              children: <Widget>[
                _expandedVideoRow(views.sublist(0, 2)),
                _expandedVideoRow(views.sublist(2, 3))
              ],
            ));
      case 4:
        return Container(
            child: Column(
              children: <Widget>[
                _expandedVideoRow(views.sublist(0, 2)),
                _expandedVideoRow(views.sublist(2, 4))
              ],
            ));
      default:
    }
    return Container();
  }

  /// Video layout wrapper
  Widget _viewRows() {
    final views = _getRenderViews();
    switch (views.length) {
      case 1:
        return Container(
            child: Column(
          children: <Widget>[_videoView(views[0])],
        ));
      case 2:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow([views[0]]),
            _expandedVideoRow([views[1]])
          ],
        ));
      case 3:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow(views.sublist(0, 2)),
            _expandedVideoRow(views.sublist(2, 3))
          ],
        ));
      case 4:
        return Container(
            child: Column(
          children: <Widget>[
            _expandedVideoRow(views.sublist(0, 2)),
            _expandedVideoRow(views.sublist(2, 4))
          ],
        ));
      default:
    }
    return Container();
  }

   /// Video view row wrapper
  Widget _expandedVideoRow(List<Widget> views) {
    final wrappedViews = views.map<Widget>(_videoView).toList();
    return Expanded(
      child: Row(
        children: wrappedViews,
      ),
    );
  }

  /// Helper function to get list of native views
  List<Widget> _getRenderViews() {
    final List<StatefulWidget> list = [];
    if (this.role == ClientRole.Broadcaster) {
      list.add(RtcLocalView.SurfaceView());
    }
    _users.forEach((int uid) => list.add(RtcRemoteView.SurfaceView(uid: uid)));
    return list;
  }

  /// Video view wrapper
  Widget _videoView(view) {
    return Expanded(child: Container(child: view));
  }

  /// Info panel to show logs
  Widget _panel() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 48),
      alignment: Alignment.bottomCenter,
      child: FractionallySizedBox(
        heightFactor: 0.5,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 48),
          child: ListView.builder(
            reverse: true,
            itemCount: _infoStrings.length,
            itemBuilder: (BuildContext context, int index) {
              if (_infoStrings.isEmpty) {
                return null;
              }
              return Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 3,
                  horizontal: 10,
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Flexible(
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: 2,
                          horizontal: 5,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.yellowAccent,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          _infoStrings[index],
                          style: TextStyle(color: Colors.blueGrey),
                        ),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  /// Toolbar layout
  Widget _toolbar() {
    if (this.role == ClientRole.Audience) return Container();
    return Container(
      alignment: Alignment.bottomCenter,
      padding: const EdgeInsets.symmetric(vertical: 48),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
        _con.call.isVideoCall 
            ? RawMaterialButton(
            onPressed: _onToggleCamera,
            child: Icon(
              isVideoOff ? Icons.videocam_off : Icons.videocam,
              color: Colors.greenAccent,
              size: 20.0,
            ),
            shape: CircleBorder(),
            elevation: 2.0,
            fillColor: Colors.white,
            padding: const EdgeInsets.all(15.0),
          ) 
            : SizedBox(height: 0, width: 0),
          SizedBox(height: 10,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RawMaterialButton(
                onPressed: _onToggleMute,
                child: Icon(
                  muted ? Icons.mic_off : Icons.mic,
                  color: muted ? Colors.white : Colors.blueAccent,
                  size: 20.0,
                ),
                shape: CircleBorder(),
                elevation: 2.0,
                fillColor: muted ? Colors.blueAccent : Colors.white,
                padding: const EdgeInsets.all(12.0),
              ),
              RawMaterialButton(
                onPressed: () {
                  _con.endCall(_con.call, context);
                },
                child: Icon(
                  Icons.call_end,
                  color: Colors.white,
                  size: 35.0,
                ),
                shape: CircleBorder(),
                elevation: 2.0,
                fillColor: Colors.redAccent,
                padding: const EdgeInsets.all(15.0),
              ),
              RawMaterialButton(
                onPressed: (){
                  if(_con.call.isVideoCall) {
                    _onSwitchCamera();
                  }
                },
                child: Icon(
                  Icons.switch_camera,
                  color: _con.call.isVideoCall ? Colors.blueAccent : Colors.blueAccent.withOpacity(0.6),
                  size: 20.0,
                ),
                shape: CircleBorder(),
                elevation: 2.0,
                fillColor: Colors.white,
                padding: const EdgeInsets.all(12.0),
              )
            ],
          ),
        ],
      ),
    );
  }

}

