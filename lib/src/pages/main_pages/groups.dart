import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/group_controller.dart';
import 'package:social_app/src/elements/group/group_grid.dart';
import 'package:social_app/src/elements/group/group_icon.dart';
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/elements/others/divider_line_widget.dart';
import 'package:social_app/src/elements/search/search.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/pages/call/pickup/pickup_layout.dart';

class GroupsPage extends StatefulWidget {

  @override
  _GroupsPageState createState() => _GroupsPageState();
}

class _GroupsPageState extends StateMVC<GroupsPage>
    with AutomaticKeepAliveClientMixin {

  GroupController _con;

  _GroupsPageState() : super(GroupController()) {
    _con = controller;
  }

  @override
  void initState() {
    _con.getGroups();
    _con.getMyGroups();
    _con.getGroupsIJoined();
    super.initState();
    _con.getCurrentUserNow();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return RefreshIndicator(
        onRefresh: _con.onRefreshGroups,
        child: Scaffold(
            appBar: _appbar(),
            body: Stack(
              children: [
                SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SearchWidget(
                        onTap: (){
                          Navigator.of(context).pushNamed("/SearchPage", arguments: RouteArgument(currentUser: _con.currentUser));
                        },
                      ),

                      _con.myGroups.isEmpty
                          ? SizedBox(height: 0,)
                          : DividerLineWidget(text: S.of(context).your_groups,),
                      _con.myGroups.isEmpty
                          ? SizedBox(height: 0,)
                          : Container(
                        height: 140,
                        child: ListView.builder(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            physics: BouncingScrollPhysics(),
                            itemCount: _con.myGroups.length,
                            itemBuilder: (ctx, index) {
                              return GroupIcon(group: _con.myGroups[index],
                                onTap: (){
                                  Navigator.of(context).pushNamed("/ViewGroup",
                                      arguments: RouteArgument(group: _con.myGroups[index], user: _con.currentUser));
                                },
                                leftMargin: index==0 ? 20 : 5,
                                rightMargin: (index== _con.myGroups.length-1) ? 10 : 0,
                              );
                            }
                        ),
                      ),

                      _con.groupsIJoined.isEmpty
                          ? SizedBox(height: 0,)
                          : DividerLineWidget(text: S.of(context).recently_joined,),
                      _con.groupsIJoined.isEmpty
                          ? SizedBox(height: 0,)
                          : Container(
                        height: 140,
                        child: ListView.builder(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            physics: BouncingScrollPhysics(),
                            itemCount: _con.groupsIJoined.length,
                            itemBuilder: (ctx, index) {
                              return GroupIcon(group: _con.groupsIJoined[index],
                                onTap: (){
                                  Navigator.of(context).pushNamed("/ViewGroup",
                                      arguments: RouteArgument(group: _con.groupsIJoined[index], user: _con.currentUser));
                                },
                                leftMargin: index==0 ? 20 : 5,
                                rightMargin: (index== _con.groupsIJoined.length-1) ? 10 : 0,
                              );
                            }
                        ),
                      ),

                      _con.groups.isEmpty ? SizedBox(height: 0,) : DividerLineWidget(text: S.of(context).others,),
                      _con.groups.isEmpty
                          ? CircularLoadingWidget(height: 50,)
                          : GroupGrid(groups: _con.groups, con: _con,)

                    ],
                  ),
                ),


                ///Loader
                _con.isLoading
                    ? Container(
                  color: Colors.black.withOpacity(0.5),
                  child: Center(
                    child: Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                          borderRadius: BorderRadius.circular(15)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CircularProgressIndicator(),
                          SizedBox(height: 10),
                          Text("Please wait...",
                              style: Theme.of(context).textTheme.bodyText2)
                        ],
                      ),
                    ),
                  ),
                )
                    : Positioned(bottom: 10, child: SizedBox(height: 0)),

              ],
            ),
          ),
    );
  }

  Widget _appbar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text(S.of(context).groups,
        style: Theme.of(context).textTheme.headline4
            .merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      automaticallyImplyLeading: false,
      actions: [
        PopupMenuButton(
            onSelected: (value) {
              _con.onMainGroupsOptions(value);
            },
            icon: RotatedBox(quarterTurns: 1, child: ImageIcon(AssetImage("assets/icons/more.png"), color: Colors.white,),),
            itemBuilder: (context) => List.generate(_con.mainGroupsPageOptions.length, (index) {
              return PopupMenuItem(
                  value: _con.mainGroupsPageOptions[index],
                  child: Text(_con.mainGroupsPageOptions[index], style: Theme.of(context).textTheme.bodyText2,),
              );
            })
        )
      ],
    );
  }

}











