import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:inview_notifier_list/inview_notifier_list.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/config/golbal_strings.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/home_controller.dart';
import 'package:social_app/src/elements/boost/boosted_post_widget_item.dart';
import 'package:social_app/src/elements/bottomsheet/new_post_home.dart';
import 'package:social_app/src/elements/bottomsheet/post_comment_newsfeed.dart';
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/elements/others/divider_line_widget.dart';
import 'package:social_app/src/elements/post/create_new_post_widget.dart';
import 'package:social_app/src/elements/post/shared_post_widget_item.dart';
import 'package:social_app/src/elements/post/user_post_widget_item.dart';
import 'package:social_app/src/elements/status/my_status_home_widget.dart';
import 'package:social_app/src/elements/status/status_widget_item.dart';
import 'package:social_app/src/elements/user/people_widget.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/models/status.dart';

class HomePage extends StatefulWidget {

  HomePage({Key key}) : super(key: key);

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends StateMVC<HomePage> {

  HomeController con;

  
  HomePageState() : super(HomeController()) {
    con = controller;
  }

  @override
  void initState() {
    super.initState();
    con.getUser();
    con.getSponsoredPosts();
    con.getStatuses();
    con.getSuggestedUsers();
    con.getPosts();
  }

  @override
  Widget build(BuildContext context) {

    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return RefreshIndicator(
      onRefresh: () async {},
        child: Scaffold(
          key: con.scaffoldKey,
          appBar: _appbar(),
          body: Stack(
            children: [

              InViewNotifierList(
                isInViewPortCondition: (double deltaTop, double deltaBottom, double vpHeight) {
                return (deltaTop < (0.5 * vpHeight) + 100.0 &&
                    deltaBottom > (0.5 * vpHeight) - 100.0);
              },
                itemCount: 1,
                shrinkWrap: true,
                primary: false,
                  builder: (BuildContext context, int index){
                    return SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [

                          _statuses(),


                          CreateNewPostWidget(user: con.currentUser,
                            onPostTap: (){ postNewThingAtHomeBottomSheet(context, con); },
                            onProfileTap: (){
                              Navigator.of(context).pushNamed("/UserProfileDetails", arguments: RouteArgument(user: con.currentUser)).then((value){
                                con.getUser();
                              });
                            },
                          ),


                          con.peopleYouMayKnow.isNotEmpty
                              ? DividerLineWidget(text: S.of(context).you_may_know,)
                              : SizedBox(height: 0,),


                          con.peopleYouMayKnow.isNotEmpty
                              ? _peopleYouMayKnowWidget()
                              : SizedBox(height: 0,),

                          con.posts.isEmpty
                              ? SizedBox(width: 0, height: 0,)
                              : DividerLineWidget(text: S.of(context).news_feed,),

                          _sponsoredPostsWidget(),

                          _postsWidget(),


                        ],
                      ),
                    );
                  }
              ),

              ///Loader
              con.isLoading
                  ? Container(
                color: Colors.black.withOpacity(0.5),
                child: Center(
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(15)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(),
                        SizedBox(height: 10),
                        Text("Please wait...",
                            style: Theme.of(context).textTheme.bodyText2)
                      ],
                    ),
                  ),
                ),
              )
                  : Positioned(bottom: 10, child: SizedBox(height: 0)),

              con.isSharePostDialog
                  ? sharePostDialog()
                  : Positioned(bottom: 10, child: SizedBox(height: 0)),

            ],
          ),
        )
    );

  }

  Widget _appbar(){
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text(S.of(context).news_feed,
        style: Theme.of(context).textTheme.headline4
            .merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      automaticallyImplyLeading: false,
      actions: [
        IconButton(
          onPressed:(){
            Navigator.of(context).pushNamed("/SearchPage", arguments: RouteArgument(currentUser: con.currentUser));
          },
          icon: ImageIcon(
            AssetImage("assets/icons/search.png"),
            size: 30,
            color: Theme.of(context).accentColor,
          ),
        ),
        IconButton(
          onPressed:(){
            Navigator.of(context).pushNamed("/Chats", arguments: RouteArgument(user: con.currentUser));
          },
          icon: ImageIcon(
            AssetImage("assets/icons/message.png"),
            size: 30,
            color: Theme.of(context).accentColor,
          ),
        ),

      ],
    );
  }

  Widget _peopleYouMayKnowWidget(){
    return Container(
      height: 185,
      child: ListView.builder(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        itemCount: con.peopleYouMayKnow.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (ctx, index) {
          return PeopleWidget(
            user: con.peopleYouMayKnow[index],
            onTap: (){
              //Open User Profile
              Navigator.of(context).pushNamed("/UserProfileDetails", arguments: RouteArgument(user: con.peopleYouMayKnow[index]));
            },
            onAddFriend: (){
              //Send or Cancel Friend Request
              con.sendOrCancelFriendRequest(con.peopleYouMayKnow[index]);
            },
            leftMargin: 10,
            rightMargin: (index == con.peopleYouMayKnow.length-1) ? 10 : 0,
          );
        },
      ),
    );
  }

  Widget _postsWidget(){
    return con.posts.isEmpty
        ? CircularLoadingWidget(height: 100,)
        :
    ListView.builder(
      physics: ScrollPhysics(),
      primary: false,
      shrinkWrap: true,
      itemCount: con.posts.length,
      itemBuilder: (context, index) {
        return InViewNotifierWidget(
          id: con.posts[index].id.toString(),
            builder: (BuildContext context, bool isInView, Widget child){

              Post _post = con.posts[index];

              return _post.shared == true || _post.shared == "true"
                        ? SharedPostWidgetItem(
                            post: _post,
                            options: _post.userId.toString() ==
                                con.currentUser.id.toString()
                                ? con.mySharedPostOptions
                                : con.options,
                            isInView: isInView,
                            likeTap: () {
                              con.postLike(_post);
                            },
                            commentTap: () {
                              this.postCommentsBottomsheet(context, _post);
                            },
                            // shareTap: () {
                            //   _con.showShareDialog(_post);
                            // },
                            onOwnerProfileTap: () {
                              Navigator.of(context).pushNamed(
                                  "/UserProfileDetails",
                                  arguments: RouteArgument(user: _post.owner));
                            },
                            onUserProfileTap: () {
                              Navigator.of(context).pushNamed(
                                  "/UserProfileDetails",
                                  arguments: RouteArgument(user: _post.user));
                            },
                            onOptionsTap: con.onPostOptions,
                            showShare: false
                          )
                        : UserPostWidgetItem(
                            post: _post,
                            options: _post.userId.toString() ==
                                con.currentUser.id.toString()
                                ? con.myPostOptions
                                : con.options,
                            isInView: isInView,
                            likeTap: () {
                              con.postLike(_post);
                            },
                            commentTap: () {
                              this.postCommentsBottomsheet(context, _post);
                            },
                            shareTap: () {
                              con.showShareDialog(_post);
                            },
                            onProfileTap: () {
                              Navigator.of(context).pushNamed(
                                  "/UserProfileDetails",
                                  arguments: RouteArgument(user: _post.user));
                            },
                            onOptionsTap: con.onPostOptions,
                            showShare: _post.postType == POST_FRIENDS || _post.postType == POST_PUBLIC
                          );
                  }
        );
      },
    );
  }

  Widget _sponsoredPostsWidget() {
    return con.sponsoredPosts.isEmpty
        ? SizedBox(height: 0,)
        :
    ListView.builder(
      physics: ScrollPhysics(),
      primary: false,
      shrinkWrap: true,
      itemCount: con.sponsoredPosts.length,
      itemBuilder: (context, index) {
        return InViewNotifierWidget(
            id: con.sponsoredPosts[index].id.toString(),
            builder: (BuildContext context, bool isInView, Widget child){

              Post _post = con.sponsoredPosts[index];

              return BoostedPostWidgetItem(
                post: _post,
                options: _post.userId.toString() ==
                    con.currentUser.id.toString()
                    ? con.sponsoredPostOptions
                    : con.options,
                isInView: isInView,
                likeTap: () {
                  con.postLike(_post);
                },
                commentTap: () {
                  this.postCommentsBottomsheet(context, _post);
                },
                shareTap: () {
                  con.showShareDialog(_post);
                },
                onProfileTap: () {
                  Navigator.of(context).pushNamed(
                      "/UserProfileDetails",
                      arguments: RouteArgument(user: _post.user));
                },
                onOptionsTap: con.onPostOptions,
                onViewCount: (){
                  con.countView(con.sponsoredPosts[index]);
                },
              );
            }
        );
      },
    );
  }

  Widget _statuses() {
    return Container(
      height: 140,
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
        shrinkWrap: false,
          primary: true,
          scrollDirection: Axis.horizontal,
          itemCount: con.statuses.isNotEmpty ? con.statuses.length + 1 : 1,
          itemBuilder: (context, index) {
            if (index == 0) {
              return con.currentUser != null ? MyStatusHomeWidget(
                  currentUser: con.currentUser,
                  isStatus: con.myStatuses.isNotEmpty,
                  status: con.myStatuses.isNotEmpty ? con.myStatuses[0] : Status(),
                  onTap: (){
                    if(!con.myStatuses.isNotEmpty) {
                      Navigator.of(context).pushNamed("/UploadStatus", arguments: RouteArgument(currentUser: con.currentUser)).then((value) {
                        if(value != null) {
                          setState((){
                            con.myStatuses.add(value);
                          });
                        }
                      });
                    } else {
                      Navigator.of(context).pushNamed("/MyStatuses", arguments: RouteArgument(currentUser: con.currentUser, statuses: con.myStatuses));
                    }
                  }
              ) : SizedBox(height: 0,);
            } else {
              return StatusWidgetItem(
                status: con.statuses[index-1],
                isSeen: con.statuses[index-1].seenBy.contains(con.currentUser.id.toString()),
                onTap: () {
                    Navigator.of(context).pushNamed("/ViewStatus", arguments: RouteArgument(currentUser: con.currentUser, status: con.statuses[index-1]));
                  },
              );
            }
          }),
    );
  }

  postNewThingAtHomeBottomSheet(BuildContext context, HomeController con) {
    var theme = Theme.of(context);
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: theme.scaffoldBackgroundColor,
        context: context,
        builder: (ctx) {
          return GestureDetector(
            onVerticalDragStart: (_){},
            child: StatefulBuilder(
              builder: (ctx, setState) {
                return PostNewThingBottomSheetStfulWidget(con: con);
              },
            ),
          );
        }
    ).then((controller) {
      if(controller != null) {
        //We can post here
        var myController  = controller as HomeController;
        if(myController.post != null &&
            (myController.post.text != null || (myController.post.mediaList != null && myController.post.mediaList.length > 0)) ) {
          con.uploadPost(myController.post);
        }
      }
    });
  }

  postCommentsBottomsheet(BuildContext context, Post post) {
    var theme = Theme.of(context);
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: theme.scaffoldBackgroundColor,
        context: context,
        builder: (ctx) {
          return GestureDetector(
            onVerticalDragStart: (_){},
            child: StatefulBuilder(
              builder: (ctx, setState) {
                return PostCommentsBottomSheet(con.currentUser, post: post,);
              },
            ),
          );
        }
    ).then((value){
      if(value != null) {
        var post = value as Post;
        con.posts.forEach((element) {
          if(element.id == post.id) {
            if(post.comments != null && post.comments.length > 0) {
              setState((){
                element.totalComment = post.comments.length;
                element.comments = post.comments;
              });
            }
          }
        });
      }
    });
  }

  Widget sharePostDialog() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Colors.black54,
      child: Center(
        child: Container(
          width: MediaQuery.of(context).size.width * 0.7,
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
          decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: BorderRadius.circular(7),
                boxShadow: [
                  BoxShadow(
                      offset: Offset(1, 1),
                      spreadRadius: 0.5,
                      blurRadius: 5,
                      color: Theme.of(context).focusColor.withOpacity(0.5)
                  )
                ]
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("Share Post with...", style: Theme.of(context).textTheme.subtitle1),
              SizedBox(height: 10),
              ListView.builder(
                itemCount: con.newPostPrivacyOptions.length,
                shrinkWrap: true,
                primary: false,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: (){
                      con.hideShareDialog(con.newPostPrivacyOptions[index].option);
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                      child: Row(
                        children:[
                          ImageIcon(AssetImage(con.newPostPrivacyOptions[index].icon)),
                          SizedBox(width: 10,),
                          Text(
                            con.newPostPrivacyOptions[index].option,
                            style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(fontSize: 18)),
                          )
                        ]
                      )
                    ),
                  );
                }
              )
            ],
          )
        )
      )
    );
  }

}

















