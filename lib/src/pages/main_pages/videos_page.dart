import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/video_page_controller.dart';
import 'package:social_app/src/elements/bottomsheet/post_comment_newsfeed.dart';
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/elements/video/related_videos.dart';
import 'package:social_app/src/elements/video/suggested_videos.dart';
import 'package:social_app/src/elements/video/video_page_current_video_widget.dart';
import 'package:social_app/src/elements/video/video_page_current_video_widget_description.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/route_argument.dart';

class VideosPage extends StatefulWidget {

  VideosPage({Key key}) : super(key: key);

  @override
  VideosPageState createState() => VideosPageState();
}

class VideosPageState extends StateMVC<VideosPage>
    with AutomaticKeepAliveClientMixin {

  VideoPageController con;

  VideosPageState() : super(VideoPageController()) {
    con = controller;
  }

  @override
  void initState() {
    super.initState();
    con.getUser();
    con.getRelatedVideos();
    con.getSuggestedVideos();
  }

  void pauseVideo(){
    if(con != null && con.currentVideoViewKey != null && con.currentVideoViewKey.currentState != null)
      {
        con.currentVideoViewKey.currentState.pauseVideo();
      }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return RefreshIndicator(
          onRefresh: () async {},
          child: Scaffold(
            key: con.scaffoldKey,
            appBar: _appbar(),
            body: Stack(
              children: [
                Column(
                  children: [
                    con.currentVideo == null
                        ? CircularLoadingWidget(height: 100,)
                        : VideoPageCurrentVideoWidget(videoPost: con.currentVideo, con: con,),
                    SizedBox(height: 3,),
                    Expanded(
                      flex: 1,
                      child: SingleChildScrollView(
                        controller: con.scrollController,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [

                            con.currentVideo == null
                              ? SizedBox(
                                  height: 0,
                                )
                              : VideoPageCurrentVideoDescription(
                                  videoPost: con.currentVideo,
                                  con: con,
                                  likeTap: () {
                                    con.postLike(con.currentVideo.post);
                                  },
                                  commentTap: () {
                                    this.postCommentsBottomsheet(
                                        context, con.currentVideo.post);
                                  },
                                  shareTap: () {
                                    con.showShareDialog(con.currentVideo.post);
                                    },
                                  onProfileTap: () {
                                    Navigator.of(context).pushNamed(
                                        "/UserProfileDetails",
                                        arguments: RouteArgument(
                                            user: con.currentVideo.post.user));
                                  },
                                  onOptionsTap: con.onPostOptions,
                                  options: con.options,
                                ),

                            RelatedVideos(con: con,),
                            SuggestedVideos(con: con,),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),


                con.isSharePostDialog
                    ? sharePostDialog()
                    : Positioned(bottom: 10, child: SizedBox(height: 0)),
              ],
            ),
          ),
      );
  }

  Widget _appbar(){
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text(S.of(context).videos,
        style: Theme.of(context).textTheme.headline4
            .merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      automaticallyImplyLeading: false,
      actions: [
        // IconButton(
        //   onPressed:(){
        //     //Open the conversations page
        //   },
        //   icon: ImageIcon(
        //     AssetImage("assets/icons/message.png"),
        //     size: 30,
        //     color: Theme.of(context).accentColor,
        //   ),
        // )
      ],
    );
  }


  postCommentsBottomsheet(BuildContext context, Post post) {
    var theme = Theme.of(context);
    showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: false,
        backgroundColor: theme.scaffoldBackgroundColor,
        context: context,
        builder: (ctx) {
          return GestureDetector(
            onVerticalDragStart: (_){},
            child: StatefulBuilder(
              builder: (ctx, setState) {
                return PostCommentsBottomSheet(con.currentUser, post: post,);
              },
            ),
          );
        }
    ).then((value){
      if(value != null) {
        var post = value as Post;
        setState((){
          con.currentVideo.post.totalComment = post.comments.length;
          con.currentVideo.post.comments = post.comments;
        });
      }
    });
  }

  Widget sharePostDialog() {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.black54,
        child: Center(
            child: Container(
                width: MediaQuery.of(context).size.width * 0.7,
                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(7),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(1, 1),
                          spreadRadius: 0.5,
                          blurRadius: 5,
                          color: Theme.of(context).focusColor.withOpacity(0.5)
                      )
                    ]
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text("Share Post with...", style: Theme.of(context).textTheme.subtitle1),
                    SizedBox(height: 10),
                    ListView.builder(
                        itemCount: con.newPostPrivacyOptions.length,
                        shrinkWrap: true,
                        primary: false,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: (){
                              con.hideShareDialog(con.newPostPrivacyOptions[index].option);
                            },
                            child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                                child: Row(
                                    children:[
                                      ImageIcon(AssetImage(con.newPostPrivacyOptions[index].icon)),
                                      SizedBox(width: 10,),
                                      Text(
                                        con.newPostPrivacyOptions[index].option,
                                        style: Theme.of(context).textTheme.bodyText1.merge(TextStyle(fontSize: 18)),
                                      )
                                    ]
                                )
                            ),
                          );
                        }
                    )
                  ],
                )
            )
        )
    );
  }

}








