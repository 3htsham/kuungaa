import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/profile_page_controller.dart';
import 'package:social_app/src/elements/others/divider_line_widget.dart';
import 'package:social_app/src/elements/others/my_scroll_behaviour.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/repositories/user_repository.dart' as userRepo;

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends StateMVC<ProfilePage> {

  ProfilePageController _con;

  _ProfilePageState() : super(ProfilePageController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.getCurrentUserNow();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;
    var handler;
    return RefreshIndicator(
        onRefresh: () async {},
        child: ScrollConfiguration(
          behavior: MyScrollBehavior(),
          child: NestedScrollView(
              physics: BouncingScrollPhysics(),
              headerSliverBuilder: (context, innerBoxIsScrolled) {
                handler = NestedScrollView.sliverOverlapAbsorberHandleFor(context);
                return [
                  SliverOverlapAbsorber(
                    handle: handler,
                    sliver: SliverAppBar(
                      actions: [

                      ],
                      forceElevated: innerBoxIsScrolled,
                      expandedHeight: size.width,
                      floating: false,
                      pinned: true,
                      elevation: 0,
                      centerTitle: true,
                      automaticallyImplyLeading: false,
                      backgroundColor: theme.scaffoldBackgroundColor,
                      flexibleSpace: FlexibleSpaceBar(
                        titlePadding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                        title: Text(
                          _con.currentUser!=null ? _con.currentUser.name : "",
                          style: textTheme.headline4
                              .merge(TextStyle(color: theme.accentColor, fontSize: 15)),
                        ),
                        background: Stack(
                          children: [
                            _con.currentUser != null 
                                ? FadeInImage.assetNetwork(
                              placeholder: "assets/img/placeholders/profile.png",
                              image: _con.currentUser.image,
                              fit: BoxFit.cover,
                              height: size.width,
                              width: size.width,
                            ) : Image.asset("assets/img/placeholders/profile.png", fit: BoxFit.cover,
                              height: size.width,
                              width: size.width,),
                            Container(
                              height: size.width,
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                      colors: [
                                        theme.scaffoldBackgroundColor.withOpacity(0.2),
                                        theme.scaffoldBackgroundColor.withOpacity(0.6),
                                        theme.scaffoldBackgroundColor
                                      ],
                                      stops: [0.0, 0.5, 1]
                                  )
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  )

                ];
              },
              body: Builder(
                  builder: (context) {
                    return CustomScrollView(
                      slivers: [
                        SliverOverlapInjector(
                          handle: handler,
                        ),
                        SliverToBoxAdapter(
                          child: SingleChildScrollView(
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                              child: Container(
                                // margin: EdgeInsets.only(bottom: 10),
                                decoration: BoxDecoration(
                                    color: theme.primaryColor,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                          offset: Offset(1, 1),
                                          spreadRadius: 0.5,
                                          blurRadius: 5,
                                          color: Theme.of(context).focusColor.withOpacity(0.5)
                                      )
                                    ]
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 7),
                                      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 7),
                                      decoration: BoxDecoration(
                                          color: theme.focusColor.withOpacity(0.4),
                                          borderRadius: BorderRadius.circular(10)
                                      ),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          Column(
                                            children: [
                                              Text('${_con.currentUser != null ? _con.currentUser.totalPosts ?? "0" : "0"}', style: textTheme.headline5.merge(TextStyle(color: theme.accentColor)),),
                                              Text(S.of(context).posts, style: textTheme.bodyText1,)
                                            ],
                                          ),
                                          Container(
                                            height: 18,
                                            width: 1,
                                            color: Theme.of(context).focusColor,
                                          ),
                                          Column(
                                            children: [
                                              Text('${_con.currentUser != null ? _con.currentUser.totalFriends ?? "0" : "0"}', style: textTheme.headline5.merge(TextStyle(color: theme.accentColor)),),
                                              Text(S.of(context).friends, style: textTheme.bodyText1,)
                                            ],
                                          ),
                                          // Container(
                                          //   height: 18,
                                          //   width: 1,
                                          //   color: Theme.of(context).focusColor,
                                          // ),
                                          // Column(
                                          //   children: [
                                          //     Text("37", style: textTheme.headline5.merge(TextStyle(color: theme.accentColor)),),
                                          //     Text("Followers", style: textTheme.bodyText1,)
                                          //   ],
                                          // ),
                                        ],
                                      ),
                                    ),
                                    ListTile(
                                      leading: ImageIcon(AssetImage("assets/icons/profile.png"), color: theme.accentColor,),
                                      title: Text(S.of(context).my_profile, style: textTheme.subtitle2,),
                                      trailing: Icon(Icons.arrow_forward_ios, size: 15, color: theme.focusColor,),
                                      onTap: (){
                                        Navigator.of(context).pushNamed("/UserProfileDetails", arguments: RouteArgument(user: _con.currentUser));
                                        },
                                    ),
                                    // ListTile(
                                    //   leading: ImageIcon(AssetImage("assets/icons/recent.png"), color: theme.accentColor),
                                    //   title: Text("Recent Activity", style: textTheme.subtitle2,),
                                    //   trailing: Icon(Icons.arrow_forward_ios, size: 15, color: theme.focusColor,),
                                    //   onTap: (){},
                                    // ),
                                    ListTile(
                                      leading: ImageIcon(AssetImage("assets/icons/save.png"), color: theme.accentColor),
                                      title: Text(S.of(context).saved, style: textTheme.subtitle2,),
                                      trailing: Icon(Icons.arrow_forward_ios, size: 15, color: theme.focusColor,),
                                      onTap: (){
                                          Navigator.of(context).pushNamed("/ViewSavedPosts", arguments: RouteArgument(currentUser: _con.currentUser));
                                        },
                                    ),

                                    ListTile(
                                      leading: ImageIcon(AssetImage("assets/icons/comapign.png"), color: theme.accentColor,),
                                      title: Text(S.of(context).my_campaigns, style: textTheme.subtitle2,),
                                      trailing: Icon(Icons.arrow_forward_ios, size: 15, color: theme.focusColor,),
                                      onTap: (){
                                        Navigator.of(context).pushNamed("/MyCampaigns", arguments: RouteArgument(currentUser: _con.currentUser));
                                      },
                                    ),

                                    ListTile(
                                      leading: ImageIcon(AssetImage("assets/icons/setting.png"), color: theme.accentColor),
                                      title: Text(S.of(context).profile_settings, style: textTheme.subtitle2,),
                                      trailing: Icon(Icons.arrow_forward_ios, size: 15, color: theme.focusColor,),
                                      onTap: (){
                                          Navigator.of(context).pushNamed("/ProfileSettings").then((value) {
                                            _con.getCurrentUserNow();
                                          });
                                        },
                                    ),
                                    // ListTile(
                                    //   leading: ImageIcon(AssetImage("assets/icons/lock.png"), color: theme.accentColor),
                                    //   title: Text("Security and Privacy", style: textTheme.subtitle2,),
                                    //   trailing: Icon(Icons.arrow_forward_ios, size: 15, color: theme.focusColor,),
                                    //   onTap: (){},
                                    // ),
                                    ListTile(
                                      leading: ImageIcon(AssetImage("assets/icons/app_k.png"), color: theme.accentColor),
                                      title: Text(S.of(context).app_settings, style: textTheme.subtitle2,),
                                      trailing: Icon(Icons.arrow_forward_ios, size: 15, color: theme.focusColor,),
                                      onTap: (){
                                          Navigator.of(context).pushNamed("/AppSettings", arguments: RouteArgument());
                                        },
                                    ),
                                    DividerLineWidget(),
                                    ListTile(
                                      title: Text(S.of(context).logout, style: textTheme.subtitle2,),
                                      onTap: (){
                                        userRepo.logout().then((value) {
                                          Navigator.of(context).pushNamedAndRemoveUntil('/Login', (Route<dynamic> route) => false);
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    );
                  }
              )
          ),
        )
    );
  }
}
