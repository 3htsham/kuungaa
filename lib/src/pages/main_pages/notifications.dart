import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:social_app/generated/i18n.dart';
import 'package:social_app/src/controllers/notifications_controller.dart';
import 'package:social_app/src/elements/follow_notification_widget_item.dart';
import 'package:social_app/src/elements/group_add_notification_widget_item.dart';
import 'package:social_app/src/elements/liked_post_notification_widget_item.dart';
import 'package:social_app/src/elements/others/CircularLoadingWidget.dart';
import 'package:social_app/src/elements/user/friend_request_notification_widget_item.dart';
import 'package:social_app/src/models/my_notification.dart';

class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends StateMVC<NotificationsPage>
    with AutomaticKeepAliveClientMixin {

  NotificationsController _con;

  _NotificationsPageState() : super(NotificationsController()) {
    _con = controller;
  }

  @override
  void initState() {
    super.initState();
    _con.getMyFriendRequests();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return RefreshIndicator(
        onRefresh: () async {},
        child: Scaffold(
          key: _con.scaffoldKey,
          appBar: _appbar(),
          body: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    _con.notifications.isEmpty
                        ? CircularLoadingWidget(
                            height: 100,
                          )
                        : ListView.builder(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: _con.notifications.length,
                            itemBuilder: (ctx, index) {
                              MyNotification noti = _con.notifications[index];
                              if (noti.type == 0)
                              {
                                ///Like
                                return LikedPostNotificationWidgetItem(noti: noti, txt: "liked your post", onTap: (){},);
                              }
                              else if(noti.type == 1)
                                {
                                  ///Use same for comment
                                  return LikedPostNotificationWidgetItem(noti: noti, txt: "commented on your post", onTap: (){},);
                                }
                              else if (noti.type == 2)
                                {
                                  ///Friend Request
                                  String txt = noti.friendRequestNotification.type == 0
                                      ? "sent you a friend request"
                                      : "accepted your friend request";

                                    return FriendRequestNotificationWidgetItem(noti: noti, txt: txt,
                                      onTap: (){
                                        // Navigator.of(context).pushNamed("/UserProfileDetails", arguments: RouteArgument(user: _con.posts[index].user));
                                      },
                                      onAccept: (){
                                        _con.acceptFriendRequestNow(noti);
                                      },
                                      onIgnore: (){
                                        _con.rejectFriendRequestNow(noti);
                                      },
                                    );

                                }
                              else if(noti.type == 3)
                                {
                                  String txt = " follows you";
                                  ///Follow
                                  return FollowNotificationWidgetItem(noti: noti, txt: txt, onTap: (){},);
                                }
                              else
                                {
                                  ///Added to the Group
                                  return GroupAddNotificationWidgetItem(noti: noti, onTap: (){},);
                                }
                            }),
                  ],
                ),
              ),


              ///Loader
              _con.isLoading
                  ? Container(
                color: Colors.black.withOpacity(0.5),
                child: Center(
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(15)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircularProgressIndicator(),
                        SizedBox(height: 10),
                        Text("Please wait...",
                            style: Theme.of(context).textTheme.bodyText2)
                      ],
                    ),
                  ),
                ),
              )
                  : Positioned(bottom: 10, child: SizedBox(height: 0)),
            ],
          ),
        )
    );
  }

  Widget _appbar(){
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      title: Text(S.of(context).notifications,
        style: Theme.of(context).textTheme.headline4
            .merge(TextStyle(color: Theme.of(context).accentColor)),
      ),
      automaticallyImplyLeading: false,
      actions: [
        // IconButton(
        //   onPressed:(){
        //     //Open the conversations page
        //   },
        //   icon: ImageIcon(
        //     AssetImage("assets/icons/message.png"),
        //     size: 30,
        //     color: Theme.of(context).accentColor,
        //   ),
        // )
      ],
    );
  }

}
