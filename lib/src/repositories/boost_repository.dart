import 'dart:convert';
import 'dart:io';

import 'package:global_configuration/global_configuration.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app/src/helpers/helper.dart';
import 'package:social_app/src/models/boost_package.dart';
import 'package:social_app/src/models/mpesa.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/user_repository.dart';
import 'package:http/http.dart' as http;

MPESAModel mpesa;

Future<MPESAModel> getMPESACredentials() async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}mpesa?$_apiToken';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    if (json.decode(body)['Mpesa'] != null) {
      mpesa = MPESAModel.fromJson(json.decode(body)['Mpesa']);
    }
  }
  return mpesa;
}

Future<Stream<BoostPackage>> getPackages() async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}pakages?$_apiToken';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getPackagesData(data))
      .expand((element) => element)
      .map((data) {
        var d = data;
        return BoostPackage.fromJson(data);
      });
}

Future<Stream<Post>> getBoostedFeed() async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}post/boosted?$_apiToken';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getPosts(data))
      .expand((element) => element)
      .map((data) {
    var d = data;
    return Post.fromJson(data);});
}

Future<Stream<Post>> getMyBoostedFeed() async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}post/boosted/my?$_apiToken';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getPosts(data))
      .expand((element) => element)
      .map((data) {
    var d = data;
    return Post.fromJson(data);});
}

Future<bool> postBoost (Post _post, String _packageId, String _merchantReqId, String _checkoutReqId) async {
  User _user = await getCurrentUser();
  var boosted = false;;
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String _postId = '&post_id=${_post.id.toString()}';
  final String _pkgId = '&pakage_id=${_packageId.toString()}';
  final String _mrchntId = '&MerchantRequestID=${_merchantReqId.toString()}';
  final String _chkoutId = '&CheckoutRequestID=${_checkoutReqId.toString()}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}post/boost?$_apiToken$_postId$_pkgId$_mrchntId$_chkoutId';

  var client = http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = json.decode(response.body);
    if(body['error'] == false || body['error'] == "false") {
      boosted = true;
    }
  }
  return boosted;
}

Future<bool> postBoostView(Post _post) async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String _postId = '&post_id=${_post.id.toString()}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}post/boosted/my?$_apiToken$_postId';

  var client = http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    storeCountView(_post.id.toString());
  }
  return true;
}

storeCountView(String id) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString(id.toString(), "true");
}