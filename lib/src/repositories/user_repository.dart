import 'dart:convert';
import 'dart:io';
import 'package:global_configuration/global_configuration.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:social_app/src/helpers/helper.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/models/friend_request.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart' as parser;

User currentUser = new User();

Future<User> login(User user) async {
  final String url = '${GlobalConfiguration().getValue('api_base_url')}login';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(user.toMap()),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    setCurrentUser(body);
    if (json.decode(body)['user'] != null) {
      currentUser = User.fromJSON(json.decode(body)['user']);
    }
  }
  return currentUser;
}

Future<User> register(User user) async {
  final String url = '${GlobalConfiguration().getValue('api_base_url')}register';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(user.toMap()),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    setCurrentUser(body);
    if (json.decode(body)['user'] != null) {
      currentUser = User.fromJSON(json.decode(body)['user']);
    }
  }
  return currentUser;
}



void setCurrentUser(jsonString) async {
  if (json.decode(jsonString)['user'] != null) {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('current_user', json.encode(json.decode(jsonString)['user']));
  }
}

Future<User> getCurrentUser() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey('current_user')) {
    currentUser = User.fromJSON(json.decode(await prefs.get('current_user')));
  }
  return currentUser;
}

Future<void> logout() async {
  currentUser = new User();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.remove('current_user');
}


Future<Stream<User>> getSuggestedUsersList() async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}users?$_apiToken';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
        Helper.getSuggestedUsersData(data))
      .expand((element) => element)
      .map((data) {
        var d = data;
        return User.fromJSON(data);
      });
}



Future<Stream<FriendRequest>> getFriendRequestsList() async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}friendrequest/index?$_apiToken';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
        Helper.getFriendRequestData(data))
      .expand((element) => element)
      .map((data) {
        var d = data;
        return FriendRequest.fromJson(data);
      });

}

/*
  User Object used to send parameter
  'receiver_id' (id of the one to whom request is send)
  in Post request body
 */
Future<bool> sendFriendRequest(User user) async {
  bool sentError = true; //If true (error sending request, else sent)
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}friendrequest/store?$_apiToken';

  var client = http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode({"receiver_id": user.id}),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if (json.decode(body)['error'] != null) {
      sentError = json.decode(body)['error'];
    }
  }
  return sentError;
}

/*
  FriendRequest Object used to send parameter
  'opponent_id' (id of the one of which request to
    accept i.e. senderId from FriendRequest class)
  in Post request body
 */
Future<bool> acceptFriendRequest(FriendRequest request) async {
  bool acceptError = true; //If true (error accepting request, else sent)
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}friendrequest/accept?$_apiToken';

  var body = {
    'friendrequest_id': request.id,
    'opponent_id': request.senderId
  };

  var client = http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(body),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if (json.decode(body)['error'] != null) {
      acceptError = json.decode(body)['error'];
    }
  }
  return acceptError;
}

/*
  FriendRequest Object used to send parameter
  'friendrequest_id' (id of the FriendRequest to
    reject i.e. id from FriendRequest class)
  in Post request body
 */
Future<bool> rejectFriendRequest(FriendRequest request) async {
  bool acceptError = true; //If true (error rejecting request, else sent)
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}friendrequest/reject?$_apiToken';

  var body = {
    'friendrequest_id': request.id
  };

  var client = http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(body),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if (json.decode(body)['error'] != null) {
      acceptError = json.decode(body)['error'];
    }
  }
  return acceptError;
}


/*
  User Object used to send parameter
  'friendrequest_id' (id of the FriendRequest to
    reject i.e. id from FriendRequest class)
  in Post request body
*/
Future<User> getUserDetails(User user) async {
  User userDetails;
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String user_id = '&user_id=${user.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}user/show?$_apiToken$user_id';

  var client = http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if (json.decode(body)['user'] != null) {
      userDetails = User.fromJSON(json.decode(body)['user']);
    }
  }
  return userDetails;
}

Future<Stream<User>> updateUser(User _user) async {
  User _currentUser = await getCurrentUser();
  final String _apiToken = 'api_token=${_currentUser.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}profile/update?$_apiToken';


  http.MultipartRequest request = http.MultipartRequest("POST", Uri.parse(url));
  var mapBody = _user.toMap();
  request.fields.addAll(mapBody);
  
  if(_user.image != null) {
    var file = await http.MultipartFile.fromPath(
        "image", _user.image, filename: _user.name,
        contentType: parser.MediaType.parse("application/octet-stream"));
    request.files.add(file);
  }


  Map<String, String> headers = {
    "Accept": "application/json",
  };

  request.headers.addAll(headers);
  var response = await request.send();

  return response.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) {
    var d = data;
    setCurrentUser(json.encode(data));
    return User.fromJSON(Helper.getUserData(data));
  });
}


Future<Stream<User>> updateUserFirebaseToken(User _user) async {
  User _currentUser = await getCurrentUser();
  final String _apiToken = 'api_token=${_currentUser.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}profile/update?$_apiToken';


  http.MultipartRequest request = http.MultipartRequest("POST", Uri.parse(url));
  var mapBody = _user.toMap();
  request.fields['firebase_token'] = _user.firebaseToken;

  Map<String, String> headers = {
    "Accept": "application/json",
  };

  request.headers.addAll(headers);
  var response = await request.send();

  return response.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) {
    var d = data;
    setCurrentUser(json.encode(data));
    return User.fromJSON(Helper.getUserData(data));
  });
}


Future<Stream<User>> getMyFriendsList() async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}friend/index?$_apiToken';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getFriendsUsersData(data))
      .expand((element) => element)
      .map((data) {
        var d = data;
        return User.fromJSON(data['opponent']);
      });
}

Future<Stream<User>> getFriendsListByUserId(User _user) async {
  User _currentUser = await getCurrentUser();
  final String _apiToken = 'api_token=${_currentUser.apiToken}';
  final String _userId = '&user_id=${_user.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}user/friends?$_apiToken$_userId';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getFriendsUsersData(data))
      .expand((element) => element)
      .map((data) {
    var d = data;
    return User.fromJSON(data['opponent']);
  });
}

Future<User> unFriendUser(User user) async {
  User userDetails;
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String _userId = '&user_id=${user.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}user/friend/remove?$_apiToken$_userId';

  var client = http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(user.toMap()),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if (json.decode(body)['user'] != null) {
      userDetails = User.fromJSON(json.decode(body)['user']);
    }
  }
  return userDetails;
}


Future<User> blockUser(User _userToBlock) async {
  User _blockedUser;
  User _user = await getCurrentUser();
  final String _apiToken = '?api_token=${_user.apiToken}';
  final String _userId = '&opponent_id=${_userToBlock.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}block$_apiToken$_userId';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if ( json.decode(body)['blocked'] != null ) {
      _blockedUser = User.fromJSON(json.decode(body)['blocked']);
    }
  }
  return _blockedUser;
}

Future<bool> unblockUser(User _user) async {
  bool _unblockedUser = false;
  User _user = await getCurrentUser();
  final String _apiToken = '?api_token=${_user.apiToken}';
  final String _userId = '&opponent_id=${_user.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}unblock$_apiToken$_userId';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    if (json.decode(body)['error'] == false || json.decode(body)['error'] == "false") {
      _unblockedUser = true;
    }
  }
  return _unblockedUser;
}

Future<Stream<User>> getBlockedPeople() async {
  User _currentUser = await getCurrentUser();
  final String _apiToken = '?api_token=${_currentUser.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}blocked$_apiToken';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getBlockedUsersData(data))
      .expand((element) => element)
      .map((data) {
    var d = data;
    return User.fromJSON(data);
  });
}



