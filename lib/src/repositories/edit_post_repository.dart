import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:global_configuration/global_configuration.dart';
import 'package:social_app/src/helpers/helper.dart';
import 'package:social_app/src/models/media.dart';
import 'package:social_app/src/models/my_image.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/models/video.dart';
import 'package:social_app/src/repositories/user_repository.dart';
import 'package:http_parser/http_parser.dart' as parser;

Future<bool> updatePost(Post _post,
                                List<String> deletedPhotos,
                                List<String> deletedVideos,
                                List<Media> newMedia) async {

  bool isUpdatedSuccessfully = false;

  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';

  if(newMedia.isNotEmpty) {
    for(int i =0; i<newMedia.length; i++) {
      Media media = newMedia[i];
      ///MediaTypes >>> 0 = image, 1 = video
      if(media.type == 0) {
        Stream<MyImage> stream = await addNewImage(media, _post, _apiToken);
        stream.listen((_img) {
          isUpdatedSuccessfully = true;
        },
            onError: (e){
              isUpdatedSuccessfully = false;},
            onDone: (){});
      } else {
        Stream<Video> stream = await addNewVideo(media, _post, _apiToken);
        stream.listen((_video) {
          isUpdatedSuccessfully = true;
        },
            onError: (e){
              isUpdatedSuccessfully = false;},
            onDone: (){});
      }
    }
  }
  if(deletedPhotos.isNotEmpty) {
    for(int i = 0; i<deletedPhotos.length; i++) {
      var _imageId = deletedPhotos[i];
      bool isDeleted = await deleteImage(_imageId, _apiToken);
      isUpdatedSuccessfully = isDeleted;
    }
  }

  if(deletedVideos.isNotEmpty) {
    for(int i = 0; i<deletedVideos.length; i++) {
      var _videoId = deletedVideos[i];
      bool isDeleted = await deleteVideo(_videoId, _apiToken);
      isUpdatedSuccessfully = isDeleted;
    }
  }

  if(_post.text != null) {
    bool isUpdated = await updateText(_post, _apiToken);
    isUpdatedSuccessfully = isUpdated;
  }

  return isUpdatedSuccessfully;

}

Future<Stream<Video>> addNewVideo(Media media, Post _post, String _apiToken) async {
  final String url = '${GlobalConfiguration().getValue('api_base_url')}add/video?$_apiToken';
  // create multipart request
  http.MultipartRequest request = http.MultipartRequest("POST", Uri.parse(url));
  var file = await http.MultipartFile.fromPath("file", media.identifier, filename: media.name, contentType: parser.MediaType.parse("application/octet-stream"));
  request.files.add(file);
  Map<String, String> mapBody = Map();
  mapBody['post_id'] = _post.id.toString();
  request.fields.addAll(mapBody);

  Map<String, String> headers = {
    "Accept": "application/json",
  };

  request.headers.addAll(headers);
  var response = await request.send();

  return response.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) {
    var d = data;
    return Video.fromJson(Helper.getVideoData(data));});
}

Future<bool> deleteVideo(String _videoId, String _apiToken) async {
  var video = '&video_id=$_videoId';
  bool isDeleted = false;
  final String url = '${GlobalConfiguration().getValue('api_base_url')}delete/video?$_apiToken$video';

  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    if(json.decode(body)['error'] == false || json.decode(body)['error'] == "false"){
      isDeleted = true;
    }
  }
  return isDeleted;
}

Future<Stream<MyImage>> addNewImage(Media media, Post _post, String _apiToken) async {
  final String url = '${GlobalConfiguration().getValue('api_base_url')}add/image?$_apiToken';
  // create multipart request
  http.MultipartRequest request = http.MultipartRequest("POST", Uri.parse(url));
  var file = await http.MultipartFile.fromPath("file", media.identifier, filename: media.name, contentType: parser.MediaType.parse("application/octet-stream"));
  request.files.add(file);
  Map<String, String> mapBody = Map();
  mapBody['post_id'] = _post.id.toString();
  request.fields.addAll(mapBody);

  Map<String, String> headers = {
    "Accept": "application/json",
  };

  request.headers.addAll(headers);
  var response = await request.send();

  return response.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) {
        var d = data;
        return MyImage.fromJson(Helper.getImageData(data));});
}

Future<bool> deleteImage(String _imageId, String _apiToken) async {
  var image = '&image_id=$_imageId';
  bool isDeleted = false;
  final String url = '${GlobalConfiguration().getValue('api_base_url')}delete/image?$_apiToken$image';

  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    if(json.decode(body)['error'] == false || json.decode(body)['error'] == "false"){
      isDeleted = true;
    }
  }
  return isDeleted;
}


Future<bool> updateText(Post post, String _apiToken) async {
  var _text = '&text=${post.text}';
  var _postId = '&post_id=${post.id.toString()}';
  bool isDeleted = false;
  final String url = '${GlobalConfiguration().getValue('api_base_url')}update/text?$_apiToken$_text$_postId';

  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    if(json.decode(body)['error'] == false || json.decode(body)['error'] == "false"){
      isDeleted = true;
    }
  }
  return isDeleted;
}

