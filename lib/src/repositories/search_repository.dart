import 'dart:convert';
import 'dart:io';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:social_app/src/models/search.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/user_repository.dart';

Future<Search> search(var text) async {
  Search _search;
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String keyword = '&keyword=$text';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}search?$_apiToken$keyword';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    if (json.decode(body)['error'] == false || json.decode(body)['error'] == "false") {
      _search = Search.fromJson(json.decode(body));
    }
  }
  return _search;
}

