import 'dart:convert';
import 'dart:io';
import 'package:global_configuration/global_configuration.dart';
import 'package:social_app/src/helpers/helper.dart';
import 'package:social_app/src/models/comment.dart';
import 'package:social_app/src/models/comment_reply.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/saved_post.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/user_repository.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart' as parser;


Future<Stream<Post>> getNewsFeed() async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}feed?$_apiToken';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getPostsData(data))
      .expand((element) => element)
      .map((data) {
        var d = data;
      return Post.fromJson(data);});
}

Future<Post> likePost(Comment data) async {
  User _user = await getCurrentUser();
  Post post;
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}like/store?$_apiToken';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(data.toJson()),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    post = Post.fromJson(json.decode(body)['post']);
  }
  return post;
}

Future<Comment> postComment(Comment data) async {
  User _user = await getCurrentUser();
  Comment comment;
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}comment/store?$_apiToken';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(data.toJson()),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if (json.decode(body)['comment'] != null) {
      comment = Comment.fromJson(json.decode(body)['comment']);
    }
  }
  return comment;
}

Future<Comment> editComment(Comment data) async {
  User _user = await getCurrentUser();
  Comment comment;
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String _commentId= '&comment_id=${data.id.toString()}';
  final String _text = '&text=${data.text.toString()}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}comment/update?$_apiToken$_commentId$_text';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if (json.decode(body)['comment'] != null) {
      comment = Comment.fromJson(json.decode(body)['comment']);
    }
  }
  return comment;
}

Future<Stream<Post>> addNewPost(Post post) async {
  User _user = await getCurrentUser();
  post.user = _user;
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}post/store?$_apiToken';

  // create multipart request
  http.MultipartRequest request = http.MultipartRequest("POST", Uri.parse(url));

  if(post.mediaList != null && post.mediaList.length > 0) {
    for(int i=0; i<post.mediaList.length; i++) {
      var element = post.mediaList[i];
      if(element.type == 0) { //Image
        var file = await http.MultipartFile.fromPath("images[]", element.identifier, filename: element.name, contentType: parser.MediaType.parse("application/octet-stream"));
          request.files.add(file);
      } else if (element.type == 1) {
        var file = await http.MultipartFile.fromPath("videos[]", element.identifier, filename: element.name, contentType: parser.MediaType.parse("application/octet-stream"));
        request.files.add(file);
      }
    }
  }

  request.fields["text"]=  post.text;
  request.fields['post_type'] = post.postType;

  Map<String, String> headers = {
    "Accept": "application/json",
  };

  request.headers.addAll(headers);
  var response = await request.send();

    return response.stream
        .transform(utf8.decoder)
        .transform(json.decoder)
        .map((data) {
      var d = data;
      return Post.fromJson(Helper.getSinglePostData(data));});

}


Future<SavedPost> savePost(Post _post) async {
  User _user = await getCurrentUser();
  SavedPost savedPost;
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}post/save?$_apiToken';
  final client = new http.Client();
  Map _postMap = Map<String, String>();
  _postMap["post_id"] = _post.id.toString();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(_postMap),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if (json.decode(body)['savedPost'] != null) {
      savedPost = SavedPost.fromJson(json.decode(body)['savedPost']);
    }
  }
  return savedPost;
}

Future<Stream<SavedPost>> getSavedPosts() async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}saved/posts?$_apiToken';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getSavedPostsData(data))
      .expand((element) => element)
      .map((data) {
        var d = data;
        return SavedPost.fromJson(data);}
      );

}


Future<Stream<Post>> getUserPosts(User user) async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String _userId = '&user_id=${user.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}user/posts?$_apiToken$_userId';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getUserPostsData(data))
      .expand((element) => element)
      .map((data) {
    var d = data;
    return Post.fromJson(data);});

}

Future<Stream<Comment>> getPostComments(Post post) async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final post_id = '&post_id=${post.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}post/comments?$_apiToken$post_id';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getCommentsData(data))
      .expand((element) => element)
      .map((data) {
    var d = data;
    return Comment.fromJson(data);});
}

Future<Post> sharePost(Post post) async {
  User _user = await getCurrentUser();
  Post _post;
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String _postId = '&post_id=${post.id}';
  final String _postType = '&post_type=${post.postType}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}post/share?$_apiToken$_postId$_postType';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    _post = Post.fromJson(json.decode(body)['sharedPost']);
  }
  return _post;
}

Future<bool> deletePost(Post _post) async {
  bool isDeleted = false;
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  var _postId = '&post_id=${_post.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}delete/post?$_apiToken$_postId';

  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    if(json.decode(body)['error'] == false || json.decode(body)['error'] == "false"){
      isDeleted = true;
    }
  }
  return isDeleted;
}

Future<bool> deleteComment(Comment _comment) async {
  bool isDeleted = false;
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  var _commentId = '&comment_id=${_comment.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}comment/delete?$_apiToken$_commentId';

  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    if(json.decode(body)['error'] == false || json.decode(body)['error'] == "false"){
      isDeleted = true;
    }
  }
  return isDeleted;
}

Future<CommentReply> postCommentReply(CommentReply data) async {
  User _user = await getCurrentUser();
  CommentReply commentReply;
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String _commentId = '&comment_id=${data.commentId}';
  final String _text = '&text=${data.text}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}comment/reply?$_apiToken$_commentId$_text';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if (json.decode(body)['reply'] != null) {
      commentReply = CommentReply.fromJson(json.decode(body)['reply']);
    }
  }
  return commentReply;
}

Future<Comment> commentLikeUnlike(Comment data) async {
  User _user = await getCurrentUser();
  Comment comment;
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String _id = '&comment_id=${data.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}comment/like?$_apiToken$_id';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(data.toJson()),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if (json.decode(body)['comment'] != null) {
      comment = Comment.fromJson(json.decode(body)['comment']);
    }
  }
  return comment;
}

Future<CommentReply> commentReplyLikeUnlike(CommentReply data) async {
  User _user = await getCurrentUser();
  CommentReply comment;
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String _id = '&reply_id=${data.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}reply/like?$_apiToken$_id';
  final client = new http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(data.toJson()),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if (json.decode(body)['reply'] != null) {
      comment = CommentReply.fromJson(json.decode(body)['reply']);
    }
  }
  return comment;
}


