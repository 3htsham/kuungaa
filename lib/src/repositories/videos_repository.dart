import 'dart:convert';
import 'package:global_configuration/global_configuration.dart';
import 'package:social_app/src/helpers/helper.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/models/video.dart';
import 'package:social_app/src/repositories/user_repository.dart';
import 'package:http/http.dart' as http;


Future<Stream<Video>> getVideos() async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}videos?$_apiToken';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
        Helper.getVideosData(data))
      .expand((element) => element)
      .map((data) {
        var d = data;
        return Video.fromJson(data);
      });
}