import 'dart:convert';
import 'dart:io';

import 'package:global_configuration/global_configuration.dart';
import 'package:social_app/src/helpers/helper.dart';
import 'package:social_app/src/models/group.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/user_repository.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart' as parser;


Future<Stream<Group>> createGroup(Group _group) async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}group/create?$_apiToken';

  http.MultipartRequest request = http.MultipartRequest("POST", Uri.parse(url));
  var mapBody = _group.toJson();
  request.fields.addAll(mapBody);

  if(_group.image != null) {
    var file = await http.MultipartFile.fromPath(
        "image", _group.image, filename: _group.name,
        contentType: parser.MediaType.parse("application/octet-stream"));
    request.files.add(file);
  }
  if(_group.cover != null) {
    var file = await http.MultipartFile.fromPath(
        "cover", _group.cover, filename: _group.name+"_cover",
        contentType: parser.MediaType.parse("application/octet-stream"));
    request.files.add(file);
  }

  Map<String, String> headers = {
    "Accept": "application/json",
  };

  request.headers.addAll(headers);
  var response = await request.send();

  return response.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) {
    var d = data;
    return Group.fromJson(Helper.getGroupData(data));
  });
}


Future<Stream<Group>> getListOfGroups() async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}group/all?$_apiToken';


  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getGroupsData(data))
      .expand((element) => element)
      .map((data) {
    var d = data;
    return Group.fromJson(data);
  });
}

Future<Stream<Group>> getUserGroups() async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}group/index?$_apiToken';


  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getGroupsData(data))
      .expand((element) => element)
      .map((data) {
        var d = data;
        return Group.fromJson(data);
      });

}

Future<Stream<Group>> getGroupsUserJoined() async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}group/joined?$_apiToken';


  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getJoinedGroupsData(data))
      .expand((element) => element)
      .map((data) {
    var d = data;
    return Group.fromJson(data);
  });

}


Future<Stream<Post>> addNewGroupPost(Post post, Group group) async {
  User _user = await getCurrentUser();
  post.user = _user;
  final String _apiToken = 'api_token=${_user.apiToken}';
  var group_id = '&group_id=${group.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}group/post/create?$_apiToken$group_id';

  // create multipart request
  http.MultipartRequest request = http.MultipartRequest("POST", Uri.parse(url));

  if(post.mediaList != null && post.mediaList.length > 0) {
    for(int i=0; i<post.mediaList.length; i++) {
      var element = post.mediaList[i];
      if(element.type == 0) { //Image
        var file = await http.MultipartFile.fromPath("images[]", element.identifier, filename: element.name, contentType: parser.MediaType.parse("application/octet-stream"));
        request.files.add(file);
      } else if (element.type == 1) {
        var file = await http.MultipartFile.fromPath("videos[]", element.identifier, filename: element.name, contentType: parser.MediaType.parse("application/octet-stream"));
        request.files.add(file);
      }
    }
  }

  request.fields["text"]=  post.text;

  Map<String, String> headers = {
    "Accept": "application/json",
  };

  request.headers.addAll(headers);
  var response = await request.send();

  return response.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) {
        var d = data;
        return Post.fromJson(Helper.getSinglePostData(data));}
      );

}

Future<Stream<Post>> getPostsOfGroup(Group group) async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final group_id = '&group_id=${group.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}group/posts?$_apiToken$group_id';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
          Helper.getGroupPostsData(data))
      .expand((element) => element)
      .map((data) {
        var d = data;
        return Post.fromJson(data);});
}


Future<Stream<User>> getGroupMembersList(Group group) async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String group_id = '&group_id=${group.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}group/members?$_apiToken$group_id';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getGroupMembersData(data))
      .expand((element) => element)
      .map((data) {
        var d = data;
        return User.fromJSON(data);
      });
}


Future<Group> joinGroup(Group group) async {
  User _user = await getCurrentUser();
  Group _group;
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String group_id = '&group_id=${group.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}group/join?$_apiToken$group_id';
  var client = http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    _group = Group.fromJson(json.decode(body)['group']);
  }
  return _group;

}

Future<Group> leaveGroup(Group group) async {
  User _user = await getCurrentUser();
  Group _group;
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String group_id = '&group_id=${group.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}group/leave?$_apiToken$group_id';
  var client = http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    _group = Group.fromJson(json.decode(body)['group']);
  }
  return _group;

}

Future<bool> removeMember(User user, Group group) async {
  User _user = await getCurrentUser();
  bool isError = true;
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String group_id = '&group_id=${group.id}';
  final String user_id = '&user_id=${user.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}remove/member?$_apiToken$group_id$user_id';
  var client = http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
  );
  if (response.statusCode == 200) {
    var body = response.body;
    isError = json.decode(body)['error'];
  }
  return !isError;
}
