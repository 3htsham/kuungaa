import 'dart:convert';
import 'dart:io';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'package:social_app/src/helpers/helper.dart';
import 'package:social_app/src/models/chat.dart';
import 'package:social_app/src/models/message.dart';
import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/repositories/user_repository.dart';


Future<Stream<Chat>> getChatsList() async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}user/chat/index?$_apiToken';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
        Helper.getChatListData(data))
      .expand((element) => element)
      .map((data) {
        var d = data;
        return Chat.fromJson(data);
      });

}


Future<bool> deleteAChat(Chat chat) async {
  bool sentError = true; //If true (error deleting request, else deleted)
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}user/chat/delete?$_apiToken';

  var client = http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(chat.toJson()),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    if (json.decode(body)['error'] != null) {
      sentError = json.decode(body)['error'];
    }
  }
  return sentError;
}

Future<Stream<Message>> getMessagesList(Chat chat) async {
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}user/message/index?$_apiToken&id=${chat.id}';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
        Helper.getMessagesListData(data))
      .expand((element) => element)
      .map((data) {
        var d = data;
        return Message.fromJson(data);
      });

}

Future<Message> sendMessage(Message msg) async {
  Message message; //If true (error deleting request, else deleted)
  User _user = await getCurrentUser();
  final String _apiToken = 'api_token=${_user.apiToken}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}user/message/send?$_apiToken';

  var client = http.Client();
  final response = await client.post(
    url,
    headers: {HttpHeaders.contentTypeHeader: 'application/json'},
    body: json.encode(msg.toJson()),
  );
  if (response.statusCode == 200) {
    var body = response.body;
    print(body);
    var data = Helper.getMessageData(json.decode(body));
    message = Message.fromJson(data);
  }
  return message;
}

Future<Stream<Message>> getMessagesListByUserId(User _user) async {
  User _currentUser = await getCurrentUser();
  final String _apiToken = 'api_token=${_currentUser.apiToken}';
  final String _user_id = '&user_id=${_user.id}';
  final String url = '${GlobalConfiguration().getValue('api_base_url')}user/messages?$_apiToken$_user_id';

  var client = http.Client();
  final streamedRest = await client.send(http.Request('post', Uri.parse(url)));

  return streamedRest.stream
      .transform(utf8.decoder)
      .transform(json.decoder)
      .map((data) =>
      Helper.getMessagesListData(data))
      .expand((element) => element)
      .map((data) {
    var d = data;
    return Message.fromJson(data);
  });

}

