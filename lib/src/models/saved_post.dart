import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/user.dart';

class SavedPost {
  var id;
  var userId;
  var postId;
  String createdAt;
  String updatedAt;
  Post post;
  User user;

  SavedPost(
      {this.id,
        this.userId,
        this.postId,
        this.createdAt,
        this.updatedAt,
        this.post});

  SavedPost.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    postId = json['post_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    post = json['post_detail'] != null
        ? new Post.fromJson(json['post_detail'])
        : null;
    user = json['user'] != null ? new User.fromJSON(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['post_id'] = this.postId;
    if (this.post != null) {
      data['post_detail'] = this.post.toJson();
    }
    if (this.user != null) {
      data['user'] = this.user.toMap();
    }
    return data;
  }
}