class Message {
  var chatId;
  var type; //1= sent message, 2= received message
  var isRead;
  var txt;
  var updatedAt;
  var createdAt;
  var id;

  var recieverId;

  DateTime time;

  Message(
      {this.chatId,
        this.time,
        this.type,
        this.recieverId,
        this.isRead,
        this.txt,
        this.updatedAt,
        this.createdAt,
        this.id});

  Message.fromJson(Map<String, dynamic> json) {
    chatId = json['chat_id'];
    type = json['type'];
    isRead = json['isRead'];
    txt = json['txt'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['chat_id'] = this.chatId;
    data['type'] = this.type;
    data['isRead'] = this.isRead;
    data['txt'] = this.txt;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['id'] = this.id;
    data['receiver_id'] = this.recieverId;
    return data;
  }
}