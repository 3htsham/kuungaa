import 'package:social_app/src/models/user.dart';
import 'package:social_app/src/models/comment_reply.dart';

class Comment {
  var userId;
  var postId;
  var text;
  var updatedAt;
  var createdAt;
  var id;
  User user;

  var isLiked = false;
  var likesCount = 0;
  var repliesCount = 0;
  List<CommentReply> replies;

  Comment(
      {this.userId,
        this.postId,
        this.text,
        this.updatedAt,
        this.createdAt,
        this.user,
        this.id});

  Comment.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    postId = json['post_id'];
    text = json['text'];
    isLiked = json['isLiked'] ?? false;
    likesCount = json['likes_count'] ?? 0;
    repliesCount = json['replies_count'] ?? 0;
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    id = json['id'];
    user = json['user'] != null ? User.fromJSON(json['user']) : null;

    if (json['reply'] != null) {
      replies = new List<CommentReply>();
      json['reply'].forEach((v) {
        replies.add(new CommentReply.fromJson(v));
      });
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['post_id'] = this.postId;
    data['text'] = this.text;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['id'] = this.id;
    return data;
  }
}