class User {

  var id;
  var email;
  var name="";
  var password;
  var firebaseToken;
  var apiToken;
  var phone;
  var address;
  var bio;
  var emailVerifiedAt;
  var updatedAt;
  var createdAt;
  var status;
  var isFriend;

  var userStatus = "";
  var nickName = "";
  var totalFriends;
  var totalPosts;
  var gender;


  var friendRequestStatus = 0; //0=not sent, 1=sent


  var location;
  var image = "";

  User({this.name, this.id, this.image, this.location, this.friendRequestStatus});

  User.fromJSON(Map<String, dynamic> jsonMap) {
    id = jsonMap['id'].toString();
    name = jsonMap['name'];
    email = jsonMap['email'];
    apiToken = jsonMap['api_token'];
    updatedAt = jsonMap['updated_at'];
    createdAt = jsonMap['created_at'];
    emailVerifiedAt = jsonMap['email_verified_at'] ?? "";
    image = jsonMap['image'] ?? "";
    status = jsonMap['status'] ?? 0;
    firebaseToken = jsonMap['firebase_token'] ?? "";
    isFriend = jsonMap['is_friend'] ?? false;
    userStatus = jsonMap['status_info'] ?? "";
    nickName = jsonMap['nickname'] ?? "";
    bio = jsonMap['bio'] ?? "";
    totalPosts = jsonMap['posts_count'] ?? 0;
    totalFriends = jsonMap['friends_count'] ?? 0;
    gender = jsonMap['gender'] ?? "male";
    // deviceToken = jsonMap['device_token'] ?? "";
    // try {
    //   phone = jsonMap['custom_fields']['phone']['view'];
    // } catch (e) {
    //   phone = "";
    // }
    // try {
    //   address = jsonMap['custom_fields']['address']['view'];
    // } catch (e) {
    //   address = "";
    // }
    // try {
    //   bio = jsonMap['custom_fields']['bio']['view'];
    // } catch (e) {
    //   bio = "";
    // }
    // if(!(jsonMap['has_media'] == "false" || jsonMap['has_media'] == false)) {
    //   image = jsonMap['media'] != null ? Media.fromJSON(jsonMap['media'][0]) : null;
    // }
  }

  Map toMap(){
    var map = new Map<String, String>();
    (id!=null && id.length>0) ? map["id"] = id.toString() ?? "" : null;
    (email !=null && email.length>0) ? map["email"] = email : null;
    (name !=null && name.length>0) ? map["name"] = name : null;
    (gender !=null && gender.length>0) ? map["gender"] = gender : null;
    (password !=null && password.length>0) ? map["password"] = password : null;
    (firebaseToken !=null && firebaseToken.length>0) ? map["firebase_token"] = firebaseToken : null;
    (phone !=null && phone.length>0) ? map["phone"] = phone : null;
    (address !=null && address.length>0) ? map["address"] = address : null;
    (bio !=null && bio.length>0) ? map["bio"] = bio : null;
    (nickName !=null && nickName.length>0) ? map["nickname"] = nickName : null;
    (userStatus !=null && userStatus.length>0) ? map["status_info"] = userStatus : null;
    return map;
  }

}