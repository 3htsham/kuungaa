class MPESAModel {
  
  var CONSUMER_KEY;
  var CONSUMER_SECRET;
  var SHORT_CODE;
  var PASS_KEY;
  var SECURITY_CREDENTIAL;

  MPESAModel.fromJson(List<dynamic> jsonMap) {
    jsonMap.forEach((element) {
      var map = element as Map;
      print(map);
      if (map['key'] == "CONSUMER_KEY") {
        CONSUMER_KEY = map['value'];
      } else if (map['key'] == "CONSUMER_SECRET") {
        CONSUMER_SECRET = map['value'];
      } else if (map['key'] == "SHORT_CODE") {
        SHORT_CODE = map['value'];
      } else if (map['key'] == "PASS_KEY") {
        PASS_KEY = map['value'];
      } else if (map['key'] == "SECURITY_CREDENTIAL") {
        SECURITY_CREDENTIAL = map['value'];
      }
    });
  }
  
}