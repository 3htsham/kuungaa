import 'package:social_app/src/models/add_group_notification.dart';
import 'package:social_app/src/models/friend_request.dart';
import 'package:social_app/src/models/group.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/user.dart';

class MyNotification {

  int id;
  int status; //0 = unread, 1 = read
  int type; //0=like profile Post, 1=comment on profile Post, 2=friendRequest, 3=Follows You, 4=add to group
  User user;
  Post post;
  Group group;
  DateTime dateTime;

  FriendRequest friendRequestNotification;
  AddGroupNotification addGroupNotification;

  MyNotification({this.id, this.status, this.dateTime,
    this.type, this.user, this.post,
    this.group, this.friendRequestNotification,
    this.addGroupNotification});

}