import 'package:social_app/src/models/user.dart';

class CommentReply {
  var id;
  var userId;
  var commentId;
  var text;
  var createdAt;
  var updatedAt;
  User user;
  var isLiked = false;
  var likesCount = 0;

  CommentReply(
      {this.id,
        this.userId,
        this.commentId,
        this.text,
        this.createdAt,
        this.updatedAt,
        this.user});

  CommentReply.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    commentId = json['comment_id'];
    text = json['text'];
    isLiked = json['isLiked'] ?? false;
    likesCount = json['likes_count'] ?? 0;
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    user = json['user'] != null ? new User.fromJSON(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['comment_id'] = this.commentId;
    data['text'] = this.text;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.user != null) {
      data['user'] = this.user.toMap();
    }
    return data;
  }
}