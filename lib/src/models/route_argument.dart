import 'package:agora_rtc_engine/rtc_engine.dart';
import 'package:social_app/src/models/boost_package.dart';
import 'package:social_app/src/models/call.dart';
import 'package:social_app/src/models/chat.dart';
import 'package:social_app/src/models/group.dart';
import 'package:social_app/src/models/mpesa.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/status.dart';
import 'package:social_app/src/models/user.dart';
import 'package:video_player/video_player.dart';

class RouteArgument {

  int currentTab;
  String title;

  var durationInMilliSeconds;
  bool progress;
  var linkToVideo;
  VideoPlayerController videoController;
  Future<void> videoPlayerFuture;


  Chat chat;
  User user;
  User currentUser;

  Group group;

  Post post;
  var index;

  List<Status> statuses;
  Status status;

  Call call;

  BoostPackage package;
  MPESAModel mpesa;

  RouteArgument({this.currentTab, this.title,
    this.durationInMilliSeconds, this.progress, this.linkToVideo, this.videoController, this.videoPlayerFuture,

    this.chat,
    this.user,
    this.currentUser,
    this.group,
    this.post,
    this.index,
    this.statuses,
    this.status,

    this.call,

    this.package,
    this.mpesa,
  });

}