class BoostPackage {
  var id;
  var name;
  var price;
  var views;
  String createdAt;
  String updatedAt;

  BoostPackage(
      {this.id,
        this.name,
        this.price,
        this.views,
        this.createdAt,
        this.updatedAt});

  BoostPackage.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = json['price'];
    views = json['views'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['price'] = this.price;
    data['views'] = this.views;
    return data;
  }
}