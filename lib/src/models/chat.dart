import 'package:social_app/src/models/user.dart';

class Chat {

  String fireStoreChatId ;
  String lastMessage;
  bool isRead;

  var id;
  var ownerId;
  var opponentId;
  String createdAt;
  String updatedAt;

  DateTime time;

  var unread;
  User opponent;

  Chat(
      {this.id,
        this.lastMessage,
        this.isRead,
        this.fireStoreChatId,
        this.ownerId,
        this.time,
        this.opponentId,
        this.createdAt,
        this.updatedAt,
        this.unread,
        this.opponent});

  Chat.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    ownerId = json['owner_id'];
    opponentId = json['opponent_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    unread = json['unread'];
    opponent = json['opponent'] != null
        ? new User.fromJSON(json['opponent'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['owner_id'] = this.ownerId;
    data['opponent_id'] = this.opponentId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['unread'] = this.unread;
    if (this.opponent != null) {
      data['opponent'] = this.opponent.toMap();
    }
    return data;
  }
}