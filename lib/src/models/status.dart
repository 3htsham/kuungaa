import 'package:social_app/src/models/user.dart';

class Status {
  var id;
  var ownerId;
  User user;
  var text;
  bool hasMedia = false;
  bool isImage = true;
  var videoLink;
  var imageLink;
  var uploadedAt;
  var imageName;
  List<String> seenBy = <String>[];

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['id'] = id;
    map['hasMedia'] = hasMedia;
    map['imageLink'] = imageLink;
    map['isImage'] = isImage;
    map['ownerId'] = ownerId;
    map['seenby'] = seenBy;
    map['text'] = text;
    map["imageName"] = imageName;
    map['uploadedAt'] = uploadedAt;
    map['videoLink'] = videoLink;
    return map;
  }

}