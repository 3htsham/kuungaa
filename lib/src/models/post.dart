import 'package:social_app/src/models/comment.dart';
import 'package:social_app/src/models/media.dart';
import 'package:social_app/src/models/user.dart';

class Post {

  var id;
  String caption;
  User user;
  String image;
  var totalLikes = 0;
  var totalComment = 0;
  DateTime dateTime;
  Media media;
  List<Media> mediaList = <Media>[];

  List<Comment> comments = <Comment>[];

  var isLiked;

  var text;
  var userId;
  var createdAt;
  var updatedAt;


  var type;
  var groupId;
  var sharedId;
  var shared;
  User owner; //If it's a shared Post

  var postType = "public"; //private, public, friends


  Post({this.id, this.caption, this.user, this.isLiked,
    this.image, this.totalLikes, this.totalComment, this.mediaList,
    this.dateTime, this.media});

  Post.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    text = json['text'];
    userId = json['user_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    postType = json['post_type'] ?? "public";
    isLiked = json['liked'] ?? false;
    totalLikes = json['likes_count'] ?? json['likes'] ?? 0;
    totalComment = json['comments_count'] ?? json['comments'] ?? 0;
    if (json['images'] != null) {
      json['images'].forEach((v) {
        mediaList.add(new Media.fromJson(v, 0));
      });
    }
    if (json['videos'] != null) {
      json['videos'].forEach((v) {
        mediaList.add(new Media.fromJson(v, 1));
      });
    }
    user = json['user'] != null ? new User.fromJSON(json['user']) : null;


    type = json['type'] ?? 1;
    groupId = json['group_id'] ??"";
    sharedId = json['shared_id'] ?? "";
    shared = json['shared'] ?? false;
    owner = json['owner'] != null ? new User.fromJSON(json['owner']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['text'] = this.text;
    data['user_id'] = this.userId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    //data['post_type'] = this.postType;
    // if (this.mediaList != null && this.mediaList.length>0) {
    //   this.mediaList.forEach((element) {
    //     if()
    //   });
    //   data['images'] = this.images.map((v) => v.toJson()).toList();
    // }

    if (this.user != null) {
      data['user'] = this.user.toMap();
    }
    return data;
  }



}