class Group {
  var id;
  var name;
  DateTime joined;
  var image;
  String cover;
  var description;

  var adminId;
  String createdAt;
  String updatedAt;
  var join;

  Group({this.id, this.name, this.joined, this.image, this.cover, this.description});

  Group.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    adminId = json['admin_id'];
    name = json['name'];
    image = json['image'];
    cover = json['cover'];
    description = json['description'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    join = json['join'] ?? json['joined'];
  }

  Map<String, String> toJson() {
    final Map<String, String> data = new Map<String, String>();
    data['name'] = this.name;
    data['description'] = this.description;
    return data;
  }

}