class Call {
  var callerId;
  var callerName;
  var callerPic;
  var receiverId;
  var receiverName;
  var receiverPic;
  var channelId;
  bool hasDialed;
  bool isVideoCall;

  Call({this.callerId,
    this.callerName,
    this.callerPic,
    this.receiverId,
    this.receiverName,
    this.receiverPic,
    this.channelId,
    this.hasDialed,
    this.isVideoCall
  });

  Map<String, dynamic> toMap() {
    Map<String, dynamic> callMap = Map();
    callMap['callerId'] = callerId;
    callMap['callerName'] = callerName;
    callMap['callerPic'] = callerPic;
    callMap['receiverId'] = receiverId;
    callMap['receiverName'] = receiverName;
    callMap['receiverPic'] = receiverPic;
    callMap['channelId'] = channelId;
    callMap['hasDialed'] = hasDialed;
    callMap['isVideoCall'] = isVideoCall ?? false;
    return callMap;
  }

  Call.fromMap(Map<String, dynamic> map) {
    this.callerId = map['callerId'];
    this.callerName = map['callerName'];
    this.callerPic = map['callerPic'];
    this.receiverId = map['receiverId'];
    this.receiverName = map['receiverName'];
    this.receiverPic = map['receiverPic'];
    this.channelId = map['channelId'];
    this.hasDialed = map['hasDialed'];
    this.isVideoCall = map['isVideoCall'];
  }

}