class Media {

  var id;
  var caption;
  var type; //0 = image, 1 = video
  var mediaUrl;
  var thumbnailUrl;

  int postId;
  String file;
  String createdAt;
  String updatedAt;

  var name;
  var identifier;

  Media({this.id, this.caption, this.type, this.mediaUrl, this.thumbnailUrl,
    this.name, this.identifier
  });

  Media.fromJson(Map<String, dynamic> json, var type) {
    id = json['id'];
    postId = json['post_id'];
    file = json['file'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    thumbnailUrl = json['thumbnail'] ?? "";
    this.type = type;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['post_id'] = this.postId;
    data['file'] = this.file;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }

}