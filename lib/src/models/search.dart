import 'package:social_app/src/models/group.dart';
import 'package:social_app/src/models/post.dart';
import 'package:social_app/src/models/user.dart';

class Search {
  var error;
  List<User> users = <User>[];
  List<Post> posts = <Post>[];
  List<Group> groups = <Group>[];

  Search({this.error, this.users, this.posts, this.groups});

  Search.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    if (json['users'] != null) {
      users = new List<User>();
      json['users'].forEach((v) {
        users.add(new User.fromJSON(v));
      });
    }
    if (json['posts'] != null) {
      posts = new List<Post>();
      json['posts'].forEach((v) {
        posts.add(new Post.fromJson(v));
      });
    }
    if (json['groups'] != null) {
      groups = new List<Group>();
      json['groups'].forEach((v) {
        groups.add(new Group.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    if (this.users != null) {
      data['users'] = this.users.map((v) => v.toMap()).toList();
    }
    if (this.posts != null) {
      data['posts'] = this.posts.map((v) => v.toJson()).toList();
    }
    if (this.groups != null) {
      data['groups'] = this.groups.map((v) => v.toJson()).toList();
    }
    return data;
  }
}