class MyImage {
  var postId;
  var file;
  var updatedAt;
  var createdAt;
  var id;

  MyImage({this.postId, this.file, this.updatedAt, this.createdAt, this.id});

  MyImage.fromJson(Map<String, dynamic> json) {
    postId = json['post_id'] ?? "";
    file = json['file'];
    updatedAt = json['updated_at'];
    createdAt = json['created_at'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this.postId;
    data['file'] = this.file;
    data['updated_at'] = this.updatedAt;
    data['created_at'] = this.createdAt;
    data['id'] = this.id;
    return data;
  }
}