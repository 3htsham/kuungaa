import 'package:social_app/src/models/user.dart';

class FriendRequest {

  int type=0; //0=sent to you, 1=sent by you
  int friendRequestStatus=1;//0=accepted,  1 = no response yet, 2=ignored,

  FriendRequest({this.type, this.friendRequestStatus,
    this.id,
    this.senderId,
    this.receiverId,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.sender
  });

  var id;
  var senderId;
  var receiverId;
  var status;
  var createdAt;
  var updatedAt;
  User sender;

  FriendRequest.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    senderId = json['sender_id'];
    receiverId = json['receiver_id'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];

    //0=accepted, 1=pending, 2=rejected
    friendRequestStatus = status=="accepted" ? 0 : status=="pending" ? 1 : 2;
    sender = json['sender'] !=null ? User.fromJSON(json['sender']) : User();
  }



}