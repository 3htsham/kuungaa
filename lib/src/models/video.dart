import 'package:social_app/src/models/post.dart';

class Video {
  var id;
  var postId;
  String file;
  String thumbnail;
  String createdAt;
  String updatedAt;
  Post post;

  Video(
      {this.id,
        this.postId,
        this.file,
        this.thumbnail,
        this.createdAt,
        this.updatedAt,
        this.post});

  Video.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    postId = json['post_id'];
    file = json['file'];
    thumbnail = json['thumbnail'] ?? "";
    createdAt = json['created_at'] ?? "";
    updatedAt = json['updated_at'] ?? "";
    post = json['post'] != null ? new Post.fromJson(json['post']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['post_id'] = this.postId;
    data['file'] = this.file;
    data['thumbnail'] = this.thumbnail;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.post != null) {
      data['post'] = this.post.toJson();
    }
    return data;
  }
}