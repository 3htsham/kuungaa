import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:social_app/config/my_storage.dart';
import 'package:social_app/src/models/user.dart';

class MyFirestore {

  static String chatsCollection = "chats";
  static String chatRoom = "chatroom";
  static String statusCollection = "statuses";

  static createChatRoomMessage(var chatId, String lastMessage, var serverChatId, Map<String, dynamic> message, User user) async {
    var doc  = FirebaseFirestore.instance.collection(chatsCollection).doc(chatId);
    message['time'] = DateTime.now().millisecondsSinceEpoch;

    await doc.collection(chatRoom).doc().set(message).then((value) {
      Map<String, dynamic> currentUser = new Map<String, dynamic>();

      currentUser['id'] = user.id.toString();
      currentUser['name'] = user.name.toString();
      currentUser['image'] = user.image.toString();

      doc.update({
      'updatedAt': DateTime.now().millisecondsSinceEpoch,
        '${user.id}': currentUser,
        'serverChatId': serverChatId.toString(),
        'lastMessage': lastMessage,
        'lastBy': user.id.toString()
      });
    },
        onError: (e){
          print(e);
        });

  }

  static createUserChatRoom(var chatId, Map<String, dynamic> data) async  {
    await FirebaseFirestore.instance
        .collection(chatsCollection)
        .doc(chatId)
        .set(data);
  }

  static Stream<QuerySnapshot> getChatMessages(var chatId) {
    return FirebaseFirestore.instance
        .collection(chatsCollection)
        .doc(chatId)
        .collection(chatRoom)
    .orderBy("time")
        .snapshots();
  }

  static Stream<QuerySnapshot> getChatsList(var userId) {
    return FirebaseFirestore.instance
        .collection(chatsCollection)
        .where("users", arrayContains: userId.toString())
    .orderBy("updatedAt", descending: true)
        .snapshots();
  }

  static CollectionReference getChatsCollection() {
    return FirebaseFirestore.instance
        .collection(chatsCollection);
  }

  static Future deleteChat(String chatId) async {
    try {
      await FirebaseFirestore.instance.collection(chatsCollection).doc("$chatId").delete();
    } catch (e) {
      print(e);
    }
  }

  static Future<bool> checkExist(String docID) async {
    bool exists = false;
    try {
      await FirebaseFirestore.instance.collection(chatsCollection).doc("$docID").get().then((doc) {
        if (doc.exists)
          exists = true;
        else
          exists = false;
      });
      return exists;
    } catch (e) {
      return false;
    }
  }

  static Future createStatus(var statusId, Map<String, dynamic> data) async {
    return await FirebaseFirestore.instance
        .collection(statusCollection)
        .doc(statusId)
        .set(data);
  }

  static Future<QuerySnapshot> getStatuses() {
    var timeBefore24Hours = DateTime.now().subtract(Duration(hours: 24));
    var timeInMillis = timeBefore24Hours.millisecondsSinceEpoch;
    return FirebaseFirestore.instance.collection(statusCollection)
        .where("uploadedAt", isGreaterThanOrEqualTo: timeInMillis)
        .orderBy("uploadedAt", descending: true).get();
  }

  static markSeen(var statusId, List<String> seenList) async {
    var doc = FirebaseFirestore.instance.collection(statusCollection).doc(statusId);
    doc.update({"seenby": seenList});
  }

  static Stream<QuerySnapshot> getMyStatuses(var userId) {
    var timeBefore24Hours = DateTime.now().subtract(Duration(hours: 24));
    var timeInMillis = timeBefore24Hours.millisecondsSinceEpoch;
    return FirebaseFirestore.instance.collection(statusCollection)
        .where("uploadedAt", isGreaterThanOrEqualTo: timeInMillis)
        .orderBy("uploadedAt", descending: true)
        .snapshots();
  }

  static DocumentReference deleteAStatus(var _statusId) {
    // var timeBefore24Hours = DateTime.now().subtract(Duration(hours: 24));
    // var timeInMillis = timeBefore24Hours.millisecondsSinceEpoch;
    return FirebaseFirestore.instance.collection(statusCollection).doc(_statusId);
  }

  static deleteStatuses() {
    var timeBefore24Hours = DateTime.now().subtract(Duration(hours: 24));
    var timeInMillis = timeBefore24Hours.millisecondsSinceEpoch;
    return FirebaseFirestore.instance.collection(statusCollection)
        .where("uploadedAt", isLessThan: timeInMillis)
        .orderBy("uploadedAt", descending: true).get().then((_querySnap) {
      _querySnap.docs.forEach((_statusItem){
        Map<String, dynamic> data = _statusItem.data();
        var imageName = data['imageName'];
        var id = data["id"];
        deleteAStatus(id).delete().then((value) {
          MyStorage.deletePhoto(imageName);
        },
            onError: (e) {
              print(e);
            });
      });
    });
  }


}