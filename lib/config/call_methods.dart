import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:social_app/src/models/call.dart';

class CallMethods {

  static String callsCollection = "calls";

  final CollectionReference callCollection = FirebaseFirestore.instance.collection(callsCollection);

  Future<bool> makeCall({Call call}) async {
    try{
      call.hasDialed = true;
      Map<String, dynamic> hasDialedMap = call.toMap(); /// For Caller's document
      call.hasDialed = false;
      Map<String, dynamic> hasNotDialedMap = call.toMap(); /// For Receiver's document

      await callCollection.doc(call.callerId).set(hasDialedMap);
      await callCollection.doc(call.receiverId).set(hasNotDialedMap);

      return true;
    } catch(e) {
      print(e);
      return false;
    }
  }

  Future<bool> endCall({Call call}) async {
    try{
      await callCollection.doc(call.callerId).delete();
      await callCollection.doc(call.receiverId).delete();
      return true;
    } catch(e) {
      print(e);
      return false;
    }
  }

  Stream<DocumentSnapshot> callStream({String uid}){
    return callCollection.doc(uid).snapshots();
  }

}