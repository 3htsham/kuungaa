import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' as material;

class App {
  BuildContext _context;
  double _height;
  double _width;
  double _heightPadding;
  double _widthPadding;

  App(_context) {
    this._context = _context;
    MediaQueryData _queryData = MediaQuery.of(this._context);
    _height = _queryData.size.height / 100.0;
    _width = _queryData.size.width / 100.0;
    _heightPadding = _height - ((_queryData.padding.top + _queryData.padding.bottom) / 100.0);
    _widthPadding = _width - (_queryData.padding.left + _queryData.padding.right) / 100.0;
  }

  double appHeight(double v) {
    return _height * v;
  }

  double appWidth(double v) {
    return _width * v;
  }

  double appVerticalPadding(double v) {
    return _heightPadding * v;
  }

  double appHorizontalPadding(double v) {
    return _widthPadding * v;
  }
}

class Colors {
  Color _mainDarkColor = Color(0xFF25d366);
  Color _mainColor = Color(0xFF25d366);
  Color _secondColor = Color(0xFF344968);
  Color _secondDarkColor = Color(0xFFccccdd);
  Color _accentColor = Color(0xFF8C98A8);
  Color _accentDarkColor = Color(0xFF9999aa);
  Color _scaffoldColor = Color(0xFFFAFAFA);
  Color _scaffoldDarkColor = Color(0xFF2C2C2C);

  Color _splashColor = Color(0xFF121212);

  Color mainDarkColor(double opacity) {
    return this._mainDarkColor.withOpacity(opacity);
  }
  Color mainColor(double opacity) {
    return this._mainColor.withOpacity(opacity);
  }

  Color secondDarkColor(double opacity) {
    return this._secondDarkColor.withOpacity(opacity);
  }
  Color secondColor(double opacity) {
    return this._secondColor.withOpacity(opacity);
  }

  Color accentDarkColor(double opacity) {
    return this._accentDarkColor.withOpacity(opacity);
  }
  Color accentColor(double opacity) {
    return this._accentColor.withOpacity(opacity);
  }

  Color scaffoldDarkColor(double opacity) {
    return _scaffoldDarkColor.withOpacity(opacity);
  }
  Color scaffoldColor(double opacity) {
    return _scaffoldColor.withOpacity(opacity);
  }

  Color splashColor(double opacity) {
    return this._splashColor.withOpacity(opacity);
  }

  List<Shadow> textShadow = [
      Shadow(
        offset: Offset(3.0, 3.0),
        blurRadius: 6.0,
        color: material.Colors.white,
      ),
      Shadow(
        offset: Offset(3.0, -3.0),
        blurRadius: 6.0,
        color: material.Colors.white,
      ),
      Shadow(
        offset: Offset(-3.0, -3.0),
        blurRadius: 6.0,
        color: material.Colors.white,
      ),
      Shadow(
        offset: Offset(-3.0, 3.0),
        blurRadius: 6.0,
        color: material.Colors.white,
      ),
    ];

  List<Shadow> textShadowBlack = [
    Shadow(
      offset: Offset(3.0, 3.0),
      blurRadius: 6.0,
      color: material.Colors.black,
    ),
    Shadow(
      offset: Offset(3.0, -3.0),
      blurRadius: 6.0,
      color: material.Colors.black,
    ),
    Shadow(
      offset: Offset(-3.0, -3.0),
      blurRadius: 6.0,
      color: material.Colors.black,
    ),
    Shadow(
      offset: Offset(-3.0, 3.0),
      blurRadius: 6.0,
      color: material.Colors.black,
    ),
  ];

}