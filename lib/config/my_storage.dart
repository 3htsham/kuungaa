

import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';

class MyStorage {
  static String statusFolder = "statuses";

  static Stream<TaskSnapshot> uploadImage(var imageFile, var name) {
    String time = DateTime.now().millisecondsSinceEpoch.toString();
    var dowurl = "";
    var ref = FirebaseStorage.instance.ref().child("$statusFolder/$name");
    // try {
    //   ref.delete();
    // } catch(e)
    // {
    //   print(e.toString());
    // }
    var uploadTask = ref.putFile(File(imageFile));
    return uploadTask.snapshotEvents;
    // uploadTask.snapshotEvents.listen((event) async {
    //   if(event.state == TaskState.success) {
    //     dowurl = await event.ref.getDownloadURL().toString();
    //   }
    // }, onError: (e){
    //   print(e);
    //   return dowurl;
    // }, onDone: (){
    //   return dowurl;
    // });
  }

  static deletePhoto(var name) {
    var ref = FirebaseStorage.instance.ref().child("$statusFolder/$name");
    try {
      ref.delete();
    } catch(e)
    {
      print(e.toString());
    }
  }

}