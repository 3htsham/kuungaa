import 'package:flutter/material.dart';
import 'package:social_app/src/elements/others/languages.dart';
import 'package:social_app/src/elements/video/full_screen_video_continue_view.dart';
import 'package:social_app/src/models/route_argument.dart';
import 'package:social_app/src/pages/boost/payment.dart';
import 'package:social_app/src/pages/boost/view_boost_packages.dart';
import 'package:social_app/src/pages/boost/view_my_campaigns.dart';
import 'package:social_app/src/pages/call/call_screen.dart';
import 'package:social_app/src/pages/chat/chats.dart';
import 'package:social_app/src/pages/chat/single_chat.dart';
import 'package:social_app/src/pages/group/create_group.dart';
import 'package:social_app/src/pages/group/view_group.dart';
import 'package:social_app/src/pages/login/login.dart';
import 'package:social_app/src/pages/login/signup.dart';
import 'package:social_app/src/pages/login/splash_screen.dart';
import 'package:social_app/src/pages/others/app_settings.dart';
import 'package:social_app/src/pages/pages.dart';
import 'package:social_app/src/pages/post/edit_post.dart';
import 'package:social_app/src/pages/post/saved_posts.dart';
import 'package:social_app/src/pages/post/view_post.dart';
import 'package:social_app/src/pages/post/view_post_media.dart';
import 'package:social_app/src/pages/profile/blocklist.dart';
import 'package:social_app/src/pages/profile/edit_profile.dart';
import 'package:social_app/src/pages/profile/profile_settings.dart';
import 'package:social_app/src/pages/profile/user_profile.dart';
import 'package:social_app/src/pages/search/search_page.dart';
import 'package:social_app/src/pages/status/my_status_list.dart';
import 'package:social_app/src/pages/status/upload_status_page.dart';
import 'package:social_app/src/pages/status/view_status_page.dart';

class RouteGenerator {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch(settings.name)
    {
      case '/Splash':
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case '/Login':
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => Login(),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/Signup":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => SignUp(),
            transitionDuration: Duration(milliseconds: 1000),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/Pages":
        return MaterialPageRoute(builder: (_) => PagesWidget(currentTab: args));
      case "/FullScreenVideoView":
        return MaterialPageRoute(builder: (_) => FullScreenVideoContinueView(arguments: args as RouteArgument,));
      case "/Chats":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => ChatsWidget(args: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/SingleChat":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => SingleChatWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/UserProfileDetails":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => UserProfileWidget(routeArgument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/EditProfile":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => EditProfileWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/CreateGroup":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => CreateGroupWidget(),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/ViewGroup":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => ViewGroupWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/ViewSavedPosts":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => SavedPostsWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
      case "/ViewPost":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => ViewPostWidget(arguments: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/ProfileSettings":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => ProfileSettingsWidget(),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/ViewPostMedia":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => ViewPostMediaWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/AppSettings":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => AppSettingsWidget(),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });


      case "/Languages":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => LanguagesWidget(),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/UploadStatus":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => UploadStatusPageWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/MyStatuses":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => MyStatusListWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/ViewStatus":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => ViewStatusPage(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/SearchPage":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => SearchPageWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/MakeCall":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => CallScreenWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/EditPost":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => EditPostWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/ViewPackages":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => ViewBoostPostPackages(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/Payment":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => PaymentWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/MyCampaigns":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => ViewMyCampaignsWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });

      case "/Blocklist":
        return PageRouteBuilder(
            pageBuilder: (_, __, ___) => BlockListWidget(argument: args as RouteArgument,),
            transitionDuration: Duration(milliseconds: 300),
            transitionsBuilder:
                (_, Animation<double> animation, __, Widget child) {
              return Opacity(
                opacity: animation.value,
                child: child,
              );
            });
    }
  }

}